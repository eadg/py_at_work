from pyspark import SparkContext
logFile = "file:///home/jrokicki/dev/pyt/py_at_work/spark/firstspark/src/out.html"
sc = SparkContext("local", "first app")
logData = sc.textFile(logFile).cache()
nA = logData.filter(lambda s: 'Spark' in s).count()
nB = logData.filter(lambda s: 'spark' in s).count()
print("Lines with a: %i, lines with b: %i" % (nA, nB))
