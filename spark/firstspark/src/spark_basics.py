from pyspark import SparkContext
from operator import add
sc = SparkContext("local", "bc app")

lis = ["scala", "java", "hadoop", "spark", "akka", "spark vs hadoop", "pyspark", "pyspark and spark"]
words = sc.parallelize(lis)


def f(x):
    print("call f(x) and x = %s" % x)

counts = words.count()
print("count method -> %i" % (counts))

collect = words.collect()
print("collect method -> %s" % (collect))

foreach = words.foreach(f)
# print("foreach method -> %s" % (foreach))

lambada = words.filter(lambda s: len(s) > 4)
print("filter method -> %s" % (lambada.collect()))

mapa = words.map(lambda s: (s, len(s)))
print("map method -> %s" % (mapa.collect()))

sizes = [len(x) for x in lis]
red = sc.parallelize(sizes)
print("reducy method -> %s" % red.reduce(add))

x = sc.parallelize([("spark", 1), ("hadoop", 4)])
y = sc.parallelize([("spark", 2), ("hadoop", 5)])
joined = x.join(y)
print("join method -> %s" % joined.collect())

words.cache()
caching = words.persist().is_cached
print("cache method -> %s" % caching)
