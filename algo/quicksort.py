import random


def quicksort(x):
    if len(x) < 2:
        return x
    else:
        pivot = x[0]
        less = [i for i in x[1:] if i <= pivot]
        greater = [i for i in x[1:] if i > pivot]
        print(pivot, less, greater)
        return quicksort(less) + [pivot] + quicksort(greater)


def gen_random(x, y, z):
    lipa = []
    for _ in range(0, z):
        r = random.randint(x, y)
        lipa.append(r)
    return lipa


if __name__ == '__main__':
    lipa = gen_random(1, 100, 10)
    # l = [100, 50, 30, 343, 123, 2313, 23, -34]
    l2 = quicksort(lipa)
    print(l2)
