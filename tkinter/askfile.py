from tkinter import filedialog
from tkinter import *

root = Tk()
fi = filedialog.askopenfile(parent=root,
                            initialdir="..", mode='rb',
                            title='Choose a file')
if fi:
    # root.update()
    print(dir(fi))
    print(fi.name)
    data = fi.read()
    print("I got %d bytes from this file." % len(data))
Label(root, text=fi.name).grid(row=0)
mainloop()
