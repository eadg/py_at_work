from tkinter import Tk, Label, Button, StringVar, W, E, filedialog, Frame
import os.path
from sys import argv
import rename_pdf


class GUI(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.fias = "Kliknij przycisk i wybierz katalog"
        self.label_text = StringVar()
        self.label_text.set(self.fias)
        self.label = Label(master, textvariable=self.label_text)

        self.choose_button = Button(master, text="Wybór katalogu", command=lambda: self.select_dir(), background="gold")
        self.choose_button.grid(row=0, sticky=W)
        self.label.grid(row=1)
        self.run_button = Button(master, text="Start", command=lambda: self.start(), background="green")
        self.run_button.grid(row=2, sticky=W)
        self.quit_button = Button(master, text="Quit", command=lambda: self.quit(), background="red")
        self.quit_button.grid(row=2, sticky=E)

    def select_dir(self):
        try:
            self.fias = filedialog.askdirectory()
        except:
            print('sth went wrong')
        self.label_text.set(self.fias)

    def start(self):
        if os.path.isdir(self.fias):
            argv.append(self.fias)
            sts = rename_pdf.Starter(rename_pdf.PDFrename)
            sts.run()
            self.label_text.set('Koniec przetwarzania. ')
        else:
            self.label_text.set('To nie jest katalog... ')

    def quit(self):
        self.master.destroy()


root = Tk()
root.title('PDF_Rename')
app = GUI(root)
root.mainloop()
