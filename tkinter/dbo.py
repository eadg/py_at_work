import tkinter as tk
from tkinter import filedialog


class DBOTIF(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()
        self.what = None

    def create_widgets(self):
        self.hi_there = tk.Button(self)
        self.hi_there["text"] = "Wybierz katalog"
        self.hi_there["command"] = self.select_dir
        self.hi_there.pack(side="top")
        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        '''
        self.label = tk.Label(self, text=self.what)
        self.label.pack(side="top")
        '''
        self.quit.pack(side="bottom")

    def select_dir(self):
        try:
            self.fias = filedialog.askdirectory()
        except:
            print('sth went wrong')
        self.what = self.fias
        print("in {}".format(self.what))

if __name__ == '__main__':
    root = tk.Tk()
    app = DBOTIF(master=root)
    app.mainloop()
