import psutil
import sys


class Checker(object):

    def __init__(self, number):
        self.pid = number

    def jboss_proc_gen(self):
        '''Generator jboss instances'''
        p = psutil.Process(self.pid)
        for proc in p.children():
            if 'run.sh' in proc.name():
                for subproc in proc.children():
                    if 'java' in subproc.name():
                        yield proc

    def jboss_term(self, timeout=4):
        '''Terminate jboss process and parrent (run.sh)'''
        jpge = self.jboss_proc_gen()
        copy_jpge = list(self.jboss_proc_gen())
        for jb_main in jpge:
            jb_child = jb_main.children()
            for jbc in jb_child:
                if jbc.is_running():
                    jbc.terminate()
            rip, alive = psutil.wait_procs(jb_child, timeout=timeout)
            if not alive:
                for jbc in alive:
                    if jbc.is_running():
                        jbc.kill()
                rip2, alive2 = psutil.wait_procs(alive, timeout=timeout)
                if not alive2:
                    for jbc in alive2:
                        print("not killed {} ".format(jbc.name()))
                        sys.exit("exit with errors")
        return copy_jpge

if __name__ == '__main__':
    check = Checker(1)
    check.jboss_term()
