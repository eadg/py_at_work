import unittest
import check_run_jboss4 as crj


class TestPackage(unittest.TestCase):

    def test_callable(self):
        self.assertTrue(list(crj.Checker(16760).jboss_proc_gen()))

    def test_kill(self):
        self.assertTrue(isinstance(crj.Checker(16760).jboss_term(), list))

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPackage)
    unittest.TextTestRunner(verbosity=2).run(suite)
