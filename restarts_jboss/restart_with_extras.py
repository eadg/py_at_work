import subprocess
import time


class ExtraRestart(object):

    def do_it(self):
        operations = ['/usr/bin/pkill soffice', '/usr/sbin/service jboss7 restart']
        for item in operations:
            subprocess.call(item.split())
            time.sleep(2)


if __name__ == '__main__':
    er = ExtraRestart()
    er.do_it()
