import psutil
import os.path
import os
import re

class Java(object):

    def getPidJBoss(self):
        p = psutil.Process(1)
        for proc in p.children():
            if 'standalone.sh' in proc.name() or 'run.sh' in proc.name():
                try:
                    pjava = proc.children()
                except psutil.NoSuchProcess:
                    print('no java jboss')
                else:
                    return pjava[0].exe()

    def getVersionJavaRe(self):
        jpath = os.path.normpath(self.getPidJBoss())
	pat = re.compile('\S+(1\..+)$')
	print(jpath)
	for s in jpath.split('/')[-1::-1]:
		if '.' in s:
			print(s)
			se = pat.match(s).group(1)
			print(re.sub('_', '.', se))


if __name__ == '__main__':
	c = Java()
	c.getVersionJavaRe()
