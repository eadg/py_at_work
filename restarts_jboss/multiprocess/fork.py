import time
import sys
import os


def do_something():
    # Do something for 10 seconds, then exist cleanly.
    time.sleep(10)
    print("byebyebyebyebyebyebyebye")
    print(os.getpid())
    sys.exit(0)

if __name__ == "__main__":
    print ("Hello and welcome to a Python forking example. I'll now fork a "
           "backgrounded child process and then exit, leaving it to run all by "
           "itself. Watch for the 'Done' in 10 seconds...")

    try:
        print(os.getpid())
        pid = os.fork()
        if pid > 0:
            print(os.getpid())
            # Exit parent process
            sys.exit(0)
    except OSError, e:
        print >> sys.stderr, "fork failed: %d (%s)" % (e.errno, e.strerror)
        sys.exit(1)

    # Configure the child processes environment
    print(os.getpid())
    os.chdir("/")
    os.setsid()
    os.umask(0)

    # Execute something in the background
    do_something()
