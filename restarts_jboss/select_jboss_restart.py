#!/usr/bin/python2.7

import psycopg2
import sys
import datetime
import base64
import ass

us, pa = ass.read_ssap(file='/srv/scripts/py/ssap', cred='cred1')

try:
    conn = psycopg2.connect(database='smyk_logs', user=base64.b64decode(us),
                            password=base64.b64decode(pa), host='jboss-pgsql1',
                            connect_timeout=3)
except psycopg2.DatabaseError as er:
    print("Timeout - problem z polaczeniem.")
    print(er)
    sys.exit(1)

now = datetime.date.today()
delta = datetime.timedelta(days=180)
180d_ago = now-delta
curs = conn.cursor()
curs.execute('SELECT * FROM jboss_restart_log WHERE datetime > %s order by id', [180d_ago])
for row in curs:
    print(row)
curs.close()
conn.close()
