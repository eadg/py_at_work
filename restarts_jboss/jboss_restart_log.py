import base64
import datetime
import glob
import os.path
import re
import sys
import time

import psutil

import psycopg2

import ass

import final_modification_along_restart as jbmod

from hostname import Hostname


class User(object):

    def __init__(self):
        self.username = self.get_process_user().username()
        self.ip = self.get_remote_ip()

    def get_pid_sshd(self):
        p = psutil.Process(1)
        for proc in p.children():
            if 'sshd' in proc.name():
                return proc

    def get_pid_service(self):
        oper = ['restart', 'stop', 'start']
        p = self.get_pid_sshd()
        child_sshd = p.children()
        for proc in child_sshd:
            for child_proc in proc.children(True):
                try:
                    proc_list = [x for x in oper if x in child_proc.cmdline()]
                    if len(proc_list) > 0:
                        return child_proc
                except psutil.NoSuchProcess:
                    return psutil.Process(1).children()[0]
        p = psutil.Process(1)
        for proc in p.children(True):
            try:
                proc_list = [x for x in oper if x in proc.cmdline()]
                if len(proc_list) > 0:
                    return proc
            except psutil.NoSuchProcess:
                return psutil.Process(1).children()[0]

    def get_process_user(self):
        p = self.get_pid_service()
        pp = psutil.Process(p.ppid())
        while(pp.username() == 'root' and pp.ppid() > 1):
            pp = psutil.Process(pp.ppid())
        return pp

    def get_remote_ip(self):
        p = self.get_process_user()
        if(p.ppid() <= 1):
            return '127.0.0.1'
        else:
            pp = psutil.Process(p.ppid())
            while(pp.name() != 'sshd'):
                try:
                    pp = psutil.Process(pp.ppid())
                except (psutil.NoSuchProcess, psutil.AccessDenied):
                    return '127.0.0.1'
            ip = self.get_established(pp)
        return ip

    def get_established(self, p):
        con = p.connections()
        ip = [c[4][0] for c in con if 'ESTABLISHED' in c.status]
        return self.ip6_to_ip4(ip[0])

    def ip6_to_ip4(self, p):
        return p.split(':').pop()


class Release(object):

    def __init__(self, path):
        self.jboss_path = ''
        if len(path) == 0:
            self.jboss_path = '/srv/jboss/jboss'
        else:
            self.jboss_path = self.get_ear()

    def get_release(self):
        if self.jboss_path == '/srv/jboss/jboss':
            return self.get_release_long_path()
        else:
            return self.get_release_short_path()

    def get_release_long_path(self):
        if os.path.islink(self.jboss_path):
            jboss_real_path = os.path.normpath(os.readlink(self.jboss_path))
            split_path = jboss_real_path.split('/')
            if any(split_path[-1] in w for w in ['jboss', 'wildfly']):
                try:
                    jboss_ver, jboss_rel = split_path[-3:-1]
                    return (jboss_ver, jboss_rel,)
                except IndexError:
                    return ('-', '-')
            else:
                try:
                    jboss_ver, jboss_rel = split_path[-2:]
                    return (jboss_ver, jboss_rel,)
                except IndexError:
                    return ('-', '-')
        else:
            return ('-', '-')

    def get_release_short_path(self):
        try:
            mod_time = os.path.getmtime(self.jboss_path)
            jboss_rel = (datetime.datetime.fromtimestamp(
                mod_time)).strftime("%Y-%m-%d %H:%M:%S")
        except OSError:
            jboss_rel = ''
        jboss_ver = self.jboss_path.split('/')[-1]
        return(jboss_ver, jboss_rel,)

    def get_ear(self):
        list_ear = [glob.glob('/srv/jboss/stand*/depl*/*.ear'),
                    glob.glob('/srv/jboss/jboss/stand*/depl*/*.ear'),
                    glob.glob(
                        '/srv/jboss/jboss/serv*/def*/depl*/___de*/*.ear')]
        return [ear[0] for ear in list_ear if ear][0]

    def get_profile_version(self):
        list_conf = [glob.glob('/srv/jboss/jboss/profile.conf'),
                     glob.glob('/srv/jboss/profile.conf')]
        try:
            with open([conf[0] for conf in list_conf if conf][0]) as f:
                ver = f.readlines()
                return (ver[1].split('=')[1]).rstrip()
        except (IOError, IndexError):
            return '-'


class Java(object):

    def get_process_jboss(self):

        p = psutil.Process(1)
        for proc in p.children():
            if 'standalone.sh' in proc.name() or 'run.sh' in proc.name():
                try:
                    pjava = proc.children()
                except psutil.NoSuchProcess:
                    print('no java jboss')
                else:
                    return pjava[0].exe() if pjava else '--'
        return '--'

    def get_jdk_version(self):
        proc = self.get_process_jboss()
        if '--' not in proc:
            jpath = os.path.normpath(proc)
            pattern = re.compile(r'\S+(1\..+)$')
            for s in jpath.split('/')[-1::-1]:
                if pattern.match(s):
                    se = pattern.match(s).group(1)
                    return re.sub('_', '.', se)
        else:
            return '-'


class DB(object):

    def update(self, data):
        us, pa = ass.read_ssap(file='/srv/scripts/py/ssap', cred='cred1')
        conn = None
        try:
            conn = psycopg2.connect(
                database='smyk_logs', user=base64.b64decode(us),
                password=base64.b64decode(pa), host='jboss-pgsql1',
                connect_timeout=3)
            cur = conn.cursor()
            cur.execute(
                'INSERT INTO jboss_restart_log (hostname, ip_address,\
                datetime, app_version, app_version_build, username, action,\
                jdk_version, jboss_version) VALUES\
                (%s, %s, %s, %s, %s, %s, %s, %s, %s)', data)
            self.delete_old(cur)
            conn.commit()
        except psycopg2.DatabaseError as er:
            if conn:
                conn.rollback()
            print("Timeout - problem z polaczeniem => %s" % er)
        finally:
            if conn:
                conn.close()

    def delete_old(self, cur):
        now = datetime.date.today()
        delta = datetime.timedelta(days=500)
        long_time_ago = now-delta
        cur.execute(
            'DELETE FROM jboss_restart_log WHERE datetime < %s',
            [long_time_ago])


class ForkPy(object):

    def forking(self):
        try:
            pid = os.fork()
            if pid > 0:
                sys.exit(0)
        except OSError as e:
            msg = "fork exception: %d %s" % (e.errno, e.strerror)
            sys.stderr.write(msg)
            sys.exit(1)

    def update_jdk(self, data):
        time.sleep(10)
        jdk = Java().get_jdk_version()
        data[7] = jdk
        return data


class Row(object):

    def __init__(self):
        rel = Release('')
        ur = User()
        he = Hostname()
        ja = Java()
        (app_version, app_version_build) = rel.get_release()
        self.data = [he.host_name, ur.ip, datetime.datetime.now(), app_version,
                     app_version_build, ur.username, sys.argv[1],
                     ja.get_jdk_version(), rel.get_profile_version()]

    def getRow(self):
        return self.data


if __name__ == '__main__':
    db = DB()
    row = Row()
    if row.getRow()[6] == 'stop':
        db.update(row.getRow())
    else:
        jbmod.do_modify()
        data_row = row.getRow()
        fp = ForkPy()
        fp.forking()
        data = fp.update_jdk(data_row)
        db.update(data)
