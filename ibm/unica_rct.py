import os
import signal
import time
from subprocess import call

from kill_em_all2 import KEA2

import psutil


class URCT(KEA2):

    def get_processes_to_restart(self):
        """Select process for processing."""
        p = psutil.Process(1)
        fnull = open('/dev/null', 'w')
        for proc in p.children():
            try:
                if 'java' in proc.name() and 'ResponseContactTrackerMain' in\
                   proc.cmdline()[-2:-1].pop():
                    os.chdir('/opt/IBM/unica/Campaign/Deliver/bin')
                    call(["./rct.sh", "stop"], stdout=fnull)
                    time.sleep(2)
                    self.kill_proc(proc)
                    time.sleep(3)
                    self.kill_proc(proc, signol=signal.SIGKILL)
            except psutil.NoSuchProcess:
                pass
        call(["systemctl", "restart", "kafka"], stdout=fnull)
        time.sleep(5)
        os.chdir('/opt/IBM/unica/Campaign/Deliver/bin')
        call(["./rct.sh", "start"], stdout=fnull)


if __name__ == '__main__':
    mod = URCT()
    mod.get_processes_to_restart()
