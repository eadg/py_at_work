import os
import signal
import time
from subprocess import call

import psutil


class URCT(object):

    def get_processes_to_restart(self, first=True):
        """Select process for processing."""
        print("First: {}".format(first))
        p = psutil.Process(1)
        fnull = open('/dev/null', 'w')
        for proc in p.children():
            try:
                # print("PROCESS: {}, {}, {}".format(
                #     proc.name(), proc.exe(), proc.cmdline()[-2:-1]))
                """
                if 'java' in proc.name() and 'kafka.Kafka' in proc.cmdline(
                )[-2:-1].pop() and first:
                    call(["systemctl", "restart", "kafka"], stdout=fnull)
                    # print('Kafka')
                """
                if 'java' in proc.name() and 'ResponseContactTrackerMain' in\
                   proc.cmdline()[-2:-1].pop() and first:
                    # print('RCT!')
                    os.chdir('/opt/IBM/unica/Campaign/Deliver/bin')
                    call(["./rct.sh", "stop"], stdout=fnull)
                    proc.send_signal(signal.SIGTERM)
                    time.sleep(10)
                    proc.send_signal(signal.SIGKILL)
                    self.get_processes_to_restart(first=False)
                else:
                    call(["systemctl", "restart", "kafka"], stdout=fnull)
                    time.sleep(5)
                    os.chdir('/opt/IBM/unica/Campaign/Deliver/bin')
                    call(["./rct.sh", "start"], stdout=fnull)
                    
                """"
                elif 'java' in proc.name() and 'ResponseContactTrackerMain' in\
                     proc.cmdline()[-2:-1].pop() and first is False:
                    proc.send_signal(signal.SIGTERM)
                    time.sleep(10)
                    proc.send_signal(signal.SIGKILL)
                    os.chdir('/opt/IBM/unica/Campaign/Deliver/bin')
                    call(["./rct.sh", "start"], stdout=fnull)
                    # print('Restarting RCT')
                """
            except psutil.NoSuchProcess as e:
                call(["systemctl", "restart", "kafka"], stdout=fnull)
                print(e)
                time.sleep(5)
                os.chdir('/opt/IBM/unica/Campaign/Deliver/bin')
                call(["./rct.sh", "start"], stdout=fnull)


if __name__ == '__main__':
    mod = URCT()
    mod.get_processes_to_restart()
