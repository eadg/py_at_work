import signal
import time

import psutil



class FLW:
    """Class for managment processes on campaign0."""

    def kill_flowcharts(
            self, kill: bool = False, sig: signal = None) -> None:
        """Kill flws on campaign0"""
        p = psutil.Process(1)
        for proc in p.children():
            if len(proc.cmdline()) == 3:
                if 'sh' in proc.exe() and 'unica_server_start.sh' in proc.cmdline()[2]:
                    proc_a = proc.children().pop()
                    # proc_a.pid
                    break
        if 'rc.unica_ac' in proc_a.name():
            proc_b = proc_a.children().pop()
        if 'unica_aclsnr' in proc_b.name():
            list_flws = proc_b.children()
        if kill:
            for fl in list_flws:
                try:
                    fl.send_signal(sig)
                except psutil.NoSuchProcess:
                    pass
        else:
            for fl in list_flws:
                print(fl.exe())

    def action(self):
        """Run it twice for sure"""
        self.kill_flowcharts(kill=True, sig=signal.SIGTERM)
        time.sleep(5)
        self.kill_flowcharts(kill=True, sig=signal.SIGKILL)

if __name__ == '__main__':
    mod = FLW()
    mod.action()
