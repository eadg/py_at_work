import os
import signal

import psutil


class KEA(object):

    def kill_mod(self, proc: psutil.Process, signol=signal.SIGKILL) -> None:
        """Kill selected processes."""
        assert proc.pid != os.getpid()
        children = proc.children(recursive=True)
        children.append(proc)
        for p in children:
            try:
                print(p.pid)
                p.send_signal(signol)
            except psutil.NoSuchProcess:
                pass
