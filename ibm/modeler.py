from kill_em_all import KEA

import psutil


class MOD(KEA):
    """Class for managment processes on modeler's server."""

    def kill_modeler_1(self, kill: bool = False) -> None:
        """Select process to kill on 0-pl-p-mod01."""
        p = psutil.Process(1)
        for proc in p.children():
            if 'modelersrv_18_0' in proc.name() and '/opt' in proc.exe():
                self.kill_mod(proc)
            elif 'wrapper' in proc.name() and 'ModelerServer' in proc.exe():
                self.kill_mod(proc)
            elif 'python' in proc.name() and 'mpd.py' in proc.cmdline()[1]:
                self.kill_mod(proc)

    def kill_modeler_2(self, kill: bool = False) -> None:
        """Select process to kill on pdc-p-mod02."""
        p = psutil.Process(1)
        for proc in p.children():
            if 'modelersrv_18_1' in proc.name() and '/srv' in proc.exe():
                if kill:
                    self.kill_mod(proc)
            elif 'wrapper' in proc.name() and 'TMWBServer' in proc.exe():
                if kill:
                    self.kill_mod(proc)
            elif 'r_start' in proc.name() and 'pasw.rstats' in proc.exe():
                if kill:
                    self.kill_mod(proc)


if __name__ == '__main__':
    mod = MOD()
    # mod.kill_modeler_1(kill=True)
    mod.kill_modeler_2(kill=True)
