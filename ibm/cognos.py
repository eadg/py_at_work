import argparse
import sys
from subprocess import call

from kill_em_all import KEA

import logger

import psutil


class COG(KEA):

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()
        self.log = logger.Logger(file_path='/tmp/cognos.log').log
        self.fnull = open('/dev/null', 'w')

    def parse_detail(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '-r', action='store_true', help=self.run_cog.__doc__, dest='run')
        parser.add_argument(
            '-k', action='store_true', help=self.kill_cog.__doc__, dest='kill')
        results = parser.parse_args()
        self.run = results.run
        self.kill = results.kill

    def run_cog(self) -> None:
        """Run cognos if not."""
        p = psutil.Process(1)
        flag = False
        for proc in p.children():
            if 'cogbootstrapservice' in proc.name() and '/opt' in proc.exe():
                flag = True
        if not flag:
            call(['/srv/scripts/cognos_server_start.sh'], stdout=self.fnull)
            self.log.info('starting cognos:)')

    def kill_cog(self, kill: bool = False) -> None:
        """Kill every process of cognos."""
        p = psutil.Process(1)
        klist_proc = ['kill cognos process:(']
        for proc in p.children(recursive=True):
            if '/opt/ibm/cognos/analytics' in proc.exe():
                klist_proc.append(f'PID{proc.pid}:NAME{proc.name()}')
                if kill:
                    self.kill_mod(proc)
        if kill:
            self.log.info(' '.join(klist_proc))

    def action(self) -> None:
        """Perform action."""
        if self.run:
            self.run_cog()
        elif self.kill:
            self.kill_cog(kill=True)


if __name__ == '__main__':
    mod = COG()
    mod.action()
