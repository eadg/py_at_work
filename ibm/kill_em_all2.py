import os
import signal

import psutil


class KEA2(object):

    def kill_proc(self, proc=None, signol=signal.SIGTERM):
        """Kill selected processes."""
        assert proc.pid != os.getpid()
        children = proc.children(recursive=True)
        children.append(proc)
        for p in children:
            try:
                p.send_signal(signol)
            except psutil.NoSuchProcess:
                pass
