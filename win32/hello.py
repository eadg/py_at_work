import glob
import os
import os.path
import shutil
import subprocess
import sys


class WrapStampTool(object):

    def __init__(self):
        self.source_path = r'//ULTIMODC/dwp/SĄDÓWKA/UZUPEŁNIENIE BRAKÓW'
        self.dest_path = r'c:/tmp'
        self.jar = r'//StampTool2.0.jar'
        self.java_path = self.find_java()

    def mkdir_if(self, path):
        if not os.path.isdir(path):
            try:
                os.makedirs(path)
            except:
                print('nie można utworzyć katalogu')

    def find_java(self):
        jpath = glob.glob(r'C:/Program Fil*/*ava/j*/bin/*')
        for p in jpath:
            if 'java.exe' in p:
                p_ok = os.path.normpath(p)
                return p_ok

    def lets_go(self):
        print('hello!')
        shutil.copyfile(self.source_path + self.jar, self.dest_path + self.jar)
        subprocess.call([self.java_path, '-Xmx1400M', '-jar', self.dest_path + self.jar])


if __name__ == '__main__':
    wst = WrapStampTool()
    wst.mkdir_if(wst.dest_path)
    wst.lets_go()
