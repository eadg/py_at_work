import os
import os.path


class TravelBase(object):

    def gen_dir(self, ipath='', name=''):
        in_path = os.path.abspath(ipath)
        it_fi_di = os.scandir(in_path)
        for entry in it_fi_di:
            if entry.is_dir() and name in entry.name:
                yield os.path.join(in_path, entry.name)
