import sys
import stor


def test_check_c_p():
    arg = ['-p /home/jrokicki/dev/pyt/py_at_work/dirtravel/', '-n sth']
    sys.argv = []
    sys.argv.extend(arg)
    dt = stor.DirTravel()
    assert dt.check_path_abs('/home/jrokicki/dev/pyt/py_at_work/dirtravel')


def test_s_p():
    arg = ['-p /home/jrokicki/dev/pyt/py_at_work/dirtravel/', '-n sth']
    sys.argv = []
    sys.argv.extend(arg)
    dt = stor.DirTravel()
    dt.argpath = '/home/jrokicki/dev/pyt/py_at_work/dirtravel/'
    assert dt.check_sub_path('/home/jrokicki/dev/pyt/py_at_work/dirtravel/ttt01')
