import argparse
import os
import os.path
import pwd
import sys
from pathlib import Path

import logger

import travel_base


class DirTravel(travel_base.TravelBase):

    """
    python3 stor.py -p /srv/ftp/ftp_komornik_nadrzedne/ -n 'komornik'
    """

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-p', action='store', help='path', dest='ap')
        self.parser.add_argument(
            '-n', action='store', help='name', dest='an', default='')
        self.parser.add_argument(
            '-f', action='store', help='filtr', dest='af', default='')
        self.parser.add_argument('-l', action='store', help='Sciezka dla pliku\
        logow. Domyslnie /dev/null', dest='logging_value', default='')
        results = self.parser.parse_args()
        tuple_res = (results.ap, results.an, results.af)
        self.argpath, self.argname, self.argfilter = tuple_res
        self.logger = logger.Logger(results.logging_value).log

    def gen_ele(self, ipath):
        '''
        Searching dir/files with diffrent uid/guid
        from parent
        '''
        os.chdir(ipath)
        ostat = os.stat(ipath)
        uid = ostat.st_uid
        gid = ostat.st_gid
        for path, dirlist, filelist in os.walk(ipath):
            dstat = os.stat(path)
            if not dstat.st_uid == uid:
                print(f"GID file: {path}")
            if not dstat.st_gid == gid:
                print(f"GID file: {path}")
            for ele in filelist:
                path_file = os.path.join(path, ele)
                fstat = os.stat(path_file)
                if not fstat.st_uid == uid:
                    print(f"UID file: {path_file}")
                if not fstat.st_gid == gid:
                    print(f"GID file: {path_file}")

    def gen_ele_id_filter(self, ipath, iuid=0, igid=0, filt='', files=True):
        '''
        Searching filtered dir/files with input uid/guid
        '''
        if not filt:
            filtr = self.argfilter
        os.chdir(ipath)
        for wpath, wdirlist, wfilelist in os.walk(ipath):
            dstat = os.stat(wpath)
            if dstat.st_uid == iuid:
                wusr = self.wrong_owner(
                    name=self.argname, path=wpath, ug='u')
                self.logger.info(
                    f"Ustawione UID={wusr} dla katalogu: {wpath}")
                os.chown(path=wpath, uid=wusr, gid=-1)
            if dstat.st_gid == igid:
                wgrp = self.wrong_owner(
                    name=self.argname, path=wpath, ug='g')
                self.logger.info(
                    f"Ustawione GID={wgrp} dla katalogu: {wpath}")
                os.chown(path=wpath, uid=-1, gid=wgrp)
            if files:
                for ele in wfilelist:
                    path_file = os.path.join(wpath, ele)
                    if filtr in path_file:
                        fstat = os.stat(path_file)
                        if fstat.st_uid == iuid:
                            wrusr = self.wrong_owner(
                                name=self.argname, path=path_file, ug='u')
                            os.chown(path=path_file, uid=wrusr, gid=-1)
                            self.logger.info(
                                f"Ustawione UID={wrusr} plik: {path_file}")
                        if fstat.st_gid == igid:
                            wrgrp = self.wrong_owner(
                                name=self.argname, path=path_file, ug='g')
                            os.chown(path=path_file, uid=-1, gid=wrgrp)
                            self.logger.info(
                                f"Ustawione GID={wrgrp} plik: {path_file}")

    def gen_ele_filtrpath(self, ipath, filt='', files=False):
        '''
        Searching filtered dir/files with diffrent uid/guid
        from parent
        '''
        if not filt:
            filtr = self.argfilter
        os.chdir(ipath)
        ostat = os.stat(ipath)
        uid = ostat.st_uid
        gid = ostat.st_gid
        for path, dirlist, filelist in os.walk(ipath):
            for ele in filelist:
                path_file = os.path.join(path, ele)
                if filtr in path_file:
                    fstat = os.stat(path_file)
                    if not fstat.st_uid == uid:
                        print(f"UID file: {path_file}")
                    if not fstat.st_gid == gid:
                        print(f"GID file: {path_file}")

    def wrong_owner(self, name=None, path=None, ug=None):
        list_path = path.split(os.sep)
        list_path.reverse()
        owner = ''
        for ele in list_path:
            if name in ele:
                owner = ele
                break
        try:
            entry_user = pwd.getpwnam(owner)
            uid, gid = entry_user[2], entry_user[3]
        except KeyError:
            print(f"Not found {owner}")
        return uid if ug == 'u' else gid

    def check_path_abs(self, path: str) -> None:
        if not os.path.abspath('.') == path:
            self.logger.info('o my gosh!')
            self.logger.info(os.path.abspath('.'))
            sys.exit(False)
            return False
        return True

    def check_sub_path(self, pth: str) -> None:
        '''
        if wpath is not subdir self.argpath sys.exit(False)
        '''
        child = Path(os.path.abspath(pth))
        print(self.argpath)
        parent = Path(self.argpath)
        if parent not in child.parents:
            return False
        return True


if __name__ == '__main__':
    dt = DirTravel()
    iterpath = dt.gen_dir(ipath=dt.argpath, name=dt.argname)
    for pa in iterpath:
        # dt.gen_ele_id(ipath=pa, iuid=0, igid=0)
        # dt.gen_ele(ipath=pa)
        # dt.gen_ele_filtrpath(ipath=pa)
        dt.gen_ele_id_filter(ipath=pa)
