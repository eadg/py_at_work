import shutil
from datetime import date
from pathlib import Path, PosixPath


class SAD:
    """SeekAndDestroy looks for old session dirs in ~/.local/share/ ."""

    def lookingfor(self) -> None:
        """If user have more active ssession."""
        users = ['agrzesiek', 'dbabala', 'dzolciak', 'jkrok', 'kmordan',
                 'lbkowalczyk', 'mtomaszewski', 'ssowinski']

        for usr in users:
            count = 0
            pastr = f'/exports/home/{usr}/.local/share/rstudio/sessions/active'
            paf = Path(pastr)
            for _ in paf.iterdir():
                count += 1
            dato = date.today()
            for dre in paf.iterdir():
                if count > 1:
                    tdelt = dato - date.fromtimestamp(dre.stat().st_mtime)
                    if tdelt.total_seconds() > 0:
                        self.act(dre)
                        count -= 1
                        # print(f"{usr}:{dre} time_mod: {dre.stat().st_mtime}")
                else:
                    print(f"{usr}: count:{count} -> break")
                    break

    def act(self, paf: PosixPath) -> None:
        """Remove unwated dir:( ."""
        shutil.rmtree(paf)


if __name__ == '__main__':
    sak = SAD()
    sak.lookingfor()
