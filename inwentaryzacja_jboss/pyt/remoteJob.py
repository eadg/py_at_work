lsrcd = '''find /etc/rc.d/ -iname '*bos*' -exec ls -al {} \;'''
lsinitd = '''find /etc/init.d/ -iname '*bos*' '''
lssrv = '''ls /srv | grep bos '''
lshome = '''ls ~'''
sudosu = '''sudo -S su -'''
echoid = '''id'''
sudopsjava = '''sudo -S ps aux | grep -i java | grep -v grep | grep -i jboss '''
psjava = '''ps aux | grep -i java | grep -v grep | grep -i jboss '''
jbossversion = '''grep VERSION /srv/jboss/jboss/profile.conf | cut -d'=' -f2'''
sudosysctl = '''sudo -S systemctl --no-pager | grep -i jboss | awk \'{print $1, $2, $3}\''''
cpfile = '''cp /srv/jboss/jboss/standalone/configuration/standalone.xml /srv/homes/jrokicki/standalone.xml_20160316'''
# service = '''service --status-all'''
service = '''COM=`whereis service | awk '{print $2}' | xargs -I {} echo {}`; $COM --status-all | grep -i jboss'''
sudoservice_s = '''sudo -S service --status-all'''
echochkc = '''COM=`whereis chkconfig | awk '{print $2}' | xargs -I {} echo {}`; $COM --list | grep -i jboss'''
echoPath = '''env | grep -i path'''
whereIs = '''whereis chkconfig'''
runlevel = '''COM=`whereis runlevel | awk '{print $2}' | xargs -I {} echo {}`; $COM | awk '{print $2}' '''
# smykrealese = '''ls -l /srv/jboss/jboss |  awk -F'-> ' '{print $2}' | cut -d'/' -f5,6'''
smykrealese = '''scp  '''
yum_install = '''yum install python-psycopg2'''
pld_install = '''poldek -i python-psycopg2'''
# jobls = '''mount | grep vda'''
# jobls = 'ls -AH /etc/init.d/jboss*'
# jobls = '''grep VERSION  /srv/jboss/jboss/profile.conf'''
