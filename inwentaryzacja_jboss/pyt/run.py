import paramiko
from socket import gethostbyname
import remoteJob
import subprocess
from os import devnull
import re
import getpass


def check_cred():
    with open('ssap') as account:
        login = account.readlines()
        # passw = getpass.getpass()
    return login[0].strip(), login[1].strip()


def is_alive(hostname):
    with open(devnull, "wb") as nul:
            ping_response = subprocess.Popen([
                "/bin/ping", "-c1", "-w1", hostname.strip()], stdout=nul, stderr=nul).wait()
    return ping_response


def conn_open(hostname, login, passw):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    host = gethostbyname(hostname.strip())
    client.connect(hostname=host, username=login, password=passw, timeout=5)
    print("otwieram połączenie "+hostname.strip())
    return client


def conn_close(connection):
    connection.close()
    print("zamykam połączenie")


def puthostname(hostname):
    raport.write(hostname.strip())
    putdelimiter()
    errors.write(hostname.strip())


def putemptyline():
    raport.write('\n')
    print('-'*30+'\n')
    # raport.write('-'*30+'\n')
    # raport.write('\n'+'-'*30+'\n')


def putdelimiter():
    raport.write(';')


def putspace():
    raport.write('|')


def check_etc(client):
    (channel_in, channel_out, channel_error) = client.exec_command(remoteJob.lsinitd)
    out = channel_out.readlines()
    if len(out) > 0:
        for line in out:
            raport.write(str(line).strip())
            putspace()
    else:
        raport.write("brak jboss-startup in /etc/"+'\n')


def cp_file(client):
    (channel_in, channel_out, channel_error) = client.exec_command(remoteJob.smykrealese)
    out = channel_out.readlines()
    if len(out) > 0:
        for line in out:
            raport.write(str(line).strip())
    else:
        raport.write("no cp")
        raport.write('\n')


def check_srv(client):
    (channel_in, channel_out, channel_error) = client.exec_command(remoteJob.lssrv)
    out = channel_out.readlines()
    if len(out) > 0:
        for line in out:
            raport.write('/srv/'+str(line).strip())
            putspace()

    else:
        raport.write("no JBoss files  in /srv")
        putspace()


def check_run_jboss(client, passw):
    (channel_in, channel_out, channel_error) = client.exec_command(
        remoteJob.psjava)
    out = channel_out.readlines()
    if len(out) > 0:
        for line in out:
            jboss_home_dir = re.search(r'-Djboss.home.dir=(.*) -D', line)
            if(jboss_home_dir):
                raport.write(jboss_home_dir.group(1))
                putdelimiter()
                check_jboss_version(jboss_home_dir.group(1))
            jboss_old_home_dir = re.search(r'-classpath (.*)/bin/run.jar:', line)
            if(jboss_old_home_dir):
                raport.write(jboss_old_home_dir.group(1))
                putdelimiter()
                check_jboss_version(jboss_old_home_dir.group(1))
            putspace()
    else:
        (channel_in, channel_out, channel_error) = client.exec_command(
            remoteJob.sudopsjava)
        channel_in.write(passw + '\n')
        channel_in.flush()
        out = channel_out.readlines()
        if len(out) > 0:
            for line in out:
                jboss_home_dir = re.search(r'-Djboss.home.dir=(.*) -D', line)
                if(jboss_home_dir):
                    raport.write(jboss_home_dir.group(1))
                    putdelimiter()
                    check_jboss_version(jboss_home_dir.group(1))
                jboss_old_home_dir = re.search(r'-classpath (.*)/bin/run.jar:', line)
                if(jboss_old_home_dir):
                    raport.write(jboss_old_home_dir.group(1))
                    putdelimiter()
                    check_jboss_version(jboss_old_home_dir.group(1))
            putspace()
        else:
            raport.write("brak procesu java-jboss")


def check_jboss_version(jboss_home):
    jbossversion = 'grep VERS ' + jboss_home + '''/profile.conf''' + ''' | cut -d'=' -f2'''
    (channel_in, channel_out, channel_error) = client.exec_command(jbossversion)
    out = channel_out.readlines()
    if len(out) > 0:
        for line in out:
            raport.write(line.strip())


def check_systemctl(client, passw):
    (channel_in, channel_out, channel_error) = client.exec_command(
        remoteJob.sudosysctl)
    channel_in.write(passw + '\n')
    channel_in.flush()
    out = channel_out.readlines()
    if len(out) > 0:
        for line in out:
            raport.write(line)
            raport.write('\n')
    else:
        raport.write('\n')
        raport.write("no feedback from systemctl")
        raport.write('\n')


def check_chkconfig(client, passw):
    (channel_in, channel_out, channel_error) = client.exec_command(
        remoteJob.echochkc)
    out = channel_out.readlines()
    err = channel_error.readlines()
    if len(out) > 0:
        for line in out:
            raport.write(str(line).strip())
            putspace()
    if len(err) > 0:
        for line in err:
            errors.write(line)
            putspace()


def check_runlevel(client, passw):
    (channel_in, channel_out, channel_error) = client.exec_command(
        remoteJob.runlevel)
    out = channel_out.readlines()
    err = channel_error.readlines()
    if len(out) > 0:
        for line in out:
            raport.write(str(line).strip())
            putspace()
    if len(err) > 0:
        for line in err:
            errors.write(line)
            putspace()


def check_service(client, passw):
    (channel_in, channel_out, channel_error) = client.exec_command(
        remoteJob.service)
    out = channel_out.readlines()
    err = channel_error.readlines()
    if len(out) > 0:
        for line in out:
            # raport.write(str(line).strip())
            jboss_service = re.search(r'.*(jboss[\S+\W+])[:\W]+.*', line)
            # jboss_service = re.search(r'.*[\W/]+([\w_-]+jboss\S+)[:\W]+.*', line)
            if(jboss_service):
                raport.write(jboss_service.group(1))
                putspace()
    if len(err) > 0:
        for line in err:
            errors.write(line)
            putspace()


def check_service_test(client, passw):
    (channel_in, channel_out, channel_error) = client.exec_command(
        remoteJob.sudosu, get_pty=True)
    channel_in.write(passw + '\n')
    channel_in.flush()
    channel_in.write(remoteJob.service + '\n')
    out = channel_out.readlines()
    # err = channel_error.readlines()
    err = out
    print(str(len(out)), str(len(err)))
    if len(out) > 0:
        for line in out:
            print(line)
            jboss_service = re.search(r'.*(jboss\S+)[:\W]+.*', line)
            if(jboss_service):
                raport.write('\n')
                raport.write(jboss_service.group(1))
    if len(err) > 0:
        for line in err:
            print(line)
            errors.write(line)


def install_package(client, passw):
    (channel_in, channel_out, channel_error) = client.exec_command(
        remoteJob.sudosu, get_pty=True)
    channel_in.write(passw + '\n')
    channel_in.flush()
    channel_in.write(remoteJob.pld_install + '\n')
    channel_in.write(remoteJob.yum_install + '\n')
    out = channel_out.readlines()
    # err = channel_error.readlines()
    err = out
    print(str(len(out)), str(len(err)))
    if len(out) > 0:
        for line in out:
            print(line)
    if len(err) > 0:
        for line in err:
            print(line)
            errors.write(line)

if __name__ == '__main__':
    # hosts = open('ro_jboss.txt')
    # hosts = open('short_list_jboss.txt')
    hosts = open('test_list_jboss.txt')
    # hosts = open('all_jboss_production.txt')
    # hosts = open('all_jboss.txt')
    raport = open('raport.txt', 'w+')
    errors = open('eraport.txt', 'w+')
    login, passw = check_cred()

    for hostname in hosts:
        if '#' not in hostname and is_alive(hostname.strip()) == 0:
            client = conn_open(hostname, login, passw)
            puthostname(hostname)
            # putdelimiter()
            # check_srv(client)
            # putdelimiter()
            # check_run_jboss(client, passw)
            # check_systemctl(client, passw)
            # cp_file(client)
            # check_whereis(client, passw)
            # check_etc(client)
            # putdelimiter()
            # check_service(client, passw)
            # putdelimiter()
            # check_chkconfig(client, passw)
            # check_runlevel(client, passw)
            install_package(client, passw)
            conn_close(client)
            putemptyline()
        else:
            raport.write(hostname.strip()+";shutdown")
            putemptyline()

    raport.write('\n\n')
    raport.close()
    errors.close()
