import paramiko
from scp import SCPClient
from socket import gethostbyname
import run


login, passw = run.check_cred()
ssh = paramiko.SSHClient()
ssh.load_system_host_keys()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
hosts = open('../all_jboss_pld.txt')
for h in hosts:
    if '#' not in h and run.is_alive(h.strip()) == 0:
        print(h)
        host = gethostbyname(str(h).strip())
        ssh.connect(host, username=login, password=passw)
        sccp = SCPClient(ssh.get_transport())
        sccp.put('jboss_restart_log.py')
        sccp.close()
hosts.close()
ssh.close()
print('koniec')
