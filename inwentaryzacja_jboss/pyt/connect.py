import paramiko
from socket import gethostbyname
import subprocess
from os import devnull


class Connect(object):
    '''Providing methods for connection via SSH'''

    def __init__(self):
        self.out = open('raport.txt', 'w')
        self.err = open('error.txt', 'w')
        with open('ssap') as account:
            lines = account.readlines()
        self.usr = lines[0].strip()
        self.pas = lines[1].strip()

    def conn_open(self, hostname):
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        host = gethostbyname(hostname.strip())
        client.connect(hostname=host, username=self.usr, password=self.pas, timeout=5)
        print("otwieram połączenie "+hostname.strip())
        return client

    def conn_chann_open(self, hostname):
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        host = gethostbyname(hostname.strip())
        client.connect(hostname=host, username=self.usr, password=self.pas, timeout=5)
        channel = client.invoke_shell()
        print("otwieram channel "+hostname.strip())
        return channel

    def conn_chann_close(self, channel):
        channel.close()
        print("zamykam channel ")

    def is_alive(self, hostname):
        with open(devnull, "wb") as nul:
                ping_response = subprocess.Popen([
                    "/bin/ping", "-c1", "-w1", hostname.strip()],
                    stdout=nul, stderr=nul).wait()
        return ping_response

    def putdelimiter(self):
        self.out.write(';')
        self.err.write(';')

    def putspace(self):
        self.out.write('|')

    def puthostname(self, hostname):
        self.out.write(hostname.strip())
        self.putdelimiter()

    def conn_close(self, connection):
        connection.close()
        self.out.write("zamykam połączenie")
        print("zamykam połączenie")
