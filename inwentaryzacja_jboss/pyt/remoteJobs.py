import connect
import time


class RemoteJob(connect.Connect):

    '''Providing remote-jobs for connection via class connect.Connect'''

    def __init__(self):
        super(RemoteJob, self).__init__()

    def sudosu(self, client):
        ''' login to sudo su - channel '''
        time.sleep(1)
        client.send("sudo su")
        client.send('\n')
        time.sleep(1)
        client.send(self.pas)
        client.send('\n')

    def extract_tar(self, client):
        # import tarfile
        (channel_in, channel_out, channel_error) = client.exec_command(
            '''tar xfvz packrhq.tgz''')
        out = channel_out.readlines()
        err = channel_error.readlines()
        if len(out) > 0:
            for line in out:
                self.out.write(line.strip())
                self.putspace()
        if len(err) > 0:
            self.err.write("error in extract_tar"+'\n')

    def chan_extract_tar(self, client):
        ''' extract tar - channel '''
        client.send("tar xzfv packrhq.tgz")
        client.send("\n")
        time.sleep(1)

    def chan_check_systemctl(self, client):
        '''implement rhq-agent@systemctl using channel '''
        self.sudosu(client)
        time.sleep(1)
        client.send("systemctl")
        client.send('\n')
        self.out.write(str(client.recv(4096)))
        print(str(client.recv(4096)))

    def chan_moving_file(self, client):
        '''implement rhq-agent@systemctl using channel '''
        # self.sudosu(client)
        time.sleep(1)
        client.send("sudo su")
        client.send('\n')
        time.sleep(1)
        client.send(self.pas)
        client.send('\n')
        client.send("/srv/rhq-agent/bin/rhq-agent-wrapper.sh stop")
        client.send("\n")
        time.sleep(4)
        client.send("mv rhq-agent-env.sh /srv/rhq-agent/bin")
        client.send("\n")
        client.send("mv rhq-agent.service /etc/systemd/system")
        client.send("\n")
        time.sleep(1)
        client.send("systemctl enable rhq-agent.service")
        client.send("\n")
        time.sleep(1)
        client.send("systemctl restart rhq-agent.service")
        client.send("\n")

    def install_python_packgages(self, client):
        time.sleep(1)
        client.send("sudo su")
        client.send('\n')
        time.sleep(1)
        client.send(self.pas)
        client.send('\n')
        client.send("yum install python-psycopg2")
        client.send('\n')
        client.send("poldek -i python-psycopg2")
        client.send('\n')
