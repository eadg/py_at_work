((nil . ((eval . (progn
                   (require 'grep)
                   (add-to-list 
                    (make-local-variable 'grep-find-ignored-directories)
                    "site-packages"))))))
