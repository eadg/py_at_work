import SimpleHTTPServer
import SocketServer


def serv_http_port(port=8080, msg='Listen on'):
    handler = SimpleHTTPServer.SimpleHTTPRequestHandler
    httpd = SocketServer.TCPServer(("", port), handler)
    print(msg + ' on ' + str(port))
    httpd.serve_forever()


if __name__ == '__main__':
    serv_http_port()
    # serv_http_port(port=8010)
