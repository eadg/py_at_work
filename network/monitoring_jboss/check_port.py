import socket
import subprocess
import time

lis = []


def check_ping(host):
    proc = subprocess.Popen(["ping", "-c1", host])
    return proc


def check_port(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    port_str = str(port)
    result = sock.connect_ex((host, port))
    if result == 0:
        log = host + ": " + port_str + " open" + "\n"
        print(log)
        sock.close()
        return log
    else:
        log = host + ": " + port_str + " close" + "\n"
        print(log)
        sock.close()
        return log


def run():
    with open('raport.txt', 'w') as f:
        for _ in range(1000):
            # check_port("jboss-teren-dev", 4447)
            # log = check_port("jboss-core-dev", 4447)
            log = check_port("jboss-dok1", 4447)
            f.write(log)
            log = check_port("jboss-core2", 4447)
            f.write(log)
            log = check_port("jboss-core2", 4447)
            f.write(log)
            # check_port("jboss-dok1", 4447)
            # check_port("jboss-service2", 4447)
            # proc = check_ping("jboss-core-dev")
            # lis.append(proc)
            time.sleep(0.1)


print("rozmiar proc_listy: " + str(len(lis)))
run()

for proc in lis:
    out, err = proc.communicate()
    # print(out)
