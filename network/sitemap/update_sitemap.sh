#!/bin/bash

cd /srv/scripts/python-sitemap

/usr/bin/python3 /srv/scripts/python-sitemap/main.py --config /srv/scripts/python-sitemap/config/config.json
/usr/bin/python3 /srv/scripts/python-sitemap/fix_sitemap.py

