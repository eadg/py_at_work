import re
import os
import os.path


class sitemap(object):

    def __init__(self):
        ''' list of banned urls '''
        self.banned = [
            'https://ultimo.pl/egzekucja-komornicza-krok-po-kroku',
            'https://ultimo.pl/mailto:praca@ultimo.pl',
            'https://ultimo.pl/tel:+48713594160',
            'https://ultimo.pl/mailto:dpo@b2upi.com',
            'https://ultimo.pl/mailto:anna.wozniak@ultimo.pl',
            'https://ultimo.pl/mailto:inspektor.danych@taktofinanse.pl',
            'https://ultimo.pl/tel:+48713234689',
            'https://ultimo.pl/tel:+48800804904',
            'https://ultimo.pl/mailto:obsluga_klienta@ultimo.pl',
            'https://ultimo.pl/mailto:ultimo@ultimo.pl',
            'https://ultimo.pl/najczesciej-zadawane-pytania',
            'https://ultimo.pl/najczesciej-zadawanepytania',
            'https://ultimo.pl/oferta-pracy/specjalista_ds_przeciwdzialania_naduzyciom_finansowym',
            'https://ultimo.pl/tel:+48713234730',
            'https://ultimo.pl/tel:+48713234790',
            'https://ultimo.pl/tel:+48713581638',
            'https://ultimo.pl/co-to-jest-eultimo',
            'https://ultimo.pl/eultimo',
            'https://ultimo.pl/ultimo/private_policy.ultimo',
            'https://ultimo.pl/ultimo.pl/',
            'https://ultimo.pl/registration/splacaj_online.ultimo',
            'https://ultimo.pl/registration/registration_step_one.ultimo',
            'https://ultimo.pl/login/password_reminder.ultimo',
            'https://ultimo.pl/login/activation_link_sender.ultimo',
            'https://ultimo.pl/javax.faces.resource/Regulamin_Serwisu_eUltimo_27-03-2015.pdf.ultimo?ln=pdf',
            'https://ultimo.pl/javax.faces.resource/Regulamin_Serwisu_eUltimo_27-03-2015.pdf.ultimo',
            'https://ultimo.pl/javax.faces.resource/Niezbednik_klienta-short.pdf.ultimo?ln=pdf',
            'https://ultimo.pl/javax.faces.resource/Niezbednik_klienta-short.pdf.ultimo',
            'https://ultimo.pl/javax.faces.resource/Abecadlo_Domowego_Budzetu-short.pdf.ultimo?ln=pdf',
            'https://ultimo.pl/javax.faces.resource/Abecadlo_Domowego_Budzetu-short.pdf.ultimo',
            'https://ultimo.pl/Zakup-portfeli-wierzytelnosci',
            'https://ultimo.pl/mailto:inspektor.danych@ultimo.pl',
            'http://ultimo.pl/uploads/Regulamin_strony_internetowej_www.ultimo.pl.pdf',
            'https://ultimo.pl/uploads/Regulamin_strony_internetowej_www.ultimo.pl.pdf',
            'https://ultimo.pl/zloz-podanie/lider-zespolu',
            'https://ultimo.pl/zloz-podanie/programista-xml',
            ]
        self.extra_address = [
            'https://ultimo.pl/zloz-podanie/specjalista-ds-windykacji-wierzytelnosci-zabezpieczonych',
            'https://ultimo.pl/zloz-podanie/terenowy-doradca-klienta-wojewodztwo',
            'https://ultimo.pl/zloz-podanie/analityk-biznesowy',
            'https://ultimo.pl/uploads/rodo/OI_TAKTO__INNI_ADMINISTRATORZY_bankovo.pdf',
            'https://ultimo.pl/zloz-podanie/analityk-w-zespole-strategii-windykacji',
            'https://ultimo.pl/zloz-podanie/mlodszy-specjalista-ds-korespondecji',
            'https://ultimo.pl/zloz-podanie/mlodszy-specjalista-ds-windykacji-prawnej-wierzytelnosci-zabezpieczonych',
            'https://ultimo.pl/uploads/rodo/RODO_obowiazek_informacyjny_NSFIZ_strona.pdf',
            'https://ultimo.pl/zloz-podanie/konserwator+',
            'https://ultimo.pl/news/kolejny-certyfikat-etyczny-dla-ultimo',
            'https://ultimo.pl/zloz-podanie/specjalista-ds-sprzedaży-internetowej',
            'https://ultimo.pl/zloz-podanie/Analityk_finansowy',
            'https://ultimo.pl/zloz-podanie/mlodszy-specjalista-ds-rozliczen-dr',
            'https://ultimo.pl/uploads/rodo/RODO_obowiazek_informacyjny_UPI_strona.pdf',
            'https://ultimo.pl/uploads/B2Holding_Annual_Report_2016.pdf',
            'https://ultimo.pl/czym-jest-obsluga-zadlu%C5%Bcenia',
            'https://ultimo.pl/jakie-zasady-obowi%C4%85zuj%C4%85-przy-sprzedazy-dlugow',
            'https://ultimo.pl/news/oswiadczenie-ultimo',
            'https://ultimo.pl/news/rekordowy-rok',
            'https://ultimo.pl/news/sprawdz-online-ultimo',
            'https://ultimo.pl/news/teleugoda',
            'https://ultimo.pl/news/ultimo-spelnia-marzenia-swoich-klientow',
            'https://ultimo.pl/news/zmiany-w-ultimo',
            'https://ultimo.pl/Obs%C5%82uga-wierzytelno%C5%9Bci-na-etapie-przeds%C4%85dowym',
            'https://ultimo.pl/obs%C5%82uga-wierzytelnosci-hipotecznych',
            'https://ultimo.pl/press/get/oswiadczenie',
            'https://ultimo.pl/przed-rozpoczeciem-pracy',
            'https://ultimo.pl/uploads/WNIOSKI_24082018/Pe%C5%82nomocnictwo.pdf',
            'https://ultimo.pl/zakup-portfeli-wierzytelnosci',
            'https://ultimo.pl/zloz-podanie/analityk_business_intelligence',
            'https://ultimo.pl/zloz-podanie/analityk_ryzyka_kredytowego',
            'https://ultimo.pl/zloz-podanie/analityk_w_dziale_modeli_statystycznych',
            'https://ultimo.pl/zloz-podanie/doradca_klienta_ds_obslugi_posprzedazowej',
            'https://ultimo.pl/zloz-podanie/Head_of_Contact_Center_Team',
            'https://ultimo.pl/zloz-podanie/informatyk_wspracia_u%C5%BCytkownik%C3%B3w_pierwsza_linia',
            'https://ultimo.pl/zloz-podanie/mlodszy_inspektor_kredytowy',
            'https://ultimo.pl/zloz-podanie/mlodszy-ksiegowy',
            'https://ultimo.pl/zloz-podanie/mlodszy_specjalista_ds_administracji_i_wsparcia_procesow',
            'https://ultimo.pl/zloz-podanie/mlodszy_specjalista_ds_obslugi_wnioskow',
            'https://ultimo.pl/zloz-podanie/Pracownik_biurowy',
            'https://ultimo.pl/zloz-podanie/praktykant',
            'https://ultimo.pl/zloz-podanie/praktykant_w_zespole_wierzytelnosci_zabezpieczonych',
            'https://ultimo.pl/zloz-podanie/praktykant_w_zespole_wycen',
            'https://ultimo.pl/zloz-podanie/specjalista_ds_prawno_administracyjnych',
            'https://ultimo.pl/zloz-podanie/specjalista_w_dziale_wspolpracy_z_komornikami_sadowymi',
            'https://ultimo.pl/zloz-podanie/trener-wewnetrzny',
            'https://ultimo.pl/uploads/dlaczego-warto/1.jpg',
            'https://ultimo.pl/uploads/dlaczego-warto/2.jpg',
            'https://ultimo.pl/uploads/dlaczego-warto/3.jpg',
            'https://ultimo.pl/uploads/dlaczego-warto/5.jpg',
            'https://ultimo.pl/uploads/dlaczego-warto/6.jpg',
            'https://ultimo.pl/uploads/dlaczego-warto/7.jpg',
            'https://ultimo.pl/uploads/dlaczego-warto/8.jpg',
            'https://ultimo.pl/uploads/dlaczego-warto/min_1479990048_10.jpg',
            'https://ultimo.pl/uploads/dlaczego-warto/min_1479990048_9.jpg',
            'https://ultimo.pl/uploads/dlaczego-warto/ultimo_do_podmiany.png',
            'https://ultimo.pl/uploads/Landing_page_na_aktualno%C5%9Bci_1.jpg',
            'https://ultimo.pl/uploads/landing-pages/dobra-propozycja/ultimo-logo.png',
            'https://ultimo.pl/uploads/niezbednik_ikony/go-work-red.png',
            'https://ultimo.pl/uploads/ULTIMO_B2H_RGB.jpg',
            'https://ultimo.pl/uploads/zdp_www.png',
            'https://ultimo.pl/uploads/zdp.png',
            ]
        # self.fp = '/home/services/httpd/vhosts/www_ultimo/web/sitemap.xml'
        # self.fp_ok = '/home/services/httpd/vhosts/www_ultimo/web/sitemap.xml.ok'
        self.fp = 'sitemap.xml'
        self.fp_ok = 'sitemap.xml.ok'

    def to_https(self):
        '''replace http to https'''
        pattern = re.compile('(http://ultimo)')
        with open(self.fp, 'r+') as f:
            data = f.read()
            fix_data = re.sub(pattern, r'https://ultimo', data)
            f.seek(0)
            f.write(fix_data)

    def filter_banned(self):
        '''filter banned urls'''
        bann = False
        fok = open(self.fp_ok, 'w')
        with open(self.fp, 'r+') as f:
            for row in f:
                for ban in self.banned:
                    if ban in row:
                        bann = True
                if not bann:
                    fok.write(row)
                bann = False
        fok.close()
        self.ren()

    def ren(self):
        '''replace final file with temporary file'''
        if os.path.isfile(self.fp_ok):
            os.rename(self.fp_ok, self.fp)

    def dedup(self):
        '''deduplication rows from sitemap'''
        setmp = set()
        regx = re.compile('.*loc>(https:.*)</loc>.*')
        fok = open(self.fp_ok, 'w')
        with open(self.fp, 'r+') as f:
            for row in f:
                if '<loc>' in row:
                    searched = re.search(regx, row)
                    if searched:
                        url = searched.group(1)
                        print(url)
                        if url not in setmp:
                            fok.write(row)
                            setmp.add(url)
                            # print('dodano: ' + url)
                else:
                    fok.write(row)
        fok.close()
        self.ren()

    def add_extra(self):
        all_line = open(self.fp).readlines()
        with open(self.fp_ok, 'w') as f:
            f.writelines(all_line[:-1])
            for extra in self.extra_address:
                row = '<url><loc>' + extra + '</loc></url>\n'
                f.write(row)
            f.writelines(all_line[-1])
        self.ren()


if __name__ == '__main__':
    s = sitemap()
    s.to_https()
    s.filter_banned()
    s.add_extra()
    s.dedup()
