import re
import os
import os.path


class sitemap(object):

    def __init__(self):
        ''' list of banned urls '''
        self.banned = [
            'https://ultimo.pl/egzekucja-komornicza-krok-po-kroku',
            'https://ultimo.pl/mailto:praca@ultimo.pl',
            'https://ultimo.pl/tel:+48713594160',
            'https://ultimo.pl/mailto:dpo@b2upi.com',
            'https://ultimo.pl/mailto:anna.wozniak@ultimo.pl',
            'https://ultimo.pl/mailto:inspektor.danych@taktofinanse.pl',
            'https://ultimo.pl/tel:+48713234689',
            'https://ultimo.pl/tel:+48800804904',
            'https://ultimo.pl/mailto:obsluga_klienta@ultimo.pl',
            'https://ultimo.pl/mailto:ultimo@ultimo.pl',
            'https://ultimo.pl/najczesciej-zadawane-pytania',
            'https://ultimo.pl/najczesciej-zadawanepytania',
            'https://ultimo.pl/oferta-pracy/specjalista_ds_przeciwdzialania_naduzyciom_finansowym',
            'https://ultimo.pl/tel:+48713234730',
            'https://ultimo.pl/tel:+48713234790',
            'https://ultimo.pl/tel:+48713581638',
            'https://ultimo.pl/co-to-jest-eultimo',
            'https://ultimo.pl/eultimo',
            'https://ultimo.pl/ultimo/private_policy.ultimo',
            'https://ultimo.pl/ultimo.pl/',
            'https://ultimo.pl/registration/splacaj_online.ultimo',
            'https://ultimo.pl/registration/registration_step_one.ultimo',
            'https://ultimo.pl/login/password_reminder.ultimo',
            'https://ultimo.pl/login/activation_link_sender.ultimo',
            'https://ultimo.pl/javax.faces.resource/Regulamin_Serwisu_eUltimo_27-03-2015.pdf.ultimo?ln=pdf',
            'https://ultimo.pl/javax.faces.resource/Regulamin_Serwisu_eUltimo_27-03-2015.pdf.ultimo',
            'https://ultimo.pl/javax.faces.resource/Niezbednik_klienta-short.pdf.ultimo?ln=pdf',
            'https://ultimo.pl/javax.faces.resource/Niezbednik_klienta-short.pdf.ultimo',
            'https://ultimo.pl/javax.faces.resource/Abecadlo_Domowego_Budzetu-short.pdf.ultimo?ln=pdf',
            'https://ultimo.pl/javax.faces.resource/Abecadlo_Domowego_Budzetu-short.pdf.ultimo',
            'https://ultimo.pl/Zakup-portfeli-wierzytelnosci',
            'https://ultimo.pl/mailto:inspektor.danych@ultimo.pl',
            ]
        self.extra_address = [
            'https://ultimo.pl/zloz-podanie/specjalista-ds-windykacji-wierzytelnosci-zabezpieczonych',
            'https://ultimo.pl/zloz-podanie/terenowy-doradca-klienta-wojewodztwo',
            'https://ultimo.pl/zloz-podanie/analityk-biznesowy',
            'https://ultimo.pl/uploads/rodo/OI_TAKTO__INNI_ADMINISTRATORZY_bankovo.pdf',
            'https://ultimo.pl/zloz-podanie/analityk-w-zespole-strategii-windykacji',
            'https://ultimo.pl/zloz-podanie/mlodszy-specjalista-ds-korespondecji',
            'https://ultimo.pl/zloz-podanie/mlodszy-specjalista-ds-windykacji-prawnej-wierzytelnosci-zabezpieczonych',
            'https://ultimo.pl/uploads/rodo/RODO_obowiazek_informacyjny_NSFIZ_strona.pdf',
            'https://ultimo.pl/market-dlugow',
            'https://ultimo.pl/zloz-podanie/konserwator+',
            'https://ultimo.pl/news/kolejny-certyfikat-etyczny-dla-ultimo',
            'https://ultimo.pl/zloz-podanie/specjalista-ds-sprzedaży-internetowej',
            'https://ultimo.pl/zloz-podanie/Analityk_finansowy',
            'https://ultimo.pl/zloz-podanie/lider-zespolu-contact-center',
            'https://ultimo.pl/zloz-podanie/mlodszy-specjalista-ds-rozliczen-dr',
            'https://ultimo.pl/uploads/rodo/RODO_obowiazek_informacyjny_UPI_strona.pdf',
            'https://ultimo.pl/uploads/B2Holding_Annual_Report_2016.pdf',
            ]
        # self.fp = '/home/services/httpd/vhosts/www_ultimo/web/sitemap.xml'
        # self.fp_ok = '/home/services/httpd/vhosts/www_ultimo/web/sitemap.xml.ok'
        self.fp = 'sitemap.xml'
        self.fp_ok = 'sitemap.xml.ok'

    def to_https(self):
        '''replace http to https'''
        pattern = re.compile('(http://ultimo)')
        with open(self.fp, 'r+b') as f:
            data = f.read()
            fix_data = re.sub(pattern, r'https://ultimo', data)
            f.seek(0)
            f.write(fix_data)

    def filter_banned(self):
        '''filter banned urls'''
        bann = False
        fok = open(self.fp_ok, 'wb')
        with open(self.fp, 'r+b') as f:
            for row in f:
                for ban in self.banned:
                    if ban in row:
                        bann = True
                if not bann:
                    fok.write(row)
                bann = False
        fok.close()
        self.ren()

    def ren(self):
        '''replace final file with temporary file'''
        if os.path.isfile(self.fp_ok):
            os.rename(self.fp_ok, self.fp)

    def dedup(self):
        '''deduplication rows from sitemap'''
        setmp = set()
        regx = re.compile('.*loc>(https:.*)</loc>.*')
        fok = open(self.fp_ok, 'wb')
        with open(self.fp, 'r+b') as f:
            for row in f:
                if '<loc>' in row:
                    searched = re.search(regx, row)
                    if searched:
                        url = searched.group(1)
                        print(url)
                        if url not in setmp:
                            fok.write(row)
                            setmp.add(url)
                            # print('dodano: ' + url)
                else:
                    fok.write(row)
        fok.close()
        self.ren()

    def add_extra(self):
        all_line = open(self.fp).readlines()
        with open(self.fp_ok, 'wb') as f:
            for line in all_line[:-1]:
                f.write(line.encode('utf-8'))
            for extra in self.extra_address:
                row = '<url><loc>' + extra + '</loc></url>\n'
                row2 = row.encode('utf-8')
                f.write(row2)
            f.write(all_line[-1].encode('utf-8'))
        self.ren()


if __name__ == '__main__':
    s = sitemap()
    s.to_https()
    s.filter_banned()
    s.add_extra()
    s.dedup()
