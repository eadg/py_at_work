import re

class sitemap(object):


    def to_https(self):
        fpath = '/home/services/httpd/vhosts/www_ultimo/web/sitemap.xml'
        # fpath = 'sitemap.xml'
        pattern = re.compile('(http://ultimo)')
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(pattern, r'https://ultimo', data)
            f.seek(0)
            f.write(fix_data)

if __name__ == '__main__':
    s = sitemap()
    s.to_https()
