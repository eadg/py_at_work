import socket

"""
Send to host message
Used for sending msg from eultimo1 to logger
"""

UDP_IP = "10.0.234.62"
# UDP_IP = "127.0.0.1"
UDP_PORT = 12201
# UDP_PORT = 5005
MESSAGE = "Hello!"

print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT)
print("message:", MESSAGE)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
