from bs4 import BeautifulSoup
import webbrowser as wbbr


def fun1():
    with open("02/mailing.html") as fp:
        s = BeautifulSoup(fp, "html.parser")
        for f in s.find_all('a'):
            la = f.get('href')
            print(la)


def fun2():
    with open("02/mailing.html") as fp, open(
            "02/mailok.html", 'w') as fw:
        s = BeautifulSoup(fp, "html.parser")
        for link in s.find_all('a'):
            if link['href'].startswith('http://bit.ly/'):
                link['href'] = 'http://dudu'
        print(s.prettify())
        fw.write(s.prettify())
    wbbr.open_new_tab("02/mailok.html")


def func3():
    with open("exp1/index.html") as fp, open(
            "exp1/indexok.html", 'w') as fw:
        s = BeautifulSoup(fp, "html.parser")
        


if __name__ == '__main__':
    fun3()
