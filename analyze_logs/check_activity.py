import gzip
from pathlib import Path
import re
import sqlite3

from timetrav import TT


class ChAc():

    def get_accounts(self) -> set:
        fname = self.get_name_log()
        zet = set()
        pat = re.compile(rb'.*\W+(ftp\w+)\W+.*', flags=re.MULTILINE)
        with gzip.open('/srv/logs/'+fname, 'rb') as f:
            for line in f:
                if b'ftp_' in line and b'Accept' in line:
                    searched = re.search(pat, line)
                    account = searched.group(1)
                    zet.add(account.decode())
        return zet

    def get_name_log(self) -> str:
        dayofyear = TT().get_nr_of_period(per='y')
        fname = f"{dayofyear}_secure.gz"
        return fname

    def list_accounts(self) -> None:
        for acc in self.get_accounts():
            print(acc)

    def update_db(self) -> None:
        con = self.initdb()
        cur = con.cursor()
        zet = self.get_accounts()
        dtt = TT(-1).get_iso()
        dt = int(dtt)
        for acc in zet:
            cur.execute("begin")
            try:
                cur.execute("insert into stat values(?, ?)", (acc, dt,))
                cur.execute("commit")
            except sqlite3.IntegrityError:
                cur.execute("rollback")
                cur.execute("select dt from stat where ac=?", (acc,))
                for row in cur:
                    if dt > row[0]:
                        cur.execute("begin")
                        cur.execute(
                            "update stat set dt=? where ac=?", (dt, acc,))
                        cur.execute("commit")

    def initdb(self) -> sqlite3.Connection:
        Path('db').mkdir(exist_ok=True)
        con = sqlite3.connect('db/acc_stat.db')
        con.isolation_level = 'DEFERRED'
        cur = con.cursor()
        try:
            cur.execute("select count(ac) from stat")
        except sqlite3.OperationalError:
            cur.execute('''create table stat(ac text, dt datetime)''')
            cur.execute('''create UNIQUE index ac_ind on stat(ac)''')
            print('db created')
        return con


if __name__ == '__main__':
    chac = ChAc()
    chac.get_accounts()
    chac.update_db()
    # chac.list_accounts()
