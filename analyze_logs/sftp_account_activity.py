import argparse
import fnmatch
import gzip
import os
from pathlib import Path
import re
import sys


class LogActivity(object):

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-p', action='store', help='path', dest='ap')
        results = self.parser.parse_args()
        self.argpath = results.ap

    def list_gz(self, ph: str) -> None:
        it = os.scandir(ph)
        for de in it:
            if de.is_file() and fnmatch.fnmatch(de, '*.log'):
                if de.stat().st_size < 41:
                    os.renames(de, ph + 'stub/' + de.name)
                elif len(re.findall('gz', de.name)) > 1:
                    self.gzopen(de)
                    os.renames(de, ph + 'done/' + de.name)


if __name__ == '__main__':
    la = LogActivity()
    la.list_gz(la.argpath)
