# -*- coding: utf-8 -*-
import socket
import sys
import urllib2
import os
import os.path
import logging
import ass

logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s',
    level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')


class Name(object):

    """
    name = pdc-t-jboss-recognizer-dev.ultimo.pl
    shortname = jboss-recognizer-dev
    sname = jboss-recognizer
    stage = dev
    funcname = recognizer-dev-pdc
    """

    def __init__(self):
        self.name, self.shortname, self.sname, self.funcname = self.get_hona()

    def get_hona(self):
        h0 = socket.gethostname()
        h1 = self.remove_suffix(h0)
        h2 = self.remove_prefix(h1)
        h3 = self.remove_stage(h2)
        h4 = self.get_jboss_function_name(h3)
        return h1, h2, h3, h4

    def remove_suffix(self, hostname):
        if 'ultimo.pl' in hostname:
            h = hostname.split('.', 1)
            return h.pop(0)
        else:
            return hostname

    def remove_prefix(self, hostname):
        """return without prefix "pdc-p or t"""
        if 'pdc' in hostname:
            pdc_host = hostname.split('-')
            no_pdc_host = '-'.join(pdc_host[2:])
            self.get_stage(no_pdc_host)
            return no_pdc_host
        else:
            self.get_stage(hostname)
            return hostname

    def get_stage(self, hostname):
        hs = hostname.split('-')
        for h in hs:
            if 'dev' in h:
                self.stage = 'dev'
            elif 'test' in h:
                self.stage = 'test'
            elif 'beta' in h:
                self.stage = 'beta'
            elif 'pre' in h:
                self.stage = 'pre-prod'
            else:
                self.stage = 'prod'

    def remove_stage(self, hostname):
        size = hostname.split('-')
        if size > 3:
            without_stage = hostname.split('-')[:2]
            return '-'.join(without_stage)
        else:
            without_stage = hostname.split('-')[:-1]
            return '-'.join(without_stage)

    def get_jboss_function_name(self, hostname):
        function_name = hostname.split('-').pop()
        func_name = function_name + '-' + self.stage + '-' + 'pdc'
        return func_name[:23]


class SubUrl(object):

    url = {
            'jboss-core-dev': 'https://subversion.ultimo.pl/projects/Smyk2.0/trunk/apps-assembly/dist-packs/jboss-core/src/main/resources/profiles/wildfly-core-dev-82/standalone/configuration/rules.properties',
            'jboss-dwp-dev': 'https://subversion.ultimo.pl/projects/Smyk2.0/trunk/apps-assembly/dist-packs/jboss-dwp/src/main/resources/profiles/wildfly-82-dwp-dev/standalone/configuration/rules.properties',
            'jboss-core1': 'https://subversion.ultimo.pl/projects/Smyk2.0/trunk/apps-assembly/dist-packs/jboss-core/src/main/resources/profiles/wildfly-core1-82/standalone/configuration/rules.properties',
            'jboss-core2': 'https://subversion.ultimo.pl/projects/Smyk2.0/trunk/apps-assembly/dist-packs/jboss-core/src/main/resources/profiles/wildfly-core2-82/standalone/configuration/rules.properties',
            'jboss-recognizer1': 'https://subversion.ultimo.pl/projects/Smyk2.0/trunk/apps-assembly/dist-packs/jboss-recognizer/src/main/resources/profiles/wildfly-recognizer1/standalone/configuration/rules.properties',
            'jboss-recognizer2': 'https://subversion.ultimo.pl/projects/Smyk2.0/trunk/apps-assembly/dist-packs/jboss-recognizer/src/main/resources/profiles/wildfly-recognizer2/standalone/configuration/rules.properties',
    }


class Env(object):

    def __init__(self):
        self.jboss_ver = self.get_jboss_ver()

    def get_jboss_ver(self):
        profile = '/srv/jboss/jboss/profile.conf'
        try:
            with open(profile) as f:
                prof = f.readlines()
                version_long = prof[1].split('=').pop(1)
                version_short = version_long.split('.').pop(0)
        except (IOError, IndexError):
            sys.exit("błąd odczytu->wychodzę")
        return version_short


class Fetch(object):

    def __init__(self, version):
        if int(version) > 4:
            self.path = '/srv/jboss/jboss/standalone/configuration/rules/'
        else:
            self.path = '/srv/jboss/jboss/default/conf/rules/'

    def read_rules(self, url):
        request = urllib2.Request(url)
        base64str = ass.read_ssap(cred='cred0')
        request.add_header("Authorization", "Basic %s" % base64str)
        try:
            logging.info('Ściągam reguły dla drools...')
            result = urllib2.urlopen(request)
            logging.info('Ściągnąłem reguły dla drools')
        except urllib2.URLError:
            logging.warn(
                "Brak zdefiniowanego adresu do pobrania rules'ów dla danego serwera. Będą się ściągać same.")
            sys.exit(0)
        for line_raw in result:
            if '=' in line_raw:
                line = line_raw.rsplit()
                (fname, url) = line.pop().split('=')
                logging.info("Pobieram: " + url)
                yield (fname, url)

    def isdir(self):
        if not os.path.isdir(self.path):
            os.makedirs(self.path)

    def save_pkg(self, tup):
        ext = '.pkg'
        fname, url = tup
        request = urllib2.Request(url)
        result = urllib2.urlopen(request)
        try:
            f = open(self.path+fname+ext, 'wb')
        except (IOError):
            sys.exit("błąd zapisu->wychodzę")
        else:
            f.write(result.read())
        finally:
            f.close()


if __name__ == '__main__':
    ne = Name()
    env = Env()
    sur = SubUrl().url
    fet = Fetch(env.jboss_ver)
    fet.isdir()
    gen = fet.read_rules(sur.get(ne.shortname, 'http://zxcv.ultimo.pl'))
    for pkg in gen:
        fet.save_pkg(pkg)
