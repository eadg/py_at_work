from unittest import TestCase, main
import rules


class Mytest(TestCase):

    def test_get_url(self):
        f = rules.Fetch(5)
        ul = rules.SubUrl()
        url = ul.url['jboss-core-dev']
        it = f.read_rules(url)
        self.assertIsInstance(it.next(), tuple)

if __name__ == '__main__':
    main()
