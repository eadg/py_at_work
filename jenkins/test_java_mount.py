import java_mount


def test_1():
    jm = java_mount.JavaMount()
    djm = jm.java_operations
    assert len(djm) == 5


def test_2():
    jm = java_mount.JavaMount()
    djm = jm.java_operations
    s = djm.get('jboss-core1')
    assert type(s) is list

def test_3():
    jm = java_mount.JavaMount()
    djm = jm.java_operations
    s = djm['jboss-core2']
    assert type(s) is list

def test_4():
    jm = java_mount.JavaMount()
    djm = jm.java_operations
    operations = djm['jboss-core1']
    assert len(operations) == 2

def test_5():
    jboss_script = 'jboss7'
    java_vers = 'jdk8'
    operations = {
        3: 'umount /srv/java',
        2: 'service ' + jboss_script + ' stop',
        4: 'ln -s /srv/jaws/' + java_vers + ' /srv/java/jdk',
        1: 'mount -t nfs 10.10.11.251:/NFS_JBOSS /srv/jaws -o ro',
        5: 'service ' + jboss_script + ' start',
    }
    sort_oper = sorted(zip(operations.keys(), operations.values()))
    assert len(sort_oper) == 5

def test_6():
    jm = java_mount.JavaMount()
    s = jm.modify_fstab()
    assert len(s) == 45
