import subprocess
import os.path as op
import re
from disk_operations import DiskOper
import final_modification as fm
import env
from logs import MyLogger


class JavaMount(object):

    def __init__(self):
        self.nm = env.Name()
        self.su = env.SubUrl()
        self.log = MyLogger().get_logger()
        self.java_version = self.su.get_jdk[self.nm.sname]
        self.java_operations = {
            'jboss-core1': ['jboss7', 'tomcat'],
            'jboss-core2': ['jboss7', 'tomcat'],
            'jboss-core-dev': ['jboss7', 'tomcat'],
            'jboss-core-test': ['jboss7', 'tomcat'],
            'jboss-service1': ['jboss7', 'solr'],
        }

    def update_java(self):
        jboss_script = fm.JBossAsService().script
        java_process = self.java_operations.get(
            self.nm.shortname, jboss_script)
        try:
            self.java_operations[self.nm.shortname]
        except KeyError:
            operations = {
                3: 'umount /srv/java',
                2: 'service ' + jboss_script + ' stop',
                4: 'ln -s /srv/jaws/' + self.java_version + ' /srv/java/jdk',
                1: 'mount -t nfs 10.10.11.251:/NFS_JBOSS /srv/jaws -o ro',
                5: 'service ' + jboss_script + ' start',
            }
        else:
            operations = {
                4: 'umount /srv/java',
                2: 'service ' + java_process[0] + ' stop',
                3: 'service ' + java_process[1] + ' stop',
                5: 'ln -s /srv/jaws/' + self.java_version + ' /srv/java/jdk',
                1: 'mount -t nfs 10.10.11.251:/NFS_JBOSS /srv/jaws -o ro',
                6: 'service ' + java_process[0] + ' start',
                7: 'service ' + java_process[1] + ' start',
            }
        sort_open = sorted(zip(operations.keys(), operations.values()))
        return sort_open

    def restore_old_java(self):
        jas = fm.JBossAsService()
        jboss_script = jas.script
        nm = env.Name()
        excluded = ['jboss-big', 'jboss-core1', 'jboss-core2']
        if nm.shortname not in excluded:
            operations = {
                3: 'umount /srv/jaws',
                2: 'service ' + jboss_script + ' stop',
                1: 'mount -t nfs 10.10.11.251:/NFS_JBOSS /srv/java',
                4: 'service ' + jboss_script + ' start',
            }
            DiskOper().mkdir_if('/srv/jaws', 'root')
            sort_oper = sorted(zip(operations.keys(), operations.values()))
            for t in sort_oper:
                self.log.info(t[1])
                commands_list = t[1].split()
                subprocess.call(commands_list)

    def lets_do_sth(self, sort_open):
        '''It takes dict as input and proceeds via subprocess.call()'''
        DiskOper().mkdir_if('/srv/jaws', 'root')
        for t in sort_open:
            self.log.info(t[1])
            commands_list = t[1].split()
            subprocess.call(commands_list)

    def is_mount(self):
        if op.ismount('/srv/java'):
            self.log.info('old path to java!! /srv/java mounted')
            return True
        else:
            return False


class Fstab(object):

    def __init__(self):
        self.log = MyLogger().get_logger()

    def modify_fstab(self):
        patt_java = re.compile(
            '(^10.10.11.251:/NFS_JBOSS.*/srv/java.*$)', flags=re.MULTILINE)
        patt_jaws = re.compile(
            '(^10.10.11.251:/NFS_JBOSS.*/srv/jaws.*$)', flags=re.MULTILINE)
        with open('/etc/fstab', 'r+') as f:
            data = f.read()
            if re.search(patt_java, data) and not re.search(patt_jaws, data):
                fix_data = re.sub(patt_java, '#' + r'\1', data)
                f.seek(0)
                f.truncate()
                f.write(fix_data)
                f.write('10.10.11.251:/NFS_JBOSS  /srv/jaws  nfs  ro  0 0\n')
                self.log.info('old java in /etc/fstab -> update fstab!')
            if not re.search(patt_java, data) and not re.search(
                    patt_jaws, data):
                f.seek(0, 2)
                f.write('10.10.11.251:/NFS_JBOSS  /srv/jaws  nfs  ro  0 0\n')
                self.log.info('lack of java in /etc/fstab -> update fstab!')

    def check_fstab_java(self):
        patt_javs = re.compile(
            '(^10.10.11.251:/NFS_JBOSS.*$)', flags=re.MULTILINE)
        with open('/etc/fstab', 'r') as f:
            data = f.read()
            se = re.search(patt_javs, data)
            if se:
                se.groups()
