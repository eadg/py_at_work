import subprocess
import env
import jenkins


class Run(object):

    def run(self, scr):
        subprocess.call([scr])


if __name__ == '__main__':
    envr = env.Env()
    envr.create_java_symlink()
    fetc = jenkins.Fetch()
    url = env.SubUrl.url[envr.ne.shortname]
    script = fetc.get_svn_root_script(url)
    bul = Run()
    bul.run(script)
