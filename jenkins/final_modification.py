import os.path
import glob
import subprocess
import shutil
import re
import env
import os
from disk_operations import DiskOper
import separate_met as seme


class JBossAsService(object):
    """make jboss as service"""

    def __init__(self):
        self.script, self.scriptpath = self.get_init_script()

    def get_init_script(self):
        path = glob.glob('/etc/init.d/jb*')
        if len(path) > 0:
            initpath = path.pop()
        else:
            initpath = glob.glob('/srv/jboss/scrip*/jb*').pop()
        initscript = initpath.split('/')[-1]
        return initscript, initpath

    def call_chkconfig(self):
        if self.script:
            subprocess.call(['chkconfig', '--add', self.script])

    def call_restart(self):
        subprocess.call(['systemctl', 'restart', self.script])

    def create_systemd_jboss(self):
        """put universal jboss.service file"""
        pass

    def is_run(self):
        pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
        for pid in pids:
            try:
                if 'jboss/jboss' in open(os.path.join(
                        '/proc/', pid, 'cmdline'), 'rb').read():
                    return True
            except (OSError, IOError):
                continue
        return False

    def stop_it(self):
        check = self.is_run()
        if check:
            nul = open(os.devnull, 'w')
            subprocess.call([self.scriptpath, 'stop'], stdout=nul)
            # check = self.is_run()


class ExternalJar(object):
    """copy additional jars to jboss_home"""

    def __init__(self):
        self.source_zorka_path = '/srv/jboss/zorka'
        self.destination_zorka_path = '/srv/jboss/jboss/zorka'

    def cp_zorka(self):
        if not os.path.isdir(self.destination_zorka_path):
            shutil.copytree(
                self.source_zorka_path, self.destination_zorka_path,
                symlinks=True)


class CallMrProper(object):
    """call proper methods depends on version jboss"""

    def __init__(self, version):
        self.en = env.Env()
        self.na = env.Name()
        if int(version) > 4:
            self.methods = self.be_wild()
        else:
            self.methods = self.be_old()

    def be_wild(self, restart=False):
        """for jboss >= 7"""
        if not restart:
            methods = [
                'fix_jboss_symlink', 'fix_7_standalone_xml',
                'fix_7_standalone_conf']
            takto_methods = [
                'fix_jboss_symlink', 'fix_7_standalone_xml_takto',
                'fix_7_standalone_conf']
            excl_takto_host = [
                'pdc-t-jboss-takto-dev', 'pdc-t-jboss-takto-esb-dev',
                'pdc-t-jboss-takto-test', 'pdc-t-jboss-takto-beta',
                'pdc-t-jboss-takto-pre-prod']
            takto_flag = False
            for takto_exl in excl_takto_host:
                if self.na.name == takto_exl:
                    takto_flag = True
            return takto_methods if takto_flag else methods
        else:
            methods = {
                'fix_conf_utf': 7,
                'fix_java_mount': None  # let's stay by manual exucute
            }
            return methods

    def be_old(self, restart=False):
        """for jboss < 7"""
        if not restart:
            methods = [
                'fix_jboss_symlink', 'fix_4_conf', 'fix_4_log4j',
                'fix_4_smykproperties', 'fix_4_zorkaproperties']
        else:
            methods = {
                'fix_conf_utf': 4,
                'fix_java_mount': None  # let's stay by manual exucute
            }
        return methods

    def run(self):
        """move execute from __main__ to here"""
        pass

    def modify_make_for_developers(self):
        patt_start = re.compile('(^start$)', flags=re.MULTILINE)
        path_to_self = 'python /srv/scripts/py/modify_make_jboss.py'
        fpath1 = self.en.get_make_name()
        fpath2 = self.en.get_common_make_name()
        fsize = os.path.getsize(fpath1)
        if fsize > 3000:
            with open(fpath1, 'r+') as f:
                data = f.read()
                fix_data = re.sub(patt_start, path_to_self + r'\n\1', data)
                f.seek(0)
                f.truncate()
                f.write(fix_data)
        else:
            with open(fpath2, 'r+') as f:
                data = f.read()
                fix_data = re.sub(patt_start, path_to_self + r'\n\1', data)
                f.seek(0)
                f.truncate()
                f.write(fix_data)

    def set_permission(self, jpath):
        DiskOper().set_group_owner_recursive(path=jpath, owner='dit')
        DiskOper().set_group_write_recursive(path=jpath)

    def run_separte_meth(self):
        ad_me = env.SubUrl.add_methods
        try:
            lism = ad_me[self.na.shortname]
        except KeyError:
            pass
        else:
            for me in lism:
                getattr(seme.J7, me)()

    def get_methods_along_restart(self):
        version = self.en.get_jboss_ver()
        if int(version) > 4:
            self.methods = self.be_wild(restart=True)
        else:
            self.methods = self.be_old(restart=True)


if __name__ == '__main__':
    na = env.Name()
    if 'prod' not in na.stage:
        envr = env.Env()
        jas = JBossAsService()
        jars = ExternalJar()
        jas.stop_it()
        cmrp = CallMrProper(envr.get_jboss_ver())
        for met in cmrp.methods:
            getattr(envr, met)()
        jas.call_chkconfig()
        envr.fix_jboss_symlink()
        jars.cp_zorka()
        jas.call_restart()
        cmrp.modify_make_for_developers()
        cmrp.set_permission('/srv/jboss')
        cmrp.run_separte_meth()
