
if __name__ == '__main__':
    contents = '''
     SHELL=/bin/sh
    PATH=/sbin:/bin:/usr/sbin:/usr/bin
    MAILTO=root
    NICE=15

    # run-parts
    01 * * * *   root    python /srv/scripts/py/clean_tmp.py
    # */1 * * * *   root    python /srv/scripts/py/start_client_nfs.py
    '''
    with open('/etc/cron.d/py_script', 'w') as f:
        f.write(contents)
