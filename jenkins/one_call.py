from logs import MyLogger

log = MyLogger().get_logger()


def zero():
    import subprocess
    subprocess.call('uptime')


def one():
    import java_mount as j_m
    jm = j_m.JavaMount()
    if jm.is_mount():
        oper = jm.update_java()
        jm.lets_do_sth(oper)


def two():
    import java_mount as j_m
    j_m.Fstab().modify_fstab()


def three():
    import os.path as op
    if not op.ismount('/srv/java'):
        log.info('not mount')
    else:
        log.info('Mounted:(')


def four():
    import java_mount as j_m
    j_m.Fstab().check_fstab_java()


def five():
    import final_modification as fm
    import env
    envr = env.Env()
    mrp = fm.CallMrProper(envr.get_jboss_ver())
    mrp.modify_make_for_developers(comment=True, st='pre_prod')


if __name__ == '__main__':
    # zero()
    # two()
    # three()
    five()
