import unittest
import env


class TestUrl(unittest.TestCase):

    def test_keys(self):
        u = env.SubUrl()
        for k, v in u.url.items():
            self.assertIsInstance(k, str)

    def test_size(self):
        u = env.SubUrl()
        self.assertGreater(len(u.url), 10)

    def test_shortname(self):
        u = env.Env()
        print('shortname: ', u.ne.shortname)
        self.assertIsInstance(u.ne.shortname, str)

    def test_name(self):
        u = env.Env()
        print('name: ', u.ne.name)
        self.assertIsInstance(u.ne.name, str)

'''
    def test_etc_hosts(self):
        h = pjd.Hostname().get_hostname()
        self.assertIsInstance(pjd.Hostname().get_etc_hosts(h), str)

    def test_file_exists(self):
        p = pjd.Package()
        jbhome = p.path
        self.assertTrue(os.path.isfile(jbhome + '/profile.conf'))

    def test_fix_standalone_conf(self):
        p = pjd.Package()
        p.rdir()
        p.mdir()
        p.cpdir()
        r = p.fix_standalone_conf()
        self.assertTrue(r >= 1)

    def test_is_tgz(self):
        p = pjd.Package()
        tarname = p.makeTar()
        self.assertTrue(tarfile.is_tarfile(tarname))
'''

if __name__ == '__main__':
    unittest.main()
