import env
import final_modification as fm


if __name__ == '__main__':
    envr = env.Env()
    path_to_make = envr.get_make_name()
    cmrp = fm.CallMrProper(envr.get_jboss_ver())
    cmrp.modify_script_srv_jboss_make_for_developers(path_to_make)
