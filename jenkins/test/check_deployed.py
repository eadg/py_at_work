import subprocess

dev7_list = ['pdc-t-jboss-core-dev', 'pdc-t-jboss-cron-dev', 'pdc-t-jboss-cti-dev', 'pdc-t-jboss-da-dev', 'pdc-t-jboss-dok-dev', 'pdc-t-jboss-dwp-dev', 'pdc-t-jboss-eod-dev', 'pdc-t-jboss-legal-dev', 'pdc-t-jboss-proc-dev', 'pdc-t-jboss-recognizer-dev', 'pdc-t-jboss-service-dev', 'pdc-t-jboss-takto-dev', 'pdc-t-jboss-teren-dev', ]
dev4_list = ['pdc-t-jboss-cti4-dev', 'pdc-t-jboss-jbpm-dev', ]

for hst in dev7_list:
    print(hst)
    subprocess.call(['ssh-keygen', '-f', '/home/jrokicki/.ssh/known_hosts', '-R', hst])
    subprocess.call(['ssh', hst, 'ls -l /srv/jboss/jboss/standalone/deployments/*ar*'])
    # subprocess.call(['ssh', '-o', '"BatchMode yes"', hst, 'ls -l /srv/jboss/jboss/standalone/deployments/*ar*'])
    # subprocess.call(['ssh', hst, 'ls -l /srv/jboss/jboss/standalone/deployments/*ar*'], timeout=5)
