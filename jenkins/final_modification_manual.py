import env
import final_modification as fm

if __name__ == '__main__':
    envr = env.Env()
    jas = fm.JBossAsService()
    jars = fm.ExternalJar()
    jas.stop_it()
    cmrp = fm.CallMrProper(envr.get_jboss_ver())
    for met in cmrp.methods:
        getattr(envr, met)()
    jas.call_chkconfig()
    envr.fix_jboss_symlink()
    jars.cp_zorka()
    jas.call_restart()
    cmrp.modify_make_for_developers()
    cmrp.set_permission('/srv/jboss/')
