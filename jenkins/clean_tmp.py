from disk_operations import DiskOper

if __name__ == '__main__':
    DiskOper().clean_dir(path='/tmp/*', value=1, interval='d', typeof='f')
