import env
import final_modification as fm


def do_modify():
    envr = env.Env()
    cmrp = fm.CallMrProper(envr.get_jboss_ver())
    cmrp.get_methods_along_restart()
    for met, arg in cmrp.methods.items():
        getattr(envr, met)(arg)
    cmrp.set_permission('/srv/jboss')
    cmrp.set_permission('/srv/scripts/py')


if __name__ == '__main__':
    do_modify()
