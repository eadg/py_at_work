import socket
import glob
import sys
import os
import re
import os.path
from collections import defaultdict
import subprocess


class SubUrl(object):

    def __init__(self):
        self.jdk = self.get_jdk()

    def get_jdk(self):
        def get_jdk_string():
            return '/srv/jaws/jdk1.8.0_172'
        dd = defaultdict(get_jdk_string)
        dd['jboss-big'] = '/srv/jaws/jdk1.8.0_131'
        dd['jboss-bik'] = '/srv/jaws/jdk1.8.0_131'
        dd['jboss-core1'] = '/srv/jaws/jdk1.8.0_144'
        dd['jboss-core2'] = '/srv/jaws/jdk1.8.0_144'
        return dd

    sb_ad = 'https://subversion.ultimo.pl/projects/BuildTools/scripts/'
    url = {
           'jboss-core-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-core/jboss-core-dev/download_svn_jboss-core-dev-remote.sh',
           'jboss-cron-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-cron/jboss-cron-dev/download_svn_jboss-cron-dev-remote.sh',
           'jboss-cti-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-cti/jboss-cti-dev/download_svn_jboss-cti-dev-remote.sh',
           'jboss-cti4-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/download_svn_jboss-cti4-dev-remote.sh',
           'jboss-da-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-da/jboss-da-dev/download_svn_jboss-da-dev-remote.sh',
           'jboss-dok-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-dok/jboss-dok-dev/download_svn_jboss-dok-dev-remote.sh',
           'jboss-dwp-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-dwp/jboss-dwp-dev/download_svn_jboss-dwp-dev-remote.sh',
           'jboss-eod-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-eod/jboss-eod-dev/download_svn_jboss-eod-dev-remote.sh',
           'jboss-jbpm-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/download_svn_jboss-jbpm-dev-remote.sh',
           'jboss-legal-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-legal/jboss-legal-dev/download_svn_jboss-legal-dev-remote.sh',
           'jboss-proc-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/jboss-proc-dev/download_svn_jboss-proc-dev-remote.sh',
           'jboss-recognizer-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-recognizer/jboss-recognizer-dev/download_svn_jboss-recognizer-dev-remote.sh',
           'jboss-service-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-service/jboss-service-dev/download_svn_jboss-service-dev-remote.sh',
           'jboss-takto-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/takto/jboss-takto/jboss-takto-dev/download_svn_jboss-takto-dev-remote.sh',
           'jboss-teren-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/download_svn_jboss-teren-dev-remote.sh',
           'takto-esb-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/takto/jboss-takto-esb/takto-esb-dev/download_svn_takto-esb-dev-remote.sh',
           'jboss-outgoing-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-outgoing/jboss-outgoing/download_svn_jboss-outgoing-remote.sh',
           'jboss-outgoing2-dev': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-outgoing/jboss-outgoing/download_svn_jboss-outgoing-remote.sh',

           'jboss-core-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-core/jboss-core-beta/download_svn_jboss-core-beta-remote.sh',
           'jboss-cron-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-cron/jboss-cron-beta/download_svn_jboss-cron-beta-remote.sh',
           'jboss-da-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-da/jboss-da-beta/download_svn_jboss-da-beta-remote.sh',
           'jboss-dwp-beta': 'https://subversion.ultimpdc-t-jboss-core-beta.ultimo.plo.pl/projects/BuildTools/scripts/pl/jboss-dwp/jboss-dwp-beta/download_svn_jboss-dwp-beta-remote.sh',
           'jboss-jbpm4-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/download_svn_jboss-jbpm-beta-remote.sh',
           'jboss-legal-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-legal/jboss-legal-beta/download_svn_jboss-legal-beta-remote.sh',
           'jboss-proc-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/jboss-proc-beta/download_svn_jboss-proc-beta-remote.sh',
           'jboss-recognizer-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-recognizer/jboss-recognizer-beta/download_svn_jboss-recognizer-beta-remote.sh',
           'jboss-service-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-service/jboss-service-beta/download_svn_jboss-service-beta-remote.sh',
           'jboss-takto-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/takto/jboss-takto/jboss-takto-beta/download_svn_jboss-takto-beta-remote.sh',
           'jboss-takto-beta2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/takto/jboss-takto/jboss-takto-beta2/download_svn_jboss-takto-beta2-remote.sh',
           'jboss-outgoing-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-outgoing/jboss-outgoing/download_svn_jboss-outgoing-remote.sh',
           'jboss-dok-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-dok/jboss-dok-beta/download_svn_jboss-dok-beta-remote.sh',
           'jboss-eod-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-eod/jboss-eod-test/download_svn_jboss-eod-test-remote.sh',
           'jboss-cti4-beta': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/download_svn_jboss-cti4-test-remote.sh',

           'jboss-cm-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/cm-kanaly/jboss-cm-test/download_svn_jboss-cm-test-remote.sh',
           'jboss-core-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-core/jboss-core-test/download_svn_jboss-core-test-remote.sh',
           'jboss-cron-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-cron/jboss-cron-test/download_svn_jboss-cron-test-remote.sh',
           'jboss-cti-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-cti/jboss-cti-test/download_svn_jboss-cti-test-remote.sh',
           'jboss-cti4-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/download_svn_jboss-cti4-test-remote.sh',
           'jboss-da-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-da/jboss-da-test/download_svn_jboss-da-test-remote.sh',
           'jboss-dok-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-dok/jboss-dok-test/download_svn_jboss-dok-test-remote.sh',
           'jboss-dwp-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/download_svn_jboss-dwp-test-remote.sh',
           'jboss-eod-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-eod/jboss-eod-test/download_svn_jboss-eod-test-remote.sh',
           'jboss-gw-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-gw/jboss-gw-test/download_svn_jboss-gw-test-remote.sh',
           'jboss-jbpm4-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/download_svn_jboss-jbpm-test-remote.sh',
           'jboss-legal-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-legal/jboss-legal-test/download_svn_jboss-legal-test-remote.sh',
           'jboss-proc-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/download_svn_jboss-proc-test-remote.sh',
           'jboss-recognizer-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-recognizer/jboss-recognizer-test/download_svn_jboss-recognizer-test-remote.sh',
           'jboss-service-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-service/jboss-service-test/download_svn_jboss-service-test-remote.sh',
           'jboss-takto-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/takto/jboss-takto/jboss-takto-test/download_svn_jboss-takto-test-remote.sh',
           'jboss-teren-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/download_svn_jboss-teren-test-remote.sh',
           'jboss-dnb-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-dnb/download_svn_jboss-dnb-test-remote.sh',
           'jboss-outgoing-test': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-outgoing/jboss-outgoing/download_svn_jboss-outgoing-remote.sh',

           'jboss-core-pre-prod': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-core/jboss-core-pre-prod/download_svn_jboss-core-pre-prod-remote.sh',
           'jboss-service-pre-prod': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-service/jboss-service-pre-prod/download_svn_jboss-service-pre-prod-remote.sh',
           'jboss-takto-pre-prod': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/takto/jboss-takto/jboss-takto-pre-prod/download_svn_jboss-takto-pre-prod-remote.sh',
           'jboss-takto-pre-prod2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/takto/jboss-takto/jboss-takto-pre-prod2/download_svn_jboss-takto-pre-prod2-remote.sh',
           'jboss-dok-pre-prod': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-dok/jboss-dok-pre-prod/download_svn_jboss-dok-pre-prod-remote.sh',
           'jboss-dwp-pre-prod': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-dwp/jboss-dwp-pre-prod/download_svn_jboss-dwp-pre-prod-remote.sh',
           'jboss-eod-pre-prod': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-eod/jboss-eod-pre-prod/download_svn_jboss-eod-pre-prod-remote.sh',
           'jboss-jbpm-pre-prod': 'https://subversion.ultimo.pl/projects//BuildTools/scripts/pl/jboss-jbpm/jboss-jbpm-pre-prod/download_svn_jboss-jbpm4-pre-prod-remote.sh',

           'jboss-teren1': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-teren/jboss-teren1/download_svn_jboss-teren1-prod-remote.sh',
           'jboss-teren2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-teren/jboss-teren2/download_svn_jboss-teren2-prod-remote.sh',
           'jboss-eod1': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-eod/jboss-eod1/download_svn_jboss-eod1-remote.sh',
           'jboss-eod2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-eod/jboss-eod2/download_svn_jboss-eod2-remote.sh',
           'jboss-legal1': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-legal/jboss-legal1/download_svn_jboss-legal1-remote.sh',
           'jboss-legal2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-legal/jboss-legal2/download_svn_jboss-legal2-remote.sh',
           'jboss-recognizer1': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-recognizer/jboss-recognizer1/download_svn_jboss-recognizer1-remote.sh',
           'jboss-recognizer2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-recognizer/jboss-recognizer2/download_svn_jboss-recognizer2-remote.sh',
           'jboss-da1': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-da/jboss-da1/download_svn_jboss-da1-remote.shaha',
           'jboss-da2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-da/jboss-da2/download_svn_jboss-da2-remote.shaha',
           'jboss-service1': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-service/jboss-service1/download_svn_jboss-service1-remote.sh',
           'jboss-service2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-service/jboss-service2/download_svn_jboss-service2-remote.sh',
           'jboss-cron': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-cron/jboss-cron/download_svn_jboss-cron-remote.sh',
           'jboss-dwp1': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/jboss-dwp0/download_svn_jboss-dwp1-remote.sh',
           'jboss-dwp2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/jboss-dwp1/download_svn_jboss-dwp2-remote.sh',
           'jboss-proc1': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-proc/jboss-proc1/download_svn_jboss-proc1-remote.sh',
           'jboss-proc2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-proc/jboss-proc2/download_svn_jboss-proc2-remote.sh',
           'jboss-dnb': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-dnb/download_svn_jboss-dnb-remote.sh',
           'jboss-jbpm2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-jbpm/jboss-jbpm/download_svn_jboss-jbpm-remote.sh',
           'jboss-proc-fix': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-proc-fix/download_svn_jboss-proc-fix-remote.sh',
           'jboss-ecourt': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/jboss-ecourt/srv/download_svn_jboss-ecourt-remote.sh',
           'jboss-outgoing': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-outgoing/jboss-outgoing/download_svn_jboss-outgoing-remote.sh',
           'jboss-core1': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-core/jboss-core1/download_svn_jboss-core1-remote.sh',
           'jboss-core2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-core/jboss-core2/download_svn_jboss-core2-remote.sh',
           'jboss-cm': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/cm-kanaly/jboss-cm/download_svn_jboss-cm-remote.sh',
           'eultimo-synch': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-gw/eultimo-synch/download_svn_jboss-gw-remote.sh',
           'jboss-takto': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/takto/jboss-takto/jboss-takto/download_svn_jboss-takto-remote.sh',
           'takto-esb': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/takto/jboss-takto-esb/takto-esb/download_svn_takto-esb-remote.sh',
           'jboss-outgoing2': 'https://subversion.ultimo.pl/projects/BuildTools/scripts/pl/jboss-outgoing/jboss-outgoing/download_svn_jboss-outgoing-remote.sh',
    }

    add_methods = {'jboss-cm-test': ['cm_ds']}


class Name(object):
    """
    name = pdc-t-jboss-recognizer-dev.ultimo.pl
    shortname = jboss-recognizer-dev
    sname = jboss-recognizer
    stage = dev
    funcname = recognizer-dev-pdc
    """

    def __init__(self):
        (self.name, self.shortname, self.stage,
         self.sname, self.funcname) = self.get_hostname()

    def get_hostname(self):
        h0 = socket.gethostname()
        h1 = self.remove_suffix(h0)
        h2 = self.remove_prefix(h1)
        h5 = self.get_stage(h2)
        h3 = self.remove_stage(h2)
        h4 = self.get_jboss_function_name(h3, h5)
        try:
            self.suburl = SubUrl()
            self.suburl.url[h2]
            return h1, h2, h5, h3, h4
        except KeyError:
            old_sname = h3
            h1 = 'pdc-t-' + old_sname + '-dev'
            h2 = self.remove_prefix(h1)
            h5 = self.get_stage(h2)
            h3 = self.remove_stage(h2)
            h4 = self.get_jboss_function_name(h3, h5)
            return h1, h2, h5, h3, h4

    def remove_suffix(self, hostname):
        if 'ultimo.pl' in hostname:
            h = hostname.split('.', 1)
            return h.pop(0)
        else:
            return hostname

    def remove_prefix(self, hostname):
        """return without prefix "pdc-p or t"""
        if 'pdc' in hostname:
            pdc_host = hostname.split('-')
            no_pdc_host = '-'.join(pdc_host[2:])
            return no_pdc_host
        else:
            return hostname

    def get_stage(self, hostname):
        if 'dev' in hostname:
            return 'dev'
        elif 'test' in hostname:
            return 'test'
        elif 'beta' in hostname:
            return 'beta'
        elif 'pre' in hostname:
            return 'pre-prod'
        else:
            return 'prod'

    def remove_stage(self, hostname):
        size = hostname.split('-')
        if size > 3:
            without_stage = hostname.split('-')[:2]
            return '-'.join(without_stage)
        else:
            without_stage = hostname.split('-')[:-1]
            return '-'.join(without_stage)

    def get_jboss_function_name(self, hostname, stage):
        function_name = hostname.split('-').pop()
        func_name = function_name + '-' + stage + '-' + 'pdc'
        return func_name[:23]


class Env(object):

    def __init__(self):
        self.ne = Name()
        self.dic = SubUrl()

    def create_java_symlink(self):
        java_link = '/srv/java/jdk'
        java_path = self.dic.jdk[self.ne.shortname]
        if not os.path.islink(java_link):
            subprocess.call(['ln', '-s', java_path, java_link])

    def get_jenkins_url(self):
        hostname = self.ne.shortname
        if 'jboss-jbpm-dev' in hostname:
            hostname = 'jboss-jbpm4-dev'
        url = 'https://jenkins.ultimo.pl/job/' + hostname + \
            '-deploy/lastBuild/injectedEnvVars/api/json?pretty=true'
        return url

    def get_make_name(self):
        make_list = glob.glob('/srv/jboss/make*.sh')
        if len(make_list) > 0:
            make_path = make_list.pop()
            return make_path

    def get_common_make_name(self, stage=None):
        '''if self.ne.stage is not correct'''
        if not stage:
            make_list = glob.glob(
                '/srv/jboss/scripts/commo*make*' + self.ne.stage + '*.sh')
        else:
            make_list = glob.glob(
                '/srv/jboss/scripts/commo*make*' + stage + '*.sh')
        if len(make_list) > 0:
            make_path = make_list.pop()
            return make_path if make_path else self.get_make_name()

    def fix_7_standalone_xml(self):
        fpath = '/srv/jboss/jboss/standalone' + '/configuration/standalone.xml'
        patt_elk = re.compile('value="hostname=(.+?)-(.+?)-(.+?).+?/>')
        patt_inet_1 = re.compile(r'(<inet-address value="\$\{.+?:).+')
        patt_inet_2 = re.compile(r'<inet-address value="\w+.+?/>')
        patt_node_ident = re.compile(
            '(.*<core-environment node-identifier=").*', flags=re.MULTILINE)
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(
                patt_elk, r'value="hostname=' + self.ne.shortname + r'"/>',
                data)
            fix_data = re.sub(
                patt_inet_1, r'\1' + self.ne.name + r'}"/>', fix_data)
            fix_data = re.sub(
                patt_inet_2, r'<inet-address value="' + self.ne.name + r'"/>',
                fix_data)
            fix_data = re.sub(
                patt_node_ident, r'\1' + self.ne.funcname + r'">', fix_data)
            f.seek(0)
            f.truncate()
            f.write(fix_data)

    def fix_7_standalone_xml_takto(self):
        fpath = '/srv/jboss/jboss/standalone' + '/configuration/standalone.xml'
        patt_elk = re.compile('value="hostname=(.+?)-(.+?)-(.+?).+?/>')
        patt_inet_1 = re.compile(r'(<inet-address value="\$\{.+?:).+')
        patt_inet_2 = re.compile(r'<inet-address value="\w+.+?/>')
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(
                patt_elk, r'value="hostname=' + self.ne.shortname + r'"/>',
                data)
            fix_data = re.sub(
                patt_inet_1, r'\1' + self.ne.name + r'}"/>', fix_data)
            fix_data = re.sub(
                patt_inet_2, r'<inet-address value="' + self.ne.name + r'"/>',
                fix_data)
            f.seek(0)
            f.truncate()
            f.write(fix_data)

    def fix_7_standalone_conf(self):
        fpath = '/srv/jboss/jboss' + '/bin/standalone.conf'
        # patt_j = re.compile('(.*JAVA_HOME=.*)', flags=re.MULTILINE)
        patt_empty_equal = re.compile('(^=.*)', flags=re.MULTILINE)
        patt_byteman = re.compile('(^BYTEMAN_)', flags=re.MULTILINE)
        patt_byteman_pkg = re.compile(
            '(.*)(JBOSS_MODULES_SYSTEM_PKGS="org.jboss.byteman")(.*)',
            flags=re.MULTILINE)
        '''
        try:
            jpath = self.dic.jdk[self.ne.shortname]
            jdk = r'JAVA_HOME='+jpath
        except KeyError:
            jdk = r'JAVA_HOME=/srv/java/jdk'
        '''
        with open(fpath, 'r+') as f:
            fix_data = f.read()
            # fix_data = re.sub(patt_j, jdk, data)
            z_str1 = 'JBOSS_MODULES_SYSTEM_PKGS='
            z_str2 = '"org.jboss.byteman,com.jitlogic.zorka.core.spy"'
            fix_data = re.sub(patt_empty_equal, r'#', fix_data)
            fix_data = re.sub(patt_byteman, r'#\1', fix_data)
            fix_data = re.sub(
                patt_byteman_pkg, r'\1' + z_str1 + z_str2 + r'\3', fix_data)
            f.seek(0)
            f.truncate()
            f.write(fix_data)
        with open(fpath, 'a+') as f:
            data = f.read()
            res = re.search('javaagent:/srv/jboss/jboss/zorka', data)
            if not res:
                f.write(
                    'JAVA_OPTS="$JAVA_OPTS -javaagent:/srv/jboss/jboss/zorka/zorka.jar=/srv/jboss/jboss/zorka"' + '\n')
                f.write('JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=utf-8"' + '\n')

    def fix_conf_utf(self, version):
        if version < 7:
            fconf = '/srv/jboss/jboss/bin/run.conf'
            fsh = '/srv/jboss/jboss/bin/run.sh'
        else:
            fconf = '/srv/jboss/jboss/bin/standalone.conf'
            fsh = '/srv/jboss/jboss/bin/standalone.sh'
        with open(fconf, 'a+') as f:
            data = f.read()
            res = re.search('-Dfile.encoding=utf-8', data)
            if not res:
                f.write('JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=utf-8"' + '\n')
        with open(fsh, 'r+') as f:
            data = f.read()
            patt_shell = re.compile('(^#!/bin/sh)', flags=re.MULTILINE)
            lang = 'LANG=pl_PL.utf-8; export LANG;'
            lang_check = re.compile(
                '^#!/bin/sh\nLANG=pl_PL.utf-8; export LANG;',
                flags=re.MULTILINE)
            res = re.search(lang_check, data)
            if not res:
                fix_data = re.sub(patt_shell, r'\1' + '\n' + lang, data)
                if len(fix_data) > 30:
                    f.seek(0)
                    f.truncate()
                    f.write(fix_data)

    def fix_java_mount(self, fake_arg=None):
        nul = open(os.devnull, 'w')
        subprocess.call(
            ['salt-call', 'state.apply', 'javamount'], stdout=nul, stderr=nul)

    def fix_4_conf(self):
        fpath = '/srv/jboss/jboss' + '/bin/run.conf'
        patt_j = re.compile('(.*JAVA_HOME=.*)', flags=re.MULTILINE)
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(patt_j, r'JAVA_HOME=/srv/java/jdk', data)
            f.seek(0)
            f.truncate()
            f.write(fix_data)
        with open(fpath, 'a+') as f:
            data = f.read()
            res = re.search('javaagent:/srv/jboss/jboss/zorka', data)
            if not res:
                f.write(
                    'JAVA_OPTS="$JAVA_OPTS -javaagent:/srv/jboss/jboss/zorka/zorka.jar=/srv/jboss/jboss/zorka"' + '\n')
                f.write('JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=utf-8"' + '\n')

    def fix_4_log4j(self):
        fpath = '/srv/jboss/jboss/server/default/conf/log4j.xml'
        patt_elk = re.compile('value="hostname=(.+?)-(.+?)-(.+?).+?/>')
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(
                patt_elk, r'value="hostname=' + self.ne.shortname + r'"/>',
                data)
            f.seek(0)
            f.truncate()
            f.write(fix_data)

    def fix_4_smykproperties(self):
        fpath = '/srv/jboss/jboss/server/default/conf/smyk.properties'
        patt_jnp = re.compile('=jnp://(.+?)-(.+?)-(.+?)(:1099.+?)')
        patt_main = re.compile('=(.+?)-(.+?)-(.+?)(:1099.+?)')
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(
                patt_jnp, r'=jnp://' + self.ne.name + r'\4', data)
            fix_data = re.sub(patt_main, r'=' + self.ne.name + r'\4', fix_data)
            f.seek(0)
            f.truncate()
            f.write(fix_data)

    def fix_4_zorkaproperties(self):
        fpath = '/srv/jboss/zorka/zorka.properties'
        patt_zorka = re.compile('(.*)jboss/jboss7.bsh(.*)')
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(
                patt_zorka, r'\1' + 'jboss/jboss5.bsh' + r'\2', data)
            f.seek(0)
            f.truncate()
            f.write(fix_data)

    def fix_jboss_symlink(self):
        jboss_link = '/srv/jboss/jboss'
        if os.path.islink(jboss_link):
            if not os.path.isdir(jboss_link):
                try:
                    os.remove(jboss_link)
                except OSError:
                    print('fix_jboss_symlink => os.remove failed')
                    self.fix_jboss_simlink_guts()
        else:
            self.fix_jboss_simlink_guts()

    def fix_jboss_simlink_guts(self):
        jboss_link = '/srv/jboss/jboss'
        g0 = glob.glob('/srv/jboss/dist*/*')[-1]
        g1 = glob.glob(g0 + '/*')[-1]
        g2 = glob.glob(g1 + '/*')[-1]
        if g2:
            os.symlink(g2, jboss_link)
        else:
            os.symlink(g1, jboss_link)

    def get_jboss_ver(self):
        '''Return jboss ver as str, e.g: '8' '''
        profile = '/srv/jboss/jboss/profile.conf'
        self.fix_jboss_symlink()
        try:
            with open(profile) as f:
                prof = f.readlines()
                version_long = prof[1].split('=').pop(1)
                version_short = version_long.split('.').pop(0)
        except (IOError, IndexError):
            sys.exit("get_jboss_ver")
        return version_short
