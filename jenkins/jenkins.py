import sys
import os
import urllib2
import ssl
import json
import ass


class Fetch(object):

    def __init__(self):
        self.path = '/tmp/'

    def initial_connection(self, url):
        """get common part of connection from rest of methods- toDo"""
        fakecontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        request = urllib2.Request(url)
        base64str = ass.read_ssap(cred='cred0')
        request.add_header("Authorization", "Basic %s" % base64str)
        try:
            result = urllib2.urlopen(request, context=fakecontext)
        except urllib2.URLError as er:
            print("Error, exit(0)" + er)
            sys.exit(0)
        return result

    def read_version_jenkins(self, url):
        fakecontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        request = urllib2.Request(url)
        base64str = ass.read_ssap(cred='cred0')
        request.add_header("Authorization", "Basic %s" % base64str)
        try:
            result = urllib2.urlopen(request, context=fakecontext)
        except urllib2.URLError as er:
            print("Error, exit(0)" + er)
            sys.exit(0)
        else:
            js = json.load(result)
            vers = []
        list_attributes = ["CURRENT_VERSION", "shortVersionType",
                           "tagProjectVersion", "releaseBuildNumber"]
        for attr in list_attributes:
            try:
                val = js["envMap"][attr]
            except KeyError:
                pass
            else:
                vers.append(val)
        return vers

    def get_svn_root_script(self, url):
        fakecontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        request = urllib2.Request(url)
        base64str = ass.read_ssap(cred='cred0')
        request.add_header("Authorization", "Basic %s" % base64str)
        try:
            result = urllib2.urlopen(request, context=fakecontext)
        except urllib2.URLError as er:
            print("URL error, exit(0)" + er)
            sys.exit(0)
        fname = url.split('/')[-1]
        path_script = self.path + fname
        try:
            f = open(path_script, 'wb')
        except IOError as ioe:
            print("IOError, exit(0)" + ioe)
            sys.exit(0)
        else:
            f.write(result.read())
        finally:
            f.close()
        os.chmod(path_script, 0o744)
        return path_script
