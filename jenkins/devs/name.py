import socket


class Name(object):
    """
    name = pdc-t-jboss-recognizer-dev.ultimo.pl
    shortname = jboss-recognizer-dev
    sname = jboss-recognizer
    stage = dev
    funcname = recognizer-dev-pdc
    """

    def __init__(self):
        self.name, self.shortname, self.sname, self.funcname = self.get_hostname()

    def get_hostname(self):
        h0 = socket.gethostname()
        h1 = self.remove_suffix(h0)
        h2 = self.remove_prefix(h1)
        h3 = self.remove_stage(h2)
        h4 = self.get_jboss_function_name(h3)
        return h1, h2, h3, h4

    def remove_suffix(self, hostname):
        if 'ultimo.pl' in hostname:
            h = hostname.split('.', 1)
            return h.pop(0)
        else:
            return hostname

    def remove_prefix(self, hostname):
        """return without prefix "pdc-p or t"""
        if 'pdc' in hostname:
            pdc_host = hostname.split('-')
            no_pdc_host = '-'.join(pdc_host[2:])
            self.get_stage(no_pdc_host)
            return no_pdc_host
        else:
            self.get_stage(hostname)
            return hostname

    def get_stage(self, hostname):
        hs = hostname.split('-')
        for h in hs:
            if 'dev' in h:
                self.stage = 'dev'
            elif 'test' in h:
                self.stage = 'test'
            elif 'beta' in h:
                self.stage = 'beta'
            elif 'pre' in h:
                self.stage = 'pre-prod'
            else:
                self.stage = 'prod'

    def remove_stage(self, hostname):
        size = hostname.split('-')
        if size > 3:
            without_stage = hostname.split('-')[:2]
            return '-'.join(without_stage)
        else:
            without_stage = hostname.split('-')[:-1]
            return '-'.join(without_stage)

    def get_jboss_function_name(self, hostname):
        function_name = hostname.split('-').pop()
        func_name = function_name + '-' + self.stage + '-' + 'pdc'
        return func_name[:23]