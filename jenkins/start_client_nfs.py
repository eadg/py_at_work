import env
import pdc_auto_mount_nfs as pamn


class NFS(object):

    def mount_nfs(self, host):
        """mount NFS from smykfiles0 by some servers"""
        nfs_client = ['jboss-service', 'jboss-da']
        if nfs_client.count(host) > 0:
            r = pamn.Run()
            r.run()

if __name__ == '__main__':
    na = env.Name()
    if 'prod' not in na.stage:
        envr = env.Env()
        nfs = NFS()
        host = envr.ne.remove_stage(envr.ne.shortname)
        envr.fix_jboss_symlink()
        nfs.mount_nfs(host)
