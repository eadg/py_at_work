import env
import final_modification as fm


if __name__ == '__main__':
    envr = env.Env()
    jas = fm.JBossAsService()
    jas.stop_it()
    cmrp = fm.CallMrProper(envr.get_jboss_ver())
    jars = fm.ExternalJar()
    for met in cmrp.methods:
        # print("call " + met)
        getattr(envr, met)()
    envr.fix_jboss_symlink()
    jars.cp_zorka()
    cmrp.set_permission('/srv/jboss/')
    cmrp.run_separte_meth()
