import sys
import subprocess
import re
import urllib2
import json
import os.path
from env import Name
from disk_operations import DiskOper
from stop_rhq_agent import Timer


class ServerMap(object):
    server = {
        'jboss-da': 'jboss-da1',
        'jboss-service': 'jboss-service1',
        'jboss-outgoing': 'jboss-da1'
    }


class ClientMap(object):
    server = {
            'jboss-da-dev': [
                'smykfiles0:/mnt/smykfiles/dev01', '/srv/smykstor_new/dev'],
            'jboss-da-test': [
                'smykfiles0:/mnt/smykfiles/test01', '/srv/smykstor_new/test'],
            'jboss-da-beta': [
                'smykfiles0:/mnt/smykfiles/beta01', '/srv/smykstor_new/beta'],
            'jboss-service-dev': [
                'smykfiles0:/mnt/smykfiles/dev01', '/srv/smykstor_new/dev'],
            'jboss-service-test': [
                'smykfiles0:/mnt/smykfiles/test01', '/srv/smykstor_new/test'],
            'jboss-service-beta': [
                'smykfiles0:/mnt/smykfiles/beta01', '/srv/smykstor_new/beta'],
            'jboss-service-pre-prod': [
                'smykfiles0:/mnt/smykfiles/test01', '/srv/smykstor_new/test'],
    }

    def __init__(self):
        self.ne = Name()

    def mkdir_and_mount(self):
        list_for_mount = ClientMap.server[self.ne.shortname]
        if not os.path.ismount(list_for_mount[1]):
            DiskOper().mkdir_if(path=list_for_mount[1], owner='smyk')
            mt = subprocess.check_output(
                ["mount", list_for_mount[0], list_for_mount[1]])
            return mt


class RemoteDiskName(object):

    def __init__(self, port):
        self.server = ServerMap().server[self.familly_host()]
        self.port = ':' + str(port)

    def read_http(self):
        """Return json like "/mnt/smykfiles/01": "a" """
        url = 'http://' + self.server + self.port
        req = urllib2.Request(url)
        try:
            res = urllib2.urlopen(req)
        except urllib2.URLError:
            os._exit(0)
        else:
            data = res.next()
        return json.loads(data)

    def get_disk_name(self, inp):
        """Convert unicode json to utf8"""
        if isinstance(inp, dict):
            return {self.get_disk_name(key): self.get_disk_name(value)
                    for key, value in inp.iteritems()}
        elif isinstance(inp, unicode):
            return inp.encode('utf-8')

    def familly_host(self):
        """truncate ending digit. look at jda2 ... and jservice2"""
        s = Name().shortname
        x, y = s.split('-')[0:2]
        pattern = re.compile('([a-z]+)\d*')
        z = re.sub(pattern, r'\1', y)
        return '-'.join((x, z))


class Diff(object):

    def __init__(self):
        if len(sys.argv) > 1:
            self.host = sys.argv[1]
        else:
            self.host = 'smykfiles0'

    def show_mount(self):
        """Return iterator from showmount command"""
        try:
            sm = subprocess.check_output(["showmount", "-e", self.host])
        except subprocess.CalledProcessError:
            pass
        paths = sm.split('\n')
        c = re.compile('.+?/\d\d$')
        for line in paths:
            path = (line.split(' ')).pop(0)
            m = re.search(c, path)
            if m:
                yield path

    def check_mount(self):
        """Return missing disks"""
        with open('/proc/mounts') as f:
            df = f.read()
        for remote in self.show_mount():
            if remote not in df:
                yield remote

    def mount(self, disk, letter, owner):
        """Mount missing disk"""
        path = self.check_local_mount_point()
        local_disk = path + letter
        DiskOper().mkdir_if(local_disk, owner)
        remote_disk = self.host + ':' + disk
        mt = subprocess.check_output(["mount", remote_disk, local_disk])
        return mt

    def check_local_mount_point(self):
        """return local path for 10.0.12.90:/mnt/smykfiles/01"""
        with open('/proc/mounts') as f:
            df = f.readlines()
        for line in df:
            if '/mnt/smykfiles/01' in line:
                long_path = (line.split(' ')).pop(1)
                path = '/'.join(long_path.split('/')[:-1])
                return path + '/'
        return '/srv/smykstor_new/'


class Run(object):

    def run(self):
        Timer(10).run_timer()
        d = Diff()
        r = RemoteDiskName(9000)
        dict_disk = r.get_disk_name(r.read_http())
        iter_miss_disk = d.check_mount()
        for miss in iter_miss_disk:
            let = dict_disk[miss]
            d.mount(disk=miss, letter=let, owner="smyk")
        cm = ClientMap()
        cm.mkdir_and_mount()


if __name__ == '__main__':
    Run().run()
