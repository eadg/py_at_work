#!/bin/bash

# cd ~/dev/pyt/pytulim/jenkins/

# scp *.py centos@10.10.10.100:~

# rsync -KLrpgoD *.py ssap centos@10.10.10.100:~
# cp -L /home/jrokicki/dev/pyt/pytulim/nfs/disk_operati*.py .
# cp -L /home/jrokicki/dev/pyt/pytulim/restarts_jboss/jboss_restart_log.py .
# cp -L /home/jrokicki/dev/pyt/pytulim/restarts_jboss/select_jboss_restart.py .
# cp -L /home/jrokicki/dev/pyt/pytulim/rules/rules.py .
# cp -L /home/jrokicki/dev/pyt/pytulim/nfs/serve_mounts.py .

find .. -maxdepth 1 -iname '*.py' -exec rsync -KLrpgoD {} . \;
rsync -KLrpgoD *.py ssap centos@10.10.10.100:/srv/salt/file/devel_jboss/jboss_scripts/
