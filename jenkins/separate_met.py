import re


class J7(object):

    '''Methods for some JBoss7 '''

    @staticmethod
    def cm_ds():
        fpath = '/srv/jboss/jboss/standalone' + '/configuration/standalone.xml'
        patt_elk = re.compile('smyk-test1')
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(patt_elk, r'smyk-db-test', data)
            f.seek(0)
            f.truncate()
            f.write(fix_data)
