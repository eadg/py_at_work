import subprocess
import env
import jenkins


class Run(object):

    def run(self, script, ver):
        if len(ver) == 1:
            subprocess.call([script, 'snapshot', ver.pop()])
        elif len(ver) == 4:
            subprocess.call([script, ver[1], ver[2], ver[3]])


if __name__ == '__main__':
    na = env.Name()
    if 'prod' not in na.stage:
        envr = env.Env()
        fetc = jenkins.Fetch()
        bul = Run()
        jenkins_url = envr.get_jenkins_url()
        scriptpath = envr.get_make_name()
        versions = fetc.read_version_jenkins(jenkins_url)
        bul.run(scriptpath, versions)
