import os
from pathlib import Path
import sys

import logger


class Elmite():

    def __init__(self):
        self.yr = sys.argv[1]
        self.get_log()

    def get_log(self):
        pa = Path(sys.argv[0]).parent
        pastr = str(pa.joinpath('plik.log'))
        self.logs = logger.Logger(file_path=pastr).log
    
    def omit(self) -> dict:
        omitlist = {}
        with open('nagranie_z_ugoda_uniq') as f:
            for line in f:
                p = Path(line.strip()).name
                omitlist[p] = 1
        return omitlist

    def wok(self):
        print(f"ARGV[0]: {sys.argv[0]}")
        omitlist = self.omit()
        print(len(omitlist))
        file2del = 0
        file2omit = 0
        for dirpath, dirname, filenames in os.walk(self.yr, topdown=False):
            if filenames:
                for filename in filenames:
                    try:
                        if omitlist[filename]:
                            self.logs.info(f"Zostawiam: {dirpath}/{filename}")                            
                            file2omit += 1
                    except KeyError:
                        os.unlink(f"{dirpath}/{filename}")
                        # print(os.stat(f"{dirpath}/{filename}"))
                        self.logs.info(f"Usuwam: {dirpath}/{filename}")
                        file2del += 1
        print(f"Rok {self.yr} usunieto: {file2del} zostało: {file2omit}")
        self.logs.info(f"Rok {self.yr} do usuniecia: {file2del} pozostawienia: {file2omit}")

if __name__ == '__main__':
    el = Elmite()
    el.wok()

