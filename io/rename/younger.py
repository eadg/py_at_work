import argparse
from datetime import date
import collections
import glob
import pathlib
import shutil
import sys
import disk_operations as do
import logger


class Younger(object):

    def __init__(self):
        self.di = dict()
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-p', action='store', help='path', dest='ap')
        self.parser.add_argument(
            '-l', action='store', help='log', dest='log')
        results = self.parser.parse_args()
        self.argpt = results.ap
        self.logger = logger.Logger(results.log)

    def scan(self) -> collections.defaultdict:
        dd = collections.defaultdict(list)
        gli = glob.iglob(self.argpt + '/*.pdf')
        for ele in gli:
            f = pathlib.PurePath(ele)
            main_key = f.name.split('_')[0]
            dt = self.get_date(f.name)
            dd[main_key].append([dt, f.name])
        return dd

    def sort_and_move(self, d: collections.defaultdict) -> None:
        old = self.argpt + '/zrobione/stare'
        new = self.argpt + '/zrobione/nowe'
        do.DiskOper().mkdir_if(old)
        do.DiskOper().mkdir_if(new)
        for k, v in d.items():
            d[k].sort(key=lambda x: x[0], reverse=True)
            index = 0
            for el in d[k]:
                if index == 0:
                    src = pathlib.PurePath(self.argpt).joinpath(el[1])
                    dst = pathlib.PurePath(new).joinpath(el[1])
                    self.logger.log.info(
                        "Najnowszy: move {} {}".format(src, dst))
                    shutil.copy(src, dst)
                if index > 0:
                    src = pathlib.PurePath(self.argpt).joinpath(el[1])
                    dst = pathlib.PurePath(old).joinpath(el[1])
                    self.logger.log.info(
                        "Starszy: move {} {}".format(src, dst))
                    shutil.copy(src, dst)
                index += 1

    def get_date(self, fname: str) -> date:
        name = fname.split('.')[0]
        lname = name.split('-')
        try:
            y = int(lname[-3])
            m = int(lname[-2])
            d = int(lname[-1])
        except ValueError:
            print("Error in file: {}".format(fname))
        dt = date(y, m, d)
        return dt


if __name__ == '__main__':
    y = Younger()
    d = y.scan()
    y.sort_and_move(d)
