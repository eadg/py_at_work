#!/bin/bash

## #Wywołanie ./find.sh szukany_string ##

echo -e "\nString $1 znaleziony w:"
ls -t /srv/scripts/samba/log/*.lo* | xargs -I % bash -c  "zcat % | grep -i -q $1  && ls -og % "
