class ABC(object):

    def __init__(self):
        self.li = []

    def read(self):
        with open('uprawnienia.txt') as f:
            for i in f:
                yield i

    def load_to_list(self):
        for i in self.read():
            self.li.append(i)
        return self.li

    def sort_list(self):
        l = self.load_to_list()
        l.sort(reverse=True)
        return l

    def set_permission(self):
        l = self.sort_list()
        for i in l:
            kat, user, *a = i.split(';')
            if a:
                print(a)
            print('kat %s' % kat)
            for u in user.split(','):
                print(kat + '=-=' + u)


class ContainerACL(object):

    def __init__(self, path):
        self.path = path

    def __iter__(self):
        with open(self.path) as f:
            for line in f:
                kat, user, *useless_scol = line.strip().split(';')
                if useless_scol:
                    user += ''.join(useless_scol)
                yield kat, user


class Modify(object):

    def setacl(self):
        cacl = ContainerACL("uprawnienia.txt")
        for kat, user in cacl:
            if kat.find('/') > 0:
                pass
                # print(kat)
            else:
                print(kat)


if __name__ == '__main__':
    # o = ABC()
    # print(*o.sort_list())
    # o.set_permission()
    # o2 = ContainerACL("uprawnienia.txt")
    # for i in o2:
    #    print(i)
    m = Modify()
    m.setacl()
