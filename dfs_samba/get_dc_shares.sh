#!/bin/bash

/srv/scripts/samba/get_dc_shares.perl  > /tmp/shares_disks.conf.$$

y=`cat /tmp/shares_disks.conf.$$|wc -l`

if [ $y -lt 25 ];then
  rm /tmp/shares_disks.conf.$$
else 
  mv /tmp/shares_disks.conf.$$ /srv/salt/resources/smbconf/dc/samba/shares_disks.conf
fi

min=`date +'%M' | sed 's/.$/0/'`
file="get_dc_shares_log_"`date +'%d_%H-'`$min.log.gz

{
salt '0-pl-p-dc*' state.sls smbconf 
salt '0-pl-p-dc*' service.reload "smb" 
} | gzip > /srv/scripts/samba/log/$file

