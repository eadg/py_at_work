import subprocess as sub


def fip():
    with open('c.txt') as f:
        data = f.readlines()
    for row in data:
        dysk, ip = row.split(',')
        ipadr = 'ip=' + ip
        sub.check_output(['pcs', 'resource', 'update', dysk, ipadr])


def fmask():
    with open('c.txt') as f:
        data = f.readlines()
    for row in data:
        dysk, ip = row.split(',')
        netmask = 'cidr_netmask=22'
        sub.check_output(['pcs', 'resource', 'update', dysk, netmask])


if __name__ == '__main__':
    fmask()
