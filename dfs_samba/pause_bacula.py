#!/usr/bin/python

import psutil
import os
import time


class BreathBac(object):

    def get_pid_bac(self):
        p = psutil.Process(1)
        for proc in p.children():
            if 'bacula-fd' in proc.name():
                return proc

    def pause_bac(self):
        p = self.get_pid_bac()
        p.suspend()

    def continue_bac(self):
        p = self.get_pid_bac()
        p.resume()


if __name__ == '__main__':
    if os.getloadavg()[1] > 15:
        b = BreathBac()
        b.pause_bac()
        time.sleep(4)
        b.continue_bac()
