import os
import sys
import subprocess
import time
import logging


class Wrap(object):

    '''https://jira.ultimo.pl/browse/SERVICE-24475'''

    def __init__(self):
        self.hostname = sys.argv[1]
        self.log = logging.getLogger()

    def call_add(self):
        self.log.info("call smbldap-useradd -w -a " + self.hostname)
        subprocess.call(["/usr/sbin/smbldap-useradd", "-w", "-a", self.hostname])

    def forking(self):
        pid = os.fork()
        if pid > 0:
            self.log.info("call forking() => kill parent -> aaargh")
            sys.exit(0)

    def call_mod(self):
        for _ in range(0, 9):
            self.log.info("call smbldap-usermod -J " + self.hostname)
            time.sleep(20)
            subprocess.call(["/usr/sbin/smbldap-usermod", "-J", self.hostname])

if __name__ == '__main__':
    wrap = Wrap()
    wrap.call_add()
    wrap.forking()
    wrap.call_mod()
