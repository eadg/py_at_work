#!/bin/bash

cd /srv/scripts/py

# archiwizacja logów

data=`python3 -c 'import timetrav ; dt=timetrav.TT(-1).get_iso(); print(dt)'` ; python3 rotate_log.py -o a -p y -a /var/log/archive  -f /var/log/exim/main.log.$data -l /var/log/archive_operations.log 
data=`python3 -c 'import timetrav ; dt=timetrav.TT(-1).get_iso(); print(dt)'` ; python3 rotate_log.py -o a -p y -a /var/log/archive  -f /var/log/exim/reject.log.$data -l /var/log/archive_operations.log 
python3 rotate_log.py -o t -p y -a /var/log/archive -f /var/log/maillog -l /var/log/archive_operations.log
python3 rotate_log.py -o t -p y -a /var/log/archive -f /var/log/exim/panic.log -l /var/log/archive_operations.log

# usuwanie starych plików
python3 rotate_log.py -o u -f /var/log/archive -n 60 -l /var/log/archive_operations.log 
