import argparse
import fnmatch
import gzip
import os
from pathlib import Path
import re
import sys


class MultiGz(object):

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-p', action='store', help='path', dest='ap')
        results = self.parser.parse_args()
        self.argpath = results.ap

    def list_gz(self, ph: str) -> None:
        it = os.scandir(ph)
        for de in it:
            if de.is_file() and fnmatch.fnmatch(de, '*.gz'):
                if de.stat().st_size < 41:
                    os.renames(de, ph + 'stub/' + de.name)
                elif len(re.findall('gz', de.name)) > 1:
                    self.gzopen(de)
                    os.renames(de, ph + 'done/' + de.name)

    def gzopen(self, finput: str) -> None:
        with gzip.open(finput) as fin:
            print(finput)
            paf = Path(finput)
            nfn = self.get_new_name(paf.name)
            fout = fin.read()
            decofout = self.decomp(fout)
            if isinstance(decofout, str):
                with open(paf.with_name(nfn), 'w') as fut:
                    fut.write(decofout)
            elif isinstance(decofout, bytes):
                with open(paf.with_name(nfn), 'wb') as fut:
                    fut.write(decofout)

    def get_new_name(self, finput: str) -> str:
        pa = Path(finput)
        list_names = pa.name.split('.')
        new_list_names = []
        dt = list_names[2].split('-').pop(0)
        new_list_names.extend(list_names[0:2])
        new_list_names.append(dt)
        return '.'.join(new_list_names)

    def decomp(self, fbin: bytes) -> str:
        self.out = None
        try:
            self.out = fbin.decode()
        except UnicodeDecodeError:
            gout = self.use_gzip_module(fbin)
            if gout == fbin:
                self.out = fbin
                return self.out
            self.decomp(gout)
        return self.out

    def use_gzip_module(self, fbin: bytes) -> bytes:
        '''to avoid nested exception....'''
        self.gout = None
        try:
            self.gout = gzip.decompress(fbin)
        except OSError:
            return fbin
        except gzip.BadGzipFile:
            return fbin
        return self.gout


if __name__ == '__main__':
    mg = MultiGz()
    mg.list_gz(mg.argpath)
