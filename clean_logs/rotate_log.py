import argparse
import glob
import os
import os.path
import shutil
import sys
from datetime import datetime as dd
from datetime import timedelta as dt

from disk_operations import DiskOper
from timetrav import TT
import logger


class Rotate():

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-p', action='store', help='Jaki timestamp zastosowac dla archiwi\
            zowanych logow: (d)ay of month, (w)eek number of the year, day of\
            (y)ear. Default brak.', dest='period_value', default='')

        self.parser.add_argument('-o', action='store', help='Typ operacji:\
        (a)rchiwizajca=>gzip+usuwanie, (t)runcate=>gzip+truncate,\
        (u)suwanie', dest='operation_value')

        self.parser.add_argument('-a', action='store', help='Katalog dla\
        archiwizowanych plikow', dest='archive_value')

        self.parser.add_argument('-l', action='store', help='Sciezka dla pliku\
        logow. Domyslnie /dev/null', dest='logging_value')

        self.parser.add_argument('-f', action='append', help='Pliki do\
        archiwizacji/kasowania', dest='files_value', default=[])

        self.parser.add_argument('-n', action='store', help='Ilosc dni wstecz\
        dla kasowaniu plikow na podstawie daty \
        utworzenia.', dest='number_days_value', type=int)

        self.parser.add_argument(
            '-c', action='store', help='Output na konsole',
            dest='number_days_value', type=bool)

        results = self.parser.parse_args()
        self.period = results.period_value
        self.kind_of_action = results.operation_value
        self.arch = self.get_arch(results.archive_value)
        self.logger = self.get_logger(results.logging_value)
        self.files = self.get_files(results.files_value)
        self.nod = self.get_nod(results.number_days_value)

    def rename(self, path):
        nr = TT().get_nr_of_period(per=self.period)
        # nr = self.get_nr_of_period(per=self.period)
        pathlist = path.split('/')
        f = pathlist[-1]
        new_f = nr + '_' + f
        newpathlist = self.arch.split('/')
        newpathlist.append(new_f)
        newpath = '/'.join(newpathlist)
        return newpath

    def what_to_remove(self):
        x_days_ago = dd.today() - dt(days=int(self.nod))
        list_files = (
            file_2_remove for file_2_remove in self.files if dd.fromtimestamp(
                os.path.getmtime(file_2_remove)) < x_days_ago)
        return list_files

    def get_files(self, f_list):
        if f_list and len(f_list) < 2:
            _files = f_list.pop()
            if os.path.isdir(_files):
                iter_files = glob.iglob(_files + '/*')
            else:
                iter_files = glob.iglob(_files)
            return iter_files
        else:
            sys.exit('Hi there, no file to action! Please use -f or -h')

    def get_arch(self, arch_path):
        if not arch_path:
            return '-'
        else:
            return arch_path

    def get_nod(self, days):
        if not days:
            days = []
        else:
            return days

    def get_logger(self, path):
        if not path:
            if os.name in 'posix':
                f = '/dev/null'
            elif os.name in 'nt':
                f = 'nul'
            return logger.Logger(f)
        else:
            return logger.Logger(path)

    def action(self, src):
        do = DiskOper()
        if self.kind_of_action == 't':
            if not os.path.isfile(src):
                sys.exit('no exist')
            dst = self.rename(src)
            do.mkdir_if(self.arch)
            try:
                shutil.copy(src, dst)
            except shutil.SameFileError:
                pass
            do.truncate(src)
            fout = do.gzip(dst)
            self.logger.log.info(
                "Gzip & Truncate: {0} => {1}".format(src, fout))
        if self.kind_of_action == 'a':
            if not os.path.isfile(src):
                sys.exit('no exist')
            dst = self.rename(src)
            do.mkdir_if(self.arch)
            try:
                shutil.copy(src, dst)
            except shutil.SameFileError:
                pass
            fout = do.gzip(dst)
            self.logger.log.info(
                "Gzip & Move: {0} => {1}".format(src, fout))
            try:
                os.remove(src)
            except FileNotFoundError:
                pass
        if self.kind_of_action == 'u':
            if not self.nod:
                return sys.exit(
                    'Brak ustawionej opcji -n /ilosc dni wstecz do usuniecia/')
            for fe in self.what_to_remove():
                if os.path.isfile(fe):
                    os.remove(fe)
                    self.logger.log.info("Remove: {0}".format(fe))


if __name__ == '__main__':
    r = Rotate()
    for f in r.files:
        r.action(f)
