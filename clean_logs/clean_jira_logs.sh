#!/bin/bash

cd /srv/scripts/py

# archiwizacja logów

python3 rotate_log.py -o t -p d -a /opt/atlassian/jira/logs -f /opt/atlassian/jira/logs/catalina.out -l /var/log/archive_operations.log 

data=`python3 -c 'import timetrav ; dt=timetrav.TT(-1).get_hyp(); print(dt)'` ; python3 rotate_log.py -o a -p y -a /opt/atlassian/jira/logs -f /opt/atlassian/jira/logs/access_log.$data -l /var/log/archive_operations.log 
data=`python3 -c 'import timetrav ; dt=timetrav.TT(-1).get_iso(); print(dt)'` ; python3 rotate_log.py -o a -p y -a /opt/atlassian/jira/logs -f /opt/atlassian/jira/logs/catalina.out-$data -l /var/log/archive_operations.log  
data=`python3 -c 'import timetrav ; dt=timetrav.TT(-1).get_hyp(); print(dt)'` ; python3 rotate_log.py -o a -p y -a /opt/atlassian/jira/logs -f /opt/atlassian/jira/logs/localhost.${data}.log -l /var/log/archive_operations.log 
data=`python3 -c 'import timetrav ; dt=timetrav.TT(-1).get_hyp(); print(dt)'` ; python3 rotate_log.py -o a -p y -a /opt/atlassian/jira/logs -f /opt/atlassian/jira/logs/catalina.${data}.log -l /var/log/archive_operations.log 

data=`python3 -c 'import timetrav ; dt=timetrav.TT(-1).get_hyp(); print(dt)'` ; python3 rotate_log.py -o a -p y -a /opt/atlassian/confluence/logs -f /opt/atlassian/confluence/logs/catalina.$data.log -l /var/log/archive_operations.log 

# usuwanie starych plików
python3 rotate_log.py -o u -f /var/atlassian/application-data/jira/export/ -n 14 -l /var/log/archive_operations.log 
python3 rotate_log.py -o u -f /opt/atlassian/jira/logs/ -n 90 -l /var/log/archive_operations.log 
python3 rotate_log.py -o u -f /opt/atlassian/confluence/logs/ -n 90 -l /var/log/archive_operations.log 
python3 rotate_log.py -o u -f /var/log/exim/ -n 180 -l /var/log/archive_operations.log


