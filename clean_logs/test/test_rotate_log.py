import glob
import inspect
import sys
import types
import unittest
import rotate_log


class RotateTest(unittest.TestCase):

    def setUp(self):
        sys.argv.clear()
        sys.argv.append('RotateTest')
        sys.argv.append('-o a')
        sys.argv.append('-a /tmp/arch')
        sys.argv.append('-f /tmp/*.log')
        # sys.argv.append('/tmp/test/b02.log')

    def test_double_aster(self):
        func_name = inspect.currentframe().f_code.co_name
        print("in method: %s" % func_name)
        path = '/var/lo*/lastlo*'
        sys.argv.append(path)
        gg = glob.glob(path)
        self.assertIsInstance(gg, type(glob.glob(path)))

    def un_test_check_kind2_generator(self):
        '''test if output is generator'''
        func_name = inspect.currentframe().f_code.co_name
        print("in method: %s" % func_name)
        self.rl = rotate_log.Rotate()
        value = self.rl.check_kind_input()
        self.assertIsInstance(value, types.GeneratorType)

    def un_test_path_with_asterisk(self):
        func_name = inspect.currentframe().f_code.co_name
        print("in method: %s" % func_name)
        self.rl = rotate_log.Rotate()
        ge = self.rl.path_with_asterisk('/home/jrokicki/*')
        ge.__next__()
        self.assertTrue

    def test_check(self):
        func_name = inspect.currentframe().f_code.co_name
        print("in method: %s" % func_name)
        self.rl = rotate_log.Rotate()
        '''
        value = self.rl.check_kind_input()
        for x in value:
            print(x)
        '''


if __name__ == '__main__':
    unittest.main()
