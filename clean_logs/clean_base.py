import os
import shutil
import sys
from datetime import datetime as dd
from datetime import timedelta as dt
try:
    import gzip
except ModuleNotFoundError:
    import zipfile


class CleanBase(object):

    '''sketch of mix-in for Rotate()'''

    def truncate(self, path):
        '''truncate file'''
        f = open(path, 'r+')
        f.truncate(0)
        f.flush()
        f.close()

    def gzip(self, path):
        print(sys.modules.keys())
        if 'gzip' in sys.modules.keys():
            self.suf = '.gz'
            with open(path, 'rb') as fi, gzip.open(path+self.suf, 'wb') as fo:
                shutil.copyfileobj(fi, fo)
        else:
            self.suf = '.zip'
            with zipfile.ZipFile(path+self.suf, 'w') as fo:
                fo.write(path)
        os.remove(path)

    def what_to_remove(self, number_days: int, input_list: list):
        x_days_ago = dd.today() - dt(days=number_days)
        list_files = (
            file_2_remove for file_2_remove in input_list if dd.fromtimestamp(
                os.path.getmtime(file_2_remove)) < x_days_ago)
        return list_files
