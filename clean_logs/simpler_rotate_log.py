import datetime
import shutil
import gzip
import os


class Rotate(object):

    def __init__(self):
        self.path = '/var/log/sftp-server.log'
        self.arch = '/var/log/archive'
        # self.path = '/tmp/a.txt'
        # self.arch = '/tmp/archive'

    def get_nr_week(self):
        d = datetime.datetime.today()
        return d.strftime('%W')

    def rename(self):
        nr = self.get_nr_week()
        pathlist = self.path.split('/')
        f = pathlist[-1]
        new_f = nr + '_' + f
        newpathlist = self.arch.split('/')
        newpathlist.append(new_f)
        newpath = '/'.join(newpathlist)
        return newpath

    def truncate(self):
        f = open(self.path, 'r+')
        f.truncate(0)
        f.flush()
        f.close()

    def gzip(self, path):
        with open(path, 'rb') as f_in, gzip.open(path+'.gz', 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
        os.remove(path)

    def copy(self, src, dst):
        shutil.copy(src, dst)


if __name__ == '__main__':
    r = Rotate()
    newpath = r.rename()
    r.copy(r.path, newpath)
    r.truncate()
    r.gzip(newpath)
