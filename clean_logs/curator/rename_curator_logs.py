import os
import re

'''
        input:  12_03_00-curator_del_old.log.gz
        output: 12_curator_del_old.log.gz
'''


def rename_example():
    r = re.compile('^\d\d')
    for f in os.listdir('.'):
        s = '%s_%s' % (r.search(f).group(), f[9:])
        os.rename(s, f)
