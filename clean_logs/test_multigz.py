from pathlib import Path
import sys
from multigzip import MultiGz


class TestCase(object):

    longname = '''test/poprawa_nazw/single/main.log.20200210-20200216.gz-20200223.gz-20200301.gz-20200308.gz-20200315.gz-20200329.gz'''

    def test_return_list_gz(self):
        sys.argv = ['multigzip.py', '-p', 'test/poprawa_nazw/single/']
        mg = MultiGz()
        re = mg.list_gz(mg.argpath)
        assert not re

    def test_name1(self):
        pa = Path(self.longname)
        assert pa.suffixes[0] == '.log'

    def test_name2(self):
        pa = Path(self.longname).name
        assert pa == 'main.log.20200210-20200216.gz-20200223.gz-20200301.gz-20200308.gz-20200315.gz-20200329.gz'

    def test_get_new_name(self):
        sys.argv = ['multigzip.py', '-p', 'test/poprawa_nazw/single/']
        mg = MultiGz()
        assert mg.get_new_name(self.longname) == 'main.log.20200210'

    def test_reg(self):
        sys.argv = ['multigzip.py', '-p', 'test/poprawa_nazw/single/']
        mg = MultiGz()
        gr = mg.check_nr_gz(self.longname)
        assert len(gr) > 1
