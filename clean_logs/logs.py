#!/usr/bin/python

import glob
import datetime
import os.path
import os

'''Kasuje starsze logi od dzisiejszych '''

l = glob.glob('/srv/rhq/logs/server.log.*')
[os.remove(fi) for fi in l if datetime.datetime.fromtimestamp(os.path.getmtime(fi)) < datetime.datetime.today()]
