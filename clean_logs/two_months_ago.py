import glob
from datetime import date, timedelta
# import os
# from dateutil.relativedelta import relativedelta


class TwoMonthsAgo(object):

    def get_past_date(self):
        two_mon = date.today() + timedelta(days=-30)
        # two_mon = date.today() + relativedelta(months=-2)
        isolist = two_mon.isoformat().split('-')
        return isolist[0], isolist[1], isolist[2]

    def do_it(self):
        y, m, d = self.get_past_date()
        # path = '/srv/jboss/jboss/standalone/tmp/big/unpacked_data/ULTIMO_EXPORT_'
        path = '/var/log'
        path_full = path + y + m + d + '*'
        print(path_full)
        iter1 = glob.iglob(path_full)
        for f in iter1:
            # os.remove(f)
            print(f)


if __name__ == '__main__':
    tma = TwoMonthsAgo()
    tma.do_it()
