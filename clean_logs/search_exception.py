import sys
import subprocess


class Search(object):

    def __init__(self):
        self.path = sys.argv[1]
        self.word = sys.argv[2]

    def is_word_there(self):
        with open(self.path) as f:
            for row in f:
                if self.word in row:
                    self.action()
                    return 0

    def action(self):
        subprocess.call(['/usr/sbin/service', 'jboss7', 'restart'])


if __name__ == '__main__':
    s = Search()
    s.is_word_there()
