from datetime import datetime
from pathlib import Path

from clean_base import CleanBase

import logger

from travel_base import TravelBase


class CleanSFTP(CleanBase, TravelBase):

    def __init__(self):
        self.logg = logger.Logger(
            file_path='/var/log/cleaner_sftp.log', clean=False)

    def action(self, dire: str) -> None:
        it = self.gen_dir(
            ipath='/srv/ftp/ftp_komornik_nadrzedne/', name='komornik')
        for i in it:
            pi = Path(i)
            spi = pi.joinpath(dire)
            fig = (spi.glob('**/*'))
            list_fig = list(fig)
            output_list = self.what_to_remove(
                number_days=45, input_list=list_fig)
            for f in output_list:
                # print(f)
                fp = Path(f)
                if fp.is_file():
                    s = 'Remove: {} Last mod: {}'.format(f, str(
                        datetime.fromtimestamp(fp.stat().st_mtime)))
                    self.logg.log.info(s)
                    fp.unlink()

    def loop_subdir(self) -> None:
        list_subdir = [
            'OD WIERZYCIELA', 'PLIKI DLA KOMORNIKA', 'ZALICZKI', 'PDF']
        for subdir in list_subdir:
            self.action(dire=subdir)

    def run(self):
        self.loop_subdir()


if __name__ == '__main__':
    csftp = CleanSFTP()
    csftp.run()
