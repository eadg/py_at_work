import spur

shell = spur.SshShell(
    hostname="jboss-teren-dev",
    username="jrokicki",
    private_key_file="/home/jrokicki/.ssh/id_rsa"
)
with shell:
    result = shell.run(["uname", "-a"])
print(result.output)
