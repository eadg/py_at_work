import glob
import os
import pathlib
import pexpect
import ass
import disk_operations as do
import logger


def dronn():
    lpath = '/srv/exports/DCC/DCC/DRONN.COM/zrzuty_NPS'
    pa = ass.read_pass(file='ass', credent=0)
    # os.chdir('/tmp')
    os.chdir(lpath)
    child = pexpect.spawnu('sftp ftp_dronn@stor1.ultimo.pl:upload')
    child.expect('Password:')
    child.sendline(pa)
    child.expect('sftp>')
    child.sendline('get -R *')
    child.expect('sftp>')
    child.sendline('bye')
    do.DiskOper().set_group_owner_recursive(path=lpath, owner='ultimo')


def henio_zip():
    pa = ass.read_pass(file='ass', credent=1)
    # lp1 = '/srv/disks/dfi/Henio/Procesy/KomornikOnline/KO_DANE'
    lp1 = 'test/KO_DANE'
    lp2 = ' (puste_podfoldery_nie_wrzucac_tu_plikow)'
    lpath = lp1 + lp2
    os.chdir(lpath)
    print(f"localpath: {os.getcwd()}")
    child = pexpect.spawnu('sftp ftp_komornik_nadrzedne@stor1.ultimo.pl')
    child.expect('Password:')
    child.sendline(pa)
    child.expect('sftp>')
    igl = glob.iglob('**/*.zip', recursive=True)
    for o in igl:
        ph = pathlib.Path(o)
        remote_path = ph.parent.joinpath('KO_DANE')
        command0 = 'mkdir ' + str(remote_path)
        child.sendline(command0)
        child.expect('sftp>')
        command1 = 'cd ' + str(remote_path)
        child.sendline(command1)
        child.expect('sftp>')
        command2 = 'put ' + str(ph)
        child.sendline(command2)
        child.expect('sftp>')
        command3 = 'cd ../..'
        child.sendline(command3)
        child.expect('sftp>')
    child.sendline('bye')


def henio_xml():
    pa = ass.read_pass(file='ass', credent=1)
    # lp1 = '/srv/disks/dfi/Henio/Procesy/KomornikOnline/KO_DANE'
    lp1 = 'test/KO_DANE'
    lp2 = ' (puste_podfoldery_nie_wrzucac_tu_plikow)'
    lpath = lp1 + lp2
    os.chdir(lpath)
    print(f"localpath: {os.getcwd()}")
    child = pexpect.spawnu('sftp ftp_komornik_nadrzedne@stor1.ultimo.pl')
    child.expect('Password:')
    child.sendline(pa)
    child.expect('sftp>')
    igl = glob.iglob('**/**/*.xml', recursive=True)
    for o in igl:
        ph = pathlib.Path(o)
        remote_path = ph.parents[1].joinpath('KO_DANE')
        print(remote_path)
        command0 = 'mkdir ' + str(remote_path)
        child.sendline(command0)
        child.expect('sftp>')
        command1 = 'cd ' + str(remote_path)
        child.sendline(command1)
        child.expect('sftp>')
        command2 = 'put ' + str(ph)
        child.sendline(command2)
        child.expect('sftp>')
        command3 = 'cd ../..'
        child.sendline(command3)
        child.expect('sftp>')
    child.sendline('bye')


def henio_ls_ko():
    pa = ass.read_pass(file='ass', credent=1)
    # lp1 = '/srv/disks/dfi/Henio/Procesy/KomornikOnline/KO_DANE'
    lp1 = 'test/KO_DANE'
    lp2 = ' (puste_podfoldery_nie_wrzucac_tu_plikow)'
    lpath = lp1 + lp2
    os.chdir(lpath)
    child = pexpect.spawnu('sftp ftp_komornik_nadrzedne@stor1.ultimo.pl')
    child.expect('Password:')
    child.sendline(pa)
    child.expect('sftp>')
    igl = glob.iglob('**/*.zip', recursive=True)
    for o in igl:
        print(f"object iglob: {o}")
        ph = pathlib.Path(o)
        remote_path = ph.parent.joinpath('KO_DANE')
        command1 = 'cd ' + str(remote_path)
        child.sendline(command1)
        child.expect('sftp>')
        child.sendline("ls")
        child.expect('sftp>')
        print(child.before)
        command3 = 'cd ../..'
        child.sendline(command3)
        child.expect('sftp>')
    child.sendline('bye')


if __name__ == '__main__':
    # dronn()
    henio_xml()
    # henio_ls_ko()
