from __future__ import print_function
import pssh


# pssh.pssh_clientsParallelSSHClient

hosts = ['jboss-teren-dev', 'jboss-teren-test', 'jboss-teren2']
client = pssh.pssh_client.ParallelSSHClient(hosts)
output = client.run_command('ps aux | grep jboss | grep java | grep -v grep')
for host, host_output in output.items():
    # print(host_output['stdout'])
    for line in host_output['stdout']:
        print("Host {0}: {1}".format(host, line))


# for k, v in output.items():
#     for line in v['stdout']:
#         print(line)

# for line in output['jboss-teren-dev'].stdout:    print(line)
