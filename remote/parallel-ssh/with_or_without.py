from __future__ import print_function
import pssh


class Sudo(object):

    def __init__(self):
        self.hosts = ['jboss-teren-dev', 'jboss-teren-test', 'jboss-teren2']

    def run_without(self):
        client = pssh.pssh_client.ParallelSSHClient(self.hosts)
        output = client.run_command(
            'ps aux | grep jboss | grep java | grep -v grep')
        for host, host_output in output.items():
            for line in host_output['stdout']:
                print("Host {0}: {1}".format(host, line))

    def run_with(self):
        client = pssh.pssh_client.ParallelSSHClient(self.hosts)
        output = client.run_command(
            'ps aux | grep jboss | grep java | grep -v grep', sudo=True)
        # print(output)
        for host, host_output in output.items():
            stdin = host_output['stdin']
            stdin.write('\n')
            for line in host_output['stdout']:
                print("Host {0}: {1}".format(host, line))


if __name__ == '__main__':
    # Sudo().run_without()
    Sudo().run_with()
