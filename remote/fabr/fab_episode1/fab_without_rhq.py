from fabric.api import *
import ass

env.hosts = ['jboss-big-dev', 'jboss-cron-dev', 'jboss-cti4-dev', 'jboss-cti-test', 'jboss-cti-test', 'jboss-cti-dev', 'jboss-cti-test', 'jboss-dwp-dev', 'jboss-jbpm-dev', 'jboss-legal-dev', 'jboss-service-dev', 'jboss-teren-dev']
host_excluded = ['jboss1', 'jboss-core1']
env.password = ass.read_pass()


def scprhq():
    put('rhq-agent.sh', '~')
    sudo('mv rhq-agent.sh /srv/rhq-agent/bin/')


def scpcheckpy():
    sudo('rm -f check_agent_rhq.py')
    put('../rhq/check_agent_rhq.py', '~')
    sudo('mv check_agent_rhq.py /etc/cron.daily/')


def stop_rhq_systemD():
    sudo('systemctl stop rhq-agent.service')
    sudo('systemctl disable rhq-agent.service')
    sudo('systemctl daemon-reload')


def stop_rhq_py():
    sudo('/srv/rhq-agent/bin/rhq-agent-wrapper.sh stop')
    sudo('if [ -f /etc/cron.hourly/check_agent_rhq.py ]; then rm -f /etc/cron.hourly/check_agent_rhq.py; fi')
    sudo('if [ -f /etc/cron.daily/check_agent_rhq.py ]; then rm -f /etc/cron.hourly/check_agent_rhq.py; fi')


@hosts('jboss-big-dev', 'jboss-cron-dev1', 'jboss-cti-dev1', 'jboss-dwp-dev1', 'jboss-jbpm-dev1', 'jboss-legal-dev1',  'jboss-service-dev1', 'jboss-teren-dev1')
def stop_low_mem_rhq():
    sudo('/srv/rhq-agent/bin/rhq-agent-wrapper.sh stop')
    sudo('systemctl stop rhq-agent')
    sudo('systemctl disable rhq-agent')
