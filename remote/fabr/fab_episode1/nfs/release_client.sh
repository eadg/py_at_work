#!/bin/bash

for i in jboss-service-beta jboss-service-test jboss-service-pre-prod jboss-da-dev jboss-da-beta jboss-da-test
do
    echo $i
    scp /home/jrokicki/dev/pyt/pytulim/nfs/auto_mount_nfs.py  jrokicki@$i:/srv/scripts/py/
done
