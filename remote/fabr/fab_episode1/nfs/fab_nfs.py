import fabric.api as fa
import ass

fa.env.password = ass.read_ass(file='../../ass/ass')
fa.env.warn_only = True
fa.env.command_timeout = 200


# @fa.hosts('jboss-da-test')
@fa.hosts('jboss-service-dev', 'jboss-service-beta', 'jboss-service-test', 'jboss-service-pre-prod', 'jboss-da-dev', 'jboss-da-beta', 'jboss-da-test', 'jboss-service2', 'jboss-da2', 'jboss-outgoing')
def release_client():
    fa.put('../../nfs/auto_mount_nfs.py', '.')
    fa.put('../../nfs/hostname.py', '.')
    fa.put('../../nfs/disk_operations.py', '.')
    # fa.put('../../nfs/py_scripts', '.')
    fa.sudo('mkdir -p /srv/scripts/py')
    fa.sudo('mv auto_mount_nfs.py /srv/scripts/py ; chmod 666 /srv/scripts/py/auto_mount_nfs.py')
    fa.sudo('mv hostname.py /srv/scripts/py ; chmod 666 /srv/scripts/py/hostname.py')
    fa.sudo('mv disk_operations.py /srv/scripts/py ; chmod 666 /srv/scripts/py/disk_operations.py')
    # fa.sudo('mv py_scripts /etc/cron.d ; chmod 640 /etc/cron.d/py_scripts ; chown root /etc/cron.d/py_scripts')


@fa.hosts('jboss-service-dev', 'jboss-service-beta', 'jboss-service-test', 'jboss-service-pre-prod', 'jboss-da-dev', 'jboss-da-beta', 'jboss-da-test', 'jboss-service2', 'jboss-da2', 'jboss-outgoing')
def release_auto_mount_nfs():
    fa.put('../../nfs/auto_mount_nfs.py', '.')
    fa.sudo('mv auto_mount_nfs.py /srv/scripts/py ; chmod 666 /srv/scripts/py/auto_mount_nfs.py')


# @fa.hosts('jboss-service-dev', 'jboss-service-beta', 'jboss-service-test', 'jboss-service-pre-prod', 'jboss-da-dev', 'jboss-da-beta', 'jboss-da-test')
@fa.hosts('jboss-service-beta', 'jboss-service-test', 'jboss-service-pre-prod', 'jboss-da-dev', 'jboss-da-beta', 'jboss-da-test')
def check_client():
    fa.sudo('python2.7 /srv/scripts/py/auto_mount_nfs.py')


@fa.hosts('jboss-da1', 'jboss-service1')
def release_server():
    fa.put('../../nfs/serve_mounts.py', '.')
    fa.sudo('mv serve_mounts.py /srv/scripts/py ; chmod 666 /srv/scripts/py/serve_mounts.py')


@fa.hosts('jboss-service-dev', 'jboss-service-beta', 'jboss-service-test', 'jboss-service-pre-prod', 'jboss-da-dev', 'jboss-da-beta', 'jboss-da-test')
def check_df():
    fa.run('df -hT ; ls -l /srv/smykstor_new/ ;')


@fa.hosts('jboss-service-dev', 'jboss-service-beta', 'jboss-service-test', 'jboss-service-pre-prod', 'jboss-da-dev', 'jboss-da-beta', 'jboss-da-test')
def start_client():
    fa.sudo("/usr/bin/python2.7 /srv/scripts/py/auto_mount_nfs.py")
