from fabric.api import sudo, put, run, env, hosts
import ass

env.hosts = ['jboss-eod-dev', 'jboss-da1', 'jboss-dwp0', 'jboss-dwp3', 'jboss-dwp-test1', 'jboss-cm', 'jboss-legal1', 'jboss-cm-test1', 'jboss-proc-test2', 'jboss-recognizer-dev1', 'jboss-teren-test1', 'jboss-core1', 'jboss-core-beta', 'jboss-dok-dev1', 'jboss-recognizer-test1', 'jboss-core-dev1', 'jboss-dok-test1', 'jboss-cti4-dev1', 'jboss-legal-dev1', 'jboss0', 'jboss-dnb-test', 'jboss-proc-beta', 'jboss-teren2', 'jboss-dwp-dev1', 'jboss-takto-test', 'jboss-recognizer2', 'jboss-core2', 'jboss-legal-test1', 'jboss-service2', 'jboss-cti2', 'jboss-core-test1', 'takto-web', 'jboss-recognizer-beta', 'jboss-cti-dev1', 'jboss-teren1', 'takto-esb-dev', 'jboss-legal2', 'jboss-da2', 'jboss-big', 'jboss-da-beta', 'jboss-proc-fix', 'jboss-dok', 'jboss-dnb', 'jboss-eod1', 'aster-jboss0', 'jboss-cti-test1', 'jboss-dok1', 'jboss-gw-test', 'jboss-cron-dev1', 'jboss-da-test1', 'jboss-dwp-beta', 'jboss-proc', 'jboss-proc-dev2', 'jboss-cti1', 'jboss-jbpm-test1', 'jboss-cron', 'jboss-groupwise', 'jboss-proc1', 'jboss-gw', 'jboss-jbpm2', 'jboss-service-dev1', 'jboss-da-dev1', 'jboss-cti4-test1', 'jboss-takto', 'jboss-proc-int', 'jboss-service1', 'jboss-service-test1', 'jboss-ecourt', 'jboss1', 'jboss-legal-beta', 'jboss-teren-dev1', 'jboss-cron-test1', 'jboss-eod2', 'jboss-service-beta', 'jboss-jbpm-beta', 'jboss-jbpm-dev1', 'jboss-takto-dev', 'jboss-cron-beta', 'jboss-recognizer1', 'eultimo-ext-test', 'jboss-takto-beta', 'jboss-outgoing']
env.hosts.extend(['jboss-gw', 'venus', 'jboss-venus', 'venus-dev'])
env.hosts.extend(['jboss-big-dev', 'jboss-test'])
env.hosts.extend(['jboss-proc-int-dev', 'jboss-eod1', 'jboss-eod2', 'dm04', 'eultimo1', ])
env.hosts.extend(['eultimo-proxy-test', 'takto-esb-beta', 'takto-esb', 'eultimo-proxy', 'dm-test', 'takto-web-test', 'jboss-takto-pre-prod', 'dev-ultimo-robin', 'takto-esb-pre-prod', 'dev-tools-drs2-b2', 'dev-tools-zrs5', ])
env.hosts.extend(['jboss-takto2', 'jboss-takto-pre-prod2', 'jboss-service-pre-prod', 'jboss-core-pre-prod', 'jboss-takto-beta2', 'takto-esb2', 'takto-esb-pre-prod2', 'takto-esb-beta2', ])
host_outsides = ['eultimo-proxy-test']
host_shutdown = ['jboss-up', 'jboss-up-test', 'jboss-proc-int-test1']
env.password = ass.read_pass()


def scprhq():
    put('rhq-agent.sh', '~')
    sudo('mv rhq-agent.sh /srv/rhq-agent/bin/')


def cp_config_rhq():
    sudo('rm -f rhq-agent-env.sh')
    sudo('rm -f rhq-agent.sh')
    sudo('rm -f check_agent_rhq.py')
    put('../rhq/rhq-agent-env.sh', '~')
    put('../rhq/rhq-agent.sh', '~')
    put('../rhq/check_agent_rhq.py', '~')
    sudo('mv rhq-agent-env.sh /srv/rhq-agent/bin/')
    sudo('mv rhq-agent.sh /srv/rhq-agent/bin/')
    sudo('mv check_agent_rhq.py /etc/cron.daily/')
    sudo('chmod 755 /srv/rhq-agent/bin/rhq-agent.sh')
    sudo('chmod 755 /etc/cron.daily/check_agent_rhq.py')
    sudo('chown root /etc/cron.daily/check_agent_rhq.py')
    sudo('monit stop rhq-agent')
    sudo('''if [ -d "/etc/monit.d" ]; then rm -f /etc/monit.d/rhq.monitrc ;
            elif [ -d "/etc/monit" ] ; then rm -f /etc/monit/rhq.monitrc ;
            elif [ -d "/etc/monitd" ] ; then rm -f /etc/monitd/rhq.monitrc ;
            fi''')
    sudo('monit reload')
    sudo('/srv/rhq-agent/bin/rhq-agent-wrapper.sh restart')
    run('.')


def scpcheckpy():
    sudo('rm -f check_agent_rhq.py')
    put('../rhq/check_agent_rhq.py', '~')
    sudo('mv check_agent_rhq.py /etc/cron.daily/')


@hosts('jboss-cti4-dev1', 'jboss-cti4-test1', 'jboss-cti-test1')
def stop_rhq_monit():
    sudo('''if [ -d "/etc/monit.d" ]; then rm -f /etc/monit.d/rhq.monitrc ;
            elif [ -d "/etc/monit" ] ; then rm -f /etc/monit/rhq.monitrc ;
            elif [ -d "/etc/monitd" ] ; then rm -f /etc/monitd/rhq.monitrc ;
            fi''')
    sudo('monit reload')


def restart_rhq():
    '''Stopped procces will bring up by the SystemD | cron-py'''
    sudo('/srv/rhq-agent/bin/rhq-agent-wrapper.sh stop')


def stop_rhq_systemD():
    sudo('systemctl stop rhq-agent.service')
    sudo('systemctl disable rhq-agent.service')
    sudo('systemctl daemon-reload')


def stop_rhq_py():
    sudo('/srv/rhq-agent/bin/rhq-agent-wrapper.sh stop')
    sudo('if [ -f /etc/cron.hourly/check_agent_rhq.py ]; then rm -f /etc/cron.hourly/check_agent_rhq.py; fi')
    sudo('if [ -f /etc/cron.daily/check_agent_rhq.py ]; then rm -f /etc/cron.hourly/check_agent_rhq.py; fi')


@hosts('jboss-big-dev', 'jboss-cron-dev1', 'jboss-cti-dev1', 'jboss-dwp-dev1', 'jboss-jbpm-dev1', 'jboss-legal-dev1',  'jboss-service-dev1', 'jboss-teren-dev1')
def stop_low_mem_rhq():
    sudo('/srv/rhq-agent/bin/rhq-agent-wrapper.sh stop')
    sudo('systemctl stop rhq-agent')
    sudo('systemctl disable rhq-agent')


def add_cron_rhq_restart():
    # put('../rhq/stop_rhq_agent')
    put('../rhq/stop_rhq_agent.py')
    # sudo('mv stop_rhq_agent /etc/cron.d')
    # sudo('chown root /etc/cron.d/stop_rhq_agent')
    # sudo('ls -l /etc/cron.d')
    sudo('mv stop_rhq_agent.py /srv/scripts/py ; chown jrokicki /srv/scripts/py/stop_rhq_agent.py ; chmod 744 /srv/scripts/py/stop_rhq_agent.py')
    # sudo('ls -l /srv/scripts/py')


def rename_cron_rhq():
    env.warn_only = True
    sudo("if [[ -f /etc/cron.d/stop_rhq_agent ]] ; then mv /etc/cron.d/stop_rhq_agent /etc/cron.d/py_scripts ; fi")


# @hosts('jboss-da-dev1', 'jboss-cti4-dev1', 'jboss-cron-dev1', 'jboss-takto-dev', 'jboss-dok-test1', 'jboss-eod-dev', 'jboss-takto-test', 'jboss-teren-test1', 'venus-dev', 'eultimo-proxy-test', 'jboss-dwp-test1', 'dev-ultimo-robin', 'jboss-cron-test1', 'jboss-legal-dev1', 'jboss-jbpm-beta', 'jboss-cm-test1', 'jboss-core-test1', 'jboss-dwp-dev1', 'jboss-recognizer-test1', 'jboss-legal-test1', 'jboss-service-test1', 'jboss-test', 'dev-tools-zrs5', 'jboss-jbpm-dev1', 'dev-tools-drs2-b2', 'jboss-cti4-test1', 'jboss-proc-dev2', 'jboss-teren-dev1', 'eultimo-ext-test', 'dm-test', 'jboss-recognizer-dev1', 'jboss-dok-dev1', 'takto-web-test', 'jboss-service-dev1', 'jboss-gw-test', 'jboss-jbpm-test1', 'jboss-dnb-test', 'jboss-da-test1', 'jboss-big-dev', 'jboss-cti-dev1', 'jboss-core-dev1', 'jboss-proc-int-dev', 'jboss-cti-test1', 'takto-esb-dev')
# @hosts('jboss-core-pre-prod', 'jboss-service-pre-prod','jboss-takto-pre-prod', 'jboss-takto-pre-prod2',  'takto-esb-pre-prod', 'takto-esb-pre-prod2',)
@hosts('jboss-da-test', 'jboss-dok-test1', 'jboss-cti4-test', 'jboss-cti4-dev', 'jboss-teren-test', 'jboss-cm-test', 'jboss-legal-test', 'jboss-dwp-test', 'jboss-service-test', 'jboss-cti-test', 'jboss-core-test', 'jboss-jbpm-test', 'dm-test', 'jboss-eod-test')
def remove_rhq():
    env.warn_only = True
    # sudo("service rhq-agent stop ; systemctl disable rhq-agent ; sed -i 's/\(.*stop_rhq_agent.*\)/#\\1/' /etc/cron.d/py_scripts")
    # sudo("/srv/rhq-agent/bin/rhq-agent-wrapper.sh stop")
    # sudo("systemctl stop rhq-agent.service  ; systemctl disable rhq-agent.service ; /srv/rhq-agent/bin/rhq-agent-wrapper.sh stop ")
    # sudo("sed -i 's/\(.*stop_rhq_agent.*\)/#\\1/' /etc/cron.d/py_scripts")
    sudo("mv /etc/cron.hourly/check_agent_rhq.py ~jrokicki/")


@hosts('aster-jboss0', 'dm04', 'eultimo-proxy', 'eultimo1', 'jboss-big', 'jboss-cm', 'jboss-core1', 'jboss-core2', 'jboss-cron', 'jboss-cti1', 'jboss-cti2', 'jboss-da1', 'jboss-da2', 'jboss-dnb', 'jboss-dok', 'jboss-dok1', 'jboss-dwp0', 'jboss-dwp3', 'jboss-ecourt', 'jboss-eod1', 'jboss-eod1', 'jboss-eod2', 'jboss-eod2', 'jboss-groupwise', 'jboss-gw', 'jboss-gw', 'jboss-jbpm2', 'jboss-legal1', 'jboss-legal2', 'jboss-outgoing', 'jboss-proc', 'jboss-proc-fix', 'jboss-proc-int', 'jboss-proc-test2', 'jboss-proc1', 'jboss-recognizer1', 'jboss-recognizer2', 'jboss-service1', 'jboss-service2', 'jboss-takto', 'jboss-takto2', 'jboss-teren1', 'jboss-teren2', 'jboss-venus', 'jboss0', 'jboss1', 'takto-esb', 'takto-esb2', 'takto-web', 'venus')
def restore_rhq():
    env.warn_only = True
    # sudo("systemctl enable rhq-agent.service ; /srv/rhq-agent/bin/rhq-agent-wrapper.sh start ; sed -i 's/#\\+\(.*stop_rhq_agent.*\)/\\1/' /etc/cron.d/py_scripts")
    sudo("systemctl restart rhq-agent.service")
