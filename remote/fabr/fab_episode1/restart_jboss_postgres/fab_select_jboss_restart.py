import fabric.api as fa
import ass
fa.env.hosts = ['jboss-eod-dev', 'jboss-da1', 'jboss-dwp0', 'jboss-dwp3', 'jboss-dwp-test1', 'jboss-cm', 'jboss-legal1', 'jboss-cm-test1', 'jboss-proc-test2', 'jboss-recognizer-dev1', 'jboss-teren-test1', 'jboss-core1', 'jboss-core-beta', 'jboss-dok-dev1', 'jboss-recognizer-test1', 'jboss-core-dev1', 'jboss-dok-test1', 'jboss-cti4-dev1', 'jboss-legal-dev1', 'jboss0', 'jboss-dnb-test', 'jboss-proc-beta', 'jboss-teren2', 'jboss-dwp-dev1', 'jboss-takto-test', 'jboss-recognizer2', 'jboss-core2', 'jboss-legal-test1', 'jboss-service2', 'jboss-cti2', 'jboss-core-test1', 'takto-web', 'jboss-recognizer-beta', 'jboss-cti-dev1', 'jboss-teren1', 'takto-esb-dev', 'jboss-legal2', 'jboss-da2', 'jboss-big', 'jboss-da-beta', 'jboss-proc-fix', 'jboss-dok', 'jboss-dnb', 'jboss-eod1', 'aster-jboss0', 'jboss-cti-test1', 'jboss-dok1', 'jboss-gw-test', 'jboss-cron-dev1', 'jboss-da-test1', 'jboss-dwp-beta', 'jboss-proc', 'jboss-proc-dev2', 'jboss-cti1', 'jboss-jbpm-test1', 'jboss-cron', 'jboss-groupwise', 'jboss-proc1', 'jboss-gw', 'jboss-jbpm2', 'jboss-service-dev1', 'jboss-da-dev1', 'jboss-cti4-test1', 'jboss-takto', 'jboss-proc-int', 'jboss-service1', 'jboss-service-test1', 'jboss-ecourt', 'jboss1', 'jboss-legal-beta', 'jboss-teren-dev1', 'jboss-cron-test1', 'jboss-eod2', 'jboss-service-beta', 'jboss-jbpm-beta', 'jboss-jbpm-dev1', 'jboss-takto-dev', 'jboss-cron-beta', 'jboss-recognizer1', 'eultimo-ext-test', 'jboss-takto-beta', 'jboss-outgoing']
fa.env.hosts.extend(['jboss-gw', 'venus', 'jboss-venus', 'venus-dev'])
fa.env.hosts.extend(['jboss-big-dev', 'jboss-test'])
fa.env.hosts.extend(['jboss-proc-int-dev', 'jboss-eod1', 'jboss-eod2', 'dm04', 'eultimo1', ])
fa.env.hosts.extend(['eultimo-proxy-test', 'takto-esb-beta', 'takto-esb-dev', 'eultimo-proxy', 'dm-test', 'takto-web-test', 'jboss-takto-pre-prod', 'dev-ultimo-robin', 'takto-esb-pre-prod', 'dev-tools-drs2-b2', 'dev-tools-zrs5', ])
fa.env.hosts.extend(['jboss-takto2', 'jboss-takto-pre-prod2', 'jboss-service-pre-prod', 'jboss-core-pre-prod', 'jboss-takto-beta2', ])

'''env.hosts = ['jboss-gw-test', 'jboss-service-beta', 'jboss-cron-beta', 'aster-jboss0', 'jboss0', 'jboss1', 'jboss-big', 'jboss-cm', 'jboss-cm-test1', 'jboss-core1', 'jboss-core2', 'jboss-core-beta', 'jboss-core-dev1', 'jboss-core-test1', 'jboss-cosmo-dev1', 'jboss-cosmo-test1', 'jboss-cron', 'jboss-cron-dev1', 'jboss-cron-test1', 'jboss-cti1', 'jboss-cti2', 'jboss-cti4-dev1', 'jboss-cti4-test1', 'jboss-cti-dev1', 'jboss-cti-test1', 'jboss-da1', 'jboss-da2', 'jboss-da-beta', 'jboss-da-dev1', 'jboss-da-test1', 'jboss-dok', 'jboss-dok1', 'jboss-dok-dev1', 'jboss-dok-test1', 'jboss-dwp0', 'jboss-dwp3', 'jboss-dwp-dev1', 'jboss-dwp-test1', 'jboss-groupwise', 'jboss-jbpm2', 'jboss-jbpm-beta', 'jboss-jbpm-dev1', 'jboss-jbpm-test1', 'jboss-legal1', 'jboss-legal2', 'jboss-legal-beta', 'jboss-legal-dev1', 'jboss-legal-test1', 'jboss-proc', 'jboss-proc1', 'jboss-proc-beta', 'jboss-proc-dev2', 'jboss-proc-int', 'jboss-proc-test2', 'jboss-recognizer1', 'jboss-recognizer2', 'jboss-recognizer-beta', 'jboss-recognizer-dev1', 'jboss-recognizer-test1', 'jboss-service1', 'jboss-service2', 'jboss-service-dev1', 'jboss-service-test1', 'jboss-teren1', 'jboss-teren2', 'jboss-teren-dev1', 'jboss-teren-test1', 'jbpm-test', 'eultimo-ext-test', 'dm-test', 'jboss-takto-dev', 'jboss-dwp-beta', 'venus-dev', 'jboss-takto-test', 'jboss-dnb-test', 'jboss-dnb', ]
env.hosts.extend(['jboss-venus', 'jboss-cti1', 'jboss-cti2', ])
host_excluded = ['jboss-big-dev', 'jboss-test', 'eultimo-proxy-test', ]
host_excluded = ['dm01', 'dm04', 'eultimo1', 'jboss-proc-test1', ]
host_shutdown = ['jboss-up', 'jboss-up-test', 'jboss-cosmo1', 'jboss-cosmo2', 'jboss-proc-int-test1', ]
'''
fa.env.password = ass.read_ass(file='../../ass/ass')
fa.env.warn_only = True


def deploy():
    fa.sudo("mkdir -p /srv/scripts/py")
    fa.sudo("mv jboss_restart_log.py /srv/scripts/py/")


def scphome():
    fa.put('../select_jboss_restart.py', '~')


def check_jboss_home():
    fa.sudo("ls -l /srv/jboss")


# @fa.hosts('dm-test')
def check_initd_jboss():
    fa.sudo("grep -i funcname /etc/init.d/*")
