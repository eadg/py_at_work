import fabric.api as fapi
import ass

fapi.env.password = ass.read_pass()
fapi.env.warn_only = True


@fapi.hosts('jboss-jbpm2', 'jboss-jbpm-dev', 'jboss-jbpm-test', 'jboss-jbpm-beta')
def release():
    fapi.put('../../release_jboss_w_postgres/check_instance_jboss/check_run_jboss4.py', '.')
    fapi.sudo('mv check_run_jboss4.py /srv/scripts/py/ ;  chmod 644 /srv/scripts/py/check_run_jboss4.py')
