import fabric.api as fa
import ass


fa.env.warn_only = True
fa.env.password = ass.read_ass(file='../../ass/ass')
fa.env.hosts = ['jboss-eod-dev', 'jboss-da1', 'jboss-dwp0', 'jboss-dwp3', 'jboss-dwp-test1', 'jboss-cm', 'jboss-legal1', 'jboss-cm-test1', 'jboss-proc-test2', 'jboss-recognizer-dev1', 'jboss-teren-test1', 'jboss-core1', 'jboss-core-beta', 'jboss-dok-dev1', 'jboss-recognizer-test1', 'jboss-core-dev1', 'jboss-dok-test1', 'jboss-cti4-dev1', 'jboss-legal-dev1', 'jboss0', 'jboss-dnb-test', 'jboss-proc-beta', 'jboss-teren2', 'jboss-dwp-dev1', 'jboss-takto-test', 'jboss-recognizer2', 'jboss-core2', 'jboss-legal-test1', 'jboss-service2', 'jboss-cti2', 'jboss-core-test1', 'takto-web', 'jboss-recognizer-beta', 'jboss-cti-dev1', 'jboss-teren1', 'takto-esb-dev', 'jboss-legal2', 'jboss-da2', 'jboss-big', 'jboss-da-beta', 'jboss-proc-fix', 'jboss-dok', 'jboss-dnb', 'jboss-eod1', 'aster-jboss0', 'jboss-cti-test1', 'jboss-dok1', 'jboss-gw-test', 'jboss-cron-dev1', 'jboss-da-test1', 'jboss-dwp-beta', 'jboss-proc', 'jboss-proc-dev2', 'jboss-cti1', 'jboss-jbpm-test1', 'jboss-cron', 'jboss-groupwise', 'jboss-proc1', 'jboss-gw', 'jboss-jbpm2', 'jboss-service-dev1', 'jboss-da-dev1', 'jboss-cti4-test1', 'jboss-takto', 'jboss-proc-int', 'jboss-service1', 'jboss-service-test1', 'jboss-ecourt', 'jboss1', 'jboss-legal-beta', 'jboss-teren-dev1', 'jboss-cron-test1', 'jboss-eod2', 'jboss-service-beta', 'jboss-jbpm-beta', 'jboss-jbpm-dev1', 'jboss-takto-dev', 'jboss-cron-beta', 'jboss-recognizer1', 'eultimo-ext-test', 'jboss-takto-beta', 'jboss-outgoing']
fa.env.hosts.extend(['jboss-gw', 'venus', 'jboss-venus', 'venus-dev'])
fa.env.hosts.extend(['jboss-big-dev', 'jboss-test'])
fa.env.hosts.extend(['jboss-proc-int-dev', 'jboss-eod1', 'jboss-eod2', ])
fa.env.hosts.extend(['eultimo-proxy-test', 'dm-test', 'takto-web-test', 'jboss-takto-pre-prod', 'dev-tools-drs2-b2', 'dev-tools-zrs5', ])
fa.env.hosts.extend(['jboss-takto2', 'jboss-takto-pre-prod2', 'jboss-service-pre-prod', 'jboss-core-pre-prod', 'jboss-takto-beta2', ])

host_dmz = ['dm04', 'eultimo1', 'takto-esb-beta', 'takto-esb', 'takto-esb-pre-prod', 'dev-ultimo-robin', 'eultimo-proxy', 'takto-esb-beta2', 'takto-esb2', 'takto-esb-pre-prod2', ]
host_excluded = ['jboss-big-dev', 'jboss-test']
host_shutdown = ['jboss-up', 'jboss-up-test', 'jboss-proc-test1', 'jboss-proc-dev1', 'jboss-cosmo1', 'jboss-cosmo2', 'jboss-cosmo-dev1', 'jboss-cosmo-test1', 'jboss-proc-int-test1', ]


def check_initd_jboss():
    fa.sudo("grep -i funcname /etc/init.d/*")


def scp_jrl_long():
    fa.put('../../restarts_jboss/jboss_restart_log.py', '~')
    fa.put('../../ass/ass.py', '~')
    fa.put('../../restarts_jboss/hostname.py', '~')
    fa.put('../../restarts_jboss/ass', '~')
    fa.sudo("mkdir -p /srv/scripts/py")
    fa.sudo("mv jboss_restart_log.py /srv/scripts/py/; mv ass.py /srv/scripts/py/; mv ass /srv/scripts/py/; mv hostname.py /srv/scripts/py/; chown root /srv/scripts/py/ass; chmod 400 /srv/scripts/py/ass;")


@fa.hosts('jboss-venus', 'jbpm-test', 'venus-dev', 'dm-test', )
def scp_jrl_short():
    fa.put('../../restarts_jboss/jboss_restart_log_home.py', '~')
    fa.sudo("mkdir -p /srv/scripts/py")
    fa.sudo("mv jboss_restart_log_home.py /srv/scripts/py/jboss_restart_log.py")


@fa.hosts(' takto-esb-pre-prod', 'takto-esb-pre-prod2', 'takto-esb2')
def scp_one_host_long():
    fa.put('../../restarts_jboss/hostname.py', '~')
    fa.put('../../restarts_jboss/jboss_restart_log.py', '~')
    fa.put('../../ass/ass.py', '~')
    fa.put('../../restarts_jboss/ass', '~')
    fa.sudo("mkdir -p /srv/scripts/py")
    fa.sudo("mv jboss_restart_log.py /srv/scripts/py/; mv ass.py /srv/scripts/py/; mv ass /srv/scripts/py/; mv hostname.py /srv/scripts/py/; chown root /srv/scripts/py/ass; chmod 400 /srv/scripts/py/ass;")


def scp_jrl_all():
    scp_jrl_long()
    scp_jrl_short()
    # scp_jrl_jboss4_long()


def scp_select_release():
    fa.put('../../restarts_jboss/select_jboss_restart.py', '~')
    fa.sudo("mkdir -p /srv/scripts/py")
    fa.sudo("mv select_jboss_restart.py /srv/scripts/py/")
    fa.sudo("chmod 744 /srv/scripts/py/select_jboss_restart.py")


'''
def scp_jrl_jboss4_long():
    @hosts()
    put('../../restarts_jboss/jboss_restart_log_jboss4.py', '~')
    put('../../restarts_jboss/securing_jmx/jboss4.py', '~')
    put('../../restarts_jboss/securing_jmx/web.xml', '~')
    sudo("mkdir -p /srv/scripts/py/securing_jmx")
    sudo("mv jboss_restart_log_jboss4.py /srv/scripts/py/jboss_restart_log.py")
    sudo("mv jboss4.py /srv/scripts/py/securing_jmx")
    sudo("mv web.xml /srv/scripts/py/securing_jmx")
    sudo("touch /srv/scripts/py/securing_jmx/__init__.py")
'''
