import fabric.api as fa
import ass

fa.env.password = ass.read_ass(file='../../ass/ass')
fa.env.warn_only = True


# @fa.hosts('jboss-eod1', 'jboss-eod2', 'jboss-outgoing', 'jboss-takto', 'takto-web', 'jboss-takto2', )
@fa.hosts('jboss-eod2', 'jboss-outgoing', 'jboss-takto', 'takto-web', 'jboss-takto2', )
def cp_pyfiles():
    fa.put('../../rhq/py_scripts', '.')
    fa.put('../../rhq/stop_rhq_agent.py', '.')
    fa.sudo("mkdir /srv/scripts/py/ -p ; mv py_scripts /etc/cron.d/ ; chown root /etc/cron.d/py_scripts ; mv stop_rhq_agent.py /srv/scripts/py/ ; chmod 744 /srv/scripts/py/stop_rhq_agent.py")
