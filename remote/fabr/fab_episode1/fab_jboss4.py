from fabric.api import *
import ass

env.hosts = ['jboss-gw-test', 'jboss-service-beta', 'jboss-cron-beta', 'aster-jboss0', 'dm04', 'eultimo1', 'jboss0', 'jboss1', 'jboss-big', 'jboss-cm', 'jboss-cm-test1', 'jboss-core1', 'jboss-core2', 'jboss-core-beta', 'jboss-core-dev1', 'jboss-core-test1', 'jboss-cosmo1', 'jboss-cosmo2', 'jboss-cosmo-dev1', 'jboss-cosmo-test1', 'jboss-cron', 'jboss-cron-dev1', 'jboss-cron-test1', 'jboss-cti1', 'jboss-cti2', 'jboss-cti4-dev1', 'jboss-cti4-test1', 'jboss-cti-dev1', 'jboss-cti-test1', 'jboss-da1', 'jboss-da2', 'jboss-da-beta', 'jboss-da-dev1', 'jboss-da-test1', 'jboss-dok', 'jboss-dok1', 'jboss-dok-dev1', 'jboss-dok-test1', 'jboss-dwp0', 'jboss-dwp3', 'jboss-dwp-dev1', 'jboss-dwp-test1', 'jboss-groupwise', 'jboss-jbpm2', 'jboss-jbpm-beta', 'jboss-jbpm-dev1', 'jboss-jbpm-test1', 'jboss-legal1', 'jboss-legal2', 'jboss-legal-beta', 'jboss-legal-dev1', 'jboss-legal-test1', 'jboss-proc', 'jboss-proc1', 'jboss-proc-beta', 'jboss-proc-dev2', 'jboss-proc-int', 'jboss-proc-int-test1', 'jboss-proc-test2', 'jboss-recognizer1', 'jboss-recognizer2', 'jboss-recognizer-beta', 'jboss-recognizer-dev1', 'jboss-recognizer-test1', 'jboss-service1', 'jboss-service2', 'jboss-service-dev1', 'jboss-service-test1', 'jboss-teren1', 'jboss-teren2', 'jboss-teren-dev1', 'jboss-teren-test1', 'jbpm-test', 'eultimo-ext-test', 'dm-test', 'jboss-takto-dev', 'jboss-dwp-beta', 'venus-dev', 'jboss-takto-test', 'jboss-dnb-test', 'jboss-dnb', ]
env.hosts.extend(['jboss-gw', 'venus', 'jboss-venus'])
env.hosts.extend(['jboss-big-dev', 'jboss-test'])
host_outsides = ['eultimo-proxy-test']
host_shutdown = ['jboss-up', 'jboss-up-test', 'jboss-proc-test1', ]
env.password = ass.read_pass()


def mem():
    run("grep MemTotal /proc/meminfo")


def check_rhq():
    sudo("ps aux | grep endorsed | grep java")


def scprhq_agent():
    put('rhq-agent.sh', '~')
    sudo('mv rhq-agent.sh /srv/rhq-agent/bin/')


def scprhq_install():
    put('/home/jrokicki/install_bin/tools_admin/rhq-enterprise-agent-4.13.1.jar', '~')
    put('/home/jrokicki/tickets/rhq/systemd/2/packrhq.tgz', '~')


def check_jboss_home():
    sudo("ls -l /srv/jboss")


def check_initd_jboss():
    sudo("grep -i funcname /etc/init.d/*")


def chownrhq():
    sudo('chown root /srv/rhq-agent/bin/rhq-agent.sh')
    sudo('chmod 755 /srv/rhq-agent/bin/rhq-agent.sh')


def check_monit_systemctl():
    # sudo("psmon=`ps aux | grep n/monit | grep -v grep` ; if [[ -z $psmon ]] ; then systemctl restart rhq-agent ; else monit restart rhq-agent ; fi")
    run("psmon=`ps aux | grep /monit | grep -v grep` ; if [[ -z $psmon ]] ; then echo 'jest systemctl' ; else echo 'jest monit' ; fi")


def check_etc_hostname():
    run("egrep '(10|192|172)' /etc/hosts | grep -v virt")


def cp_java_link():
    sudo("cp -a /srv/java/jdk /srv/java/rhq")
