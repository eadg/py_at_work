from fabric.tasks import Task
import fabric.api
import fabric.context_managers
import mgmt_port
import fabric.api
import ass


class elk_cli(Task):

    _ports = mgmt_port.Mgmt().port
    fabric.api.env.password = ass.read_pass()

    def run_long(self, filecli):
        host_str = fabric.api.env.host_string
        fabric.api.put('../../elk/' + filecli)
        with fabric.context_managers.shell_env(JAVA_HOME='/srv/java/jdk/'):
            if host_str in self._ports:
                host = host_str + ':' + self._ports[host_str]
            else:
                host = host_str+':9990'
            comm = "/srv/jboss/jboss/bin/jboss-cli.sh -c controller=" + host + " --user=admin --password=****** --file=" + filecli
            fabric.api.run(comm)

    def run_short(self, filecli):
        host_str = fabric.api.env.host_string
        fabric.api.put('../../elk/' + filecli)
        with fabric.context_managers.shell_env(JAVA_HOME='/srv/java/jdk/'):
            if host_str in self._ports:
                host = host_str + ':' + self._ports[host_str]
            else:
                host = host_str+':9990'
            comm = "/srv/jboss/bin/jboss-cli.sh -c controller=" + host + " --user=admin --password=****** --file=" + filecli
            fabric.api.run(comm)
