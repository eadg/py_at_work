import fabric.api
import fabric.context_managers
from fabric.api import hosts
import mgmt_port
from elk_overall import elk_cli

fabric.api.env.warn_only = True

_ports = mgmt_port.Mgmt().port


def _putGelf8():
    fabric.api.put('../../elk/gelf.tar')
    mkdir8 = 'mkdir -p /srv/jboss/jboss/modules/system/layers/base/biz/paluch/logging/main'
    mvdir8 = 'mv gelf/* /srv/jboss/jboss/modules/system/layers/base/biz/paluch/logging/main'
    fabric.api.run('tar xfv gelf.tar')
    fabric.api.sudo(mkdir8)
    fabric.api.sudo(mvdir8)


@hosts('jboss-proc-beta', 'jboss-service-beta', 'jboss-cron-beta', 'jboss-da-beta', 'jboss-legal-beta')
def _remove_elk_jboss8_beta():
    elk_cli().run_long('remove_elk.cli')


@hosts('jboss-cron-dev', 'jboss-core-dev', 'jboss-da-dev', 'jboss-legal-dev', 'jboss-teren-dev', 'jboss-service-dev', 'jboss-proc-dev', 'jboss-takto-dev', )
def _remove_elk_jboss8_dev():
    elk_cli().run_long('remove_elk.cli')


@hosts('jboss-cron-dev', 'jboss-core-dev', 'jboss-da-dev', 'jboss-legal-dev', 'jboss-service-dev', 'jboss-proc-dev', 'jboss-takto-dev', 'jboss-teren-dev', )
def add_elk_jboss8_dev():
    _putGelf8()
    elk_cli().run_long('remove_elk.cli')
    elk_cli().run_long('add_elk.cli')


@hosts('jboss-cron-beta', 'jboss-da-beta', 'jboss-legal-beta', 'jboss-proc-beta', 'jboss-service-beta', )
def add_elk_jboss8_beta():
    _putGelf8()
    elk_cli().run_long('remove_elk.cli')
    elk_cli().run_long('add_elk.cli')


@hosts('jboss-cron-test', 'jboss-da-test', 'jboss-dnb-test', 'jboss-legal-test', 'jboss-proc-test', 'jboss-service-test', 'jboss-takto-test', 'jboss-teren-test', )
def add_elk_jboss8_test():
    _putGelf8()
    elk_cli().run_long('remove_elk.cli')
    elk_cli().run_long('add_elk.cli')


@hosts('jboss-cosmo-dev', 'jboss-dok-dev', 'jboss-recognizer-dev', )
def add_elk_jboss78_dev():
    _putGelf8()
    elk_cli().run_long('remove_elk.cli')
    elk_cli().run_long('add_elk.cli')


@hosts('jboss-core-beta', 'jboss-recognizer-beta', )
def add_elk_jboss78_beta():
    _putGelf8()
    elk_cli().run_long('remove_elk.cli')
    elk_cli().run_long('add_elk.cli')


@hosts('jboss-core-test', 'jboss-cosmo-test', 'jboss-dok-test', 'jboss-recognizer-test')
def add_elk_jboss78_test():
    _putGelf8()
    elk_cli().run_long('remove_elk.cli')
    elk_cli().run_long('add_elk.cli')


@hosts('jboss-cm')
def add_elk_one_jboss():
    _putGelf8()
    # elk_cli().run_long('remove_elk.cli')
    elk_cli().run_long('add_elk_one_jb8.cli')
