class Mgmt(object):

    port = {'jboss-takto-dev': '9999', 'jboss-cosmo-dev': '9999',
            'jboss-dok-dev': '9999', 'jboss-recognizer-dev': '9999',
            'jboss-core-beta': '9999', 'jboss-recognizer-beta': '9999',
            'jboss-cti-dev': '9999', 'jboss-takto-test': '9999',
            'jboss-core-test': '9999', 'jboss-cosmo-test': '9999',
            'jboss-dok-test': '9999', 'jboss-recognizer-test': '9999',
            'venus': '9999'}
