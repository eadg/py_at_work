import fabric.api
import fabric.context_managers
from fabric.api import hosts
import mgmt_port
from elk_overall import elk_cli

fabric.api.env.warn_only = True

_ports = mgmt_port.Mgmt().port


def _put_gelf_7long():
    fabric.api.put('../../elk/gelf.tar')
    mkdir8 = 'mkdir -p /srv/jboss/jboss/modules/biz/paluch/logging/main'
    fabric.api.sudo(mkdir8)
    fabric.api.run('tar xfv gelf.tar')
    mvdir8 = 'mv gelf/* /srv/jboss/jboss/modules/biz/paluch/logging/main'
    fabric.api.sudo(mvdir8)


def _put_gelf_7short():
    fabric.api.put('../../elk/gelf.tar')
    mkdir8 = 'mkdir -p /srv/jboss/modules/biz/paluch/logging/main'
    fabric.api.sudo(mkdir8)
    fabric.api.run('tar xfv gelf.tar')
    mvdir8 = 'mv gelf/* /srv/jboss/modules/biz/paluch/logging/main'
    fabric.api.sudo(mvdir8)


@hosts('jboss-cti-dev', 'venus-dev', )
def _remove_elk_jboss7_dev():
    elk_cli().run_long('remove_elk.cli')


@hosts('jboss-gw-test', 'jboss-cm-test', 'jboss-cti-test', 'jboss-proc-int-test', 'eultimo-ext-test', 'dm-test', 'jbpm-test')
def _remove_elk_jboss7_test():
    elk_cli().run_long('remove_elk.cli')


@hosts('eultimo1', )
def add_elk_jboss7_long_dev():
    _put_gelf_7long()
    elk_cli().run_long('add_elk.cli')


@hosts('venus-dev', )
def add_elk_jboss7_short_dev():
    _put_gelf_7short()
    elk_cli().run_short('add_elk.cli')


@hosts('jboss-gw-test', 'jboss-cm-test', 'jboss-cti-test', 'jboss-proc-int-test', 'eultimo-ext-test', )
def add_elk_jboss7_long_test():
    _put_gelf_7long()
    elk_cli().run_long('add_elk.cli')


@hosts('jboss-venus')
def add_elk_jboss7_short_test():
    _put_gelf_7short()
    elk_cli().run_short('add_elk_one.cli')
