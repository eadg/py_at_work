import fabric.api as fa
import ass

fa.env.password = ass.read_ass(file='../../ass/ass')
fa.env.warn_only = True
fa.env.hosts = ['tomcat-int1', 'tomcat-int2', 'tomcat-ext1', 'tomcat-ext2', 'tomcat-drools-test','tomcat-drools']


# @fa.hosts('jboss-eod1', 'jboss-eod2', 'jboss-outgoing', 'jboss-takto', 'takto-web', 'jboss-takto2', )
# @fa.hosts('tomcat-int1', 'tomcat-int2', 'tomcat-ext1', 'tomcat-ext2', 'tomcat-drools-test', 'tomcat-teren0', 'tomcat-teren1', 'tomcat-teren', 'tomcat-drools')
# @fa.hosts('tomcat-int1', 'tomcat-int2', 'tomcat-ext1', 'tomcat-ext2', 'tomcat-drools-test','tomcat-drools')
def get_info():
    # fa.put('../../rhq/stop_rhq_agent.py', '.')
    fa.sudo("ps aux | egrep '(catalina|nginx: master|httpd|java)'| grep -v grep ; systemctl | service --status-all;")


# @fa.hosts('tomcat-int1', 'tomcat-int2', 'tomcat-ext1', 'tomcat-ext2', 'tomcat-drools-test',)
def get_version():
    # fa.put('../../rhq/stop_rhq_agent.py', '.')
    fa.sudo("/srv/tomcat/tomcat/bin/version.sh")
