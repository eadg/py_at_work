from fabric.api import run, sudo, env, put, hosts
import ass

env.hosts = ['jboss-eod-dev', 'jboss-da1', 'jboss-dwp0', 'jboss-dwp3', 'jboss-dwp-test1', 'jboss-cm', 'jboss-legal1', 'jboss-cm-test1', 'jboss-proc-test2', 'jboss-recognizer-dev1', 'jboss-teren-test1', 'jboss-core1', 'jboss-core-beta', 'jboss-dok-dev1', 'jboss-recognizer-test1', 'jboss-core-dev1', 'jboss-dok-test1', 'jboss-cti4-dev1', 'jboss-legal-dev1', 'jboss0', 'jboss-dnb-test', 'jboss-proc-beta', 'jboss-teren2', 'jboss-dwp-dev1', 'jboss-takto-test', 'jboss-recognizer2', 'jboss-core2', 'jboss-legal-test1', 'jboss-service2', 'jboss-cti2', 'jboss-core-test1', 'takto-web', 'jboss-recognizer-beta', 'jboss-cti-dev1', 'jboss-teren1', 'takto-esb-dev', 'jboss-legal2', 'jboss-da2', 'jboss-big', 'jboss-da-beta', 'jboss-proc-fix', 'jboss-dok', 'jboss-dnb', 'jboss-eod1', 'aster-jboss0', 'jboss-cti-test1', 'jboss-dok1', 'jboss-gw-test', 'jboss-cron-dev1', 'jboss-da-test1', 'jboss-dwp-beta', 'jboss-proc', 'jboss-proc-dev2', 'jboss-cti1', 'jboss-jbpm-test1', 'jboss-cron', 'jboss-groupwise', 'jboss-proc1', 'jboss-gw', 'jboss-jbpm2', 'jboss-service-dev1', 'jboss-da-dev1', 'jboss-cti4-test1', 'jboss-takto', 'jboss-proc-int', 'jboss-service1', 'jboss-service-test1', 'jboss-ecourt', 'jboss1', 'jboss-legal-beta', 'jboss-teren-dev1', 'jboss-cron-test1', 'jboss-eod2', 'jboss-service-beta', 'jboss-jbpm-beta', 'jboss-jbpm-dev1', 'jboss-takto-dev', 'jboss-cron-beta', 'jboss-recognizer1', 'eultimo-ext-test', 'jboss-takto-beta', 'jboss-outgoing']
env.hosts.extend(['jboss-gw', 'venus', 'jboss-venus', 'venus-dev'])
env.hosts.extend(['jboss-big-dev', 'jboss-test'])
env.hosts.extend(['jboss-proc-int-dev', 'jboss-eod1', 'jboss-eod2', 'dm04', 'eultimo1', ])
env.hosts.extend(['eultimo-proxy-test', 'takto-esb-beta', 'takto-esb', 'eultimo-proxy', 'dm-test', 'takto-web-test', 'jboss-takto-pre-prod', 'dev-ultimo-robin', 'takto-esb-pre-prod', 'dev-tools-drs2-b2', 'dev-tools-zrs5', ])
env.hosts.extend(['jboss-takto2', 'jboss-takto-pre-prod2', 'jboss-service-pre-prod', 'jboss-core-pre-prod', 'jboss-takto-beta2', 'takto-esb2', 'takto-esb-pre-prod2', 'takto-esb-beta2', ])
host_shutdown = ['jboss-up', 'jboss-up-test', 'jboss-proc-int-test1', 'jboss-cosmo1', 'jboss-cosmo2', 'jboss-cosmo-dev1', 'jboss-cosmo-test1', ]
env.password = ass.read_pass()


def mem():
    run("grep MemTotal /proc/meminfo")


def check_jboss_home():
    sudo("ls -l /srv/jboss")


def check_initd_jboss():
    env.warn_only = True
    sudo("grep -i funcname /etc/init.d/*")


def check_monit_systemctl():
    # sudo("psmon=`ps aux | grep n/monit | grep -v grep` ; if [[ -z $psmon ]] ; then systemctl restart rhq-agent ; else monit restart rhq-agent ; fi")
    run("psmon=`ps aux | grep /monit | grep -v grep` ; if [[ -z $psmon ]] ; then echo 'jest systemctl' ; else echo 'jest monit' ; fi")


def check_etc_hostname():
    run("egrep '(10|192|172)' /etc/hosts | grep -v virt")


def cp_java_link():
    sudo("cp -a /srv/java/jdk /srv/java/rhq")


def check_java_directory():
    run("ls -l /srv/java")


def check_home_directory():
    run("ls -la ~")


def check_cron_day_hour():
    env.warn_only = True
    sudo("ls /etc/cron.daily/*py ; ls /etc/cron.hourly/*.py ;")


def install_cron_rhq_agent():
    '''to do'''
    put('../rhq/jboss_restart_log.py', '~')
    sudo("mkdir -p /srv/scripts/py")
    sudo("mv jboss_restart_log.py /srv/scripts/py/")


@hosts('jboss-recognizer-dev')
def memory_jboss_utilize():
    run("cat /proc/meminfo | grep MemTotal | awk '{print \"MM\", int($2/1024)}'")
    sudo("ps aux | grep jboss | egrep -v '(rhq|grep|sh)' | egrep -o 'Xmx[0-9]+' | sed  's/Xmx\([0-9]\+\)/\\1/' | awk '{if ($1 < 20) print \"JJ\", int($1*1024); else print \"JJ\", $1}'")
    sudo("ps aux | grep java | egrep -v '(rhq|grep)' | wc | awk '{print \"PP\", $1}'")


def cp_scripts_to_srv():
    env.warn_only = True
    # sudo('mv /srv/jboss/scripts/report_incident.sh /srv/jboss/scripts/store_incident.sh')
    put('/home/jrokicki/dev/pyt/pytulim/release_jboss_w_postgres/select_jboss_restart.py', '.')
    sudo("mkdir -p /srv/scripts/py ; mv select_jboss_restart.py /srv/scripts/py ; chown root.root /srv/scripts/py/select_jboss_restart.py ; chmod 775 /srv/scripts/py/select_jboss_restart.py")


def cp_stop_rhq_agent():
    env.warn_only = True
    put('../rhq/stop_rhq_agent.py')
    sudo('mkdir -p /srv/scripts/py ; mv stop_rhq_agent.py /srv/scripts/py ; chown jrokicki /srv/scripts/py/stop_rhq_agent.py ; chmod 744 /srv/scripts/py/stop_rhq_agent.py')


def check_rhq():
    sudo("ps aux | grep endorsed | grep java")


def scprhq_agent():
    put('rhq-agent.sh', '~')
    sudo('mv rhq-agent.sh /srv/rhq-agent/bin/')


def scprhq_install():
    put('/home/jrokicki/install_bin/tools_admin/rhq-enterprise-agent-4.13.1.jar', '~')
    put('/home/jrokicki/tickets/rhq/systemd/3/packrhq.tgz', '~')


def chownrhq():
    sudo('chown root /srv/rhq-agent/bin/rhq-agent.sh')
    sudo('chmod 755 /srv/rhq-agent/bin/rhq-agent.sh')


def change_xmx():
    env.warn_only = True
    sudo("sed -i 's/Xmx64/Xmx128/' /srv/rhq-agent/bin/rhq-agent.sh")


def rename_cron_rhq():
    env.warn_only = True
    sudo("if [[ -f /etc/cron.d/stop_rhq_agent ]] ; then mv /etc/cron.d/stop_rhq_agent /etc/cron.d/py_scripts ; fi")


def remove_rhq():
    env.warn_only = True
    # sudo("service rhq-agent stop ; systemctl disable rhq-agent ; sed -i 's/\(.*stop_rhq_agent.*\)/#\\1/' /etc/cron.d/py_scripts")
    sudo("systemctl stop rhq-agent.service  ; systemctl disable rhq-agent.service ; /srv/rhq-agent/bin/rhq-agent-wrapper.sh stop ")


def update_ass_method():
    env.warn_only = True
    put("../ass/ass.py", "~")
    sudo("mv ass.py /srv/scripts/py/ ; rm /srv/scripts/py/ass.pyc")
    # sudo("rm /srv/scripts/py/ass ; rm /srv/scripts/py/ass.py ; mv ass.pyc /srv/scripts/py/")


# @hosts('jboss-core-beta.old', 'jboss-cron-beta.old', 'jboss-da-beta.old', 'jboss-dwp-beta.old', 'jboss-legal-beta.old', 'jboss-proc-beta.old', 'jboss-recognizer-beta.old', 'jboss-service-beta.old', 'jboss-takto-beta.old', 'jboss-takto-beta.old2', 'jboss-takto-beta2.old2', )
@hosts('jboss-takto-beta2')
def jboss_stop_beta7():
    env.warn_only = True
    sudo("service jboss7 stop")


@hosts('jboss-jbpm-beta.old', )
def jboss_stop_betar4():
    env.warn_only = True
    sudo("service jboss stop")
