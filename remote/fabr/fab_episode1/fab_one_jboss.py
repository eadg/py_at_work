import fabric.api as fa
import ass

fa.env.password = ass.read_pass()
fa.env.warn_only = True


@fa.hosts('jboss-service-beta', 'jboss-service1', 'jboss-service2', 'jboss-service-dev1', 'jboss-service-test1')
def check_java_directory():
    fa.run("ls -l /srv/java")


@fa.hosts('jboss-cosmo-dev')
def memory_jboss_utilize():
    fa.run("cat /proc/meminfo | grep MemTotal | awk '{print \"MM\", int($2/1024)}'")
    fa.sudo("ps aux | grep jboss | egrep -v '(rhq|grep|sh)' | egrep -o 'Xmx[0-9]+' | sed  's/Xmx\([0-9]\+\)/\\1/' | awk '{if ($1 < 20) print \"JJ\", int($1*1024); else print \"JJ\", $1}'")
    fa.sudo("ps aux | grep java | egrep -v '(rhq|grep)' | wc | awk '{print \"PP\", $1}'")


# @fa.hosts('eultimo-proxy', 'eultimo-proxy-test', )
@fa.hosts('eultimo-proxy-test', )
def cp_scripts_to_srv():
    fa.put('/home/jrokicki/dev/shell/report_incident.sh', '.')
    fa.sudo("mkdir /srv/jboss/scripts -p ; mv report_incident.sh /srv/jboss/scripts/ ; chown root.root /srv/jboss/scripts/report_incident.sh ; chmod 775 /srv/jboss/scripts/report_incident.sh")


@fa.hosts('jboss-dok')
def from_dok():
    f = gen_open('/home/jrokicki/tickets/tiff_jboss_dok1/kopia_nadzorowana/lista_plikow.txt')
    # f = gen_open('/home/jrokicki/tickets/tiff_jboss_dok1/after_vacation/skrypt_v2/diff_ver1_ver2_pretty.txt')
    # f = gen_open('/home/jrokicki/tickets/tiff_jboss_dok1/zad_16095/file_to_copy_short.txt')
    for i in f:
        print(i)
        fa.get(remote_path='/srv/data/zad_16095/'+i, local_path='/home/jrokicki/tickets/tiff_jboss_dok1/kopia_nadzorowana/')
        # get(remote_path='/srv/data/zad_16095/'+i, local_path='/home/jrokicki/tickets/tiff_jboss_dok1/after_vacation/diff_tiff/')
        # get(remote_path='/srv/data/uploaded_docs/recognized_uploaded_docs/'+i, local_path='/home/jrokicki/tickets/tiff_jboss_dok1/zad_16095/tiff/')
        # get(remote_path='/srv/data/uploaded_docs/recognized_uploaded_docs/2016/'+i, local_path='/home/jrokicki/tickets/tiff_jboss_dok1/zad_16095/tiff/2016/')


@fa.hosts('jboss-dok')
def to_dok():
    f = gen_open('/home/jrokicki/tickets/tiff_jboss_dok1/after_vacation/diff_tiff/list_diff_tif.txt')
    # f = gen_open('/home/jrokicki/tickets/tiff_jboss_dok1/zad_16095/file_to_jboss_dok_old.txt')
    for i in f:
        print(i)
        fa.put('/home/jrokicki/tickets/tiff_jboss_dok1/after_vacation/diff_tiff/'+i, '~/tif-dok1')
        # put('/home/jrokicki/tickets/tiff_jboss_dok1/zad_16095/tiff/'+i, '~/tif-dok1')


@fa.hosts('jboss-dok')
def from_dokk():
    f = gen_open('/home/jrokicki/tickets/tiff_jboss_dok1/kopia_nadzorowana/rev_lista_plikow.txt')
    # f = gen_open('/home/jrokicki/tickets/tiff_jboss_dok1/after_vacation/skrypt_v2/diff_ver1_ver2_pretty.txt')
    # f = gen_open('/home/jrokicki/tickets/tiff_jboss_dok1/zad_16095/file_to_copy_short.txt')
    for i in f:
        print(i)
        fa.get(remote_path='/srv/data/zad_16095/'+i, local_path='/home/jrokicki/tickets/tiff_jboss_dok1/kopia_nadzorowana/')
        # get(remote_path='/srv/data/zad_16095/'+i, local_path='/home/jrokicki/tickets/tiff_jboss_dok1/after_vacation/diff_tiff/')
        # get(remote_path='/srv/data/uploaded_docs/recognized_uploaded_docs/'+i, local_path='/home/jrokicki/tickets/tiff_jboss_dok1/zad_16095/tiff/')
        # get(remote_path='/srv/data/uploaded_docs/recognized_uploaded_docs/2016/'+i, local_path='/home/jrokicki/tickets/tiff_jboss_dok1/zad_16095/tiff/2016/')


def gen_open(path):
    with open(path) as f:
        for l in f.readlines():
            s = l.strip()
            yield s


@fa.hosts('jboss-legal-beta')
def restart_jboss():
    fa.sudo("systemctl restart jboss7")
