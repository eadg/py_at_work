import fabric
import ass

# wszystkie, ale niektóre nie odpowiadają (brak zmiany w PAM)
# fabric.api.env.hosts = ['bases-dm1', 'sites-dm1', '0-pl-p-lr1', '0-ro-p-db01', 'vf-bay1', 'sites-ilias1', '0-pl-t-as60', 'dmz-rev1', 'dmz-mail1', 'nat-pf', 'sites-web1', 'dmz-dns1', 'tkt-as-tst1', '0-pl-p-stor1', '0-pl-p-as06', '0-ro-t-db01', 'dmz-mail2', '0-pl-b-as02', 'dmz-dns2', 'vf-vpn', '0-pl-p-lr0', 'vf-smarthost', '0-pl-t-as70', 'dmz-rev2']
# fabric.api.env.hosts = ['0-pl-t-as70', 'dmz-rev2']

fabric.api.env.password = ass.read_ass(file='../../ass/ass')
fabric.api.env.warn_only = True


def uname():
    fabric.api.run("uname -a")
