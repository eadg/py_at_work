from fabric.api import *
import ass

env.hosts = ['venus', 'jboss-gw-test', 'jboss-service-beta', 'jboss-cron-beta', 'aster-jboss0', 'dm04', 'eultimo1', 'jboss0', 'jboss1', 'jboss-big', 'jboss-cm', 'jboss-cm-test1', 'jboss-core1', 'jboss-core2', 'jboss-core-beta', 'jboss-core-dev1', 'jboss-core-test1', 'jboss-cosmo1', 'jboss-cosmo2', 'jboss-cosmo-dev1', 'jboss-cosmo-test1', 'jboss-cron', 'jboss-cron-dev1', 'jboss-cron-test1', 'jboss-cti1', 'jboss-cti2', 'jboss-cti4-dev1', 'jboss-cti4-test1', 'jboss-cti-dev1', 'jboss-cti-test1', 'jboss-da1', 'jboss-da2', 'jboss-da-beta', 'jboss-da-dev1', 'jboss-da-test1', 'jboss-dok', 'jboss-dok1', 'jboss-dok-dev1', 'jboss-dok-test1', 'jboss-dwp-dev1', 'jboss-dwp-test1', 'jboss-groupwise', 'jboss-jbpm2', 'jboss-jbpm-beta', 'jboss-jbpm-dev1', 'jboss-jbpm-test1', 'jboss-legal1', 'jboss-legal2', 'jboss-legal-beta', 'jboss-legal-dev1', 'jboss-legal-test1', 'jboss-proc', 'jboss-proc1', 'jboss-proc-beta', 'jboss-proc-dev2', 'jboss-proc-int', 'jboss-proc-int-test1', 'jboss-proc-test2', 'jboss-recognizer1', 'jboss-recognizer2', 'jboss-recognizer-beta', 'jboss-recognizer-dev1', 'jboss-recognizer-test1', 'jboss-service1', 'jboss-service2', 'jboss-service-dev1', 'jboss-service-test1', 'jboss-teren1', 'jboss-teren2', 'jboss-teren-dev1', 'jboss-teren-test1', 'eultimo-ext-test', 'dm-test', 'jboss-takto-dev', 'jboss-dwp-beta', 'jboss-takto-test', 'jboss-dnb-test', 'jboss-dnb', ]
host_shutdown = ['jboss-up', 'jboss-up-test']
host_excluded = ['jboss-big-dev', 'jboss-proc-test1', 'jboss-test']
env.password = ass.read_pass()


def mem():
    run("grep MemTotal /proc/meminfo")


def check_rhq():
    sudo("ps aux | grep endorsed | grep java")


def check_initd_jboss():
    sudo("grep -i funcname /etc/init.d/*")


def check_jboss_version():
    run("grep VERS /srv/jboss/jboss/profile.conf | cut -d'=' -f2")
