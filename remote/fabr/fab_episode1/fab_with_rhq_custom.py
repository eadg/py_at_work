from fabric.api import sudo, put, run, env, hosts
import ass

# env.hosts = ['jboss-gw-test', 'jboss-service-beta', 'jboss-cron-beta', 'aster-jboss0', 'dm04', 'eultimo1', 'jboss0', 'jboss1', 'jboss-big', 'jboss-cm', 'jboss-cm-test1', 'jboss-core1', 'jboss-core2', 'jboss-core-beta', 'jboss-core-dev1', 'jboss-core-test1', 'jboss-cosmo1', 'jboss-cosmo2', 'jboss-cosmo-dev1', 'jboss-cosmo-test1', 'jboss-cron', 'jboss-cron-test1', 'jboss-cti1', 'jboss-cti2', 'jboss-da1', 'jboss-da2', 'jboss-da-beta', 'jboss-da-dev1', 'jboss-da-test1', 'jboss-dok', 'jboss-dok1', 'jboss-dok-dev1', 'jboss-dok-test1', 'jboss-dwp0', 'jboss-dwp3', 'jboss-dwp-test1', 'jboss-groupwise', 'jboss-jbpm2', 'jboss-jbpm-beta', 'jboss-jbpm-test1', 'jboss-legal1', 'jboss-legal2', 'jboss-legal-beta', 'jboss-legal-test1', 'jboss-proc', 'jboss-proc1', 'jboss-proc-beta', 'jboss-proc-dev2', 'jboss-proc-int', 'jboss-proc-int-test1', 'jboss-proc-test2', 'jboss-recognizer1', 'jboss-recognizer2', 'jboss-recognizer-beta', 'jboss-recognizer-dev1', 'jboss-recognizer-test1', 'jboss-service1', 'jboss-service2', 'jboss-service-test1', 'jboss-teren1', 'jboss-teren2', 'jboss-teren-test1', 'jbpm-test', 'eultimo-ext-test', 'dm-test', 'jboss-takto-dev', 'jboss-dwp-beta', 'venus-dev', 'jboss-takto-test', 'jboss-dnb-test', 'jboss-dnb', ]
# env.hosts.extend(['jboss-gw', 'venus', 'jboss-venus', 'jboss-test'])
host_outsides = ['eultimo-proxy-test']
host_shutdown = ['jboss-up', 'jboss-up-test']
env.password = ass.read_pass()


def scprhq():
    put('rhq-agent.sh', '~')
    sudo('mv rhq-agent.sh /srv/rhq-agent/bin/')


def cp_config_rhq():
    sudo('rm -f rhq-agent-env.sh')
    sudo('rm -f rhq-agent.sh')
    sudo('rm -f check_agent_rhq.py')
    put('../rhq/rhq-agent-env.sh', '~')
    put('../rhq/rhq-agent.sh', '~')
    put('../rhq/check_agent_rhq.py', '~')
    sudo('mv rhq-agent-env.sh /srv/rhq-agent/bin/')
    sudo('mv rhq-agent.sh /srv/rhq-agent/bin/')
    sudo('mv check_agent_rhq.py /etc/cron.daily/')
    sudo('chmod 755 /srv/rhq-agent/bin/rhq-agent.sh')
    sudo('chmod 755 /etc/cron.daily/check_agent_rhq.py')
    sudo('chown root /etc/cron.daily/check_agent_rhq.py')
    sudo('monit stop rhq-agent')
    sudo('''if [ -d "/etc/monit.d" ]; then rm -f /etc/monit.d/rhq.monitrc ;
            elif [ -d "/etc/monit" ] ; then rm -f /etc/monit/rhq.monitrc ;
            elif [ -d "/etc/monitd" ] ; then rm -f /etc/monitd/rhq.monitrc ;
            fi''')
    sudo('monit reload')
    sudo('/srv/rhq-agent/bin/rhq-agent-wrapper.sh restart')
    run('.')


def scpcheckpy():
    sudo('rm -f check_agent_rhq.py')
    put('../rhq/check_agent_rhq.py', '~')
    sudo('mv check_agent_rhq.py /etc/cron.daily/')


@hosts('jboss-cti4-dev1', 'jboss-cti4-test1', 'jboss-cti-test1')
def stop_rhq_monit():
    sudo('''if [ -d "/etc/monit.d" ]; then rm -f /etc/monit.d/rhq.monitrc ;
            elif [ -d "/etc/monit" ] ; then rm -f /etc/monit/rhq.monitrc ;
            elif [ -d "/etc/monitd" ] ; then rm -f /etc/monitd/rhq.monitrc ;
            fi''')
    sudo('monit reload')


# @hosts('dm01')
# @hosts('jboss-recognizer1', 'jboss-recognizer-beta', 'jboss-core-test', 'jboss-dok1', 'dm01', 'jboss-legal1', 'jboss-core-test', 'jboss-proc-dev', 'jboss-proc-test', 'jboss-core2', 'jboss-legal1', 'jboss-core-dev', 'ss-dok-test', 'jboss-dok1', 'jboss-proc-dev', 'jboss-proc-test', 'jboss-da-beta', 'jboss-da-test', 'jboss-proc', 'jboss-service1', 'jboss-core2', 'jboss-legal1', 'jboss-core-dev')
@hosts('jboss-recognizer1', 'jboss-recognizer-beta', 'jboss-core-test', 'jboss-dok1', 'dm01', 'jboss-legal1')
def stop_wrapper():
    sudo('/srv/rhq-agent/bin/rhq-agent-wrapper.sh stop')


def stop_rhq_systemD():
    sudo('systemctl stop rhq-agent.service')
    sudo('systemctl disable rhq-agent.service')
    sudo('systemctl daemon-reload')


def stop_rhq_py():
    sudo('/srv/rhq-agent/bin/rhq-agent-wrapper.sh stop')
    sudo('if [ -f /etc/cron.hourly/check_agent_rhq.py ]; then rm -f /etc/cron.hourly/check_agent_rhq.py; fi')
    sudo('if [ -f /etc/cron.daily/check_agent_rhq.py ]; then rm -f /etc/cron.hourly/check_agent_rhq.py; fi')


@hosts('jboss-big-dev', 'jboss-cron-dev1', 'jboss-cti-dev1', 'jboss-dwp-dev1', 'jboss-jbpm-dev1', 'jboss-legal-dev1',  'jboss-service-dev1', 'jboss-teren-dev1')
def stop_low_mem_rhq():
    sudo('/srv/rhq-agent/bin/rhq-agent-wrapper.sh stop')
    sudo('systemctl stop rhq-agent')
    sudo('systemctl disable rhq-agent')


def add_cron_rhq_restart():
    # put('../rhq/stop_rhq_agent')
    put('../rhq/stop_rhq_agent.py')
    # sudo('mv stop_rhq_agent /etc/cron.d')
    # sudo('chown root /etc/cron.d/stop_rhq_agent')
    # sudo('ls -l /etc/cron.d')
    sudo('mv stop_rhq_agent.py /srv/scripts/py')
    sudo('chown jrokicki /srv/scripts/py/stop_rhq_agent.py')
    sudo('chmod 744 /srv/scripts/py/stop_rhq_agent.py')
    # sudo('ls -l /srv/scripts/py')
