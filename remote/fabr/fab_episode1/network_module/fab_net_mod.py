import fabric.api as fa
import ass

fa.env.password = ass.read_ass(file='../../ass/ass')
fa.env.warn_only = True


@fa.hosts('takto-esb-beta2', 'jboss-takto-beta', 'jboss-takto-beta2', 'jboss-takto-pre-prod', 'jboss-takto-pre-prod2', 'takto-esb-pre-prod2', )
def cp_server_socket():
    fa.put('../../network/socket_server.py', '.')
