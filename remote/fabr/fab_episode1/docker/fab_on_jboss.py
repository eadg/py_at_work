import fabric.api as fa

# fa.env.hosts = ['jboss-core-dev', 'jboss-cron-dev', 'jboss-dwp-dev', 'jboss-legal-dev', 'jboss-proc-dev', 'jboss-service-dev', ]


def get_pack():
    fa.run('date ; hostname; ')
    fa.sudo('python packjboss2docker.py /srv/jboss/jboss/ ; chown jrokicki jboss*.tgz')
    fa.get("jboss*.tgz", local_path='.')


@fa.hosts('10.101.1.185', )
def put_pack():
    flist = fa.put("jboss*.tgz")
    fullname = flist.pop().split('/').pop()
    fname = fullname.split('.').pop(0)
    fa.local("rm -rf jboss*.tgz")
    comm = 'mv jboss*.tgz /srv/build/ ; cd /srv/build ; mkdir -p ' + fname +\
        '; tar zxfv ' + str(fullname) + '; cd ' + fname + '; ./build_service.sh'
    fa.sudo(comm)
    comm = "docker service ls | grep -i jboss | awk '{print $2}' | \
        xargs -I {} docker service ps {}"
    fa.sudo(comm)


# @fa.hosts('jboss-core-dev', 'jboss-cron-dev', 'jboss-dwp-dev', 'jboss-legal-dev', 'jboss-proc-dev', 'jboss-service-dev', )
@fa.hosts('jboss-cron-dev', 'jboss-dwp-dev' , 'jboss-legal-dev', 'jboss-proc-dev', 'jboss-service-dev', 'jboss-core-dev', )
def do_all():
    fa.execute(get_pack)
    fa.execute(put_pack)
