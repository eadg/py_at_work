import os


def del_old_dirs(days=40, path='/srv/registry/docker/registry/v2/blobs/sha256'):
    d = os.listdir(path)
    os.chdir(path)
    d.sort(key=lambda x: os.path.getmtime(x))
    keep_value = len(d) - days
    d_ok = d[:keep_value]
    print(d_ok)
