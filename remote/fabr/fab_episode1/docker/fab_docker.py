import fabric.api as fa
import ass

fa.env.password = ass.read_ass(file='../../ass/ass')
fa.env.warn_only = True
fa.env.hosts = ['jboss-core-dev', 'jboss-cron-dev', 'jboss-da-dev', 'jboss-dok-dev', 'jboss-dwp-dev', 'jboss-jbpm-dev', 'jboss-legal-dev', 'jboss-proc-dev', 'jboss-service-dev', 'jboss-recognizer-dev', 'jboss-teren-dev', ]
# env.hosts = ['jboss-core-dev', 'jboss-legal-dev', 'jboss-service-dev']


@fa.hosts('web3',)
def update_smyk_ro():
    fa.sudo("rm -f /var/lib/www/websmyk_docker/lib/*.jar ; cp /var/lib/www/websmyk_trunk/lib/*.jar /var/lib/www/websmyk_docker/lib/ ; \
        cd /var/lib/www/websmyk_docker/lib/ ; ls *.jar | xargs -I % sed -i 's/smyk-.*-SNAPSHOT.jar/%/' ../*.jnlp")


def release_jboss2docker():
    fa.put('../../docker/packjboss2docker.py', '.')
    fa.sudo('mv packjboss2docker.py /srv/scripts/py/')


@fa.hosts('10.101.1.184/24', )
def clean_registry():
    fa.sudo('ls -t /srv/registry/docker/registry/v2/blobs/sha256/ | tac | head -n 10 | xargs -I % rm -rf %')


@fa.hosts('jboss-core-dev', )
def release_fab2core():
    fa.put('fab_on_jboss.py', '.')
    fa.sudo('mv fab_on_jboss.py /srv/scripts/py/fabric/fab_docker.py')


@fa.hosts('jboss-cron-dev', )
def get_pack():
    fa.sudo("python packjboss2docker.py /srv/jboss/jboss/")
    fa.get("jboss*.tgz", local_path='.')


@fa.hosts('1-ro-a-doc05.ro.ultimo.eu', )
def put_pack():
    flist = fa.put("jboss*.tgz")
    fullname = flist.pop().split('/').pop()
    fname = fullname.split('.').pop(0)
    fa.local("rm -rf jboss*.tgz")
    comm = 'mv jboss*.tgz /srv/build/ ; cd /srv/build ; mkdir -p ' + fname +\
        '; tar zxfv ' + str(fullname) + '; cd ' + fname + '; ./build_service.sh'
    fa.sudo(comm)
    comm = "docker service ls | grep -i jboss | awk '{print $2}' | \
        xargs -I {} docker service ps {}"
    fa.sudo(comm)


@fa.hosts('jboss-cron-dev', )
def do_all():
    fa.execute(get_pack)
    fa.execute(put_pack)
