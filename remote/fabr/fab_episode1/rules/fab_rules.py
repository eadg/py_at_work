import fabric.api as fa
import ass

fa.env.password = ass.read_ass(file='../../ass/ass')
fa.env.warn_only = True


@fa.hosts('jboss-core-dev', 'jboss-core1', 'jboss-core2')
def release():
    fa.put('../../rules/rules.py', '.')
    fa.sudo('chmod 664 rules.py; mv rules.py /srv/jboss/scripts/ ; ')
