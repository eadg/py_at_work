from fabric.api import *
import ass

env.hosts = ['aster-jboss0', 'jboss-cti1', 'jboss-cti2', 'jboss-cti4-dev1', 'jboss-cti4-test1', 'jboss-cti-dev1', 'jboss-cti-test1', ]
env.password = ass.read_pass()



def check_profile_jboss():
    run("grep -i version /srv/jboss/jboss/profile.conf")
