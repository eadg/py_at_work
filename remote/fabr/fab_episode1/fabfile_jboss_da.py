from fabric.api import *
import ass

env.hosts = [ 'jboss-da1', 'jboss-da2', 'jboss-da-beta', 'jboss-da-dev1', 'jboss-da-test1', ]
host_excluded = ['jboss-big-dev', 'jboss-proc-test1', 'jboss-test', 'eultimo-proxy-test']
host_excluded = ['aster-jboss0', 'dm04', 'eultimo1', 'jboss-cti1', 'jboss-cti2', ]
host_shutdown = ['jboss-up', 'jboss-up-test']
env.password = ass.read_pass()


def deploy():
    sudo("mkdir -p /srv/scripts/py")
    sudo("mv jboss_restart_log.py /srv/scripts/py/")


def mem():
    run("grep MemTotal /proc/meminfo")


def check_rhq():
    sudo("ps aux | grep endorsed | grep java")


def scphome():
    put('../select_jboss_restart.py', '~')


def scprhq_agent():
    put('rhq-agent.sh', '~')
    sudo('mv rhq-agent.sh /srv/rhq-agent/bin/')


def scprhq_install():
    put('/home/jrokicki/install_bin/tools_admin/rhq-enterprise-agent-4.13.1.jar', '~')
    put('/home/jrokicki/tickets/rhq/systemd/2/packrhq.tgz', '~')


def check_jboss_home():
    sudo("ls -l /srv/jboss")


def check_initd_jboss():
    sudo("grep -i funcname /etc/init.d/*")


def chownrhq():
    sudo('chown root /srv/rhq-agent/bin/rhq-agent.sh')
    sudo('chmod 755 /srv/rhq-agent/bin/rhq-agent.sh')


def check_monit_systemctl():
    # sudo("psmon=`ps aux | grep n/monit | grep -v grep` ; if [[ -z $psmon ]] ; then systemctl restart rhq-agent ; else monit restart rhq-agent ; fi")
    run("psmon=`ps aux | grep /monit | grep -v grep` ; if [[ -z $psmon ]] ; then echo 'jest systemctl' ; else echo 'jest monit' ; fi")
