from fabric.api import *
import ass

env.hosts = ['jboss-gw', 'jboss-venus', 'jboss-dwp0', 'jboss-dwp3', 'jbpm-test', 'venus-dev']
env.password = ass.read_pass()


def mem():
    run("grep MemTotal /proc/meminfo")


def check_rhq():
    sudo("ps aux | grep endorsed | grep java")


def check_initd_jboss():
    sudo("grep -i funcname /etc/init.d/*")


def check_jboss_version():
    run("grep VERS /srv/jboss/profile.conf | cut -d'=' -f2")
