import fabric.api as fa
import ass

fa.env.password = ass.read_ass(file='../../ass/ass')
fa.env.warn_only = True


@fa.hosts('0-pl-p-dfs1', '0-pl-p-dfs2', '0-pl-p-dfs3', '0-pl-p-dfs4',)
def uptime():
    fa.run("uptime")
    fa.run("systemctl status bacula-fd pcsd")


@fa.hosts('0-pl-p-dfs1', '0-pl-p-dfs2', '0-pl-p-dfs3', '0-pl-p-dfs4',)
def release():
    fa.put('../../dfs/pause_bacula.py', '.')
    fa.sudo("mkdir /srv/scripyts -p ; mv pause_bacula.py /srv/scripyts/ ; chown root.root /srv/scripyts/pause_bacula.py ; chmod 775 /srv/scripyts/pause_bacula.py")


@fa.hosts('0-pl-p-dfs2',)
def pcs():
    fa.sudo("pcs status")


@fa.hosts('0-pl-p-dfs1', '0-pl-p-dfs2', '0-pl-p-dfs3', '0-pl-p-dfs4',)
def dfh():
    fa.run("df -h | grep vda")
