import fabric2
from paramiko.ssh_exception import AuthenticationException
import ass


class FabAgain(object):

    def __init__(self):
        self.hosts = self.load_host()[:5]
        # self.hosts = ['jboss-teren-dev', 'jboss-teren-test', 'jboss-teren2']
        # self.hosts = ['takto-esb', 'jboss-teren-dev', 'jboss-teren2']

    def load_host(self):
        hosts = []
        with open('/home/jrokicki/tickets/tls/hosts_non-production.txt') as f:
            for line in f:
                hosts.append(line.rstrip())
        return hosts

    def get_jboss(self, h):
        uname = h.run('uname -s', hide=True)
        if 'Linux' in uname.stdout:
            cmd = "df -h / | tail -n1 | awk '{print $5}'"
            return h.run(cmd, hide=False).stdout.strip()

    def get_jboss_su(self, h):
        pasw = ass.read_pass(credent=2)
        conf = fabric2.Config(overrides={'sudo': {'password': pasw}})
        c = fabric2.Connection(h, config=conf)
        c.sudo('ls -l /root', hide='stderr')

    def run(self, host):
        for cxn in fabric2.SerialGroup(*host):
            print("{}: {}".format(cxn, self.get_jboss(cxn)))

    def runsu(self, host):
        pasw = ass.read_pass(credent=2)
        for cxn in fabric2.SerialGroup(*host, password=pasw):
            cxn.sudo('ls -l /root', hide=True)
            # print("{}: {}".format(cxn, self.get_jboss_su(cxn)))

    def serialsu(self):
        pasw = ass.read_pass(credent=2)
        for host in self.hosts:
            conf = fabric2.Config(overrides={'sudo': {'password': pasw}})
            c = fabric2.Connection(host, config=conf)
            try:
                print("{} in {}".format(
                    host, c.sudo("ps aux | grep nginx | grep -v grep ")))
                # "ps aux | egrep '(sbin/httpd|sbin/nginx)'|grep -v grep"))
            except AuthenticationException as e:
                print('Error connection to: {}. Exception: {}'.format(host, e))

    def lists_root(self):
        '''
        # https://github.com/fabric/fabric/issues/1912
        It's not possible to sudo with SerialGroup in the present.
        Maybe future release will improve it.
        '''
        # fabric2.SerialGroup(*self.hosts).sudo('ls -l /tmp')


if __name__ == '__main__':
    f = FabAgain()
    # f.runsu(f.hosts)
    # f.get_jboss_su('jboss-teren-dev')
    f.serialsu()
    # f.lists_root()
