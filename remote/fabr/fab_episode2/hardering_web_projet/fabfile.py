from fabric.api import run, env, settings, sudo, hide, hosts, parallel, put
import ass
import base

# lis = base.load_host()
# 2>&1 for tee
# lis = base.get_short_list()
lis = base.get_short_hardssl()
env.hosts = lis
env.password = ass.read_pass(credent=2)
env.sudo_password = ass.read_pass(credent=2)
env.skip_bad_hosts = True


# @parallel
def check_httpd():
    with hide('warnings', 'running', 'stderr'), settings(warn_only=True):
        sudo("ps aux|grep -E 'sbin/httpd|sbin/nginx|tomcat'|grep -v grep")


def lets_hard():
    put(local_path='stuff/ssl.conf',
        remote_path='/etc/nginx/conf.d/', use_sudo=False)


@parallel
@hosts('pdc-t-container-edu2a', 'pdc-t-jboss-takto-pre-prod')
def check_short():
    with hide('running'), settings(warn_only=True):
        sudo("date;ps aux|grep -E 'sbin/httpd|sbin/nginx|tomcat'|grep -v grep")
        # sudo("ps aux |grep -E 'sbin/httpd|sbin/nginx|tomcat' |grep -v grep")
        run('whoami')
