def load_host(file_host=None, number=0, in_filter=None):
    hosts = []
    with open(file_host) as f:
        for line in f:
            if not line.startswith('#'):
                if in_filter:
                    if in_filter in line:
                        hosts.append(line.strip())
                elif not in_filter:
                    hosts.append(line.strip())
    return hosts[-number:]


def get_short_list(n):
    list_shorter = load_host()[-n:]
    return list_shorter


def get_short_hardssl():
    return 'pdc-t-digital-beta'
