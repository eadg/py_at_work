def test_get_name():
    sec = 1562836483.10847
    sec_1 = int(sec)
    sec_2 = round(sec-sec_1, 20)
    assert sec == sec_1 + sec_2


def test_epoch():
    sec = 1562836483.10847
    sec_1 = int(sec)
    import datetime
    utc = datetime.datetime.utcfromtimestamp(sec_1)
    y = utc.year
    m = utc.month
    d = utc.day
    assert (y, m, d) == (2019, 7, 11)


def test_path():
    y, m, d = '2019', '07', '01'
    sec = 1562836483.10847
    str_sec = str(sec)
    sec_1, sec_2 = str_sec.split('.')
    path = '/rec/current/' + '/'.join([y, m, d]) + '/' +\
        sec_1 + '_' + sec_2 + '.*'
    print('Path: {}'.format(path))
    assert path == '/rec/current/2019/07/01/1562836483_10847.*'
