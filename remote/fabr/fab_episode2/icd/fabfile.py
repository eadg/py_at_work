import datetime
from pathlib import Path
from fabric.api import env, hosts, local
from fabric.operations import get
import ass

env.password = ass.read_pass(credent=2)
env.sudo_password = ass.read_pass(credent=2)
env.skip_bad_hosts = True


@hosts('icd0')
def get_file():

    def sox_call(s):
        pp = Path(s)
        if 'gsm' in pp.name:
            cal_sox = 'sox ' + s + ' ' + str(
                pp.parent.joinpath(pp.stem + '.wav'))
            local("echo " + cal_sox)
            local(cal_sox)
        elif 'raw' in pp.name:
            cal_mv = 'cp -a ' + s + ' ' + str(
                pp.parent.joinpath(pp.stem + '.raw'))
            local(cal_mv)
            cal_sox = 'sox -r 16000 -c 1 -e signed -b 16 ' + str(
                pp.parent.joinpath(pp.stem + '.raw')) + ' ' + str(
                    pp.parent.joinpath(pp.stem + '.wav'))
            local(cal_sox)

    # sec = '1287556724.4697'  # gsm
    # sec = '1541153672.93817'  # gsm
    sec = '1562836483.108475'  # sraw

    sec_1, sec_2 = sec.split('.')
    epoch = datetime.datetime.fromtimestamp(int(sec_1))
    y, m = epoch.strftime('%Y'), epoch.strftime('%m')
    d, h = epoch.strftime('%d'), epoch.strftime('%H')
    path = '/rec/current/' + '/'.join([y, m, d, h+'00']) + '/' + \
        sec_1 + '_' + sec_2 + '.*'
    gets = get(remote_path=path, local_path='/tmp/', use_sudo=False)
    # gets = get(remote_path=path, local_path='/tmp/', use_sudo=True)
    for i in gets:
        print(i)
        sox_call(i)
