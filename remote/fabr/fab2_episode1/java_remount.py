import socket
from fabric2 import Connection
import paramiko
from fabric_base import BaseFab


class JavaMount(BaseFab):

    def __init__(self, path):
        super().__init__(path)

    def remount(self, jversion=7):
        host_list = self.get_host(num=1)
        for host in host_list:
            with Connection(host, connect_timeout=3) as con:
                try:
                    con.connect_kwargs.password = self.pa
                    con.connect_kwargs.passphrase = self.pa
                    c1 = "service jboss{} stop".format(jversion)
                    c2 = "salt-call state.apply javamount"
                    c3 = "service jboss{} start".format(jversion)
                    # c4 = "uname -a"
                    out1 = con.sudo(c1, password=self.pa, warn=True, hide=True)
                    out2 = con.sudo(c2, password=self.pa, warn=True, hide=True)
                    out3 = con.sudo(c3, password=self.pa, warn=True, hide=True)
                    # out4 = con.sudo(
                    # c4, password=self.pa, warn=True, hide=True)
                except (socket.timeout, socket.gaierror,
                        paramiko.ssh_exception.BadAuthenticationType,
                        paramiko.ssh_exception.NoValidConnectionsError):
                    print('error connection: {0}'.format(host))
                else:
                    sout1 = out1.stdout.strip()
                    if con.is_connected and sout1:
                        self.logger.info('{0}'.format(host))
                        self.logger.info(out1.stdout.strip())
                        self.logger.info(out2.stdout.strip())
                        self.logger.info(out3.stdout.strip())

    def is_ro_mounted(self):
        host_list = self.get_host(num=0)
        for host in host_list:
            # print(host)
            with Connection(host, connect_timeout=3) as con:
                try:
                    con.connect_kwargs.password = self.pa
                    con.connect_kwargs.passphrase = self.pa
                    c1 = "mount | grep jaws | awk '{print $6}'|cut -d, -f1"
                    c2 = "ps aux|grep jdk|grep -v grep|awk '{print $9}'"
                    out1 = con.sudo(c1, password=self.pa, warn=True, hide=True)
                    out2 = con.sudo(c2, password=self.pa, warn=True, hide=True)
                except (socket.timeout, socket.gaierror,
                        paramiko.ssh_exception.BadAuthenticationType,
                        paramiko.ssh_exception.NoValidConnectionsError):
                    print('error connection: {0}'.format(host))
                else:
                    sout1 = out1.stdout.strip()
                    if con.is_connected and sout1:
                        self.logger.info('{0}, {1}, {2}'.format(
                            host, sout1, out2.stdout.strip()))

    def is_ro_mounted2(self):
        host_list = self.get_host(num=1)
        comm = ['uname -a', 'uptime']
        for hl in host_list:
            print(hl)
            # return host
            self.connect(*comm, host=hl)
        return True


if __name__ == '__main__':
    file_host = '/home/jrokicki/tickets/jboss_lists/jboss_prod.txt'
    # jboss_dev = 'jboss-teren-dev'
    # jm = JavaMount(jboss_dev)
    jm = JavaMount(file_host)
    # jm.is_ro_mounted()
    jm.is_ro_mounted()
