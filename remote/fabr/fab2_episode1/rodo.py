import socket
from fabric import Connection
import paramiko
from fabric_base import BaseFab


class Rodo(BaseFab):

    def __init__(self, path):
        super().__init__(path)

    def is_rodo_alive(self):
        host_list = self.get_host()
        for host in host_list:
            with Connection(host, connect_timeout=3) as con:
                try:
                    con.connect_kwargs.password = self.pa
                    con.connect_kwargs.passphrase = self.pa
                    c0 = "curl -k https://pdc-p-rodprot.ultimo.pl"
                    c1 = ":8443/rodo/login"
                    cm = c0 + c1
                    out1 = con.sudo(cm, password=self.pa, warn=True, hide=True)
                except (socket.timeout, socket.gaierror,
                        paramiko.ssh_exception.BadAuthenticationType,
                        paramiko.ssh_exception.NoValidConnectionsError):
                    print('error connection: {0}'.format(host))
                else:
                    sout1 = out1.stdout.strip()
                    if con.is_connected and sout1:
                        self.logger.info('{0}, {1}'.format(host, sout1))


if __name__ == '__main__':
    jump_host = 'jboss-teren-dev'
    ro = Rodo(jump_host)
    ro.is_rodo_alive()
