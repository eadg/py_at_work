#!/bin/bash


grep server_name /etc/nginx/vhosts.d/*.conf | awk '{if ($3 ~ /ultimo/) print $3}' | cut -d ';' -f1 | sort | uniq
