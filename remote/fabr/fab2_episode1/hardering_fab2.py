from fabric2 import Connection
import socket
from fabric_base import BaseFab
import paramiko
import invoke


class HardWeb(BaseFab):

    def __init__(self, path):
        super().__init__(path)

    def is_web_server(self):
        host_list = self.get_host(num=0)
        print(host_list)
        for host in host_list:
            print('connecting {0}'.format(host))
            with Connection(host, connect_timeout=3) as con:
                try:
                    con.connect_kwargs.password = self.pa
                    con.connect_kwargs.passphrase = self.pa
                    c = "ps aux|grep -E 'sbin/httpd|sbin/nginx|tomcat'|grep\
                        -v gre"
                    ng = con.sudo(c, password=self.pa, warn=True, hide=True)
                except (socket.timeout, socket.gaierror,
                        paramiko.ssh_exception.BadAuthenticationType,
                        paramiko.ssh_exception.NoValidConnectionsError):
                    print('error connection: {0}'.format(host))
                else:
                    ng_out = ng.stdout.strip()
                    if con.is_connected and ng_out:
                        self.logger.info('{0}'.format(host))

    def lets_hard(self):
        host_list = self.get_host(
            path='/home/jrokicki/tickets/tls/hosts_prod_ok.txt', num=1)
        for hst in host_list:
            with Connection(hst) as con:
                con.connect_kwargs.password = self.pa
                con.connect_kwargs.passphrase = self.pa
                print("Connected to host: {}".format(con.host))
                con.put(local='stuff/ssl.conf', remote='ssl.conf')
                con.put(local='stuff/dhparam.pem', remote='dhparam.pem')
                mv1 = 'mv ssl.conf /etc/nginx/conf.d/'
                mv2 = 'mv dhparam.pem /etc/certs/'
                lsprx = 'cat /etc/nginx/nginx.c*f /etc/nginx/vhosts.d/*.c*f'
                con.sudo(mv1, password=self.pa)
                con.sudo(mv2, password=self.pa)
                try:
                    self.run_properly(conn=con, comm='coredns', pasw=self.pa)
                except invoke.exceptions.UnexpectedExit:
                    print('Not coredns')
                self.run_properly(conn=con, comm='nginx', pasw=self.pa)
                con.sudo(lsprx, password=self.pa, warn=True)
                print(con.is_connected)

    def run_properly(self, conn=None, comm=None, pasw=None):
        checkstr = 'cat /proc/1/comm'
        awk = "awk '{print $1}'"
        result = conn.sudo(checkstr, password=pasw)
        print("result of systemctl/service: {} ".format(result.stdout))
        if result.stdout.strip() in 'systemd':
            # print('in systemd')
            checkstr2 = "systemctl -a | grep {} | {} ".format(comm, awk)
            print("aaaa {}".format(checkstr2))
            result2 = conn.sudo(checkstr2, password=pasw)
            print(result2.stdout)
            rest = 'systemctl restart {}'.format(result2.stdout)
        else:
            # print('in service')
            rest = '/sbin/service {} restart'.format(comm)
        conn.sudo(rest, password=pasw)


if __name__ == '__main__':
    file_host = '/home/jrokicki/tickets/tls/hosts_prod_ok.txt'
    hy = HardWeb(file_host)
    # hy.is_web_server()
    hy.lets_hard()
