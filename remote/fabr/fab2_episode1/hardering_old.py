from fabric2 import Connection
import ass
import hosts

# lis = hosts.load_host()
# 2>&1 for tee
# lis = hosts.get_short_list()

# env.hosts = lis

# env.sudo_password = ass.read_pass(credent=2)
# env.skip_bad_hosts = True
# c.connect_kwargs.password = p
# c.connect_kwargs.passphrase = p


def lets_hard():
    # lis = hosts.get_short_hardssl()
    flist = hosts.load_host(
        # file_host='/home/jrokicki/tickets/tls/hosts_non_prod_ok.txt')
        # file_host='/home/jrokicki/tickets/tls/one.txt')
        file_host='/home/jrokicki/tickets/tls/rest_of_hosts_non_prod_ok.txt')
    p = ass.read_pass(credent=2)
    for hst in flist:
        with Connection(hst) as c:
            c.connect_kwargs.password = p
            c.connect_kwargs.passphrase = p
            print("Connected to host: {}".format(c.host))
            c.put(local='stuff/ssl.conf', remote='ssl.conf')
            c.put(local='stuff/dhparam.pem', remote='dhparam.pem')
            mv1 = 'mv ssl.conf /etc/nginx/conf.d/'
            mv2 = 'mv dhparam.pem /etc/certs/'
            lsprx = 'cat /etc/nginx/nginx.conf /etc/nginx/vhosts.d/*.conf'
            c.sudo(mv1, password=p)
            c.sudo(mv2, password=p)
            run_properly(conn=c, comm='nginx', pasw=p)
            c.sudo(lsprx, password=p, warn=True)
            print(c.is_connected)


def run_properly(conn=None, comm=None, pasw=None):
    checkstr = 'cat /proc/1/comm'
    result = conn.run(checkstr)
    if result == 'systemd':
        rest = 'systemctl restart {}'.format(comm)
    else:
        rest = '/sbin/service {} restart'.format(comm)
    conn.sudo(rest, password=pasw)


if __name__ == '__main__':
    lets_hard()
