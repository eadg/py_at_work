from fabric_base import BaseFab


class DFS(BaseFab):

    def __init__(self, path):
        super().__init__(path)

    def last(self):
        host_list = self.get_host(num=1)
        comm = ['last']
        for hl in host_list:
            print(hl)
            self.connect(*comm, host=hl)
        return True


if __name__ == '__main__':
    file_host = '/home/jrokicki/tickets/jboss_lists/jboss_prod.txt'
    # jboss_dev = 'jboss-teren-dev'
    # jm = JavaMount(jboss_dev)
    ds = DFS(file_host)
    # jm.is_ro_mounted()
    ds.last()
