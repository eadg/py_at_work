from fabric2 import Connection
from fabric_base import BaseFab
import hosts


class JB(BaseFab):

    def __init__(self, path):
        super().__init__(path)

    def change_java_version(self):
        host_list = hosts.load_host(
            file_host=self.path, number=0, in_filter='pre')
        for host in host_list:
            # host = 'pdc-t-jboss-teren-test'
            print(host)
            with Connection(host, connect_timeout=3) as con:
                con.connect_kwargs.password = self.pa
                con.connect_kwargs.passphrase = self.pa
                vers = con.run(
                        "cd /srv/scripts/py ; python -c 'import env;\
                        print(env.Env().get_jboss_ver())'", hide=True,
                        warn=False)
                try:
                    jbversion = int(vers.stdout.strip())
                except ValueError:
                    jbversion = 0
                if jbversion < 7:
                    jb_ver = 'jboss'
                else:
                    jb_ver = 'jboss7'
                self.logger.info(
                    'Is connected host {1} to {0} v: {2} '.format(
                        con.is_connected, host, jb_ver))
                self.change_link_and_restart(conn=con, version=jb_ver)
                '''
                if not jb_ver:
                    self.change_link(conn=con)
                else:
                    self.change_link_and_restart()
                '''
            # return host
        return host_list

    def change_link_and_restart(self, conn=None, version=None):
        conn.sudo('ls -l /srv/java', password=self.pa, warn=False)
        conn.sudo(
            'service {} stop'.format(version), password=self.pa,
            warn=False)
        conn.sudo(
            'rm -f /srv/java/jdk', password=self.pa, warn=False)
        conn.sudo(
            'ln -s /srv/jaws/jdk1.8.0_201 /srv/java/jdk',
            password=self.pa, warn=False)
        conn.sudo(
            'systemctl restart rpcbind.service rpc-statd.service',
            password=self.pa, warn=False)
        conn.sudo(
            'service {} start'.format(version), password=self.pa,
            warn=False)

    def change_link(self, conn=None):
        conn.sudo('ls -l /srv/java', password=self.pa, warn=False)
        conn.sudo(
            'rm -f /srv/java/jdk', password=self.pa, warn=False)
        conn.sudo(
            'ln -s /srv/jaws/jdk1.8.0_201 /srv/java/jdk',
            password=self.pa, warn=False)

    def get_jboss_pid(self):
        host_list = hosts.load_host(file_host=self.path, number=0)
        for host in host_list:
            if 'pre' in host:
                with Connection(host, connect_timeout=3) as con:
                    con.connect_kwargs.password = self.pa
                    con.connect_kwargs.passphrase = self.pa
                    vers = con.run(
                        "ps aux|grep jboss|grep java|grep -v grep|awk\
                        '{print $2}'", hide=True)
                    self.logger.info('JBoss {0} has pid {1}'.format(
                        host, vers.stdout.strip()))


if __name__ == '__main__':
    file_host = '/home/jrokicki/tickets/tls/jb_non-prod.txt'
    hy = JB(file_host)
    hy.change_java_version()
    # hy.get_jboss_pid()
