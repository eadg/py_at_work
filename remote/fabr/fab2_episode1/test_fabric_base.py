from fabric_base import BaseFab
import hosts
import java_remount


def test_return_load_host():
    host_list = hosts.load_host(
        file_host='/home/jrokicki/tickets/tls/hosts_prod_ok.txt', number=0)
    # print(host_list)
    assert len(host_list) > 0


def test_biger_and_big():
    host_list1 = hosts.load_host(
            file_host='/home/jrokicki/tickets/tls/hosts_prod_ok.txt', number=0)
    host_list2 = hosts.load_host(
            file_host='/home/jrokicki/tickets/tls/hosts_prod_ok.txt', number=1)
    assert len(host_list1) > len(host_list2)


def teest_is_ro_mounted2():
    file_host = '/home/jrokicki/tickets/jboss_lists/single_dev.txt'
    jm = java_remount.JavaMount(file_host)
    ret = jm.is_ro_mounted2()
    # print(ret)
    assert ret == 'pdc-t-jboss-jbpm4-beta'


def test_is_ro_mounted2_connection():
    comm = ['uname -a', 'uptime']
    ret = BaseFab('.')
    ret.connect(*comm, host='jboss-teren-dev', sudo=True)
    assert isinstance(ret, object)


def test_is_ro_mounted2_connection2():
    file_host = '/home/jrokicki/tickets/jboss_lists/single_dev.txt'
    jm = java_remount.JavaMount(file_host)
    ret = jm.is_ro_mounted2()
    assert ret


def test_jump_conn1():
    comm = ['uname -a']
    ret = BaseFab('.')
    ret.jump_connect(
        *comm, jhost='jboss-cron-dev', host='jboss-teren-dev', sudo=True)
    assert isinstance(ret, object)


def test_jump_conn_shell():
    comm = ['uname -a', 'uptime']
    ret = BaseFab('.')
    ret.jump_connect(
        *comm, jhost='jboss-teren-dev', host='0-pl-p-dfs1', sudo=True)
    assert isinstance(ret, object)
