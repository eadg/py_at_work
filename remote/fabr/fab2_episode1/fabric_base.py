import os.path
import socket
from fabric import Connection
import paramiko
import ass
import hosts
import logger


class BaseFab(object):

    def __init__(self, path):
        self.logger = logger.Logger('info.log', clean=True).log
        self.pa = ass.read_pass(credent=0)
        self.path = path

    def get_host(self, path=None, num=0):
        if os.path.isfile(self.path):
            host_list = hosts.load_host(file_host=self.path, number=num)
        elif isinstance(self.path, str):
            return [self.path]
        else:
            host_list = hosts.load_host(file_host=path, number=num)
        return host_list

    def connect(self, *commands, host=None, sudo=True):
        outs = []
        with Connection(host, connect_timeout=3) as con:
            try:
                con.connect_kwargs.password = self.pa
                con.connect_kwargs.passphrase = self.pa
                for com in commands:
                    ou = con.sudo(com, password=self.pa, warn=True, hide=True)
                    outs.append(ou)
            except (socket.timeout, socket.gaierror,
                    paramiko.ssh_exception.BadAuthenticationType,
                    paramiko.ssh_exception.NoValidConnectionsError):
                print('error connection: {0}'.format(host))
            else:
                sout1 = outs[0].stdout.strip()
                if con.is_connected and sout1:
                    self.logger.info('{0}'.format(host))
                    for out in outs:
                        self.logger.info(out.stdout.strip())
                    return sout1

    def jump_connect(self, *commands, jhost=None, host=None, sudo=True):
        outs = []
        with Connection(jhost, connect_timeout=3) as con:
            try:
                con.connect_kwargs.password = self.pa
                con.connect_kwargs.passphrase = self.pa
                for com in commands:
                    ou = con.run("ssh {1} {0}".format(com, host))
                    outs.append(ou)
            except (socket.timeout, socket.gaierror,
                    paramiko.ssh_exception.BadAuthenticationType,
                    paramiko.ssh_exception.NoValidConnectionsError):
                print('error connection: {0}'.format(host))
            else:
                sout1 = outs[0].stdout.strip()
                if con.is_connected and sout1:
                    self.logger.info('{0}'.format(host))
                    for out in outs:
                        self.logger.info(out.stdout.strip())
                    return sout1
