import argparse
import datetime
import getpass
from pathlib import Path
import sys
import compress
import gen_password
import logger
try:
    from fabric import Connection
except ImportError:
    from fabric2 import Connection


class Icd(object):

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()
        self.logger = logger.Logger('info.log')

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-f', action='store', help='Name of sound file', dest='fname')
        self.parser.add_argument(
            '-j', action='store', help='jira ticket', dest='jname')
        self.parser.add_argument(
            '-r', action='store_true', help='sox remote', dest='rsox')
        results = self.parser.parse_args()
        self.jname = results.jname
        self.fname = results.fname
        self.rsox = results.rsox
        if not self.jname or not self.fname:
            sys.exit('Parameters -f and -j are required')

    def get_file(self, local_sox=False):
        p = getpass.getpass()
        sec_1, sec_2 = self.fname.split('.')
        epoch = datetime.datetime.fromtimestamp(int(sec_1))
        y, m = epoch.strftime('%Y'), epoch.strftime('%m')
        d, h = epoch.strftime('%d'), epoch.strftime('%H')
        aster_path = '/rec/current/' + '/'.join([y, m, d, h+'00']) + '/' + \
            sec_1 + '_' + sec_2 + '.*'
        with Connection('icd0') as c:
            c.connect_kwargs.password, c.connect_kwargs.passphrase = p, p
            self.logger.log.info("Connected to host: {}".format(c.host))
            self.logger.log.info("Searching: {}".format(aster_path))
            result_path = c.run("ls {}".format(aster_path), hide=True)
            self.logger.log.info("Found: {} ".format(
                result_path.stdout.strip()))
            ppg = Path(result_path.stdout.strip())
            name_file = ppg.name
            local_file = '/tmp/' + name_file
            if not local_sox:
                c.get(remote=ppg, local=local_file)
                new_file = self.sox_call_local(str_path=local_file, conn=c)
            else:
                c.run("cp {} {}".format(ppg, local_file))
                new_file = self.sox_call_remote(str_path=local_file, conn=c)
                new_file = str(new_file)
                c.get(remote=new_file, local=new_file)
                c.run("rm {}".format(new_file))
            self.logger.log.info("Converted to: {} is done".format(new_file))
        return new_file

    def sox_call_local(self, str_path, conn):
        pps = Path(str_path)
        if 'gsm' in pps.name:
            cal_sox = 'sox ' + str_path + ' ' + str(
                pps.parent.joinpath(pps.stem + '.wav'))
            conn.local(cal_sox)
        elif 'raw' in pps.name:
            cal_mv = 'cp -a ' + str_path + ' ' + str(
                pps.parent.joinpath(pps.stem + '.raw'))
            conn.local(cal_mv)
            cal_sox = 'sox -r 16000 -c 1 -e signed -b 16 ' + str(
                pps.parent.joinpath(pps.stem + '.raw')) + ' ' + str(
                    pps.parent.joinpath(pps.stem + '.wav'))
            conn.local(cal_sox)
        self.logger.log.info("Run local: {}".format(cal_sox))
        return pps.parent.joinpath(pps.stem + '.wav')

    def sox_call_remote(self, str_path, conn):
        pps = Path(str_path)
        if 'gsm' in pps.name:
            cal_sox = 'sox ' + str_path + ' ' + str(
                pps.parent.joinpath(pps.stem + '.wav'))
            conn.run(cal_sox, hide=True)
            conn.run('rm {}'.format(str_path))
        elif 'raw' in pps.name:
            cal_mv = 'cp -a ' + str_path + ' ' + str(
                pps.parent.joinpath(pps.stem + '.raw'))
            conn.run(cal_mv, hide=True)
            cal_sox = 'sox -r 16000 -s -w -c 1 ' + str(
                pps.parent.joinpath(pps.stem + '.raw')) + ' ' + str(
                    pps.parent.joinpath(pps.stem + '.wav'))
            conn.run(cal_sox, hide=True)
            conn.run('rm {} {}'.format(
                str_path, pps.parent.joinpath(pps.stem + '.raw')))
        self.logger.log.info("Run remote: {}".format(cal_sox))
        return pps.parent.joinpath(pps.stem + '.wav')

    def only_copy(self):
        self.get_file(local_sox=icd.rsox)

    def do_all(self):
        pw = gen_password.GenPas().return_secure_pass()
        # icd = Icd()
        pfname = Path(self.fname)
        if not pfname.is_file():
            pp = self.get_file(local_sox=icd.rsox)
            icd.logger.log.info("Generated password: {}".format(pw))
            compress.zip_secure(
                zip_path='/tmp/kopia_nadzorowana_'+icd.jname+'.zip',
                files=pp, pswd=pw, logger=icd.logger)
        else:
            pass
            # with open


if __name__ == '__main__':
    icd = Icd()
    icd.only_copy()
