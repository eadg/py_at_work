import os
from pathlib import Path
import pexpect

import ass
import logger


class SftpSync():

    def __init__(self):
        self.logg = logger.Logger(file_path='/var/log/sftp_sync.log')
        self.logg_daily = logger.Logger(
            file_path='/tmp/sftp_daily.log', clean=True)

    def send_one_file(self, ph: Path) -> int:
        lp1 = '/srv/henry/RPA/Komornik Online - potrzebne procesy/'
        lp2 = 'nowy/NoweKOpliki-rozpakowane'
        lp = lp1 + lp2
        os.chdir(lp)
        pa = ass.read_pass(file='/srv/scripts/py/ssap', credent=1)
        remote_path = ph.parents[1].joinpath('XML')
        command0 = 'mkdir ' + str(remote_path)
        child = pexpect.spawnu('sftp ftp_komornik_nadrzedne@stor.b2-impact.pl')
        child.expect('Password:')
        child.sendline(pa)
        child.expect('sftp>')
        try:
            phstr = str(ph)
            child.sendline(command0)
            child.expect('sftp>')
            command1 = 'cd ' + str(remote_path)
            child.sendline(command1)
            child.expect('sftp>')
            command2 = 'put ' + phstr
            child.sendline(command2)
            child.expect('sftp>')
            command3 = 'cd ../..'
            child.sendline(command3)
            child.expect('sftp>')
            self.logg.log.info("File %s was sent to destination", phstr)
            return 0
        except (pexpect.EOF, pexpect.TIMEOUT):
            self.logg.log.error("File %s wasn't sent to destination", phstr)
            return 1

    def send_o_f_confirmation(self, ph: Path) -> int:
        lp = '/srv/henry/KomornikOnline/done'
        os.chdir(lp)
        pa = ass.read_pass(file='/srv/scripts/py/ssap', credent=1)
        remote_path = ph.parents[1].joinpath('XML')
        command0 = 'mkdir ' + str(remote_path)
        child = pexpect.spawnu('sftp ftp_komornik_nadrzedne@stor.b2-impact.pl')
        child.expect('Password:')
        child.sendline(pa)
        child.expect('sftp>')
        try:
            phstr = str(ph)
            child.sendline(command0)
            child.expect('sftp>')
            command1 = 'cd ' + str(remote_path)
            child.sendline(command1)
            child.expect('sftp>')
            command2 = 'put ' + phstr
            child.sendline(command2)
            child.expect('sftp>')
            command2a = 'ls ' + phstr
            child.sendline(command2a)
            child.expect(phstr)
            command3 = 'cd ../..'
            child.sendline(command3)
            child.expect('sftp>')
            command4 = 'bye'
            child.sendline(command4)
            child.close()
            self.logg_daily.log.info("%s został poprawnie wysłany", phstr)
            return 0
        except (pexpect.EOF, pexpect.TIMEOUT):
            self.logg_daily.log.error(
                "%s nie został wysłany, następna próba jutro", phstr)
            return 1

    def update_log_only(self, ph: Path) -> None:
        phstr = str(ph)
        self.logg_daily.log.error(
            "%s miał rozmiar poniżej 50B i nie został wysłany", phstr)
