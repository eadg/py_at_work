from pssh.clients import ParallelSSHClient
from pssh.exceptions import Timeout
import ass


class Sudo(object):

    def __init__(self):
        self.hosts = self.load_host()
        # self.hosts = ['jboss-teren-dev', 'jboss-teren-test', 'jboss-teren2']
        # self.hosts = ['takto-esb', 'jboss-teren-dev', 'jboss-teren2']

    def load_host(self):
        hosts = []
        with open('/home/jrokicki/tickets/tls/hosts_non-production.txt') as f:
            for line in f:
                hosts.append(line.rstrip())
        return hosts

    def run_without(self, pty=False):
        client = ParallelSSHClient(self.hosts)
        output = client.run_command(
            'ps aux | grep jboss | grep java | grep -v grep',
            use_pty=pty, timeout=5
        )
        for host, host_output in output.items():
            for line in host_output['stdout']:
                print("Host {0}: {1}".format(host, line))

    def run_with(self, pty=True):
        client = ParallelSSHClient(self.hosts)
        output = client.run_command(
            'ps aux | grep jboss | grep java | grep -v grep',
            use_pty=pty, timeout=1, sudo=True
        )
        for host, host_output in output.items():
            try:
                stdin = host_output['stdin']
                # print(type(stdin))
                pas = ass.read_pass(credent=2)
                stdin.write(pas+'\n')
                stdin.flush()
                for line in host_output['stdout']:
                    print("Host: {0}:: {1}".format(host, line))
            except Timeout:
                print("{0} -> taaaaaaajm is on my side".format(host))


if __name__ == '__main__':
    Sudo().run_without(pty=False)
    # print(Sudo().hosts)
    # Sudo().run_with(pty=True)
