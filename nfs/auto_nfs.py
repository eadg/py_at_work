#!/bin/python

import sys
import unittest
import subprocess
import re


class Diff(object):

    def __init__(self):
        if len(sys.argv) > 1:
            self.host = sys.argv[1]
        else:
            self.host = 'smykfiles0'

    def nfs(self):
        sm = subprocess.check_output(["showmount", "-e", self.host])
        paths = sm.split('\n')
        c = re.compile('.+?/\d\d$')
        for line in paths:
            path = (line.split(' ')).pop(0)
            m = re.search(c, path)
            # if '/' in line:
            if m:
                yield path

    def check_mount(self):
        df = subprocess.check_output(["df", "-h"])
        for remote in self.nfs():
            if remote in df:
                print(remote + ' is')
            else:
                print(remote + ' NO is')

    def mount(self):
        pass


class TestPackage(unittest.TestCase):

    def test_argv_no_arg(self):
        sys.argv.append('test')
        d = Diff()
        self.assertIsInstance(d.host, str)
        self.assertGreater(len(d.host), 0)
        self.assertEquals(d.host, 'test')
        sys.argv.pop()

    def test_argv_arg(self):
        d = Diff()
        self.assertIsInstance(d.host, str)
        self.assertEquals(d.host, 'smykfiles0')
        self.assertGreater(len(d.host), 0)


if __name__ == '__main__':
    # suite = unittest.TestLoader().loadTestsFromTestCase(TestPackage)
    # unittest.TextTestRunner(verbosity=2).run(suite)
    d = Diff()
    d.check_mount()
