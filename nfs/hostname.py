import socket


class Hostname(object):

    def __init__(self):
        self.host_name = self.getHostname()

    def getHostname(self):
        ghn = socket.gethostname()
        if not ('0-' or '1-') in ghn:
            return self.removeSuffix(ghn)
        else:
            return self.getEtcHosts(ghn)

    def getEtcHosts(self, host):
        try:
            hosts_tuple = socket.gethostbyaddr(host)
            if not hosts_tuple[1]:
                return self.removeSuffix(hosts_tuple[0])
            else:
                listh = [h for h in hosts_tuple[1] if not ('0-' or '1-') in h]
                return self.removeSuffix(listh[0]) if any(
                    listh) else self.removeSuffix(host)
        except (socket.error, socket.herror):
            return self.removeSuffix(host)

    def removeSuffix(self, hostname):
        if 'ultimo.pl' in hostname:
            return hostname.split('.', 1)[0]
        else:
            return hostname
