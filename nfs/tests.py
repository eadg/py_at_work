import unittest
from auto_nfs import Diff
import sys


class TestPackage(unittest.TestCase):

    def test_argv_no_arg(self):
        sys.argv.append('test')
        d = Diff()
        self.assertIsInstance(d.host, str)
        self.assertGreater(len(d.host), 0)
        self.assertEquals(d.host, 'test')
        sys.argv.pop()

    def test_argv_arg(self):
        d = Diff()
        self.assertIsInstance(d.host, str)
        self.assertEquals(d.host, 'smykfiles0')
        self.assertGreater(len(d.host), 0)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPackage)
    unittest.TextTestRunner(verbosity=2).run(suite)
