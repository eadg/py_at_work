import wsgiref.simple_server as wss
import json
import os.path
import re
import os
import threading
import time


class CustomWsgiReq(wss.WSGIRequestHandler):

    '''RequestHandler with blank log_message -> for off spaming'''

    def log_message(self, format, *args):
        pass


class WebJson(object):

    '''App return dict [path_disk]=letter as WSGI'''

    def __init__(self, ttl):
        self.ttl = ttl

    def read(self):
        d = dict()
        with open('/proc/mounts') as f:
            lines = f.readlines()
            for line in lines:
                if 'clientaddr=' in line:
                    pieces = re.split('\s+', line)
                    raw_path = (pieces.pop(0).split(':').pop(1))
                    key = os.path.normpath(raw_path)
                    value = (pieces.pop(0).split(' ').pop(0).split('/')).pop()
                    d[key] = value
        return d if len(d) > 0 else '-'

    def wsgi_app(self, environ, start_response):
        status = '200 OK'
        headers = [('Content-Type', 'application/json')]
        start_response(status, headers)
        json_disks = json.dumps(self.read())
        return json_disks

    def run(self):
        httpd = wss.make_server('', 9000, self.wsgi_app, handler_class=CustomWsgiReq)
        httpd.serve_forever()

    def daying(self):
        time.sleep(self.ttl)
        os._exit(0)

if __name__ == '__main__':
    d = WebJson(55)
    t = threading.Thread(target=d.daying)
    t.start()
    d.run()
