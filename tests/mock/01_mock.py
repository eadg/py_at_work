from unittest.mock import MagicMock
from production import *


if __name__ == '__main__':
    thing = ProductionClass()
    thing.method = MagicMock(return_value=7)
    what = thing.method(3, 4, 5, key='valuE')
    print(what)
    thing.method.assert_called_with(3, 4, 5, key='value')
    # print(what2)
