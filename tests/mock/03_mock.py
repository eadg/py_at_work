from unittest.mock import Mock

mock = Mock(side_effect=KeyError('foo'))
# mock()

values = {'a': 1, 'b': 2, 'c': 3}
def side_effecto(arg):
    return values[arg]

mock.side_effect = side_effecto
print(mock('c'))


mock.side_effect = [5, 4, 3, 2, 1]
print(mock('c'))
print(mock('c'))
print(mock('c'))
print(mock('c'))
print(mock('c'))
