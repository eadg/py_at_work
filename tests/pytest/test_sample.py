def func(x):
    if isinstance(x, int):
        return x+1
    if isinstance(x, str):
        if len(x) > 1:
            z = list(x)
            x = z.pop(0)
            print(x)
        return ord(x) + 1


def test_int():
    assert func(3) == 4


def test_str():
    assert func('ab') == ord('b')
