#!/usr/bin/python2.7

import os
import dbus


class Executer(object):

    '''Check if jboss is dead and than restart it'''

    def __init__(self):
        self.cmd = ['systemctl restart jboss7']

    def isRun(self):
        pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
        for pid in pids:
            try:
                if '/srv/jboss/jboss/bin/standalone.sh' in open(os.path.join(
                        '/proc/', pid, 'cmdline'), 'rb').read():
                    return True
            except OSError:
                continue
        return False

    def reRun(self):
        sysbus = dbus.SystemBus()
        systemd1 = sysbus.get_object(
            'org.freedesktop.systemd1', '/org/freedesktop/systemd1')
        manager = dbus.Interface(
            systemd1, 'org.freedesktop.systemd1.Manager')
        manager.RestartUnit('jboss7.service', 'fail')


if __name__ == '__main__':
    e = Executer()
    check = e.isRun()
    if not check:
        e.reRun()
