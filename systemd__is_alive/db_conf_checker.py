import hashlib
import pathlib
import platform
import subprocess


rodo_webinf_path1 = 'C:/Program Files/Apache Software Foundation/'
rodo_webinf_path2 = 'Tomcat 8.5_Tomcat8RODOprotector/apki/test/'
rodo_webinf_path3 = 'database.properties'
# rodo_webinf_path2 = 'Tomcat 8.5_Tomcat8RODOprotector/webapps/rodo/WEB-INF'


rodo_webinf_path = rodo_webinf_path1 + rodo_webinf_path2 + rodo_webinf_path3


p = pathlib.Path(rodo_webinf_path)
print(type(p))


def get_sum(path_to_file):
    f = open(path_to_file, 'rb')
    blake = hashlib.blake2b(f.read())
    hasz = blake.hexdigest()
    return hasz


def get_tomcat():
    if platform.system() in 'Windows':
        h = get_sum(rodo_webinf_path)
        print(h)
        win_com3 = 'sc query | findstr SERVI | findstr /I tomc'
        subprocess.run(win_com3, capture_output=True)
        print(win_com3)


if __name__ == '__main__':
    get_tomcat()
