import dbus
import os
import subprocess
import sys
import logging


class Operation(object):

    def __init__(self):
        if len(sys.argv) == 2:
            self.oper = sys.argv[1]
        else:
            print('Run it with one argument: either start or stop or restart')
            sys.exit(1)
        self.pathpid = '/srv/scripts/runhalt.pid'
        self.log = logging.getLogger()

    def doit(self):
        if self.oper in ['start', 'stop', 'restart']:
            self.log.warning("Wykonana operacja: " + self.oper)
            self.enable()
            getattr(self, self.oper)()
        else:
            print('Run it with one argument: EITHER start OR stop OR restart')
            sys.exit(1)

    def start(self):
        with open(self.pathpid, 'w+'):
            pass
        fp = ForkPy()
        fp.forking()

    def stop(self):
        pid = self.isRun()
        if int(pid) > 0:
            self.log.warn('jest pid')
            subprocess.call(['kill', '-15', pid])
            try:
                os.remove(self.pathpid)
            except OSError as e:
                print('Occurs error %s' % e)

    def restart(self):
        self.stop()
        self.start()

    def isRun(self):
        pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
        for pid in pids:
            try:
                if 'x11vnc' in open(os.path.join(
                        '/proc/', pid, 'cmdline'), 'r').read():
                    return pid
            except OSError:
                continue
        return 0

    def enable(self):
        '''
        doc: how to use dbus lib
        manager.RestartUnit('nslcd.service', 'fail')
        manager.EnableUnitFiles(['nslcd.service'], 'True', 'True')
        manager.StoptUnit('nslcd.service', 'fail')
        manager.StarttUnit('nslcd.service', 'fail')
        example of using dbus instance of subprocess
        -=-=-=-=-
        Method requires root privilege :(
        '''
        sysbus = dbus.SystemBus()
        systemd1 = sysbus.get_object(
            'org.freedesktop.systemd1', '/org/freedesktop/systemd1')
        manager = dbus.Interface(
            systemd1, 'org.freedesktop.systemd1.Manager')
        manager.EnableUnitFiles(['x11vnc.service'], 'True', 'True')


class ForkPy(object):

    def forking(self):
        try:
            pid = os.fork()
            if pid > 0:
                sys.exit(0)
            else:
                self.run_x11vnc()
        except OSError as e:
            msg = "fork exception: %d %s" % (e.errno, e.strerror)
            sys.stderr.write(msg)
            sys.exit(1)

    def run_x11vnc(self):
        comm1 = 'x11vnc -auth /run/user/1000/gdm/Xauthority'
        comm2 = ' -usepw -ncache 10 -noxdamage -shared -forever'
        command = comm1 + comm2
        commlist = command.split()
        subprocess.call(commlist)


if __name__ == '__main__':
    o = Operation()
    o.doit()
