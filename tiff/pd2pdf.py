import pathlib

import img2pdf
from pdf2image import convert_from_path

from disk_operations import DiskOper

out_dir_name = 'ZMIENIONE'
ppi = pathlib.Path('/tmp')
input_path = ppi.resolve()
DiskOper().mkdir_if(pathlib.Path(input_path).joinpath(out_dir_name))

out_dir_name = 'ZMIENIONE'
output_path = pathlib.Path(input_path).joinpath(out_dir_name)
list_pdf = input_path.glob('*.pdf')
for f in list_pdf:
    file_pdf = convert_from_path(f)
    print(len(file_pdf))
    # with open(full_output, "wb") as f1:
    num = 1
    for img in file_pdf:
        if len(file_pdf) > 1:
            full_output = str(
                output_path.joinpath('unsec_' + f.stem + num + '.pdf'))
        else:
            full_output = str(output_path.joinpath('unsec_' + f.stem + '.pdf'))
        print(type(img))
        img.save(full_output)
        num += 1
