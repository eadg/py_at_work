import os
import os.path
import sys
from PIL import Image, ImageSequence, TiffImagePlugin
from disk_operations import DiskOper
import logger


class StarterTiffSplit(object):

    def __init__(self):
        self.input_path = sys.argv[1:]

    def run(self):
        for inpa in self.input_path:
            ts = PDFSplit(inpa)
            ts.make_dir_out()
            ts.lets_iterate(inpa)
            ts.logging.close_logs("Koniec przetwarzania.")


class PDFSplit(object):

    def __init__(self, path_arg):
        self.input_path = path_arg
        self.output_path, self.log_path = self.prepare_output()
        self.make_dir_out()
        self.logging = logger.Logger(
            os.path.normpath(self.output_path + '/log.txt'))

    def prepare_output(self):
        out_dir_name = 'PODZIELONE'
        sep = DiskOper().get_proper_os_sep(self.input_path)
        input_list = self.input_path.split(sep)
        if len(input_list) > 1:
            # input_list.insert(-1, out_dir_name)
            input_list.append(out_dir_name)
            outpath = (os.sep).join(input_list)
            logpath = outpath[:-1]
            return outpath, logpath
        else:
            work_path = out_dir_name + '/' + self.input_path
            return work_path, work_path

    def make_dir_out(self):
        DiskOper().mkdir_if(self.output_path)

    def lets_iterate(self, file_img):
        img = Image.open(file_img)


if __name__ == '__main__':
    sts = StarterTiffSplit()
    sts.run()
