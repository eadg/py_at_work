from PIL import TiffImagePlugin
from PIL import Image, ImageSequence
import os


def parse_tif(filePath, numFrames):
    img = Image.open(filePath)
    for i in range(numFrames):
        try:
            img.seek(i)
            img.save('Block_%s.tif'%(i,))
        except EOFError: #end of file error
            pass

def split_tif(filePath):
    img = Image.open(filePath)
    for i, page in enumerate(ImageSequence.Iterator(img)):
        page.save("page%d.tif" % i)


def to_list(filePath):
    liti = []
    img = Image.open(filePath)
    for i, page in enumerate(ImageSequence.Iterator(img)):
        liti.append(page)
    return liti


with TiffImagePlugin.AppendingTiffWriter(path_tmp+'/'+merged_file_name, True) as tf:
    for tiff_in in list_file:
        try:
            im= Image.open(path_tmp+'/'+tiff_in)
            im.save(tf)
            tf.newFrame()
            im.close()
        except:
            pass


def tif5p(filePath):
    img = Image.open(filePath)
    with TiffImagePlugin.AppendingTiffWriter('merged_file_name.tif', True) as tf:
        for page in ImageSequence.Iterator(img):
            page.save(tf)
            tf.newFrame()


def tif5p(filePath):
    os.remove('merged_file_name.tif')
    img = Image.open(filePath)
    pages = (2, 3)
    with TiffImagePlugin.AppendingTiffWriter('merged_file_name.tif', True) as tf:
        for i, page in enumerate(ImageSequence.Iterator(img)):
            if i in pages:
                page.save(tf)
                tf.newFrame()
