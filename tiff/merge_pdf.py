from PyPDF2 import PdfFileMerger

merger = PdfFileMerger()
for i in range(0, 8):
    name = "SD-BackupPC-{0}.pdf".format(i)
    print(name)
    merger.append(name)

merger.write("result.pdf")
merger.close()
