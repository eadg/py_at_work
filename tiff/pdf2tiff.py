import os
import pathlib
import platform

from pdf2image import convert_from_path
from PIL import TiffImagePlugin
import pytesseract

from disk_operations import DiskOper
from starter_img_oper import Starter
import logger

class Pdf2Tiff():
    '''
    Class provide method to convert pdf to tiff
    '''

    def __init__(self, path_arg):
        self.path = path_arg
        self.input_path, self.output_path, self.log_path = self.prepare_output(
            self.path)
        self.make_dir_out()
        self.logging = logger.Logger(self.log_path)
        self.home_scr = self.get_home_scripts()
        if 'Wind' in platform.system():
            os.environ['PATH'] += os.pathsep + self.home_scr + '\\poppler\\bin\\'
            pytesseract.pytesseract.tesseract_cmd = self.home_scr + '\\Tesseract-OCR\\tesseract.exe'

    def get_home_scripts(self):
        fpa = logger.__file__
        pa = pathlib.Path(fpa).parents[2]
        return str(pa)

    def prepare_output(self, path):
        '''Return pathlib.Path objects for input_path output_path log_path'''
        out_dir_name = 'ZMIENIONE'
        ppi = pathlib.Path(path)
        input_path = ppi.resolve()
        output_path = pathlib.Path(input_path).joinpath(out_dir_name)
        log_path = output_path.joinpath('log.txt')
        return input_path, output_path, log_path

    def make_dir_out(self):
        DiskOper().mkdir_if(self.output_path)

    def lets_iterate(self):
        '''Save every pdf-files from inputpath as series images tiff-files'''
        if self.input_path.is_dir():
            list_pdf = self.input_path.glob('*.pdf')
            for f in list_pdf:
                file_pdf = convert_from_path(f)
                full_output = str(self.output_path.joinpath(f.stem + '.tiff'))
                with TiffImagePlugin.AppendingTiffWriter(
                        full_output, True) as joined:
                    for img in file_pdf:
                        img.save(joined, compression='tiff_lzw')
                        joined.newFrame()
                self.logging.log.info("Save {}".format(full_output))
        elif self.input_path.is_file():
            self.input_path = self.input_path.parent
            self.lets_iterate()


if __name__ == '__main__':
    sts = Starter(Pdf2Tiff)
    sts.run()
