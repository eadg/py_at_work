import argparse
import math
import multiprocessing as mp
import pathlib
import subprocess
import time


class Master:
    """Master class run workers."""

    def __init__(self):
        self.parse_detail()

    def parse_detail(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '-m', action='store', help='file name', dest='mode')
        results = parser.parse_args()
        self.mode = results.mode

    def lets_iterate(self) -> None:
        pp = pathlib.Path('.')
        pp.joinpath('ZROBIONE').mkdir(exist_ok=True)
        self.scan(pp)

    def work(self, sleep: str, pathfile: str) -> None:
        path_script = pathlib.Path(__file__).parents[0]
        path_servent = path_script.joinpath('servent_gs.py')
        command = ['python', path_servent, sleep, pathfile, self.mode]
        subprocess.call(command)

    def get_number_workers(self):
        num = math.ceil(mp.cpu_count() * 0.75)
        if num < 2:
            return 2
        return num

    def scan(self, pp_input) -> None:
        pool = mp.Pool(self.get_number_workers())
        pp_gen = pp_input.glob('*.pdf')
        seconds = 0
        for pathfile in pp_gen:
            pathfile_str = str(pathfile)
            seconds += 1
            sec_str = str(seconds)
            pool.apply_async(self.work, (sec_str, pathfile_str,))
        pool.close()
        pool.join()


if __name__ == '__main__':
    start = time.time()
    mas = Master()
    mas.lets_iterate()
    end = time.time()
    delta = math.ceil(end-start)
    print(f"Przetwarzanie zakoczone. Calkowity czas: {delta}s")
