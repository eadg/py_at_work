import argparse
import importlib
import os.path
import sys
from tkinter import Button, E, Frame, Label, StringVar, Tk, W, filedialog

from starter_img_oper import Starter


class GUI(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()
        self.fias = "Kliknij przycisk i wybierz katalog"
        self.label_text = StringVar()
        self.label_text.set(self.fias)
        self.label = Label(master, textvariable=self.label_text)
        self.choose_button = Button(
            master, text="Wybór katalogu",
            command=lambda: self.select_dir(), background="gold")
        self.choose_button.grid(row=0, sticky=W)
        self.label.grid(row=1)
        self.run_button = Button(
            master, text="Start", command=lambda: self.start(),
            background="green")
        self.run_button.grid(row=2, sticky=W)
        self.quit_button = Button(
            master, text="Quit", command=lambda: self.quit(),
            background="red")
        self.quit_button.grid(row=2, sticky=E)

    def parse_detail(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '-f', action='store', help='file name', dest='file_name')
        parser.add_argument(
            '-c', action='store', help='class name', dest='class_name')
        results = parser.parse_args()
        self.cla = results.class_name
        self.mod = results.file_name

    def select_dir(self):
        self.fias = filedialog.askdirectory()
        self.label_text.set(self.fias)

    def start(self):
        sys.argv = sys.argv[:1]
        if os.path.isdir(self.fias):
            sys.argv.append(self.fias)
            mod = importlib.import_module(self.mod)
            cla = getattr(mod, self.cla)
            sts = Starter(cla)
            sts.run()
            self.label_text.set('Koniec przetwarzania. ')
        else:
            self.label_text.set('To nie jest katalog... ')

    def quit(self):
        self.master.destroy()


if __name__ == '__main__':
    root = Tk()
    app = GUI(master=root)
    root.title(app.cla)
    root.mainloop()
