import math
import os
import os.path
import platform
import subprocess
import sys
import time

from cross_platform import CRPL


class Servent:

    '''Worker class'''

    def get_gs(self) -> str:
        crpl = CRPL()
        if 'Wind' in platform.system():
            os.environ['PATH'] += os.pathsep + crpl.get_win_drive(
                sys.argv[0]) + '\\installed\\gs\\bin\\'
            return 'gswin64c.exe'
        if 'Linux' in platform.system():
            return 'gs'
        sys.exit('Brak ghostscript')

    def do_work(self, sec: int, pathf: str, mode: str) -> None:
        ghs = self.get_gs()
        print(f'Start pliku {sec}: {pathf}')
        start = time.time()
        arg_out = '-sOutputFile=' + 'ZMIENIONE\\' + pathf
        if 'g' in mode:
            com = [ghs, '-q', '-dNOPAUSE', '-dBATCH', '-sDEVICE=pdfwrite',
                   '-dPDFSETTINGS=/prepress', '-dDisableGlyphBoundingBox',
                   '-sColorConversionStrategy=Gray', '-dAutoRotatePages=/None',
                   '-MdProcessColorModel=/DeviceGray', arg_out, pathf]
        elif 'c' in mode:
            com = [ghs, '-q', '-dNOPAUSE', '-dBATCH', '-sDEVICE=pdfwrite',
                   '-dPDFSETTINGS=/prepress', '-dDisableGlyphBoundingBox',
                   arg_out, pathf]
        subprocess.run(com, capture_output=True, check=True)
        stop = time.time()
        delta = math.ceil(stop-start)
        print(f'Plik {sec}: {pathf} zrobiony w {delta}s')


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('Potrzebne sa 3 argumenty', file=sys.stderr)
        sys.exit(1)
    serv = Servent()
    serv.do_work(int(sys.argv[1]), sys.argv[2], sys.argv[3])
