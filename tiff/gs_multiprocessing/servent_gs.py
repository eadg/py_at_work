import math
import os
import os.path
import platform
import subprocess
import sys
import time


class Servent:

    '''Worker class'''

    def get_gs(self) -> str:
        if 'Wind' in platform.system():
            cwin = 'C:\\Program Files'
            ph1 = cwin + '\\PDFCreator\\ghostscript\\bin\\gswin32c.exe'
            ph2 = cwin + '\\gs\\gs9.56.1\\bin\\gswin64c.exe'
            ph3 = cwin + ' (x86)\\PDFCreator\\ghostscript\\bin\\gswin32c.exe'
            list_paths = [ph1, ph2, ph3]
            for pat in list_paths:
                if os.path.isfile(pat):
                    return pat
        elif 'Linux' in platform.system():
            return 'gs'
        sys.exit('Brak ghostscript')

    def do_work(self, sec: int, pathf: str, mode: str) -> None:
        ghs = self.get_gs()
        print(f'Start pliku {sec}: {pathf}')
        start = time.time()
        arg_out = '-sOutputFile=' + 'ZROBIONE/' + pathf
        if 'g' in mode:
            com = [ghs, '-q', '-dNOPAUSE', '-dBATCH', '-sDEVICE=pdfwrite',
                   '-dPDFSETTINGS=/prepress', '-dDisableGlyphBoundingBox',
                   '-sColorConversionStrategy=Gray', '-dAutoRotatePages=/None',
                   '-dProcessColorModel=/DeviceGray', arg_out, pathf]
        elif 'c' in mode:
            com = [ghs, '-q', '-dNOPAUSE', '-dBATCH', '-sDEVICE=pdfwrite',
                   '-dPDFSETTINGS=/prepress', '-dDisableGlyphBoundingBox',
                   arg_out, pathf]
        subprocess.run(com, capture_output=True, check=True)
        stop = time.time()
        delta = math.ceil(stop-start)
        print(f'Plik {sec}: {pathf} zrobiony w {delta}s')


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('Potrzebne sa 3 argumenty', file=sys.stderr)
        sys.exit(1)
    serv = Servent()
    serv.do_work(int(sys.argv[1]), sys.argv[2], sys.argv[3])
