import argparse
import math
import multiprocessing as mp
import pathlib
import subprocess
import time

from starter_img_oper import Starter

from x2y import X2Y


class Master(X2Y):
    """Master class run workers."""

    def __init__(self, path_arg):
        super().__init__(path_arg)
        self.mode = 'c'
        # self.parse_detail()

    def parse_detail(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '-m', action='store', help='file name', dest='mode')
        results = parser.parse_args()
        self.mode = results.mode

    def lets_iterate(self) -> None:
        pp = pathlib.Path('.')
        self.scan(pp)

    def work(self, sleep: str, pathfile: str) -> None:
        path_script = pathlib.Path(__file__).parents[0]
        path_servent = path_script.joinpath('servent_gs_gui.py')
        command = ['python', path_servent, sleep, pathfile, self.mode]
        subprocess.call(command)

    def get_number_workers(self):
        num = math.ceil(mp.cpu_count() * 0.75)
        if num < 2:
            return 2
        return num

    def scan(self, pp_input) -> None:
        pool = mp.Pool(self.get_number_workers())
        pp_gen = pp_input.glob('*.pdf')
        seconds = 0
        for pathfile in pp_gen:
            pathfile_str = str(pathfile)
            seconds += 1
            sec_str = str(seconds)
            pool.apply_async(self.work, (sec_str, pathfile_str,))
        pool.close()
        pool.join()


if __name__ == '__main__':
    start = time.time()
    sts = Starter(Master)
    sts.run()
    end = time.time()
    delta = math.ceil(end-start)
    print(f"Przetwarzanie zakoczone. Calkowity czas: {delta}s")
