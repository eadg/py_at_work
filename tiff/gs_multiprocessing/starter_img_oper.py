import sys


class Starter(object):

    def __init__(self, name):
        self.input_path = sys.argv[1]
        self.name_class = name

    def run(self):
        ts = self.name_class(self.input_path)
        ts.make_dir_out()
        ts.lets_iterate()
        ts.logging.close_logs("Koniec przetwarzania.")
