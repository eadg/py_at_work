import os
import pathlib
import platform
import subprocess
import sys
import time

from cross_platform import CRPL


class Master:

    '''Master class: sequence way'''

    def __init__(self):
        crpl = CRPL()
        if 'Wind' in platform.system():
            os.environ['PATH'] += os.pathsep + crpl.get_win_drive(sys.argv[0])\
                + 'C:\\Program Files\\PDFCreator\\ghostscript\\bin\\'
            self.bin_gs = 'gswin32c.exe'
        else:
            self.bin_gs = 'gs'

    def lets_iterate(self) -> None:
        pp = pathlib.Path('.')
        pp.joinpath('ZROBIONE').mkdir(exist_ok=True)
        self.scan(pp)

    def scan(self, pp_input) -> None:
        pp_gen = pp_input.glob('*.pdf')
        seconds = 0
        for pathfile in pp_gen:
            pathfile_str = str(pathfile)
            seconds += 1
            arg_out = '-sOutputFile=' + '/tmp/' + str(seconds) + '.pdf'
            subprocess.run(
                ['gs', '-q', '-dNOPAUSE', '-dBATCH', '-dPDFSETTINGS=/prepress',
                 '-sDEVICE=pdfwrite', '-dDisableGlyphBoundingBox', arg_out,
                 pathfile_str],
                capture_output=True, check=True)
            print(f'I just did some hard work for {seconds} s!')


if __name__ == '__main__':
    start = time.time()
    mas = Master()
    mas.lets_iterate()
    end = time.time()
    print("\n czas: {}".format(end-start))
