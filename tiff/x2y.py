import os.path
# import pathlib
from cross_platform import CRPL
from disk_operations import DiskOper
import logger


class X2Y(object):

    def __init__(self, path_arg):
        self.input_path = path_arg
        self.sep = CRPL().get_proper_os_sep(path_arg)
        self.output_path, self.log_path = self.prepare_output()
        self.make_dir_out()
        self.logging = logger.Logger(self.log_path)

    def prepare_output(self):
        out_dir_name = 'ZMIENIONE'
        p = os.path.abspath(self.input_path)
        # Last changes in 04.09:
        # ppi = pathlib.Path(path)
        # input_path = ppi.resolve()
        p_list = p.split(self.sep)
        p_list.append(out_dir_name+self.sep)
        outpath = self.sep.join(p_list)
        logpath = outpath + 'log.txt'
        return outpath, logpath

    def make_dir_out(self):
        DiskOper().mkdir_if(self.output_path)

    def lets_iterate(self):
        raise NotImplementedError()
