import os
import pathlib
import platform

from pdf2image import convert_from_path
from PyPDF2 import PdfFileMerger

import logger
from disk_operations import DiskOper
from starter_img_oper import Starter

class Pdf2Pdf():

    def __init__(self, path_arg):
        self.path = path_arg
        self.input_path, self.output_path, self.log_path = self.prepare_output(
            self.path)
        self.make_dir_out()
        self.logging = logger.Logger(self.log_path)
        self.home_scr = self.get_home_scripts()       
        if 'Wind' in platform.system():
            os.environ['PATH'] += os.pathsep + self.home_scr + '\\poppler\\bin\\'

    def get_home_scripts(self):
        fpa = logger.__file__
        pa = pathlib.Path(fpa).parents[2]
        return str(pa)

    def prepare_output(self, path):
        '''Return pathlib.Path objects for input_path output_path log_path'''
        out_dir_name = 'ZMIENIONE'
        ppi = pathlib.Path(path)
        input_path = ppi.resolve()
        output_path = pathlib.Path(input_path).joinpath(out_dir_name)
        log_path = output_path.joinpath('log.txt')
        return input_path, output_path, log_path

    def make_dir_out(self):
        DiskOper().mkdir_if(self.output_path)

    def lets_iterate(self):
        """Save every pdf-file from inputpath as series pdf-files:)."""
        if self.input_path.is_dir():
            list_pdf = self.input_path.glob('*.pdf')
            for f in list_pdf:
                file_pdf = convert_from_path(f)
                num = 1
                list_files = []
                for img in file_pdf:
                    if len(file_pdf) > 1:
                        full_output = str(self.output_path.joinpath(
                            'unsec_' + f.stem + '___' + str(num) + '.pdf'))
                        list_files.append(full_output)
                    else:
                        full_output = str(self.output_path.joinpath(
                            'unsec_' + f.stem + '.pdf'))
                    img.save(full_output)
                    num += 1
                self.logging.log.info("Save {}".format(full_output))
                if len(list_files) > 1:
                    full_output = str(
                        self.output_path.joinpath('unsec_' + f.stem + '.pdf'))
                    self.merge(list_files, full_output)
        elif self.input_path.is_file():
            self.input_path = self.input_path.parent
            self.lets_iterate()

    def merge(self, lf: list, name: str) -> None:
        merger = PdfFileMerger()
        for i in lf:
            merger.append(i)
        merger.write(name)
        merger.close()
        for i in lf:
            os.remove(i)


if __name__ == '__main__':
    sts = Starter(Pdf2Pdf)
    sts.run()
