import argparse
from pathlib import Path
import os
import sys
import pdfkit
import logger
from disk_operations import DiskOper


class Html2Pdf(object):

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-p', action='store', help='path', dest='ap')
        self.parser.add_argument('-l', action='store', help='Sciezka dla pliku\
        logow. Domyslnie /dev/null', dest='logging_value', default='')
        results = self.parser.parse_args()
        self.argpath = results.ap
        self.logger = logger.Logger(results.logging_value).log

    def list(self):
        self.abs_in_path = os.path.abspath(self.argpath)
        scan_it = os.scandir(self.abs_in_path)
        self.out_path = DiskOper().mkdir_if(self.argpath + '/ZMIENIONE')
        for entry in scan_it:
            if entry.is_file and 'htm' in entry.name:
                # print(entry.name)
                self.conf(entry.name)

    def conf(self, name):
        sufix = '.pdf'
        stem, _ = name.split('.')
        inp = self.argpath + os.path.sep + name
        oup = self.argpath + '/ZMIENIONE/' + stem + sufix
        options = {
            'quiet': '', 'margin-left': '10mm', 'margin-right': '10mm',
            'margin-top': '43mm', 'margin-bottom': '13mm', 'page-size': 'A4'}
        self.logger.info('Konwersja ' + inp + ' do ' + oup)
        pdfkit.from_file(inp, oup, options)

    def conf2(self, name):
        '''based on Path objects'''
        pl_input = Path(self.abs_in_path)
        pl_input_list = list(pl_input.parts)
        pl_input_list[-2] = 'ZMIENIONE'
        print(pl_input_list)


if __name__ == '__main__':
    hp = Html2Pdf()
    hp.list()
    # hp.conf2('test.html')
