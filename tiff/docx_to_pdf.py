import os
import os.path
import pathlib

from docx2pdf import convert

from disk_operations import DiskOper
from starter_img_oper import Starter
from x2y import X2Y


class Docx2Pdf(X2Y):

    def __init__(self, path_arg):
        super().__init__(path_arg)

    def lets_iterate(self):
        list_img = os.listdir(self.input_path)
        for f in list_img:
            full_input_name = os.path.normpath(self.input_path + self.sep + f)
            pp_input = pathlib.Path(full_input_name)
            if pp_input.is_file() and DiskOper().name_checker(
                    full_input_name, 'docx'):
                full_output_name = os.path.normpath(
                    self.output_path + self.sep + pp_input.stem + '.pdf')
                try:
                    convert(full_input_name, full_output_name)
                except TypeError as e:
                    self.logging.log.error(
                            "Błąd: {} w pliku: {}".format(e, full_input_name))
                self.logging.log.info("Input: {} -> Output: {}".format(
                    full_input_name, full_output_name))


if __name__ == '__main__':
    sts = Starter(Docx2Pdf)
    sts.run()
