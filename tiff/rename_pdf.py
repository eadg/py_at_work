import os
import os.path
import pathlib
import platform
import re
import shutil

from pdf2image import convert_from_path
import pytesseract

from cross_platform import CRPL
from disk_operations import DiskOper
from starter_img_oper import Starter
import logger

class PdfRename():
    '''
    Class provide method to rename pdf files according to str in files.
    '''

    def __init__(self, path_arg):
        crpl = CRPL()
        self.input_path = path_arg
        self.sep = crpl.get_proper_os_sep(path_arg)
        self.output_path, self.log_path = self.prepare_output()
        self.make_dir_out()
        self.logging = logger.Logger(self.log_path)
        self.home_scr = self.get_home_scripts()
        if 'Wind' in platform.system():
            os.environ['PATH'] += os.pathsep + self.home_scr + '\\poppler\\bin\\'
            pytesseract.pytesseract.tesseract_cmd = self.home_scr + '\\Tesseract-OCR\\tesseract.exe'

    def get_home_scripts(self):
        fpa = logger.__file__
        pa = pathlib.Path(fpa).parents[2]
        return str(pa)

    def prepare_output(self):
        out_dir_name = 'ZMIENIONE'
        p = os.path.abspath(self.input_path)
        p_list = p.split(self.sep)
        p_list.append(out_dir_name+self.sep)
        outpath = self.sep.join(p_list)
        logpath = outpath + 'log.txt'
        return outpath, logpath

    def make_dir_out(self):
        DiskOper().mkdir_if(self.output_path)

    def lets_iterate(self):
        list_pdf = os.listdir(self.input_path)
        pat = re.compile(r'numerze\W+(\d+)\W+z')
        for f in list_pdf:
            full_input_name = os.path.normpath(self.input_path + self.sep + f)
            if os.path.isfile(full_input_name) and 'pdf' in f:
                try:
                    img = convert_from_path(full_input_name)
                    text = pytesseract.image_to_string(img.pop())
                    searched = re.search(pat, text)
                    number = searched.group(1)
                    full_output_name = os.path.normpath(
                        self.output_path + self.sep + str(number) + '.pdf')
                    self.logging.log.info("Plik: {0} Zapisuje w: {1}".format(
                        full_input_name, full_output_name))
                    shutil.copyfile(full_input_name, full_output_name)
                except pytesseract.pytesseract.TesseractError as TE:
                    self.logging.log.error("Błąd {}".format(TE))


if __name__ == '__main__':
    sts = Starter(PdfRename)
    sts.run()
