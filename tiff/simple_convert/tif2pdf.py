import os
from pathlib import Path
import time

import img2pdf


def conv() -> None:
    os.chdir('/srv/www/intradok/__tmp__/')
    with open('argv.txt', encoding='utf-8') as filet:
        ftif = filet.readline().strip()
    fpdf = Path(ftif).stem + '.pdf'
    with open('argv2.txt', 'w', encoding='utf-8') as filet2:
        filet2.writelines(ftif)
        filet2.writelines(fpdf)
    with open(fpdf, "wb") as fout, open(ftif, "rb") as fin:
        fout.write(img2pdf.convert(fin))
    os.unlink('/srv/www/intradok/__tmp__/argv.txt')


if __name__ == '__main__':
    conv()

"""
    '''
        with open(fpdf, "wb") as fout, open(ftif, "rb") as fin:
            fout.write(img2pdf.convert(fin))
        with open('argv2.txt', 'w', encoding='utf-8') as filet:
            filet.writelines("nana")
    '''
    # ftif = 'KFN_DOW_I kwartał_2014.tiff'
    # ftif = 'a.tiff'
"""
