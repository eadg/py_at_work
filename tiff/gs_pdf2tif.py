import subprocess


class GS(object):

    def gen_open(self, path):
        with open(path) as f:
            for l in f.readlines():
                s = l.strip()
                yield s

    def run_gs(self):
        f = self.gen_open('/home/jrokicki/tickets/pdf2tif/list_files.txt')
        for i in f:
            print(i)
            file = i.split('.')[0]+'.tif'
            subprocess.call(["gs", "-o", file,  "-sDEVICE=tiff24nc", "-r200x200", "-dFirstPage=1", "-dLastPage=1", i])
