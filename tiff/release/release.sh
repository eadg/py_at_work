#!/bin/bash

find .. -maxdepth 1 -iname '*.py' -exec rsync -KLrpgoD {} . \;
find .. -maxdepth 1 -iname '*.cmd' -exec rsync -KLrpgoD {} . \;
rsync -KLrpgoD *.py jrokicki@wspolny.ultimo.pl:/srv/disks/dbo/installed/scripts/py/
rsync -KLrpgoD *.cmd jrokicki@wspolny.ultimo.pl:/srv/disks/dbo/installed/
