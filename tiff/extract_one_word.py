import argparse
import pathlib
import os
import os.path
import re
import sys

import pytesseract
from pdf2image import convert_from_path
from pdf2image.exceptions import PDFPageCountError

import logger


class ExtractOneWord():
    """Class extract client number from file."""

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-p', action='store', help='path', dest='ap')
        self.parser.add_argument('-l', action='store', help='Sciezka dla pliku\
        logow. Domyslnie /dev/null', dest='logging_value', default='')
        self.parser.add_argument('-o', action='store', help='Sciezka dla pliku\
        csv.', dest='csv')
        results = self.parser.parse_args()
        self.input_path = results.ap
        self.logging = logger.Logger(results.logging_value).log
        self.csv = results.csv

    def lets_iterate(self):
        # pat = re.compile(r'Numer\W+Klienta:\W+(\d+)\W+')
        pat = re.compile(r'[^,](\d+)\W+Za')
        with os.scandir(self.input_path) as it, open(self.csv, 'a') as fi:
            for entry in it:
                if entry.is_file() and 'pdf' in entry.name:
                    ppf = pathlib.Path(self.input_path).joinpath(entry.name)
                    # print(entry.name)
                    num = 0
                    try:
                        list_pages = convert_from_path(ppf.resolve())
                    except PDFPageCountError:
                        pass
                    for _ in list_pages:
                        num += 1
                    for i in range(2, num, 8):
                        # print(f"strona: {i}")
                        text = pytesseract.image_to_string(list_pages[i])
                        searched = re.search(pat, text)
                        if searched:
                            number = searched.group(1)
                        else:
                            number = 'xxxxx'
                        print(number)
                        line_log = entry.name + " " +  number + " " + str(num)
                        self.logging.info(line_log)
                        line = entry.name + '|' + number + '\n'
                        fi.write(line)


if __name__ == '__main__':
    eow = ExtractOneWord()
    eow.lets_iterate()
