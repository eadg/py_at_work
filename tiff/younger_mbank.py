import collections
import glob
import os
import pathlib
import platform
import re
import shutil
from datetime import date

import pytesseract
from pdf2image import convert_from_path
from pdf2image.exceptions import PDFSyntaxError

from disk_operations import DiskOper
from starter_img_oper import Starter
from x2y import X2Y
import logger

class Younger(X2Y):

    def __init__(self, path_arg):
        super().__init__(path_arg)
        self.home_scr = self.get_home_scripts()
        if 'Wind' in platform.system():
            os.environ['PATH'] += os.pathsep + self.home_scr + '\\poppler\\bin\\'
            pytesseract.pytesseract.tesseract_cmd = self.home_scr + '\\Tesseract-OCR\\tesseract.exe'

    def get_home_scripts(self):
        fpa = logger.__file__
        pa = pathlib.Path(fpa).parents[2]
        return str(pa)

    def lets_iterate(self) -> None:
        d = self.scan()
        self.sort_and_move(d)

    def scan(self) -> collections.defaultdict:
        dd = collections.defaultdict(list)
        gli = glob.iglob(self.input_path + '/*.pdf')
        for ele in gli:
            f = pathlib.PurePath(ele)
            if '_' in f.name:
                main_key = f.name.split('_')[0]
            else:
                main_key = f.stem
            dd[main_key].append([f.name])
        return dd

    def sort_and_move(self, d: collections.defaultdict) -> None:
        p = pathlib.Path(self.output_path)
        old = p.joinpath('stare')
        new = p.joinpath('nowe')
        DiskOper().mkdir_if(old)
        DiskOper().mkdir_if(new)
        for k, v in d.items():
            sortlist = []
            for li in v:
                fl = li.pop()
                f = pathlib.PurePath(self.input_path, fl)
                data = self.get_text(f)
                sortlist.append([f.name, data])
            sortlist.sort(key=lambda x: x[1], reverse=True)
            index = 0
            for el in sortlist:
                src = pathlib.PurePath(self.input_path).joinpath(el[0])
                if index == 0:
                    dst = pathlib.PurePath(new).joinpath(el[0])
                    self.logging.log.info(
                        "Najnowszy: move {} {}".format(src, dst))
                if index > 0:
                    dst = pathlib.PurePath(old).joinpath(el[0])
                    self.logging.log.info(
                        "Starszy: move {} {}".format(src, dst))
                shutil.move(src, dst, copy_function=shutil.copyfile)
                index += 1
            d[k] = None

    def get_text(self, fname: pathlib.PurePath) -> date:
        pat = re.compile(r'[lłk][oó]d[zźż],?(\W+)(dnia){0,1}\W*(\S+)\W*.*$',
                         flags=re.MULTILINE | re.I)
        try:
            img = convert_from_path(fname)
            if len(img) > 2:
                dt = date(6, 6, 6)
                self.logging.log.info("Plik {} ma date {}".format(
                    fname.name, dt))
                return dt
            text = pytesseract.image_to_string(img.pop(0), lang='pol')
            # print("file {}: text {}".format(fname, text))
            searched = re.search(pat, text)
            str_data = searched.group(3)
        except PDFSyntaxError:
            pass
        dt = self.get_date(stda=str_data, opp=fname)
        return dt

    def get_date(self, stda: str, opp: pathlib.PurePath) -> date:
        self.logging.log.info("Plik {} ma date {}".format(opp.name, stda))
        if '-' in stda:
            lst_data = stda.split('-')
            try:
                y = int(lst_data[0])
                m = int(lst_data[1])
                d = int(lst_data[2])
            except ValueError:
                self.logging.log.warning(
                    "Can't split: {1} in {0}".format(opp, stda))
                if len(lst_data) < 3:
                    pat_data = re.compile(r'\d+')
                    tempdata = re.findall(pat_data, stda)
                    lst_data = "".join(tempdata)
                    y = int(lst_data[0:4])
                    m = int(lst_data[4:6])
                    d = int(lst_data[6:])
                else:
                    y = int(stda[0:4])
                    m = int(stda[5:7])
                    d = int(stda[8:10])
        elif '.' in stda:
            lst_data = stda.split('.')
            try:
                y = int(lst_data[2])
                m = int(lst_data[1])
                d = int(lst_data[0])
            except ValueError:
                self.logging.log.warning(
                    "Can't split: {1} in {0}".format(opp, stda))
                y = int(stda[0:2])
                m = int(stda[3:5])
                d = int(stda[6:10])
        dt = date(y, m, d)
        return dt


if __name__ == '__main__':
    sts = Starter(Younger)
    sts.run()
