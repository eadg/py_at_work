import collections
import glob
import pathlib
import shutil
from datetime import date

from disk_operations import DiskOper

from starter_img_oper import Starter

from x2y import X2Y


class Younger(X2Y):

    def __init__(self, path_arg):
        super().__init__(path_arg)

    def lets_iterate(self):
        d = self.scan()
        self.sort_and_move(d)

    def scan(self) -> collections.defaultdict:
        dd = collections.defaultdict(list)
        gli = glob.iglob(self.input_path + '/*.pdf')
        for ele in gli:
            f = pathlib.PurePath(ele)
            main_key = f.name.split('_')[0]
            dt = self.get_date(f.name)
            dd[main_key].append([dt, f.name])
        return dd

    def get_date(self, fname: str) -> date:
        name = fname.split('.')[0]
        lname = name.split('-')
        try:
            y = int(lname[-3])
            m = int(lname[-2])
            d = int(lname[-1])
        except ValueError:
            print("Error in file: {}".format(fname))
        dt = date(y, m, d)
        return dt

    def sort_and_move(self, d: collections.defaultdict) -> None:
        p = pathlib.Path(self.output_path)
        old = p.joinpath('stare')
        new = p.joinpath('nowe')
        DiskOper().mkdir_if(old)
        DiskOper().mkdir_if(new)
        for k, v in d.items():
            d[k].sort(key=lambda x: x[0], reverse=True)
            index = 0
            for el in d[k]:
                if index == 0:
                    src = pathlib.PurePath(self.input_path).joinpath(el[1])
                    dst = pathlib.PurePath(new).joinpath(el[1])
                    self.logging.log.info(
                        "Najnowszy: move {} {}".format(src, dst))
                    shutil.copy(src, dst)
                if index > 0:
                    src = pathlib.PurePath(self.input_path).joinpath(el[1])
                    dst = pathlib.PurePath(old).joinpath(el[1])
                    self.logging.log.info(
                        "Starszy: move {} {}".format(src, dst))
                    shutil.copy(src, dst)
                index += 1


if __name__ == '__main__':
    sts = Starter(Younger)
    sts.run()
