import os
import os.path
import pathlib
import img2pdf
from PIL import Image
from disk_operations import DiskOper
from starter_img_oper import Starter
from x2y import X2Y


class ImgToPdf(X2Y):

    def __init__(self, path_arg):
        super().__init__(path_arg)

    def lets_iterate(self):
        list_img = os.listdir(self.input_path)
        for f in list_img:
            full_input_name = os.path.normpath(self.input_path + self.sep + f)
            pp_input = pathlib.Path(full_input_name)
            if pp_input.is_file() and DiskOper().name_checker(
                    full_input_name, 'png'):
                full_output_name = os.path.normpath(
                    self.output_path + self.sep + pp_input.stem + '.pdf')
                self.remove_alpha_channel(full_input_name)
                with open(full_output_name, "wb") as f1:
                    try:
                        f1.write(img2pdf.convert(full_input_name))
                    except TypeError as e:
                        self.logging.log.error(
                            "Błąd: {} w pliku: {}".format(e, full_input_name))
                self.logging.log.info("Input: {} -> Output: {}".format(
                    full_input_name, full_output_name))

    def remove_alpha_channel(self, img_path):
        with Image.open(img_path) as im:
            if im.mode in ('RGBA', 'LA') or (
                    im.mode == 'P' and 'transparency' in im.info):
                alpha = im.convert('RGBA').getchannel('A')
                bg = Image.new('RGB', im.size, (255, 255, 255))
                bg.paste(im, mask=alpha)
                bg.save(img_path)


if __name__ == '__main__':
    sts = Starter(ImgToPdf)
    sts.run()
