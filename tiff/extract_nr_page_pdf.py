import os
from PyPDF2 import PdfFileReader, PdfFileWriter


pdf_file_path = '/tmp/dbopdf'
os.chdir(pdf_file_path)
list_img = os.listdir(pdf_file_path)
for file_pdf in list_img:
    file_base_name = file_pdf.replace('.pdf', '')
    pdf = PdfFileReader(file_pdf)
    np = pdf.getNumPages()
    pages = range(0, np, 8)
    pdfWriter = PdfFileWriter()

    for page_num in pages:
        pdfWriter.addPage(pdf.getPage(page_num))
        pdf.getPage

    with open('{0}_extracted.pdf'.format(file_base_name), 'wb') as f:
        pdfWriter.write(f)
        f.close()
