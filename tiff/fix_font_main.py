import argparse
import math
import multiprocessing as mp
import pathlib
import subprocess
import time

from starter_img_oper import Starter

from x2y import X2Y


class Main(X2Y):
    """Main class run workers."""

    def __init__(self, path_arg):
        super().__init__(path_arg)
        # self.mode = 'c'

    def parse_detail(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '-m', action='store', help='file name', dest='mode')
        results = parser.parse_args()
        self.mode = results.mode

if __name__ == '__main__':
    sts = Starter(Main)
    sts.run()
