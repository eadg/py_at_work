import os
import os.path
import platform
import subprocess
import pathlib

import logger
from cross_platform import CRPL
from disk_operations import DiskOper
from starter_img_oper import Starter

class HtmlPdf():

    '''
    Class provide method to create pdf from html
    '''

    def __init__(self, path_arg):
        crpl = CRPL()
        self.input_path = path_arg
        self.sep = crpl.get_proper_os_sep(path_arg)
        self.output_path, self.log_path = self.prepare_output()
        self.make_dir_out()
        self.logging = logger.Logger(self.log_path)
        self.home_scr = self.get_home_scripts()
        if 'Wind' in platform.system():
            os.environ['PATH'] += os.pathsep + self.home_scr + '\\wkhtmltox\\bin\\'
            self.bin_wkhtml = 'wkhtmltopdf.exe'
        else:
            self.bin_wkhtml = 'wkhtmltopdf'

    def get_home_scripts(self):
        fpa = logger.__file__
        pa = pathlib.Path(fpa).parents[2]
        return str(pa)

    def prepare_output(self):
        out_dir_name = 'ZMIENIONE'
        p = os.path.abspath(self.input_path)
        p_list = p.split(self.sep)
        p_list.append(out_dir_name+self.sep)
        outpath = self.sep.join(p_list)
        logpath = outpath + 'log.txt'
        return outpath, logpath

    def make_dir_out(self):
        DiskOper().mkdir_if(self.output_path)

    def lets_iterate(self):
        list_html = os.listdir(self.input_path)
        for f in list_html:
            full_input_name = os.path.normpath(self.input_path + self.sep + f)
            pp_input = pathlib.Path(full_input_name)
            if pp_input.is_file() and DiskOper().name_checker(
                    full_input_name, 'htm'):
                self.check_input(full_input_name)
                full_output_name = os.path.normpath(
                    self.output_path + self.sep + pp_input.stem + '.pdf')
                subprocess.run([self.bin_wkhtml, '--quiet',
                                '--page-size', 'A4',
                                '--margin-left', '10mm',
                                '--margin-right', '10mm',
                                '--margin-top', '43mm',
                                '--margin-bottom', '13mm',
                                full_input_name, full_output_name])
                self.logging.log.info("input {} output {}".format(
                    full_input_name, full_output_name))

    def check_input(self, f_in):
        head = b'<head><META http-equiv="Content-Type" content="text/html; charset=utf-8"></head>'
        with open(f_in, 'rb+') as f:
            content = f.read()
            if b'<head>' not in content:
                f.seek(0, 0)
                f.write(head + b'\n' + content)


if __name__ == '__main__':
    sts = Starter(HtmlPdf)
    sts.run()
