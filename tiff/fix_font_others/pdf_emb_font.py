import glob
import os
import pathlib
import platform
import sys
import subprocess

from cross_platform import CRPL

from starter_img_oper import Starter

from x2y import X2Y

cmd='gs -q -dNOPAUSE -dBATCH -dPDFSETTINGS=/prepress -sDEVICE=pdfwrite -sOutputFile=output.pdf smykPDF_8184_4398084_764175070367883071_zrodlowy.pdf'

class FixFont(X2Y):

    def __init__(self, path_arg):
        super().__init__(path_arg)
        crpl = CRPL()
        if 'Wind' in platform.system():
            os.environ['PATH'] += os.pathsep + crpl.get_win_drive(
                sys.argv[0]) + '\\installed\\gs\\bin\\'
            self.bin_gs = 'gswin64c.exe'
        else:
            self.bin_gs = 'gs'

    def lets_iterate(self) -> None:
        d = self.scan()

    def scan(self) -> None:
        gli = glob.iglob(self.input_path + '/*.pdf')
        p = pathlib.Path(self.output_path)
        for ele in gli:
            fin = pathlib.PurePath(ele)
            arg_out = '-sOutputFile=' + self.output_path + fin.name
            subprocess.run([self.bin_gs, '-q', '-dNOPAUSE', '-dBATCH', '-dPDFSETTINGS=/prepress',
                            '-sDEVICE=pdfwrite', arg_out, fin])
            self.logging.log.info('przetworzony: '+fin.name)


if __name__ == '__main__':
    sts = Starter(FixFont)
    sts.run()
