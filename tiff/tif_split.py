import configparser
from collections import defaultdict
import glob
import os
import os.path
import sys
from PIL import Image, ImageSequence, TiffImagePlugin
from disk_operations import DiskOper
from cross_platform import CRPL
import logger
from starter_img_oper import Starter


class TiffSplit(object):

    def __init__(self, path_arg):
        self.input_path = path_arg
        self.sep = CRPL().get_proper_os_sep(path_arg)
        self.output_path, self.log_path = self.prepare_output()
        self.make_dir_out()
        self.logging = logger.Logger(
            os.path.normpath(self.output_path + '/log.txt'))

    '''
    def prepare_output(self):
        out_dir_name = 'PODZIELONE'
        sep = crpl.get_proper_os_sep(self.input_path)
        input_list = self.input_path.split(sep)
        if len(input_list) > 1:
            # input_list.insert(-1, out_dir_name)
            input_list.append(out_dir_name)
            outpath = (os.sep).join(input_list)
            logpath = outpath[:-1]
            return outpath, logpath
        else:
            work_path = out_dir_name + '/' + self.input_path
            return work_path, work_path
    '''

    def prepare_output(self):
        out_dir_name = 'ZMIENIONE'
        p = os.path.abspath(self.input_path)
        p_list = p.split(self.sep)
        p_list.append(out_dir_name+self.sep)
        outpath = self.sep.join(p_list)
        logpath = outpath + 'log.txt'
        return outpath, logpath

    def get_files(self):
        norm_path_f5 = os.path.normpath(self.input_path + '/*')
        norm_path_f3 = os.path.normpath(self.input_path + '/3/*')
        file_bigger = glob.glob(norm_path_f5)
        file_shorter = glob.glob(norm_path_f3)
        return file_shorter, file_bigger

    def lets_iterate(self):
        ini = INI()
        page_big, page_small = ini.parse()
        out_dir_5 = self.output_path + '/'
        out_dir_3 = self.output_path + '/3/'
        file_shorter, file_bigger = self.get_files()
        for f in file_bigger:
            if os.path.isfile(f):
                _, fname = os.path.split(f)
                self.logging.log.info('Przetwarzam %s' % f)
                name, ext = fname.split('.')
                file_output_1 = out_dir_5 + name + '_1.' + ext
                file_output_2 = out_dir_5 + name + '_2.' + ext
                self.lets_doit(f, file_output_1, page_big[0])
                self.lets_doit(f, file_output_2, page_big[1])
        for f in file_shorter:
            if os.path.isfile(f):
                _, fname = os.path.split(f)
                self.logging.log.info('Przetwarzam %s' % f)
                name, ext = fname.split('.')
                file_output_1 = out_dir_3 + name + '.' + ext
                self.lets_doit(f, file_output_1, page_small[0])

    def lets_doit(self, file_img, output, pages):
        img = Image.open(file_img)
        with TiffImagePlugin.AppendingTiffWriter(output, True) as splited:
            for i, page in enumerate(ImageSequence.Iterator(img)):
                if i in pages:
                    page.save(splited)
                    splited.newFrame()
        if len(pages) > i:
            log_out = os.path.normpath(output)
            self.logging.log.warning(
                'Błąd, plik wynikowy %s jest mniejszy niz powinien' % log_out)
        if DiskOper().remove_if_empty(output):
            self.logging.log.warning(
                'Plik %s z jedna strona - nie będzie przetwarzany' % log_out)

    def make_dir_out(self):
        do = DiskOper()
        do.mkdir_if(self.output_path)
        do.mkdir_if(self.output_path + '/3')


class INI(object):

    def parse(self):
        '''
            defaultdict(list) => allows easy create & update lists as values.
            Here list is built function in Python.
            Closure with scope and nonlocal.
        '''
        config = configparser.ConfigParser()
        ph = self.get_path()
        config.read(ph)
        config.sections()
        td = config['podzial']['tif_duzy']
        tm = config['podzial']['tif_maly']
        dict_range = defaultdict(list)

        def make_dict_range(str_ini, key):
            nonlocal dict_range
            spli_str_ini = str_ini.split(';')
            for li in spli_str_ini:
                objrange = self.get_range(li)
                dict_range[key].append(objrange)

        make_dict_range(td, 'tif_duzy')
        make_dict_range(tm, 'tif_maly')
        return dict_range['tif_duzy'], dict_range['tif_maly']

    def get_range(self, strrange):
        dashlist = strrange.split('-')
        intrange = [int(i) for i in dashlist]
        intrange[0] -= 1
        return range(intrange[0], intrange[1])

    def get_path(self):
        dirpath = os.path.dirname(sys.argv[0])
        if dirpath:
            ph = os.path.dirname(sys.argv[0]) + '/config.ini'
            return ph
        else:
            return 'config.ini'


if __name__ == '__main__':
    sts = Starter(TiffSplit)
    sts.run()
