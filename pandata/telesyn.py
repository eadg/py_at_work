import collections
import csv


dd = collections.defaultdict(list)
with open('teleefor3.csv', newline='') as csvfile:
    sample = csvfile.read(1024)
    dialect = csv.Sniffer().sniff(sample)
    header = csv.Sniffer().has_header(sample)
    csvfile.seek(0)
    teleread = csv.reader(csvfile, dialect)
    if header:
        next(teleread)
    for row in teleread:
        dd[row[0]].append(row[1:])
with open('output.csv', 'w', newline='\n') as output:
    for i, k in dd.items():
        if len(k) > 1:
            k.sort(key=lambda x: x[4], reverse=True)
            s = ",".join(k.pop(0)[:-1])
            s = s.replace('+48', '')
            print(f"{i}," + s, file=output)
        else:
            s = ",".join(k.pop(0)[:-1])
            s = s.replace('+48', '')
            print(f"{i}," + s, file=output)
