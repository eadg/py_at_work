# from django.shortcuts import render
from django.shortcuts import render
# from django.http import HttpResponse
from .models import Path
from .forms import NameForm


def index(request):
    return render(request, 'tifsplit/index.html')
    # return HttpResponse("<!DOCTYPE html><title>tif-split</title>Hello from view.func -> index().</html>")


def all_dirs(request):
    paths = Path.objects.all()
    return render(request, 'tifsplit/paths/listpath.html', {'paths': paths})


def form_path(request):
    paths = Path.objects.all()[1]
    # paths = get_object_or_404(pa)
    if request.method == 'POST':
        form = NameForm(request.Post)
        if form.is_valid():
            cd = form.cleaned_data
            print(cd)
    else:
        form = NameForm()
    return render(request, 'tifsplit/paths/listform.html', {'paths': paths,
                                                            'form': form})
