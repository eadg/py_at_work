from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('all/', views.all_dirs, name='all_dirs'),
    path('myform/', views.form_path, name='form_path'),
]
