from django.apps import AppConfig


class TifsplitConfig(AppConfig):
    name = 'tifsplit'
