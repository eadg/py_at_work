from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from tifsplit.views import index


class SmokeTest(TestCase):

    def test_always_fail(self):
        found = resolve('/admin/')
        print(type(found))
        self.assertEqual(1 + 1, 2)

    def test_root_url(self):
        found = resolve('/tif/')
        print(type(found.func))
        self.assertEqual(found.func, index)

    def test_page_returns_html(self):
        request = HttpRequest()
        response = index(request)
        self.assertTrue(response.content.startswith(b'<!DOCTYPE html>'))
        self.assertIn(b'<title>', response.content)
        self.assertTrue(response.content.strip().endswith(b'</html>'))

    def test_return_correct_html(self):
        request = HttpRequest()
        response = index(request)
        expected_html = render_to_string('tifsplit/index.html')
        self.AssertEqual = (response.content.decode(), expected_html)
