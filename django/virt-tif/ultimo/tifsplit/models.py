from django.db import models


class MyManager(models.Manager):
    def get_queryset(self):
        return super(MyManager, self).get_queryset()


class Path(models.Model):
    input_path = models.CharField(max_length=500)
    output_path = models.CharField(max_length=500)
    objects = models.Manager()
    my_manager = MyManager()
