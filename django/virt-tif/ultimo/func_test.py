from selenium import webdriver
import unittest


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_start_and_django_in_title(self):
        self.browser.get('http://localhost:8000/tif')
        self.assertIn('tif', self.browser.title)
        head_txt = self.browser.find_element_by_tag_name('h5').text
        self.assertIn('view', head_txt)
        # self.fail('End of test')

if __name__ == '__main__':
    unittest.main()
