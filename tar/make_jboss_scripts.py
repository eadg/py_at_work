from sys import argv
from glob import glob
import re
import tarfile


class MakeScripts(object):
    def __init__(self):
        self.path = '/home/jrokicki/workspace'
        if len(argv) > 1:
            list_of_files = glob(argv[1]+'/download_*')
            for file in list_of_files:
                scan = re.search(r'(.*\/download_[^d].+)', file)
                if scan:
                    self.file = file
        else:
            self.file = 'brak'
            print('brak katalogu jako argumentu wywołania')

    def getPath(self):
        lista = list()
        if self.file != 'brak':
            f = open(self.file)
            for line in f:
                scan = re.search(r'workspace_location(\/(.+))', line)
                if scan:
                    list_scp = str(scan.group(1)).split()
                    lista.append(list_scp)
            f.close()
        return lista

    def makeTar(self):
        self.mytar = tarfile.open("pliki.tar.gz", "w:gz")
        for pliki in self.getPath():
            if len(pliki[1]) <= 1:
                print('root path ' + pliki[0])
            else:
                print('not root path ' + pliki[0])
            path_ok = str(pliki[0]).rstrip('\*')
            self.mytar.add(self.path+path_ok)

    def testTar(self):
        for line in self.mytar.getmembers():
            print(line)

    def closeTar(self):
        self.mytar.close()


if __name__ == '__main__':
    ms = MakeScripts()
    # ms.getPath()
    ms.makeTar()
    # ms.testTar()
    ms.closeTar()
