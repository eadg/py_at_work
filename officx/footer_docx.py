import argparse
import os
import shutil
import sys
from stat import S_IWUSR, S_IREAD
from zipfile import ZipFile
from lxml import etree


class Futrek():

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-n', action='store', help='imie nazwisko', dest='name_value')
        self.parser.add_argument(
            '-m', action='store', help='e-mail', dest='mail_value')
        self.parser.add_argument(
            '-o', action='store', help='stanowisko', dest='occup_value')
        self.parser.add_argument(
            '-t', action='store', help='telefon kom', dest='mobile_value')
        results = self.parser.parse_args()
        self.name = results.name_value
        self.email = results.mail_value
        self.occu = results.occup_value
        self.mobile = results.mobile_value

    def prepare(self) -> None:
        try:
            os.mkdir('tmp')
        except FileExistsError:
            pass
        try:
            shutil.copy('temple_of_footer.docx', 'tmp')
        except PermissionError:
            pass
        os.chdir('tmp')
        os.chmod('temple_of_footer.docx', S_IWUSR | S_IREAD)
        ZipFile('temple_of_footer.docx').extractall()
        os.unlink('temple_of_footer.docx')

    def clean(self) -> None:
        os.chdir('..')
        shutil.rmtree('tmp')

    def xmloza(self) -> None:
        sch = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}t'
        with open('word/document.xml', 'r') as wdt:
            tri = etree.parse(wdt)
            rut = tri.getroot()
            for i in rut.iter(sch):
                if 'Imię i Nazwisko' in i.text:
                    if len(i.attrib.values()) > 0:
                        i.text = self.name
                if 'Stanowisko w kolorze' in i.text:
                    i.text = self.occu
                if '48 xxx xxx xxx' in i.text:
                    i.text = self.mobile
                if 'imie.nazwisko@' in i.text:
                    i.text = self.email
            tri.write('new.xml')

    def build(self) -> None:
        shutil.move('new.xml', 'word/document.xml')
        shutil.make_archive('poc.docx', 'zip', '.')
        shutil.move('poc.docx.zip', '../out/poc.docx')

        # print(etree.tostring(tri))
        # https://stackoverflow.com/questions/37401149/editing-a-docx-file
        # https://techoverflow.net/2020/11/11/how-to-modify-file-inside-a-zip-file-using-python/
        # https://www.geeksforgeeks.org/modify-xml-files-with-python/


if __name__ == '__main__':
    futro = Futrek()
    futro.prepare()
    futro.xmloza()
    futro.build()
    futro.clean()
