from datetime import datetime
import glob
import os
import sqlite3
from pathlib import Path

import send_attach_mail as sam
import logger
import sftp_expect_prod as sep


class DB():

    def __init__(self):
        self.con = sqlite3.connect('db/files.db')
        self.logg = logger.Logger(file_path='/var/log/sftp_sync.log')

    def createdb(self) -> None:
        self.con.isolation_level = 'DEFERRED'
        cur = self.con.cursor()
        try:
            cur.execute("select count(path) from files;")
            for f1 in cur:
                self.logg.log.info("%s files was sent until now", f1[0])
                self.logg.close_logs()
        except sqlite3.OperationalError:
            print('Lets create it')
            cur.execute('''create table files(path text)''')
            cur.execute('''create UNIQUE index path_ind on files(path)''')
            print('created')

    def insertdb(self) -> None:
        lp = '/srv/henry/KomornikOnline/done'
        os.chdir(lp)
        igl = glob.iglob('**/**/*.xml')
        self.con.isolation_level = None
        cur = self.con.cursor()
        sss = sep.SftpSync()
        for i in igl:
            dt = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            p = Path(i)
            fsize = p.stat().st_size
            if fsize > 50:
                try:
                    cur.execute("begin")
                    cur.execute("insert into files values(?,?)", (i, dt))
                except sqlite3.IntegrityError:
                    cur.execute("rollback")
                    # print("Error: {} - nothing to send".format(e))
                else:
                    # print("send {}".format(p))
                    result = sss.send_o_f_confirmation(p)
                    # result = sss.send_one_file(p)
                    if result:
                        cur.execute("rollback")
                    else:
                        cur.execute("commit")
            else:
                sss.update_log_only(p)
        self.con.close()


class DB_Maintance():

    def __init__(self):
        self.con = sqlite3.connect('db/files.db')
        # self.logg = logger.Logger(file_path='/var/log/sftp_sync.log')

    def rm_rows(self):
        self.con.isolation_level = None
        cur = self.con.cursor()
        with open('test/resend.txt') as f:
            for row in f:
                rowok = row.rstrip()
                try:
                    cur.execute('begin')
                    cur.execute(
                        'delete from files where path like (?)', (rowok,))
                    cur.execute('commit')
                except sqlite3.Error as e:
                    print(e)
                    cur.execute('rollback')


if __name__ == '__main__':
    db = DB()
    db.createdb()
    db.insertdb()
    sam.send('/tmp/sftp_daily.log')
    '''
    db = DB_Maintance()
    db.rm_rows()
    '''
