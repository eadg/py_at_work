import argparse
from collections import defaultdict
import csv
import sys
import psycopg2 as psy

import ass
import logger


class Regulation:

    """SD-208461"""

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()
        self.connect()

    def parse_detail(self) -> None:
        """Parsing input"""
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-f', action='store', help='path', dest='ap')
        self.parser.add_argument('-l', action='store', help='Sciezka dla pliku\
        logow. Domyslnie /dev/null', dest='logging_value', default='')
        results = self.parser.parse_args()
        self.argpath = results.ap
        self.logger = logger.Logger(results.logging_value).log

    def connect(self) -> None:
        """Make connection"""
        pa = ass.read_pass(file='ssap')
        self.con = psy.connect(dbname='regulacje', password=pa, user='korespondent',
                host='pdc-p-pgsql0', connect_timeout=3)

    def read_numer_aktu(self) -> None:
        """Make iterator for file"""
        with open(self.argpath, encoding="utf-8") as file:
            for row in file.readlines():
                yield row[:-1]

    def act_a(self):
        """DELETE FROM groups"""
        it = self.read_numer_aktu()
        with self.con:
            with self.con.cursor() as cur:
                for row in it:
                    sqs = "DELETE FROM groups WHERE id=(%s)"
                    cur.execute(sqs, [row])
                    if cur.rowcount == 1:
                        self.logger.info(row)
                    else:
                        self.logger.error(row)

    def act_b(self):
        """Remove user from specified groups"""
        SP_GR = '19|'
        it = self.read_numer_aktu()
        with self.con:
            with self.con.cursor() as cur:
                for row in it:
                    sqs = "SELECT id_group FROM users WHERE id=(%s)"
                    cur.execute(sqs, [row])
                    tup_gr = cur.fetchone()
                    str_gr = tup_gr[0]
                    self.logger.info(str_gr)
                    if SP_GR in str_gr:
                        new_gr = str_gr.replace(SP_GR, '', 1)
                        squ = "UPDATE users SET id_group=(%s) where id=(%s)"
                        cur.execute(squ, [new_gr, row])
                        log_update = "User: %s id_group: %s" % (row, new_gr)
                        self.logger.info(log_update)

    def act_c(self):
        """Check recipient for active act"""
        dd = defaultdict(list)
        with self.con:
            with self.con.cursor() as cur, self.con.cursor() as cur2:
                sqs1 = "SELECT id, id_group FROM akt_wewnetrzny WHERE aktywny=1 and id_group!='0'"
                sqs2 = "SELECT id FROM regulacja WHERE id_akt_wewnetrzny=(%s)"
                cur.execute(sqs1)
                for record in cur:
                    it = self.read_numer_aktu()
                    list_group = record[1].split(',')
                    for gr_db in list_group:
                        for gr_file in it:
                            if gr_file == gr_db:
                                cur2.execute(sqs2, [record[0]])
                                for record2 in cur2:
                                    dd[gr_file].append(record2[0])
                                    log_update = "Grupa %s | Regulacja %s" % (gr_file, record2)
                                    self.logger.info(log_update)
        for k,v in dd.items():
            print(f"Grupa {k} | Regulacja: {v}")

    def act_d(self):
        """Replace groups with new content"""
        it = self.read_numer_aktu()
        csvgroup = csv.reader(it, delimiter=';')
        with self.con:
            with self.con.cursor() as cur:
                sqs = "INSERT INTO groups(nazwa, smyk_group_id) VALUES (%s, %s)"
                for row in csvgroup:
                    if not row[1]:
                        print(f"{row[0]},0")
                        cur.execute(sqs, (row[0], 0))
                    else:
                        print(f"{row[0]},{row[1]}")
                        cur.execute(sqs, (row[0], row[1]))

    def con_close(self):
        """Connection close"""
        self.con.close()

if __name__ == '__main__':
    reg = Regulation()
    reg.act_d()
    reg.con_close()
