from mysql.connector import connect, Error


class GenIntGro():

    def __init__(self):
        self.lets_connect()

    def lets_connect(self) -> None:
        self.cnx = connect(
            user='dokowl', password='', database='dok_owl2',
            host='pdc-p-mysql0.ultimo.pl', ssl_disabled=True, use_pure=False)
        if self.cnx:
            print('we are connected!')

    def select_1(self) -> None:
        cursor = self.cnx.cursor()
        query1 = ("SELECT id, username, disabled FROM users ORDER BY id")
        cursor.execute(query1)
        for (f1, f2, f3) in cursor:
            print("{} -> {} -> {}".format(f1, f2, f3))
        cursor.close()

    def select_2(self) -> None:
        cursor = self.cnx.cursor()
        query2 = ("SELECT * FROM membergroup")
        cursor.execute(query2)
        for (f1, f2, f3) in cursor:
            print("{} -> {}  -> {}".format(f1, f2, f3))
        cursor.close()

    def select_3(self) -> None:
        cursor = self.cnx.cursor()
        query3 = ("SELECT * FROM groups")
        cursor.execute(query3)
        for (f1, f2) in cursor:
            print("{} -> {}".format(f1, f2))
        cursor.close()

    def select_4(self) -> None:
        cursor = self.cnx.cursor()
        query3 = ("SELECT count(*) as kaunt FROM membergroup\
        WHERE userid=1414 AND groupid=20")
        cursor.execute(query3)
        for (f1) in cursor:
            print(f"{f1} -> :)")
        cursor.close()

    def select_5(self) -> None:
        cursor = self.cnx.cursor()
        query3 = ("SELECT groupid FROM membergroup\
        WHERE userid=1414 AND groupid IN (0, 21, 17) ")
        cursor.execute(query3)
        for (f1) in cursor:
            print(f"{f1} ")
        cursor.close()

    def select_6(self) -> None:
        cursor = self.cnx.cursor()
        query3 = ("SELECT count(*) as kaunt FROM membergroup\
        WHERE userid=1414 AND groupid IN (0, 21, 17)")
        cursor.execute(query3)
        for (f1) in cursor:
            print(f"{f1} -> :)")
        cursor.close()

    def select_7(self) -> None:
        """Set Henio free."""
        cursor = self.cnx.cursor()
        query7 = ("UPDATE users SET disabled=0 WHERE id=1429")
        print("Henio is free")
        cursor.execute(query7)
        cursor.close()

    def select_8(self) -> None:
        """Show tables."""
        cursor = self.cnx.cursor()
        query = ("SHOW TABLES")
        cursor.execute(query)
        for (f1) in cursor:
            print(f"{f1}")
        cursor.close()

    def select_9(self, tab_name: str) -> None:
        cursor = self.cnx.cursor()
        query = (f"SELECT * FROM {tab_name}")
        cursor.execute(query)
        for (f1) in cursor:
            print(f"{f1}")
        cursor.close()

    def conn_close(self) -> None:
        self.cnx.close()
        print('connection breakdown!')


if __name__ == '__main__':
    gig = GenIntGro()
    # gig.select_9(tab_name='owl_log')
    gig.select_7()
    gig.conn_close()
