import ass

import psycopg as psy


class Regulation:

    def read_numer_aktu(self):
        with open('baw.txt', encoding="utf-8") as file:
            for row in file.readlines():
                yield row[:-1]

    def act(self):
        pa = ass.read_pass(file='ssap')
        print(pa)
        it = self.read_numer_aktu()
        with psy.connect(
                dbname='regulacje', password=pa, user='korespondent',
                host='pdc-p-pg0', connect_timeout=3) as conn, open(
                    'wrong.txt', 'w',  encoding="utf-8") as bfile, open(
                    'done.txt', 'w',  encoding="utf-8") as okfile:
            with conn.cursor() as cur:
                for row in it:
                    sqs = "SELECT id FROM akt_wewnetrzny WHERE numer_aktu=(%s)"
                    cur.execute(sqs, [row])
                    if cur.rowcount == 1:
                        for record in cur.fetchall():
                            print(f"{row} in {record}")
                            self.sql_act(id_reg=record[0], curs=cur)
                            okfile.writelines(row+'\n')
                    else:
                        print(f"anomalia: {cur.rowcount}: {row}")
                        bfile.writelines(row+'\n')

    def sql_act(self, id_reg: str, curs: psy.Cursor):
        sqlu1 = "UPDATE regulacja SET id_nazwa_kategorii=12 WHERE "
        sqlu2 = "id_akt_wewnetrzny=(%s)"
        sqlu = sqlu1 + sqlu2
        curs.execute(sqlu, [id_reg])
        sqs1 = "SELECT * FROM regulacje_historyczne WHERE "
        sqs2 = "id_regulacja_poprzednia=(%s)"
        sqs = sqs1 + sqs2
        curs.execute(sqs, [id_reg])
        if curs.rowcount < 1:
            sqi = "INSERT INTO regulacje_historyczne VALUES ((%s), 277)"
            print(curs.rowcount)
            curs.execute(sqi, [id_reg])


if __name__ == '__main__':
    reg = Regulation()
    reg.act()
