#!/usr/bin/python

import os
from subprocess import call
import random
import time
import init_daemon as i_d


class Timer(object):

    '''Go sleep for random time'''

    def __init__(self, delay):
        self.delay = delay

    def run_timer(self):
        r = random.randint(1, self.delay)
        time.sleep(r)


class Executer(object):

    '''Check agent RHQ if is running and next stop or restart it'''

    def __init__(self, comm):
        self.cmd = ['/srv/rhq-agent/bin/rhq-agent-wrapper.sh', comm]

    def isRun(self):
        pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
        for pid in pids:
            try:
                if 'rhq-agent' in open(os.path.join(
                        '/proc/', pid, 'cmdline'), 'rb').read():
                    return True
            except IOError:
                continue
        return False

    def reRun(self):
        os.environ['JAVA_HOME'] = '/srv/java/rhq'
        nul = open(os.devnull, 'w')
        call(self.cmd, stdout=nul)


if __name__ == '__main__':
    Timer(14400).run_timer()
    comm = i_d.PrimalDaemon().action()
    e = Executer(comm)
    if e.isRun() is True:
        e.reRun()
    else:
        e = Executer('start')
        e.reRun()
