#!/usr/bin/python

import os
from subprocess import call


class RHQ(object):

    '''Skrypt sprawdza czy jest uruchomiony proces agenta RHQ i
        ewentualnie uruchamia ponownie.'''

    def isRun(self):
        pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
        for pid in pids:
            try:
                if 'rhq-agent' in open(os.path.join(
                        '/proc/', pid, 'cmdline'), 'rb').read():
                    return True
            except:
                continue
        return False

    def restart(self):
        os.environ['JAVA_HOME'] = '/srv/java/rhq'
        nul = open(os.devnull, 'w')
        call(['/srv/rhq-agent/bin/rhq-agent-wrapper.sh', 'restart'], stdout=nul)


if __name__ == '__main__':
    rhq = RHQ()
    if rhq.isRun() is False:
        rhq.restart()
