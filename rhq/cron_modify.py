import re


class CronModify():

    def __init__(self, f):
        self.file = f

    def add_hash(self):
        patt_in = re.compile('^(#*)(.*stop_rhq_agent.*)', flags=re.MULTILINE)
        patt_out = re.compile('^(#{1}.*stop_rhq_agent.*)', flags=re.MULTILINE)
        with open(self.file, 'r+') as f:
            data = f.read()
            fix_data = re.sub(patt_in, r'#\2', data)
            f.seek(0)
            f.write(fix_data)
            f.seek(0)
            match_final = re.search(patt_out, f.read())
        return '#' if match_final else 'error'

if __name__ == '__main__':
    cm = CronModify('stop_rhq_agent')
    cm.add_hash()
