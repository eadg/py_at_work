import unittest
import metrics_rhq
import datetime


class TestPackage(unittest.TestCase):

    def test_file_exists(self):
        m = metrics_rhq.Metrics(1)
        self.assertTrue(isinstance(m.delta_day, datetime.datetime))

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPackage)
    unittest.TextTestRunner(verbosity=2).run(suite)
