import glob
import datetime
import os
import shutil

"""
'''Truncate old metrics @ rhq-storage '''

metrics_agg = glob.glob('/srv/ibmarray/rhq-data/data/rhq/aggregate_metrics/*')
metrics_raw = glob.glob('/srv/ibmarray/rhq-data/data/rhq/raw_metrics/*')

four_days_old = datetime.datetime.today() - datetime.timedelta(days=4)
three_days_old = datetime.datetime.today() - datetime.timedelta(days=3)

a = [os.truncate(file_to_clean, 0) for file_to_clean in metrics_agg if datetime.datetime.fromtimestamp(os.path.getmtime(file_to_clean)) < four_days_old]
b = [os.truncate(file_to_clean, 0) for file_to_clean in metrics_raw if datetime.datetime.fromtimestamp(os.path.getmtime(file_to_clean)) < three_days_old]

with open('/srv/scripts/log.txt', 'w') as f:
    s = ('%s %s' % (a, b))
    f.write(s)
"""


class Metrics(object):

    def __init__(self, day, path):
        self.delta_day = datetime.datetime.today() - datetime.timedelta(
            days=day)
        self.path_metrics = '/srv/ibmarray/rhq-data/data/rhq/' + path + '/*'
        self.backup_metrics = '/srv/backup_ramdisk/'

    def backup(self):
        '''make backup all data from ramdisk'''
        self.rdir(self.backup_metrics)
        shutil.copytree(self.path_metrics, self.backup_metrics)

    def rdir(self):
        '''remove folders made by previous execute backup'''
        try:
            shutil.rmtree(self.backup_metrics, ignore_errors=True)
        except OSError:
            print('path not exists')

    def truncate(self):
        files_metrics = glob.glob(self.path_metrics)
        for f in files_metrics:
            pass


if __name__ == '__main__':
    m = Metrics(day=3, path='rr')
