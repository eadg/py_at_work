import unittest
import cron_modify


class Testcron_modify(unittest.TestCase):

    def setUp(self):
        self.cm = cron_modify.CronModify('stop_rhq_agent')

    def test_add_hash(self):
        self.assertIn('#', self.cm.add_hash())

if __name__ == '__main__':
    # suite = unittest.TestLoader().loadTestsFromTestCase(Testcron_modify)
    # unittest.TextTestRunner(verbosity=2).run(suite)
    unittest.main(verbosity=2)
