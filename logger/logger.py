import logging
import os


class Logger():

    def __init__(self, file_path=None, clean=False, console=False):
        self.clean = clean
        self.console = console
        self.log = self.init_logs(file_path)

    def init_logs(self, file_path):
        if self.clean:
            self.clean_before_use(file_path)
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        if not file_path:
            file_path = self.get_null()
        fh = logging.FileHandler(file_path)
        formatter = logging.Formatter(datefmt='%Y-%m-%d %H:%M:%S', fmt='\
        %(asctime)s.%(msecs)03d %(levelname)-8s %(message)s')
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        if self.console:
            ch = logging.StreamHandler()
            ch.setFormatter(formatter)
            logger.addHandler(ch)
        return logger

    def close_logs(self, last_msg=None):
        if last_msg:
            self.log.info(last_msg)
        handlers = self.log.handlers[:]
        for handler in handlers:
            handler.close()
            self.log.removeHandler(handler)
        logging.shutdown()

    def clean_before_use(self, f):
        try:
            os.remove(f)
        except IOError:
            print('no file {0}'.format(f))

    def get_null(self):
        if os.name in 'posix':
            f = '/dev/null'
            return f
        return 'nul'
