import os
import os.path
import sys
import glob
import shutil
import tarfile
import re
from hostname import Hostname


class Package(object):

    def __init__(self):
        self.work_path = '/tmp/'
        self.host_name = Hostname().host_name
        self.local = self.work_path + self.host_name
        self.subdir = ['bin', 'deployments']
        # self.jboss_path = sys.argv[1]
        if len(sys.argv) > 1:
            self.jboss_path = sys.argv[1]
        else:
            self.jboss_path = '/srv/jboss/jboss'

    def chdir(self):
        '''chdir to work_path'''
        print(u'\u2023 ' + 'change workdir to ' + self.work_path)
        os.chdir(self.work_path)

    def mdir(self):
        '''mkdir for folders without subdirectory
            @cpdir() copytree makes directories by self'''
        print(u'\u2023 ' + 'create structure directories')
        try:
            os.mkdir(self.local)
            for s in self.subdir:
                os.mkdir(self.local+'/'+s)
        except OSError:
            print('path exists')

    def rdir(self, mode='full'):
        '''remove folders and tgz made by previous run script'''
        if mode == 'full':
            print(u'\u2023 ' + 'remove folders ' + self.local + ' and *.tgz made by previous run script')
            try:
                shutil.rmtree(self.local, ignore_errors=True)
                if os.path.isfile(self.local + ".tgz"):
                    os.remove(self.local + ".tgz")
            except OSError:
                print('path not existss')
        elif mode == 'partial':
            print(u'\u2023 ' + 'clean, remove folders ' + self.local)
            try:
                shutil.rmtree(self.local, ignore_errors=True)
            except OSError:
                print('path not existss')

    def cpdir(self):
        '''copy tree jboss folders'''
        print(u'\u2023 ' + 'copy jboss folders to: ' + self.local)
        shutil.copy(self.jboss_path + '/profile.conf', self.local)
        shutil.copy(self.jboss_path + '/bin/standalone.conf', self.local + '/bin')
        for f in glob.glob(self.jboss_path + '/standalone/deployments/*.*ar'):
            shutil.copy(f, self.local + '/deployments')
        shutil.copytree(
            self.jboss_path + '/standalone/configuration/', self.local + '/configuration')
        shutil.copytree(self.jboss_path + '/modules/', self.local + '/modules')

    def makeTar(self):
        print(u'\u2023 ' + 'tar jboss folder: ' + self.local)
        mytar = tarfile.open(self.local + ".tgz", "w:gz")
        mytar.add(self.host_name)
        return(self.local + ".tgz")

    def fix_standalone_conf(self):
        print(u'\u2023 ' + 'fixing standalone.conf')
        fpath = self.local + '/bin/standalone.conf'
        patt_j = re.compile('(JAVA_HOME=)')
        patt_b = re.compile('(^BYTEMAN_)', flags=re.MULTILINE)
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(patt_j, r'#\1', data)
            fix_data = re.sub(patt_b, r'#\1', fix_data)
            f.seek(0)
            f.write(fix_data)
        return len(fix_data)-len(data)

    def refix_standalone_xml(self):
        print(u'\u2023 ' + 'fixing standalone.xml')
        fpath = self.local + '/configuration/standalone.xml'
        patt_elk = re.compile('value="hostname=(.+?)-(.+?)-(.+?).+?/>')
        patt_inet = re.compile('<inet-address value=".+?/>')
        patt_pgsql = re.compile('jboss-pgsql1')
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(patt_elk, r'value="hostname=\g<1>-\g<2>-alpha"/>', data)
            fix_data = re.sub(patt_inet, r'<inet-address value="0.0.0.0"/>', fix_data)
            fix_data = re.sub(patt_pgsql, r'jboss-pgsql3', fix_data)
            f.seek(0)
            f.truncate()
            f.write(fix_data)

    def do_all(self):
        self.chdir()
        self.rdir()
        self.mdir()
        self.cpdir()
        self.fix_standalone_conf()
        self.refix_standalone_xml()
        self.makeTar()
        self.rdir(mode='partial')


if __name__ == '__main__':
    p = Package()
    p.do_all()
