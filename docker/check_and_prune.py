import os
import subprocess


fnull = open(os.devnull, 'w')
stat = subprocess.call(["systemctl", "status", "docker"], stdout=fnull)

if stat == 0:
    subprocess.call(
        ["/usr/bin/docker", "system", "prune", "-a", "-f"], stdout=fnull)
