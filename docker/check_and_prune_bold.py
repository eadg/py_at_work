import os
import subprocess

stat = os.system('systemctl status docker')

if not stat:
    subprocess.call(["/usr/bin/docker", "system", "prune", "-a", "-f"])
    subprocess.call(["/usr/bin/docker", "system", "prune", "--volumes", "-f"])
