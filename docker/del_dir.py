import os
import shutil


class manage_files(object):

    def __init__(self, count=40,
                 path='/srv/registry/docker/registry/v2/blobs/sha256'):
        self.count = count
        self.path = path

    def keep_file(self):
        os.chdir(self.path)
        list_dir = os.listdir('.')
        list_dir.sort(key=lambda x: os.path.getmtime(x))
        print("len: " + len(list_dir))
        if(len(list_dir) - self.count > 0):
            keep_value = len(list_dir) - self.count
            list_to_del = list_dir[:keep_value]
            for f in list_to_del:
                yield(f)
        else:
            print("len(path)-count <= 0")

    def del_others(self):
        shutil.rmtree()
