## I built container:

> [root@fdlasfaldfjaslf:SD-15493]# cat Dockerfile

> FROM alpine:latest 

> RUN ["apk", "add", "busybox-extras", "curl"]

## After run: docker run -it --rm 872b28a97520 /bin/sh

I could test connection:

> / # telnet jb-main-beta 4447
"pdc-t-jb-main-beta.ultimo.pl
Connection closed by foreign host

> / # nc -zv jb-main-beta 4447
> jb-main-beta (10.10.12.32:4447) open

> / # curl jb-main-beta:4447 --output -
> "pdc-t-jb-main-beta.ultimo.pl/ #

> / # nc -zvv jb-main-beta 4448
> nc: jb-main-beta (10.10.12.32:4448): Connection refused
> sent 0, rcvd 0

## And it was looking everything ok.
