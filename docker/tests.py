import os
import os.path
import tarfile
import unittest
import packjboss2docker as pjd


class TestPackage(unittest.TestCase):

    def test_get_hostname(self):
        self.assertIsInstance(pjd.Hostname().get_hostname(), str)

    def test_etc_hosts(self):
        h = pjd.Hostname().get_hostname()
        self.assertIsInstance(pjd.Hostname().get_etc_hosts(h), str)

    def test_file_exists(self):
        p = pjd.Package()
        jbhome = p.path
        self.assertTrue(os.path.isfile(jbhome + '/profile.conf'))

    def test_fix_standalone_conf(self):
        p = pjd.Package()
        p.rdir()
        p.mdir()
        p.cpdir()
        r = p.fix_standalone_conf()
        self.assertTrue(r >= 1)

    def test_fix_standalone_xml(self):
        '''if inetx-address is 0.0.0.0'''
        pass

    def test_is_tgz(self):
        p = pjd.Package()
        tarname = p.makeTar()
        self.assertTrue(tarfile.is_tarfile(tarname))

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPackage)
    unittest.TextTestRunner(verbosity=2).run(suite)
