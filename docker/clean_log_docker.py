import argparse
import heapq
from pathlib import Path
import subprocess
import sys
import time
# import psutil
import disk_operations as do
import clean_base
import logger


class ManageDocker(clean_base.CleanBase):

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()
        self.call_number = 1

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-p', action='store', help='Path to docker', dest='path')
        self.parser.add_argument(
            '-l', action='store',
            help='Sciezka dla logu. Domyslnie /dev/null', dest='logg_val')
        results = self.parser.parse_args()
        self.path = results.path
        self.logger = logger.Logger(results.logg_val)

    def disk_usage(self):
        du = do.DiskOper().disk_usage(self.path, percent=True)
        # du = psutil.disk_usage(self.path).percent
        return du

    def list_logs(self):
        logs_size = []
        pp = Path(self.path, '')
        for child in pp.glob('**/*.log*'):
            logs_size.append([child.stat().st_size, child])
        big = heapq.nlargest(self.call_number, logs_size, key=lambda s: s[0])
        pp = Path(big[self.call_number-1][1])
        dkid = pp.stem[0:12]
        subout = subprocess.run(
            ['docker', 'container', 'inspect', '-f="{{.Config.Image}}"', dkid],
            check=True, stdout=subprocess.PIPE)
        print(subout.stdout)
        if 'mongo' in str(subout.stdout):
            self.call_number = self.call_number + 1
            self.list_logs()
            print("oh no mongo container, I'll try next one")
        else:
            print("call number: {}".format(self.call_number))
            big_name = pp.name
            self.logger.log.info(
                f"truncate the biggest: {big_name} \
                size: {big[self.call_number-1][0]}")
        return pp

    def clean_and_restart(self, path):
        self.truncate(path)
        pp = Path(path)
        container_name = pp.stem[0:12]
        subprocess.run(
            ['docker', 'restart', container_name], stdout=subprocess.DEVNULL)
        self.logger.log.info(f"restart docker: {container_name}")

    def run(self):
        while self.disk_usage() > 85:
            biglog = self.list_logs()
            print(biglog)
            self.clean_and_restart(biglog)
            time.sleep(2)


if __name__ == '__main__':
    md = ManageDocker()
    md.run()
