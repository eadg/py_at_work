def read_ass(file='./ass', user=False):
    with open(file) as f:
        words = f.readline().strip().split(':')
    return words.pop() if not user else (words[0], words[1])
