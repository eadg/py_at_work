from base64 import b64decode
import str_bin


def read_ssap(file='/srv/scripts/py/ssap', cred=0):
    with open(file) as f:
        data = f.readlines()
    for d in data:
        if cred+':' in d:
            t = d.split(':')
            if len(t) > 2:
                return t[1].strip(), t[2].strip()
            elif len(t) == 2:
                return t[1].strip()


def read_pass(file='/srv/scripts/py/ssap', credent=0):
    with open(file) as f:
        data = f.readlines()
    search_cred = 'cred' + str(credent)
    for d in data:
        if search_cred in d:
            t = d.split(':')
            if len(t) > 2:
                t1 = t[1].strip()
                t2 = t[2].strip()
                bt1 = b64decode(t1)
                bt2 = b64decode(t2)
                return str_bin.to_str(bt1), str_bin.to_str(bt2)
            elif len(t) == 2:
                t1 = t[1].strip()
                bt1 = b64decode(t1)
                return str_bin.to_str(bt1)


if __name__ == '__main__':
    # pass
    print(read_pass(credent=0))
