import subprocess
import sys
import time


def do_work(n):
    # time.sleep(n)
    arg_out = '-sOutputFile=' + '/tmp/' + str(n) + '.pdf'
    subprocess.run(
        ['gs', '-q', '-dNOPAUSE', '-dBATCH', '-dPDFSETTINGS=/prepress',
         '-sDEVICE=pdfwrite', '-dDisableGlyphBoundingBox', arg_out,
         '/tmp/smykPDF_8184_4398084_764175070367883071_z1.pdf'],
        capture_output=True, check=True)
    print('I just did some hard work for {}s!'.format(n))


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Please provide one integer argument', file=sys.stderr)
        exit(1)
    try:
        seconds = int(sys.argv[1])
        do_work(seconds)
    except Exception as e:
        print(e)
