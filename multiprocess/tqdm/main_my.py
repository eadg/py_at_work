import multiprocessing as mp
import subprocess
import time

from tqdm import tqdm

NUMBER_OF_DOCUMENTS = 7
NUMBER_OF_CONCURRENT_TASKS = mp.cpu_count() - 2
progress_bar = tqdm(total=NUMBER_OF_DOCUMENTS)


def work(sec_sleep):
    command = ['python', 'worker_my.py', sec_sleep]
    subprocess.call(command)


def update_progress_bar(_):
    progress_bar.update()


if __name__ == '__main__':
    start = time.time()
    pool = mp.Pool(NUMBER_OF_CONCURRENT_TASKS)

    for seconds in [str(x) for x in range(1, NUMBER_OF_DOCUMENTS + 1)]:
        pool.apply_async(work, (seconds,), callback=update_progress_bar)

    pool.close()
    pool.join()
    end = time.time()
    time.sleep(1)
    print("\n czas: {}".format(end-start))
