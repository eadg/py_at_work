import multiprocessing as mp
from time import time


def worker_process(i):
    return i * i  # square the argument


def process_result(return_value):
    print(return_value)


def main():
    start = time()
    pool = mp.Pool()
    for i in range(100):
        pool.apply_async(worker_process, args=(i,), callback=process_result)
    pool.close()
    pool.join()
    end = time()
    print(f"total {end-start}")


if __name__ == '__main__':
    main()
