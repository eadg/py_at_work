import multiprocessing as mp
import subprocess

from tqdm import tqdm

number_cpu = mp.cpu_count() - 1

progress_bar = tqdm(total=number_cpu)


def work(sec_sleep):
    command = ['gs', '-q', '-dNOPAUSE', '-dBATCH', '-dPDFSETTINGS=/prepress',
               '-sDEVICE=pdfwrite', '-dDisableGlyphBoundingBox',
               '-sOutputFile=output.pdf',
               'smykPDF_8184_4398084_764175070367883071_zrodlowy.pdf', sec_sleep]
    command3 = ['touch', '/tmp/'+sec_sleep]
    command2 = ['python', 'worker.py', sec_sleep]
    subprocess.run(command3, check=True, capture_output=True)


def update_progress_bar(_):
    progress_bar.update()


if __name__ == '__main__':
    pool = mp.Pool(number_cpu)

    for seconds in [str(x) for x in range(1, number_cpu + 1)]:
        pool.apply_async(work, (seconds,), callback=update_progress_bar)

    pool.close()
    pool.join()
