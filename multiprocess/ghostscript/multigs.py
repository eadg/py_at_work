import glob
import pathlib
import queue
import subprocess
import time
from multiprocessing import Process, Queue, cpu_count


def do_job(tasks_to_accomplish, tasks_that_are_done):
    while True:
        try:
            task = tasks_to_accomplish.get_nowait()
        except queue.Empty:

            break
        else:
            print(task)
            # tasks_that_are_done.put(task + ' is done by ' + current_process().name)
            tasks_that_are_done.put('done')
            time.sleep(.5)
    return True


def main():
    start = time.time()
    # number_of_task = 10
    # list_file = glob.glob(
    #    '/home/jrokicki/dev/pyt/py_at_work/tiff/tests/EDC_czcionki/s*_z?.p*')
    # number_of_task = len(list_file)
    # print(number_of_task)
    number_of_processes = cpu_count()*2
    # number_of_processes = 16
    tasks_to_accomplish = Queue()
    tasks_that_are_done = Queue()
    processes = []

    for i in range(2):
        tasks_to_accomplish.put("Task no " + str("{0}: ".format(i)))
        nfile = "plik{}".format(i)
        tasks_to_accomplish.put(open(nfile).readline())

    for i in (glob.iglob(
            '/home/jrokicki/dev/pyt/py_at_work/tiff/tests/EDC_czcionki/s*_z?.p*')):
        tasks_to_accomplish.put("Task-open-file:{}".format(i))
        fin = pathlib.PurePath(i)
        arg_out = '-sOutputFile=' + '/tmp/' + fin.name
        tasks_to_accomplish.put(subprocess.run(
            ['gs', '-q', '-dNOPAUSE', '-dBATCH', '-dPDFSETTINGS=/prepress',
             '-sDEVICE=pdfwrite', '-dDisableGlyphBoundingBox', arg_out, fin]))

    for w in range(number_of_processes):
        p = Process(target=do_job, args=(
            tasks_to_accomplish, tasks_that_are_done))
        processes.append(p)
        p.start()

    for p in processes:
        p.join()

    while not tasks_that_are_done.empty():
        print(tasks_that_are_done.get())

    end = time.time()
    print("czas: {}".format(end-start))

    return True


if __name__ == '__main__':
    main()
