from multiprocessing import Pool
import time


def f(x):
    print("call f()")
    return x*x


if __name__ == '__main__':
    lis = [x for x in range(0, 1000)]
    print("len: {}".format(len(lis)))
    start = time.time()
    with Pool(1) as p:
        for i in lis:
            # print("i:{}".format(i))
            p.apply_async(f, args=(i,), callback=print)
    end = time.time()
    print(f"total time {end-start}")
