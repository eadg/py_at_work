from multiprocessing import Pool
import time


def f(x):
    return x*x


def process_result(return_value):
    print(return_value)


if __name__ == '__main__':
    lis = [x for x in range(0, 1000)]
    print(len(lis))
    start = time.time()
    pool = Pool()
    for i in lis:
        pool.apply_async(f, args=(i,), callback=process_result)
    pool.close()
    pool.join()
    end = time.time()
    print(f"total time {end-start}")
