import os
import sys
import urllib.parse
from pathlib import Path
from time import sleep
import img2pdf

from flask import Flask, request, abort


app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'GET':
        abort(404)
    if request.method == 'POST':
        req_data = request.get_data(as_text=True)
        dekot = urllib.parse.unquote_plus(req_data)
        _, paf = dekot.split('=')
        pafh = Path(paf)
        os.chdir('/srv/www/intradok/__tmp__')
        # flag = name_checker(pafh)
        flag = 1
        fopy = ForkPy()
        fopy.forking()
        fopy.slip(2)
        if '__tmp__' in os.getcwd():
            if flag:
                pafh.unlink()
            Path(__file__).touch(exist_ok=True)
        return ''
    abort(404)


def name_checker(file_name: Path):
    """Check suffix."""
    print(file_name.absolute())
    suf = file_name.suffix
    output_name = file_name.stem + '.pdf'
    out = Path(output_name)
    # out = output_name.encode(encoding='UTF-8')
    if 'pdf' in suf:
        return 1
    if 'tif' in suf:
        with open(out, "wb") as fou:
            try:
                print(out)
                fou.write(img2pdf.convert(file_name))
            except TypeError as ers:
                print(ers)
        return 1
    return 0


class ForkPy():

    def forking(self):
        try:
            pid = os.fork()
            if pid > 0:
                sys.exit(0)
        except OSError as e:
            msg = "fork exception: %d %s" % (e.errno, e.strerror)
            sys.stderr.write(msg)
            sys.exit(1)

    def slip(self, data):
        sleep(data)


if __name__ == "__main__":
    app.run(host='localhost', port=6000, threaded=True, debug=True)
