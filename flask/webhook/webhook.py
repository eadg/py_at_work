import os
import subprocess
from flask import Flask, request
import git
import json

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'GET':
        return '<h3>Hello From Hooks</h3>'
    if request.method == 'POST':
        req_data = request.get_json()
        # if request.headers['X-Gitlab-Token'] == os.environ['GitLabToken']:
        if request.headers['X-Gitlab-Token'] == 'secret_token:)':
            if req_data["object_kind"] == 'push':
                pull()
                json_check()
                # restart_uwsgi()
                # static_update()
                return '{"success": "true"}'


def pull():
    os.chdir('/srv/takto-report')
    # print("katalog {}".format(os.getcwd()))
    gr = git.Repo('.')
    # print(gr.git)
    # gr.git.status()
    gr.git.pull()


def json_check():
    with open("operations.json") as f:
        jn = json.load(f)
    if jn["restart"]:
        subprocess.run(["sudo", "/bin/systemctl", "restart", "taktoreport"])
    if jn["static"]:
        subprocess.run(
            ["sudo", "/bin/systemctl", "start", "takto_collect_static"])


def restart_uwsgi():
    with open("operations.json") as f:
        jn = json.load(f)
    if jn["restart"]:
        subprocess.run(["sudo", "/bin/systemctl", "restart", "taktoreport"])


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=6000, threaded=True, debug=True)
