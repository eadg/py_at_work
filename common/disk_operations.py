import datetime
import glob
import os
import os.path
import shutil
import subprocess
import sys


class DiskOper():

    def mkdir_if(self, path, owner=None):
        '''make dir if missing'''
        norm_path = os.path.normpath(path)
        if not os.path.isdir(norm_path):
            try:
                os.makedirs(norm_path)
                if owner:
                    subprocess.check_output(["chown", owner, norm_path])
            except subprocess.CalledProcessError:
                print("set_group_owner(path={}, onwer={})".format(path, owner))
            except IOError:
                print("maybe need root")
            else:
                print("Dir {} was created {}".format(norm_path, owner))
                return os.path.abspath(norm_path)

    def clean_dir(self, path=None, value=None, interval=None):
        '''clean directory:
                path= what
                value= how many units of interval
                interval= dAY, hOUR
            IE: clean_dir(path='/tmp/*.gz', value=5, interval='d')
        '''
        path = os.path.normpath(path)
        if interval == 'd':
            da = datetime.datetime.today() - datetime.timedelta(days=value)
        elif interval == 'h':
            da = datetime.datetime.today() - datetime.timedelta(hours=value)
        else:
            raise PDCValueException()
        list_file = glob.glob(path)
        for f in list_file:
            if os.path.isfile(f):
                if datetime.datetime.fromtimestamp(os.path.getmtime(f)) < da:
                    os.remove(f)
            if os.path.islink(f):
                if not os.path.exists(f):
                    os.remove(f)

    def set_group_owner_recursive(self, path=None, owner=None):
        try:
            subprocess.call(["chgrp", "-RHh", owner, path])
        except subprocess.CalledProcessError:
            print("set_group_owner(path={}, onwer={})".format(path, owner))

    def set_group_write_recursive(self, path=None):
        try:
            subprocess.call(["chmod", "g+w", path, "-R"])
        except subprocess.CalledProcessError:
            print("set chmod(path={})".format(path))

    def remove_if(self, path):
        try:
            os.path.isfile(path)
            os.remove(path)
        except OSError:
            print('no file: %s' % path)

    def remove_if_empty(self, path):
        if os.stat(path).st_size == 0:
            self.remove_if(path)
            return True
        return False

    def name_checker(self, path, word):
        import pathlib
        pp = pathlib.Path(path)
        ppl = pp.name.lower()
        wdl = word.lower()
        if wdl in ppl:
            return True

    def truncate(self, lpath):
        '''truncate file'''
        try:
            os.path.isfile(lpath)
            f = open(lpath, 'r+')
            f.truncate(0)
            f.flush()
            f.close()
        except OSError:
            print("Its no file: {}".format(lpath))
            # print(f"Its no file: {lpath} ")

    def gzip(self, path):
        try:
            import gzip
        except ModuleNotFoundError:
            import zipfile
        if 'gzip' in sys.modules.keys():
            suf = '.gz'
            with open(path, 'rb') as fi, gzip.open(path+suf, 'wb') as fo:
                shutil.copyfileobj(fi, fo)
        else:
            suf = '.zip'
            with zipfile.ZipFile(path+suf, 'w') as fo:
                fo.write(path)
        os.remove(path)
        return path+suf

    def disk_usage(self, path, percent: bool = False):
        """usage:

        first argument can be "/", "/home"
        percent argument: False (default) or True
        """

        t, u, f = shutil.disk_usage(path)
        du = 1 - (f/t)
        if percent:
            du = f'{du*100 :.0f}'
            return int(du)
        return du

class Error(Exception):
    pass

class PDCValueException(Error):
    pass
