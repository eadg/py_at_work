import os
import socket
import subprocess
from pathlib import Path
from zipfile import ZipFile


def zip_not_work(zip_path=None, files=None, pswd=None):
    with ZipFile(file=zip_path, mode='w') as myzip:
        myzip.write(files)
        myzip.setpassword(str.encode(pswd))


def zip_secure(zip_path=None, files=None, pswd=None, logger=None):
    if isinstance(files, str):
        pp = Path(files)
    elif isinstance(files, Path):
        pp = files
    if pp.is_file():
        host = socket.gethostname()
        os.chdir(pp.parent)
        with open(os.devnull, 'w') as f:
            subprocess.call(
                ["zip", zip_path, "-P", pswd, pp.name], stdout=f)
            if logger:
                logger.log.info(
                    'zip: {1} was created on {0}'.format(host, zip_path))
