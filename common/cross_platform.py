import os
import os.path
import platform


class CRPL(object):

    def get_proper_os_sep(self, value):
        if isinstance(value, str):
            path = os.path.abspath(value)
            if '/' in path:
                return '/'
            elif '\\' in path:
                return '\\'
        elif isinstance(value, list):
            path = value.pop(0)
            self.get_proper_os_sep(path)

    def get_win_drive(self, wpath):
        if 'Wind' in platform.system():
            drive, _ = os.path.splitdrive(wpath)
            return drive
        else:
            print("It isn't Windows")
