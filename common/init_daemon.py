class PrimalDaemon(object):

    '''Is init or SystemD running?'''

    def action(self):
        with open('/proc/1/comm') as f:
            daemon = f.read().strip()
        if daemon == 'systemd':
            return 'stop'
        elif daemon == 'init':
            return 'restart'
        else:
            raise OSError

    def get_type_init(self):
        with open('/proc/1/comm') as f:
            daemon = f.read().strip()
        return daemon

    def get_check_command(self):
        checkstr = 'cat /proc/1/comm'
        return checkstr
