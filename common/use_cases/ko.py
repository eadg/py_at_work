import disk_operations as d_o

do = d_o.DiskOper()
with open('/tmp/lista.txt') as f:
    for line in f:
        name = line.strip()
        usgr = "{0}.ftp".format(name)
        path = "/srv/ftp/{0}/KO_DANE/".format(name)
        do.mkdir_if(path, owner=usgr)
