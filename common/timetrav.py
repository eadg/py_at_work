from datetime import date, timedelta


class TT():

    def __init__(self, period=0):
        self.period = int(period)

    def get_hyp(self):
        yd = date.today() + timedelta(days=self.period)
        return yd

    def get_iso(self):
        yd = date.today() + timedelta(days=self.period)
        isolist = yd.isoformat().split('-')
        return isolist[0]+isolist[1]+isolist[2]

    def get_nr_of_period(self, per: str = None):
        d = date.today() + timedelta(days=self.period)
        if per == 'd':
            return d.strftime('%d')
        if per == 'w':
            return d.strftime('%W')
        if per == 'y':
            return d.strftime('%j')
        if per == 'm':
            return d.strftime('%m')
        raise SyntaxError()


if __name__ == '__main__':
    y = TT()
    print(y.get_iso())
    print(y.get_nr_of_period(per='m'))
