from timetrav import TT


def test_get_hyp():
    assert str(TT(-1).get_hyp()) == '2020-05-19'


def test_get_iso():
    assert str(TT(-1).get_iso()) == '20200519'
