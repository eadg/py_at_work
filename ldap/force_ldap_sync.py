import ass

import ldap3

import logger


class ForceLdapSync(object):

    def __init__(self):
        self.pas = ass.read_pass()
        self.ht = ldap3.Server('10.10.11.212')
        self.user = 'uid=jrokicki,ou=people,ou=accounts,o=ultimo'
        self.lcon = ldap3.Connection(
            self.ht, user=self.user, password=self.pas, auto_bind=True)
        self.logging = logger.Logger('log.txt')

    def change_sth(self, in_text: str) -> None:
        mod_str = '\\lorien\\' + in_text
        self.lcon.search('ou=people,ou=accounts,o=ultimo', '(uid=jrokicki)')
        self.lcon.modify(self.lcon.entries[0].entry_dn, {
            'sambaHomePath': [(ldap3.MODIFY_REPLACE, mod_str)]})

    def set_ftp_takto_status(self, in_text: str) -> None:
        for i in range(2, 22):
            name = "(uid=ftp_takto_p{})".format(i)
            self.lcon.search('ou=system,ou=accounts,o=ultimo', name)
            self.lcon.modify(self.lcon.entries[0].entry_dn, {
                'accountActive': [(ldap3.MODIFY_REPLACE, in_text)]})

    def remove_ldap_leaves(self) -> None:
        with open('./tests/ftp_out.txt') as f:
            for i in f.readlines():
                # print(i)
                name = "(uid={})".format(i)
                self.lcon.search('ou=system,ou=accounts,o=ultimo', name)
                # self.lcon.delete(self.lcon.entries[0].entry_dn)
                # self.logging.log.info(self.lcon.result)
                self.logging.log.info(self.lcon.entries[0].entry_dn)

    def close(self):
        self.lcon.unbind()


if __name__ == '__main__':
    fls = ForceLdapSync()
    # fls.remove_ldap_leaves()
    # fls.change_sth('Rokickii')
    # fls.change_sth('Rokicki')
    fls.set_ftp_takto_status(0)
    fls.close()
