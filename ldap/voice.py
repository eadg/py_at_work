import ssl
import ldap3

import ass

class VoicePoc():

    def __init__(self):
        pas = ass.read_pass(credent=3)
        ser = ldap3.Server(
            'voice-db', use_ssl=True, tls=ldap3.Tls(
                validate=ssl.CERT_NONE, version=ssl.PROTOCOL_TLSv1))
        user = 'CN=Administrator,CN=Users,DC=voice,DC=pl'
        self.con = ldap3.Connection(ser, user=user, password=pas, auto_bind=True)

    def search(self) -> None:
        self.con.search('CN=Users,DC=voice,DC=pl', '(objectclass=person)')
        print(self.con.entries)

    def modify(self) -> None:
        self.con.search('CN=Users,DC=voice,DC=pl', '(CN=aborowska)')
        self.con.modify(
            self.con.entries[0].entry_dn, {'givenName': [(ldap3.MODIFY_REPLACE, 'Ana')]})


if __name__ == '__main__':
    adhd = VoicePoc()
    adhd.modify()
