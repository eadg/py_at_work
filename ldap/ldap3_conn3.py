import ass

import ldap3


class LdapHQ(object):

    def __init__(self):
        self.pas = ass.read_pass()
        self.ht = ldap3.Server('10.10.11.212')
        self.user = 'uid=jrokicki,ou=people,ou=accounts,o=ultimo'
        self.lcon = ldap3.Connection(
            self.ht, user=self.user, password=self.pas, auto_bind=True)

    def list_user(self):
        self.lcon.search(
            'ou=people,ou=accounts,o=ultimo', '(objectclass=person)')
        self.print_close(self.lcon)

    def change_sth(self, in_text: str) -> None:
        mod_str = '\\lorien\\' + in_text
        self.lcon.search('ou=people,ou=accounts,o=ultimo', '(uid=jrokicki)')
        self.lcon.modify(self.lcon.entries[0].entry_dn, {
            'sambaHomePath': [(ldap3.MODIFY_REPLACE, mod_str)]})
        self.print_close(self.lcon)

    def print_close(self, con):
        print(con.entries)
        con.unbind()


class Ldap(object):

    def __init__(self):
        self.pas = ass.read_pass()
        self.ht = ldap3.Server('10.10.11.212')
        self.user = 'uid=jrokicki,ou=people,ou=accounts,o=ultimo'
        self.lcon = ldap3.Connection(
            self.ht, user=self.user, password=self.pas, auto_bind=True)

    def list_user(self):
        self.lcon.search(
            'ou=people,ou=accounts,o=ultimo', '(objectclass=person)')
        self.print_close(self.lcon)

    def get_user(self, ui):
        filter_uid = f'(uid={ui})'
        self.lcon.search(
            'ou=people,ou=accounts,o=ultimo', filter_uid)
        self.print_close(self.lcon)

    def pass_expire(self):
        conn = ldap3.Connection(
            self.ht, user=self.user, password=self.pas, auto_bind=True)
        conn.search(
            search_base='ou=people,ou=accounts,o=ultimo',
            search_filter='(objectclass=person)',
            attributes=['sambaPwdMustChange'])
        for ent in conn.entries:
            dd = ent.entry_dn
            if 'jrokicki' in dd:
                li = ent.entry_raw_attributes['sambaPwdMustChange']
                for ii in li:
                    if int(ii) > 118203889:
                        print(dd.split(',').pop(0))
                        print(ii)
        conn.unbind()

    def print_close(self, con):
        print(con.entries)
        con.unbind()

    def lets_modify(self):
        """Modify emails address: DN: uid=xxx,ou=people,dc=ultimo,dc=pl."""
        """TODO: check if attributes=['mail'] in conn.search is necessary"""
        conn = ldap3.Connection(
            self.ht, user=self.user, password=self.pas, auto_bind=True)
        conn.search('ou=people,ou=accounts,o=ultimo',
                    '(objectclass=person)', attributes=['mail'])
        slo = {}
        with open('emails.csv') as f:
            for line in f:
                k, v = line.split(';')
                slo[k.rstrip()] = v.rstrip()
        for ent in conn.entries:
            dd = ent.entry_dn
            for k, v in slo.items():
                if k in dd:
                    conn.modify(dd, {'mail': [(ldap3.MODIFY_REPLACE, [v])]})
        conn.unbind()


if __name__ == '__main__':
    # ld = Ldap()
    # ld.get_user('jrokicki')
    ld = LdapHQ()
    ld.change_sth('Rokickii')
    # ld.change_sth('Rokicki')
    # ld.print(ld.lcon)
    # ld.list_user()
    # lu = ld.list_user()
    # ld.print_close(lu)
    # ld.pass_expire()
