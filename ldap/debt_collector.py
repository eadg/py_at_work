import ldap3

from ldap3_conn3 import Ldap


class LdapDebtColl(Ldap):

    def __init__(self):
        super().__init__()

    def set_debt_coll_deactive(self) -> None:
        search_base = 'ou=system,ou=accounts,o=ultimo'
        with open('tests/ftp_takto.txt') as f:
            for line in f:
                ui = line.strip()
                filter_uid = f'(uid={ui})'
                self.lcon.search(search_base, filter_uid)
                if len(self.lcon.entries) == 1:
                    self.lcon.modify(self.lcon.entries[0].entry_dn, {
                        'accountActive': [(ldap3.MODIFY_REPLACE, 0)]})

    def get_debt_coll_deactive(self) -> None:
        search_base = 'ou=system,ou=accounts,o=ultimo'
        with open('tests/ftp_name.txt') as f:
            for line in f:
                ui = line.strip()
                filter_uid = f'(uid={ui})'
                lattr = ['uid', 'accountActive']
                self.lcon.search(search_base, filter_uid, attributes=lattr)
                if self.lcon.entries[0].accountActive == 0:
                    print(self.lcon.entries[0].uid,
                          self.lcon.entries[0].accountActive)

    def get_debt_coll_deactive2(self) -> None:
        '''it works as well'''
        search_base = 'ou=system,ou=accounts,o=ultimo'
        search_filter = '(&(objectclass=person)(uid=ftp_komornik_*))'
        search_attr = ['gidNumber']
        self.lcon.search(search_base, search_filter, attributes=search_attr)
        for kom in self.lcon.entries:
            print(kom)

    def set_debt_coll_bash(self) -> None:
        search_base = 'ou=system,ou=accounts,o=ultimo'
        search_filter = '(&(objectclass=person)(uid=ftp_komornik_krakow6))'
        search_attr = ['loginShell']
        self.lcon.search(search_base, search_filter, attributes=search_attr)
        for komor in self.lcon.entries:
            self.lcon.modify(komor.entry_dn, {'loginShell': [
                (ldap3.MODIFY_REPLACE, '/bin/bash')]})

    def get_ftp_account_active(self) -> None:
        '''get active ftp_* account'''
        search_base = 'ou=system,ou=accounts,o=ultimo'
        search_filter = '(&(objectclass=person)(uid=ftp_*)(accountActive=1))'
        search_attr = ['uid']
        self.lcon.search(search_base, search_filter, attributes=search_attr)
        for kom in self.lcon.entries:
            dd = kom.entry_attributes_as_dict
            print(dd['uid'].pop())

    def set_some_accounts_deactive(self) -> None:
        search_base = 'ou=system,ou=accounts,o=ultimo'
        with open('tests/lista_kont_lock2.txt') as f:
            for line in f:
                ui = line.strip()
                filter_uid = f'(uid={ui})'
                self.lcon.search(search_base, filter_uid)
                if len(self.lcon.entries) == 1:
                    self.lcon.modify(self.lcon.entries[0].entry_dn, {
                        'accountActive': [(ldap3.MODIFY_REPLACE, 0)]})


if __name__ == '__main__':
    ld = LdapDebtColl()
    # ld.get_debt_coll_deactive2()
    # ld.get_ftp_account_active()
    # ld.set_debt_coll_bash()
    ld.set_some_accounts_deactive()
