import argparse
import ssl
import sys
from typing import Tuple
import ldap3
import ass

class VoiceAd():

    def __init__(self):
        pas = ass.read_pass(credent=3)
        se = ldap3.Server(
            'voice-db', use_ssl=True, tls=ldap3.Tls(
                validate=ssl.CERT_NONE, version=ssl.PROTOCOL_TLSv1))
        user = 'CN=Administrator,CN=Users,DC=voice,DC=pl'
        self.con = ldap3.Connection(se, user=user, password=pas, auto_bind=True)

    def getcre(self) -> Tuple[str, str]:
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '-p', action='store', help='new password', dest='npass')
        parser.add_argument(
            '-u', action='store', help='user', dest='login')
        results = parser.parse_args()
        return results.login, results.npass

    def change(self) -> None:
        login, new_pass = self.getcre()
        user_dn = f"CN={login},CN=Users,DC=voice,DC=pl"
        ldap3.extend.microsoft.modifyPassword.ad_modify_password(
            self.con, user_dn, new_pass, old_password=None)
        sys.exit(1)

if __name__ == '__main__':
    adhd = VoiceAd()
    adhd.change()
