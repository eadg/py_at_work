class A(object):

    def __init__(self, val):
        self._value = val
        print('call __intit__')

    def __getattr__(self, name):
        print('call __getatrr__')
        new_val = 'Ustawienie wartości %s' % name
        setattr(self, name, new_val)
        return new_val

    def __getattribute__(self, same):
        print('call __getatrribute__')
        return 0


class B(object):

    def __init__(self, mymin, mymax):
        print('call __intit__')
        self.mymini = mymin
        self.mymax = mymax
        self.current = None

    def __getattr__(self, item):
        print('call __getatrr__')
        print(item)
        self.__dict__[item] = 0
        return 0

    def __getattribute__(self, item):
        print('call __getatrribute__')
        print(item)
        if item.startswith('cur'):
            raise AttributeError
        return object.__getattribute__(self, item)
        # or you can use ---return super().__getattribute__(item)
        # note this class subclass object


if __name__ == '__main__':
    b = B(1, 2)
    print(b.mymin)
    print(b.current)
    '''
    a = A('test')
    print(a.no_exist)
    print('Teraz self ma już name: %s' % a.name)
    print('Teraz też self ma już name: %s' % a.name)
    print(a.no_exist2)
    '''
