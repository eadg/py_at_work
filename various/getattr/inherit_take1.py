class A(object):

    def __init__(self, value):
        print('call __intit__A')
        self.value = value


class B(A):

    def __init__(self, value):
        print('call __intit__B')
        super(B, self).__init__(value)

    def __call__(self):
        print('call __call__')

    def __str__(self):
        print('call __str__')
        return '__str__'

    def __repr__(self):
        pass

if __name__ == '__main__':
    b = B('testB')
    print(b)
    b()
    a = A('testA')
    print(a.__repr__())
