class C(object):

    def __init__(self, om):
        self._om = om

    def gtom(self):
        return self._om

    def stom(self, v):
        self._om = v

    def dlom(self):
        del self._om

    om = property(fget=gtom, fset=stom, fdel=dlom)

if __name__ == '__main__':
    c = C(24)
    print(c.om)
    c.stom(45)
    print(c.om)
    c.dlom()
    # print(c.om)
