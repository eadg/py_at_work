import glob
import re

# fpath1 = 'bank109_dokumenty2cbd.pl'


def change_nr1():
    patt_user = re.compile('(^my \$username.+$)', flags=re.MULTILINE)
    patt_pass = re.compile('(^my \$password.+$)', flags=re.MULTILINE)

    list_perls = glob.glob('*.pl')
    for fpath in list_perls:
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(patt_user, '#' + r'\1' + r'\nmy $username="auto_smyk_upload";', data)
            fix_data = re.sub(patt_pass, '#' + r'\1' + r'\nmy $password="$kl3hh^uY3";', fix_data)
            f.seek(0)
            f.truncate()
            f.write(fix_data)


def change_nr2():
    patt_user = re.compile('(^my \$username.+$)', flags=re.MULTILINE)
    patt_pass = re.compile('(^my \$password.+$)', flags=re.MULTILINE)
    list_perls = glob.glob('*.pl')
    for fpath in list_perls:
        with open(fpath, 'r+') as f:
            data = f.read()
            fix_data = re.sub(patt_user, r"my $username='auto_smyk_upload';", data)
            fix_data = re.sub(patt_pass, r"my $password='$kl3hh^uY3';", fix_data)
            f.seek(0)
            f.truncate()
            f.write(fix_data)


if __name__ == '__main__':
    change_nr2()
