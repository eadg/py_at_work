#!/usr/bin/perl -w

# skrypt ten bedzie kopiowal pewien typ dokumentu do bazy i automatycznie bedzie dodawal polozenie plikowe
# dokument jest dodawany do wszystkich klientow produktu i oczywiscie do produktu

# Parametry

# 1 - host
# 2 - baza
# 3 - id_typ_dokument
# 4 - katalog z plikami do wrzucenia
# 5 - operator

if ( scalar(@ARGV) != 6) {

  print "Blednie uruchomoiny skrypt\n";
  print "Syntax : $0 host baza id_typ_dokument katalog_z_dokumentami id_operator id_bank\n";
  print "gdzie:\n";
  print "\t\thost - host gdzie jest baza\n";
  print " \t\tbaza - nazwa bazy\n";
  print " \t\tid_typ_dokument - id typu dokumentu z d_cbd_typ_dokument\n";
  print " \t\tkatalog_z_dokumentami - katalog gdzie znajduja sie dokumenty\n";
  print " \t\tid_operator - id operatora w systemie SMYK\n";
  print " \t\tid_bank - id bank w systemie SMYK\n";
  exit 0;
}

# STALE

my $SMYK_HOME		= "/home/smyk/";

my $TYP_POLOZENIE	= 301;

use DBI;
use strict;
use File::Copy;

my $host		= shift(@ARGV);
my $dbname		= shift(@ARGV);
my $id_typ_dokument	= shift(@ARGV);
my $katalog		= shift(@ARGV)."/";
my $opr			= shift(@ARGV);
my $id_bank		= shift(@ARGV);

#my $username		= "blalalalalalak";
my $username='auto_smyk_upload';
#my $password		= "ewewewee";
my $password='$kl3hh^uY3';

my $dbh = DBI->connect("dbi:Pg:dbname=$dbname;host=$host", $username, $password, {RaiseError => 1, AutoCommit => 0}) or
          die "Blad laczenia sie z serwerem BD\n";
=cut
