import pickle
import os.path


class Ping(object):

    def __init__(self):
        self.count = 0
        self.f = 'pp.txt'

    def is_first_time(self):
        if not os.path.isfile(self.f):
            with open(self.f, 'a'):
                pass

    def pong(self):
        self.is_first_time()
        with open(self.f, 'rb') as f:
            try:
                state = pickle.load(f)
            except EOFError:
                state = 0
            print(state)
        with open(self.f, 'wb') as f:
            self.count = state + 1
            # print(self.count)
            pickle.dump(self.count, f)

if __name__ == '__main__':
    p = Ping()
    p.pong()
