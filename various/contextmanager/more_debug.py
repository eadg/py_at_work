from contextlib import contextmanager
import logging


def my_fun():
    logging.warn('its warn')
    logging.debug('its debug')
    logging.info('its info')


@contextmanager
def more_debug(level):
    lr = logging.getLogger()
    old_level = lr.getEffectiveLevel()
    lr.setLevel(level)
    try:
        yield
    finally:
        lr.setLevel(old_level)

if __name__ == '__main__':
    my_fun()

    with more_debug(logging.DEBUG):
        my_fun()
