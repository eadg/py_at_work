def deco(f):

    print('before wrap')

    def wrap(arg):
        print('inside wrap')
        return '<h> {0} </h>'.format(f(arg))
    print('after wrap')
    return wrap


@deco
def prod_meth(text):
    return 'prod_meth %s' % text


print(prod_meth('akuku'))
