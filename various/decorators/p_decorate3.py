# practice write deco base helion item_42


def trace(func):

    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        print('%r, %r, %r' % (args, kwargs, result))
        return result
    return wrapper


@trace
def pr(n=3):
    return int(n) + 1


def pr1(n=5):
    return int(n) + 1

if __name__ == '__main__':
    pr()
    f = trace(pr1)
    f(n=1)
