class Sample(object):

    def __str__(self):
        return "SAMPLE"


class A(object):

    def __new__(cls):
        return super(A, cls).__new__(Sample)


if __name__ == '__main__':
        print(A())
