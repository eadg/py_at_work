class A(object):

    def __init__(self):
        print("inside init")

    def __new__(cls):
        print("inside new")
        return super(A, cls).__new__(cls)

if __name__ == '__main__':
    A()
