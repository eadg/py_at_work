import argparse
import datetime
import re
import sys


class IchimokuTimeFrame(object):

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-t', action='store', help='Begining', dest='time')
        results = self.parser.parse_args()
        try:
            self.begin = datetime.date.fromisoformat(results.time)
        except ValueError:
            self.begin = self.fixing_input(results.time)
            print('Input date was converted to {}'.format(self.begin))

    def get_time_frame(self):
        list_tframe = []
        for i in (9, 17, 26, 33, 42, 65, 76, 129, 172):
            d_delta, start_day = self.calc_tframe(i)
            list_tframe.append((d_delta, start_day))
        for t in list_tframe:
            print("T{0}: {1}".format(t[0], t[1]), end=" ")
        print()

    def calc_tframe(self, d_delta):
        count = 1
        start_day = self.begin
        d1 = datetime.timedelta(days=1)
        while count < d_delta:
            next_day = start_day + d1
            if next_day.weekday() < 5:
                count += 1
            start_day = next_day
        return d_delta, start_day

    def fixing_input(self, s):
        ymr_list = re.split(r'\W', s)
        s1 = '-'.join(ymr_list)
        if len(s1) < 10:
            y, m, d = s1.split('-')
            while len(y) < 4:
                pattern = re.compile('^2.+')
                if not re.match(pattern, y):
                    y = '2{}'.format(y)
                else:
                    liy = list(y)
                    liy.insert(1, 0)
                    yok = str()
                    for i in liy:
                        yok += str(i)
                    y = yok
            if len(m) < 2:
                m = '0{}'.format(m)
            if len(d) < 2:
                d = '0{}'.format(d)
            s1 = '{}-{}-{}'.format(y, m, d)
        return datetime.date.fromisoformat(s1)


if __name__ == '__main__':
    fr = IchimokuTimeFrame()
    fr.get_time_frame()
