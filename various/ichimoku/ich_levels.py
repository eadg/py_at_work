import argparse
import decimal
import sys


class IchimokuPriceLevel(object):

    def __init__(self):
        if len(sys.argv) < 2:
            sys.argv.append('-h')
        self.parse_detail()

    def parse_detail(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument(
            '-a', action='store', help='Wave A', dest='wave_a')
        self.parser.add_argument(
            '-b', action='store', help='Wave B', dest='wave_b')
        self.parser.add_argument(
            '-c', action='store', help='Wave C', dest='wave_c')
        self.parser.add_argument(
            '-d', action='store', help='Current', dest='current')
        results = self.parser.parse_args()
        decimal.getcontext().prec = 5
        self.wave_a, self.wave_b, self.wave_c, self.cur = self.fixing_input(
            results)

    def get_term(self):
        if self.wave_b > self.wave_c > self.wave_a:
            return 'U'
        elif self.wave_a > self.wave_c > self.wave_b:
            return 'D'
        else:
            sys.exit("Probably input data isn't correct")

    def get_level(self):
        e = (self.wave_b - self.wave_a) + self.wave_b
        n = (self.wave_b - self.wave_a) + self.wave_c
        v = (self.wave_b - self.wave_c) + self.wave_b
        nt = (self.wave_c - self.wave_a) + self.wave_c
        e_v = (e+v)/2
        e_n = (e+n)/2
        term = self.get_term()
        wave_templ = "E {0}, V {1}, N {2}, NT {3}, (E+V)/2 {4}, (E+N)/2 {5}"
        wave_data = (e, n, v, nt, e_v, e_n)
        wave_up_data = [x-self.cur for x in wave_data]
        wave_down_data = [self.cur-x for x in wave_data]
        if 'U' in term:
            print("Trend is UP: " + wave_templ.format(*wave_data))
            print("Possible prof: " + wave_templ.format(*wave_up_data))
        if 'D' in term:
            print("Trend is down: " + wave_templ.format(*wave_data))
            print("Possible prof: " + wave_templ.format(*wave_down_data))

    def get_input_data(self):
        print("Waves=>A: {0} B: {1} C: {2} Current: {3}".format(
            self.wave_a, self.wave_b, self.wave_c, self.cur))

    def fixing_input(self, res):
        l_wav = [res.wave_a, res.wave_b, res.wave_c, res.current]
        l_wav_dot = [x.replace(',', '.') for x in l_wav]
        l_decimal = [decimal.Decimal(x) for x in l_wav_dot]
        return tuple(l_decimal)


if __name__ == '__main__':
    lev = IchimokuPriceLevel()
    lev.get_level()
