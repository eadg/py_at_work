from weakref import WeakKeyDictionary


class Grade(object):
    def __init__(self):
        self._value = WeakKeyDictionary()

    def __get__(self, instance, instance_type):
        if instance is None:
            return self
        return self._value.get(instance, 0)

    def __set__(self, instance, value):
        if not (0 <= value <= 100):
            raise ValueError('Ocena musi być wartością z zakresu od 0 do 100')
        self._value[instance] = value


class Exam(object):
    math_grade = Grade()
    writing_grade = Grade()
    science_grade = Grade()


if __name__ == '__main__':
    first_exam = Exam()
    first_exam.writing_grade = 82
    first_exam.science_grade = 99
    print('Pisanie', first_exam.writing_grade)
    print('Nauka', first_exam.science_grade)
    second_exam = Exam()
    second_exam.writing_grade = 75
    print('First writing exam', first_exam.writing_grade)
    print('Second writing exam', second_exam.writing_grade)
    print('Nauka', second_exam.science_grade)
