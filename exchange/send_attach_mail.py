from pathlib import Path
import smtplib
from email.message import EmailMessage

def send(ph: str) -> None:
    serv_mail = smtplib.SMTP('poczta.ultimo.pl')

    msg = EmailMessage()
    msg['Subject'] = 'komornicy-xml'
    msg['From'] = 'henio@ultimo.pl'
    emails = ['ajankiewicz@ultimo.pl', 'akolodziejczyk@ultimo.pl', 'ajurek2@ultimo.pl',
              'izabela.spyra@ultimo.pl']
    msg["To"] = ", ".join(emails)

    sf = Path(ph).stat().st_size
    if sf > 0:
        with open(file=ph, encoding='utf-8') as fp:
            msg.set_content(fp.read())
    else:
        msg.set_content('Brak nowych plików xml')

    serv_mail.send_message(msg)
    serv_mail.quit()
