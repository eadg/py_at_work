import smtplib
import ssl


localhost = '127.0.0.1'
context = ssl.create_default_context()
ssl._create_default_https_context = ssl._create_unverified_context()
weak = ssl._create_unverified_context()
mess = """\
Subject: Hi there

Sending from Python."""
mail0 = 'mail0.ultimo.pl'


def send1():
    with smtplib.SMTP(localhost, 25) as server:
        # server.ehlo()
        # server.starttls(context=context)
        # server.ehlo()
        server.sendmail('jrokicki@ultimo.pl', 'jrokicki@ultimo.pl', mess)


def send2():
    with smtplib.SMTP(mail0, 25) as server:
        # server.ehlo()
        server.starttls(context=weak)
        server.ehlo()
        server.sendmail('rodomail@ultimo.pl', 'Jakub.Rokicki@ultimo.pl', mess)


if __name__ == '__main__':
    send2()
