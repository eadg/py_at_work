import re
import random
import string
import sys


class GenPas(object):

    def __init__(self):
        self.special = '!@#$%^&*_(){}[]+-'
        if len(sys.argv) < 2:
            self.len_pas = 8
        else:
            if re.search('[a-zA-Z]', sys.argv[1]):
                self.len_pas = 8
            else:
                self.len_pas = int(sys.argv[1])
            if self.len_pas < 4:
                self.len_pas = 8

    def gen(self):
        string_all = string.ascii_letters + string.digits + self.special
        list_all = [x for x in string_all]
        random.shuffle(list_all)
        pa = ''.join(random.SystemRandom().choice(
            ''.join(list_all)) for _ in range(self.len_pas))
        return pa

    def get_and_check(self):
        p = self.gen()
        spec = self.get_shuffle_list(self.special)
        upper = self.get_shuffle_list(string.ascii_uppercase)
        lower = self.get_shuffle_list(string.ascii_lowercase)
        digit = self.get_shuffle_list(string.digits)
        flag_s, flag_u, flag_l, flag_d = 0, 0, 0, 0
        for i in spec:
            if i in p:
                flag_s = True
                break
        for i in upper:
            if i in p:
                flag_u = True
                break
        for i in lower:
            if i in p:
                flag_l = True
                break
        for i in digit:
            if i in p:
                flag_d = True
                break
        if flag_s and flag_u and flag_l and flag_d:
            return p
        else:
            return 0

    def get_shuffle_list(self, s):
        list_s = [x for x in s]
        random.shuffle(list_s)
        join_list_s = ''.join(list_s)
        return join_list_s

    def return_secure_pass(self, verbose=False):
        if verbose:
            self.get_info()
        while True:
            psec = self.get_and_check()
            if psec:
                return psec

    def get_info(self):
        print('''
        Normal use: python gen_password 9
        It generates 9-characters password.
        ''')
        if len(sys.argv) < 2:
            print('''
            Password has 8 characters default.
            Please add digital argument if you want change it.
            ''')
        else:
            if re.search('[a-zA-Z]', sys.argv[1]):
                print('''
                Input contains letters...password will be 8 chars.''')
            if self.len_pas < 4:
                print('''
                Length < 4 is too short to be secured.
                Generated pass must have lower, upper, digital & special char.
                Therefore password will be augment to 8 chars.''')


if __name__ == '__main__':
    pg = GenPas().return_secure_pass(verbose=True)
    print(pg)
